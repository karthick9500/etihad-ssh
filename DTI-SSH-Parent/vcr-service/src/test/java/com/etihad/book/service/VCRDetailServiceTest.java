/**
 * 
 */
package com.etihad.book.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.schema.common.Message;
import com.etihad.book.schema.common.Messages;
import com.etihad.book.vcr.controller.VcrController;
import com.etihad.book.vcr.schema.SearchParameters;
import com.etihad.book.vcr.schema.VCRDisplayRequest;
import com.etihad.book.vcr.schema.VCRDisplayResponse;
import com.etihad.book.vcr.schema.VCRTicketingInfo;
import com.etihad.book.vcr.service.impl.VCRDisplayServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Danish
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class VCRDetailServiceTest {
	
	
	private MockMvc mockMvc;

	@MockBean
	VCRDisplayServiceImpl vcrService;

	@InjectMocks
	private VcrController vcrController;

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private Config config;

	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(vcrController).build();

	}

	@Test
	public void testControllerForGetVCRData() throws Exception {

		Mockito.when(vcrService.getVCRDisplayData(Mockito.anyObject()))
				.thenReturn(getTestResponseForVCRDetailData());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/service/v1/vcr/get-vcr-detail/")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(getVCRDetailRequest()))
				.accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content()
						.string(objectMapper.writeValueAsString(getTestResponseForVCRDetailData())))
				.andExpect(jsonPath("$.messages", notNullValue()))
				.andExpect(jsonPath("$.messages.message[0].code", is("0")))
				.andExpect(jsonPath("$.messages.message[0].text", is("Success")));

	}
	
	private VCRDisplayRequest getVCRDetailRequest() {
		VCRDisplayRequest request = new VCRDisplayRequest();
		SearchParameters searchParams = new SearchParameters();
		searchParams.setBinarySecurityToken("Shared/IDL:IceSess\\/SessMgr:1\\.0.IDL/Common/!ICESMS\\/CERTG!ICESMSLB\\/CRT.LB!-3170940443955548277!1507542!0");
		searchParams.seteTicketNumber("6072134615499");
		request.setSearchParameters(searchParams);
		return request;
	}
	
	private VCRDisplayResponse getTestResponseForVCRDetailData() {
		
		final VCRDisplayResponse response = new VCRDisplayResponse();
		
		final Messages messages = new Messages();
		final List<Message> msg = new ArrayList<>();
		final Message message = new Message();
		message.setCode("0");
		message.setText("Success");
		msg.add(message);
		messages.setErrorMessages(msg);
		VCRTicketingInfo vcrTicket = new VCRTicketingInfo();
		response.setMessages(messages);
		response.setVcrTicketingInfo(vcrTicket);
		return response;
	}
	
	

}
