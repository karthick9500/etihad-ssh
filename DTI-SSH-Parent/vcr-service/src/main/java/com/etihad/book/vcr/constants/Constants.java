package com.etihad.book.vcr.constants;

/**
 * 
 * @author CTS
 *
 */
public class Constants {
	
	private Constants() {	
	}

	public static final int PNR_LENGTH = 6;
	public static final int TICKET_LENGTH = 13;
	public static final String PNR_REGEX = "[6][0][7]{1}\\d{10}";
	public static final String PNR_REGEX_ANOTHER = "[0][0][0]{1}\\d{10}";
	public static final String GET_VCR_DETAIL = "/get-vcr-detail";
	public static final String VCR_SERVICE = "/service/v1/vcr/";
	
}
