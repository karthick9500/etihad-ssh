/**
 * 
 */
package com.etihad.book.vcr.schema;

import java.util.List;

/**
 * @author 641852
 *
 */
public class TicketingResInfo {

	private String accountingCode;
	private String issueDate;
	private String issuingAgent;
	private String numCoupons;
	private String eTicketNumber;
	
	
	private CustomerInfos customerInfo;
	private ItineraryRef itineraryRef;
	private List<TicketDataInfo> ticketDataInfo;
	private MaskedPaymentCard maskedPaymentCard;
	private PaymentCard paymentCard;
	/**
	 * @return the accountingCode
	 */
	public String getAccountingCode() {
		return accountingCode;
	}
	/**
	 * @param accountingCode the accountingCode to set
	 */
	public void setAccountingCode(String accountingCode) {
		this.accountingCode = accountingCode;
	}
	/**
	 * @return the issueDate
	 */
	public String getIssueDate() {
		return issueDate;
	}
	/**
	 * @param issueDate the issueDate to set
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	/**
	 * @return the issuingAgent
	 */
	public String getIssuingAgent() {
		return issuingAgent;
	}
	/**
	 * @param issuingAgent the issuingAgent to set
	 */
	public void setIssuingAgent(String issuingAgent) {
		this.issuingAgent = issuingAgent;
	}
	/**
	 * @return the numCoupons
	 */
	public String getNumCoupons() {
		return numCoupons;
	}
	/**
	 * @param numCoupons the numCoupons to set
	 */
	public void setNumCoupons(String numCoupons) {
		this.numCoupons = numCoupons;
	}
	/**
	 * @return the eTicketNumber
	 */
	public String geteTicketNumber() {
		return eTicketNumber;
	}
	/**
	 * @param eTicketNumber the eTicketNumber to set
	 */
	public void seteTicketNumber(String eTicketNumber) {
		this.eTicketNumber = eTicketNumber;
	}
	/**
	 * @return the customerInfo
	 */
	public CustomerInfos getCustomerInfo() {
		return customerInfo;
	}
	/**
	 * @param customerInfo the customerInfo to set
	 */
	public void setCustomerInfo(CustomerInfos customerInfo) {
		this.customerInfo = customerInfo;
	}
	/**
	 * @return the itineraryRef
	 */
	public ItineraryRef getItineraryRef() {
		return itineraryRef;
	}
	/**
	 * @param itineraryRef the itineraryRef to set
	 */
	public void setItineraryRef(ItineraryRef itineraryRef) {
		this.itineraryRef = itineraryRef;
	}
	
	/**
	 * @return the maskedPaymentCard
	 */
	public MaskedPaymentCard getMaskedPaymentCard() {
		return maskedPaymentCard;
	}
	
	/**
	 * @param maskedPaymentCard the maskedPaymentCard to set
	 */
	public void setMaskedPaymentCard(MaskedPaymentCard maskedPaymentCard) {
		this.maskedPaymentCard = maskedPaymentCard;
	}
	/**
	 * @return the paymentCard
	 */
	public PaymentCard getPaymentCard() {
		return paymentCard;
	}
	/**
	 * @param paymentCard the paymentCard to set
	 */
	public void setPaymentCard(PaymentCard paymentCard) {
		this.paymentCard = paymentCard;
	}
	/**
	 * @return the ticketDataInfo
	 */
	public List<TicketDataInfo> getTicketDataInfo() {
		return ticketDataInfo;
	}
	/**
	 * @param ticketDataInfo the ticketDataInfo to set
	 */
	public void setTicketDataInfo(List<TicketDataInfo> ticketDataInfo) {
		this.ticketDataInfo = ticketDataInfo;
	}
	
}
