package com.etihad.book.vcr.schema;

public class PaymentCard {

	private String authorizationCode;
	private String authorizationType;
	private String code;
	private String expireDate;
	private String extendedPayment;
	private String number;
	private String presentCC_Ind;
	/**
	 * @return the authorizationCode
	 */
	public String getAuthorizationCode() {
		return authorizationCode;
	}
	/**
	 * @param authorizationCode the authorizationCode to set
	 */
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}
	/**
	 * @return the authorizationType
	 */
	public String getAuthorizationType() {
		return authorizationType;
	}
	/**
	 * @param authorizationType the authorizationType to set
	 */
	public void setAuthorizationType(String authorizationType) {
		this.authorizationType = authorizationType;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the expireDate
	 */
	public String getExpireDate() {
		return expireDate;
	}
	/**
	 * @param expireDate the expireDate to set
	 */
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}
	/**
	 * @return the extendedPayment
	 */
	public String getExtendedPayment() {
		return extendedPayment;
	}
	/**
	 * @param extendedPayment the extendedPayment to set
	 */
	public void setExtendedPayment(String extendedPayment) {
		this.extendedPayment = extendedPayment;
	}
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * @return the presentCC_Ind
	 */
	public String getPresentCC_Ind() {
		return presentCC_Ind;
	}
	/**
	 * @param presentCC_Ind the presentCC_Ind to set
	 */
	public void setPresentCC_Ind(String presentCC_Ind) {
		this.presentCC_Ind = presentCC_Ind;
	}
}
