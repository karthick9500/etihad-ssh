/**
 * 
 */
package com.etihad.book.vcr.schema;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 641852
 *
 */

public class VCRTicketingInfo {
	
	private String itenaryBaseFairAmount;
	private String itenaryBaseCurrencyCode;
	private String itenaryTotalFairAmount;
	private String itenaryTotalCurrencyCode;

	private List<TaxInfo> tax = new ArrayList<>();
	private List<PTCFareCalculation> ptcFareCalculation = new ArrayList<>();
	private List<CouponInfo> coupon = new ArrayList<>();
	private TicketingResInfo ticketing;
	
	/**
	 * @return the itenaryBaseFairAmount
	 */
	public String getItenaryBaseFairAmount() {
		return itenaryBaseFairAmount;
	}
	/**
	 * @param itenaryBaseFairAmount the itenaryBaseFairAmount to set
	 */
	public void setItenaryBaseFairAmount(String itenaryBaseFairAmount) {
		this.itenaryBaseFairAmount = itenaryBaseFairAmount;
	}
	/**
	 * @return the itenaryBaseCurrencyCode
	 */
	public String getItenaryBaseCurrencyCode() {
		return itenaryBaseCurrencyCode;
	}
	/**
	 * @param itenaryBaseCurrencyCode the itenaryBaseCurrencyCode to set
	 */
	public void setItenaryBaseCurrencyCode(String itenaryBaseCurrencyCode) {
		this.itenaryBaseCurrencyCode = itenaryBaseCurrencyCode;
	}
	/**
	 * @return the itenaryTotalFairAmount
	 */
	public String getItenaryTotalFairAmount() {
		return itenaryTotalFairAmount;
	}
	/**
	 * @param itenaryTotalFairAmount the itenaryTotalFairAmount to set
	 */
	public void setItenaryTotalFairAmount(String itenaryTotalFairAmount) {
		this.itenaryTotalFairAmount = itenaryTotalFairAmount;
	}
	/**
	 * @return the itenaryTotalCurrencyCode
	 */
	public String getItenaryTotalCurrencyCode() {
		return itenaryTotalCurrencyCode;
	}
	/**
	 * @param itenaryTotalCurrencyCode the itenaryTotalCurrencyCode to set
	 */
	public void setItenaryTotalCurrencyCode(String itenaryTotalCurrencyCode) {
		this.itenaryTotalCurrencyCode = itenaryTotalCurrencyCode;
	}
	/**
	 * @return the tax
	 */
	public List<TaxInfo> getTax() {
		return tax;
	}
	/**
	 * @param tax the tax to set
	 */
	public void setTax(List<TaxInfo> tax) {
		this.tax = tax;
	}
	/**
	 * @return the ptcFareCalculation
	 */
	public List<PTCFareCalculation> getPtcFareCalculation() {
		return ptcFareCalculation;
	}
	/**
	 * @param ptcFareCalculation the ptcFareCalculation to set
	 */
	public void setPtcFareCalculation(List<PTCFareCalculation> ptcFareCalculation) {
		this.ptcFareCalculation = ptcFareCalculation;
	}
	/**
	 * @return the coupon
	 */
	public List<CouponInfo> getCoupon() {
		return coupon;
	}
	/**
	 * @param coupon the coupon to set
	 */
	public void setCoupon(List<CouponInfo> coupon) {
		this.coupon = coupon;
	}
	/**
	 * @return the ticketing
	 */
	public TicketingResInfo getTicketing() {
		return ticketing;
	}
	/**
	 * @param ticketing the ticketing to set
	 */
	public void setTicketing(TicketingResInfo ticketing) {
		this.ticketing = ticketing;
	}
	
}
