package com.etihad.book.vcr.schema;

import java.util.List;

public class CustomerInfos {

	private String cashCheckAmount;
	private String code1;
	private String code2;
	private List<String> form;
	private String passengerType;
	private String surname;
	/**
	 * @return the cashCheckAmount
	 */
	public String getCashCheckAmount() {
		return cashCheckAmount;
	}
	/**
	 * @param cashCheckAmount the cashCheckAmount to set
	 */
	public void setCashCheckAmount(String cashCheckAmount) {
		this.cashCheckAmount = cashCheckAmount;
	}
	/**
	 * @return the code1
	 */
	public String getCode1() {
		return code1;
	}
	/**
	 * @param code1 the code1 to set
	 */
	public void setCode1(String code1) {
		this.code1 = code1;
	}
	/**
	 * @return the code2
	 */
	public String getCode2() {
		return code2;
	}
	/**
	 * @param code2 the code2 to set
	 */
	public void setCode2(String code2) {
		this.code2 = code2;
	}

	/**
	 * @return the passengerType
	 */
	public String getPassengerType() {
		return passengerType;
	}
	/**
	 * @return the form
	 */
	public List<String> getForm() {
		return form;
	}
	/**
	 * @param form the form to set
	 */
	public void setForm(List<String> form) {
		this.form = form;
	}
	/**
	 * @param passengerType the passengerType to set
	 */
	public void setPassengerType(String passengerType) {
		this.passengerType = passengerType;
	}
	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}
	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

}
