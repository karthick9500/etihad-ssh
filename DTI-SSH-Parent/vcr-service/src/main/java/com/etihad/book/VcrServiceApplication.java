package com.etihad.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VcrServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(VcrServiceApplication.class, args);
	}
}
