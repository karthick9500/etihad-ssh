/**
 * 
 */
package com.etihad.book.vcr.adaptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.vcr.schema.VCRDisplayRequest;
import com.etihad.book.vcr.schema.VCRDisplayResponse;
import com.sabre.webservices.sabrexml._2011._10.ObjectFactory;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRQ;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRQ.SearchOptions;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRQ.SearchOptions.Ticketing;

/**
 * @author CTS
 *
 */
@Component
public class VCRDisplayAdaptor {

	@Autowired
	private Config config;

	@Autowired
	private Utils utils;

	private VCRDisplayResponse response = new VCRDisplayResponse();

	public VCRDisplayRQ createSOAPRequestForVCRDispayData(VCRDisplayRequest request)
			throws ApplicationException {

		final ObjectFactory factory = new ObjectFactory();
		VCRDisplayRQ soapRequest = factory.createVCRDisplayRQ();
		soapRequest.setReturnHostCommand(false);
		soapRequest.setVersion("2.2.2");
		SearchOptions searchOptions = new SearchOptions();
		
		Ticketing ticketing = new Ticketing();
		ticketing.setETicketNumber(request.getSearchParameters().geteTicketNumber());
		searchOptions.setTicketing(ticketing);
		soapRequest.setSearchOptions(searchOptions);
		
		return soapRequest;
	}


}
