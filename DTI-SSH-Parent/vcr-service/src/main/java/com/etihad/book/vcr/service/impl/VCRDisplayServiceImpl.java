/**
 * 
 */
package com.etihad.book.vcr.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.vcr.constants.Constants;

import com.etihad.book.vcr.adaptor.VCRDisplayAdaptor;
import com.etihad.book.vcr.adaptor.VCRDisplayResponseAdaptor;
import com.etihad.book.vcr.broker.VCRDisplayBroker;
import com.etihad.book.vcr.schema.VCRDisplayRequest;
import com.etihad.book.vcr.schema.VCRDisplayResponse;
import com.etihad.book.vcr.service.IVCRDetailService;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRQ;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS;

/**
 * 
 * @author 641852
 *
 */
@Service
public class VCRDisplayServiceImpl implements IVCRDetailService {

	@Autowired
	private VCRDisplayBroker vcrDisplayBroker;
	@Autowired
	private VCRDisplayAdaptor vcrDisplayAdaptor;
	@Autowired
	private VCRDisplayResponseAdaptor vcrDisplayRespAdaptor;
	@Autowired
	private Utils utils;

	@Override
	public VCRDisplayResponse getVCRDisplayData(VCRDisplayRequest request) throws ApplicationException {

		VCRDisplayResponse response = null;
		VCRDisplayRQ soapRequest;
		VCRDisplayRS soapResponse = null;

		List<String> errorCodes = validateForVCRDisplayRequest(request);

		if (!errorCodes.isEmpty()) {
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}

		if (Constants.TICKET_LENGTH == request.getSearchParameters().geteTicketNumber().length()
				&& StringUtils.isNumeric(request.getSearchParameters().geteTicketNumber())
				&& (Pattern.matches(Constants.PNR_REGEX, request.getSearchParameters().geteTicketNumber()) || Pattern
						.matches(Constants.PNR_REGEX_ANOTHER, request.getSearchParameters().geteTicketNumber()))) {

			soapRequest = vcrDisplayAdaptor.createSOAPRequestForVCRDispayData(request);
			try {
 
				soapResponse = vcrDisplayBroker.processVCRDisplayData(soapRequest,
						request.getSearchParameters().getBinarySecurityToken());

				response = vcrDisplayRespAdaptor.processJSONResponseForVCRDisplayData(soapResponse);

			} catch (Exception e) {
				errorCodes.add("vcr.101");
				throw new ApplicationException(utils.populateResponseHeader(errorCodes));
			}
		}
		response.setMessages(utils.populateResponseHeader(errorCodes));
		return response;
	}
 
	/**
	 * This method validates the ticket enquiry request
	 * 
	 * @param request
	 * @return
	 */
	private List<String> validateForVCRDisplayRequest(VCRDisplayRequest request) {

		List<String> errorCodes = new ArrayList<>(5);
		if (null == request) {
			errorCodes.add("vcr.103");
		} else {
			if (null == request.getSearchParameters()) {
				errorCodes.add("vcr.104");
			} else {
				if (StringUtils.isEmpty(request.getSearchParameters().geteTicketNumber())) {
					errorCodes.add("vcr.105");
				}
			}
		}
		return errorCodes;
	}

}
