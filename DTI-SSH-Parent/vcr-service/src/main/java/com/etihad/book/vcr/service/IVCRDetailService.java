/**
 * 
 */
package com.etihad.book.vcr.service;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.vcr.schema.VCRDisplayRequest;
import com.etihad.book.vcr.schema.VCRDisplayResponse;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS;

/**
 * @author CTS
 *
 */
public interface IVCRDetailService 
{
	public VCRDisplayResponse getVCRDisplayData(VCRDisplayRequest request)  throws ApplicationException ;
}