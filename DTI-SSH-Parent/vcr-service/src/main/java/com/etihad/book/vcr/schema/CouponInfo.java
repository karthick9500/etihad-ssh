package com.etihad.book.vcr.schema;

import java.util.ArrayList;
import java.util.List;

public class CouponInfo {

	private List<String> number = new ArrayList<>();
	private String entitlementNumber;
	private String statusCode;
	private String custLoyaltyMembId;
	private String custLoyaltyProgramID;
	private FlightSegementInfo flightSegement;


	/**
	 * @return the entitlementNumber
	 */
	public String getEntitlementNumber() {
		return entitlementNumber;
	}

	/**
	 * @param entitlementNumber the entitlementNumber to set
	 */
	public void setEntitlementNumber(String entitlementNumber) {
		this.entitlementNumber = entitlementNumber;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the custLoyaltyMembId
	 */
	public String getCustLoyaltyMembId() {
		return custLoyaltyMembId;
	}

	/**
	 * @param custLoyaltyMembId the custLoyaltyMembId to set
	 */
	public void setCustLoyaltyMembId(String custLoyaltyMembId) {
		this.custLoyaltyMembId = custLoyaltyMembId;
	}

	/**
	 * @return the custLoyaltyProgramID
	 */
	public String getCustLoyaltyProgramID() {
		return custLoyaltyProgramID;
	}

	/**
	 * @param custLoyaltyProgramID the custLoyaltyProgramID to set
	 */
	public void setCustLoyaltyProgramID(String custLoyaltyProgramID) {
		this.custLoyaltyProgramID = custLoyaltyProgramID;
	}

	/**
	 * @return the flightSegement
	 */
	public FlightSegementInfo getFlightSegement() {
		return flightSegement;
	}

	/**
	 * @param flightSegement the flightSegement to set
	 */
	public void setFlightSegement(FlightSegementInfo flightSegement) {
		this.flightSegement = flightSegement;
	}

	/**
	 * @return the number
	 */
	public List<String> getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(List<String> number) {
		this.number = number;
	}

	
} 
