/**
 * 
 */
package com.etihad.book.vcr.broker;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.Holder;

import org.ebxml.namespaces.messageheader.From;
import org.ebxml.namespaces.messageheader.MessageData;
import org.ebxml.namespaces.messageheader.MessageHeader;
import org.ebxml.namespaces.messageheader.PartyId;
import org.ebxml.namespaces.messageheader.Service;
import org.ebxml.namespaces.messageheader.To;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xmlsoap.schemas.ws._2002._12.secext.Security;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRQ;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS;
import com.webservices_sabre_com.websvc.VCRDisplayPortType;

/**
 * @author CTS
 *
 */
@Component
public class VCRDisplayBroker {
	
	@Autowired
	private VCRDisplayPortType vcrDisplayObjectProxy;
	
	@Autowired
	private Utils utils;
	
	@Autowired
	private Config config;

	public VCRDisplayRS processVCRDisplayData(VCRDisplayRQ soapRequest,String token) throws ApplicationException {

		VCRDisplayRS soapResponse = null;
		List<String> errorCodes = new ArrayList<>();
		try {
		soapResponse = vcrDisplayObjectProxy.vcrDisplayRQ(getMessageHeader(), getSecurityHeader(token), soapRequest);
		} catch(Exception e) {
			errorCodes.add("vcr.101");
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		return soapResponse;
	}

	private Holder<MessageHeader> getMessageHeader() {

		MessageHeader header = new MessageHeader();
		header.setConversationId(config.getProperty("sabre.conversationId"));
		header.setAction("VCR_DisplayLLSRQ");
		header.setCPAId("ipcc");
		header.setMessageData(getMessageData());

		header.setFrom(getFrom());
		header.setTo(getTo());

		Service service = new Service();
		service.setType("sabreXML");
		service.setValue("VCR_DisplayLLSRQ");
		header.setService(service);
		return new Holder<>(header);	
	}

	private MessageData getMessageData() {

		MessageData messageData = new MessageData();
		messageData.setMessageId(config.getProperty("sabre.messageId"));
		messageData.setRefToMessageId("");
		messageData.setTimestamp("2018-11-27T09:58:31Z");

		return messageData;
	}

	private From getFrom() {
		From from = new From();
		PartyId fromPartyId = new PartyId();
		fromPartyId.setValue(config.getProperty("sabre.fromParty"));
		from.getPartyId().add(fromPartyId);
		return from;
	}

	private To getTo() {
		To to = new To();
		PartyId toPartyId = new PartyId();
		toPartyId.setValue(config.getProperty("sabre.toParty"));
		to.getPartyId().add(toPartyId);
		return to;
	}

	private Holder<Security> getSecurityHeader(String token) {

		Security security = new Security();
		security.setBinarySecurityToken(token);

		return new Holder<>(security);
	}


}
