/**
 * 
 */
package com.etihad.book.vcr.schema;

import com.etihad.book.schema.common.GenericResponse;

import lombok.Data;

/**
 * @author CTS
 *
 */
public class VCRDisplayResponse extends GenericResponse {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private VCRTicketingInfo vcrTicketingInfo;

	/**
	 * @return the vcrTicketingInfo
	 */
	public VCRTicketingInfo getVcrTicketingInfo() {
		return vcrTicketingInfo;
	}

	/**
	 * @param vcrTicketingInfo the vcrTicketingInfo to set
	 */
	public void setVcrTicketingInfo(VCRTicketingInfo vcrTicketingInfo) {
		this.vcrTicketingInfo = vcrTicketingInfo;
	}

}
