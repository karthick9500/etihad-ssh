/**
 * 
 */
package com.etihad.book.vcr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.vcr.constants.Constants;
import com.etihad.book.vcr.schema.SearchParameters;
import com.etihad.book.vcr.schema.VCRDisplayRequest;
import com.etihad.book.vcr.schema.VCRDisplayResponse;
import com.etihad.book.vcr.service.impl.VCRDisplayServiceImpl;
import com.etihad.book.vcr.util.VCRClient;


/**
 * @author 641852
 *
 */
@CrossOrigin
@RestController
@RequestMapping(Constants.VCR_SERVICE)
public class VcrController {

	@Autowired
	VCRDisplayServiceImpl serviceV;
	
	/**
	 * This method will be used to map the request to a specific URI and return the response
	 * 
	 * @return VCRDisplayResponse
	 * @throws ApplicationException 
	 */
	@GetMapping (value = Constants.GET_VCR_DETAIL+"/{eTicketNumber}",produces = "application/json; charset=UTF-8")
	public VCRDisplayResponse getVCRDetailData(@PathVariable String eTicketNumber) throws ApplicationException  {
		VCRDisplayRequest vcrRequest = new VCRDisplayRequest();
		SearchParameters searchParam = new SearchParameters();
		searchParam.setBinarySecurityToken(VCRClient.createSessionSCHD());
		searchParam.seteTicketNumber(eTicketNumber);
		vcrRequest.setSearchParameters(searchParam);
		return serviceV.getVCRDisplayData(vcrRequest);
	}
	
	/**
	 * This method will be used to map the request to a specific URI and return the response
	 * 
	 * @return VCRDisplayResponse
	 * @throws ApplicationException 
	 */
	@PostMapping(path=Constants.GET_VCR_DETAIL , produces = "application/json; charset=UTF-8")
	public VCRDisplayResponse getVCRDetailData(@RequestBody VCRDisplayRequest request) throws ApplicationException  {
		return serviceV.getVCRDisplayData(request);
	}

}
