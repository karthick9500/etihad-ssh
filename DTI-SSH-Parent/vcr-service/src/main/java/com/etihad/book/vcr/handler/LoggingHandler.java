package com.etihad.book.vcr.handler;


import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 * @author Danish
 *
 */
public class LoggingHandler implements SOAPHandler<SOAPMessageContext> {

	@Override
	public void close(MessageContext context) {
		//close
		
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		
		SOAPMessage soapMsg = context.getMessage();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		printOutBoundMsg(soapMsg,out);
		return true;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		
		Boolean isRequest = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		SOAPMessage soapMsg = context.getMessage();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		if(isRequest) {
			printOutBoundMsg(soapMsg,out);
		}
		else{
			/*printInBoundMsg(soapMsg,out);
			try {
				soapMsg.getSOAPPart().getEnvelope().getBody().addNamespaceDeclaration("ns4", "http://services.sabre.com/res/or/v1_17");
				soapMsg.getSOAPPart().getEnvelope().getBody().addNamespaceDeclaration("ns6", "http://webservices.sabre.com/pnrbuilder/v1_17");
				soapMsg.getSOAPPart().getEnvelope().getBody().addNamespaceDeclaration("ns7", "http://services.sabre.com/res/orr/v1_17");
			} catch (SOAPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			printInBoundMsg(soapMsg,out);
		}
		return true;
	}

	private void printOutBoundMsg(SOAPMessage soapMsg, ByteArrayOutputStream out) {
		try {
			soapMsg.writeTo(out);
			System.out.println("OutBound :" + out.toString());
        } catch (Exception e) {
        	//
        }
		
	}
	
	private void printInBoundMsg(SOAPMessage soapMsg, ByteArrayOutputStream out) {
		try {
			soapMsg.writeTo(out);
			System.out.println("InBound :" + out.toString());
        } catch (Exception e) {
        	//
        }
		
	}

	@Override
	public Set<QName> getHeaders() {
		return Collections.emptySet();
	}

	

}
