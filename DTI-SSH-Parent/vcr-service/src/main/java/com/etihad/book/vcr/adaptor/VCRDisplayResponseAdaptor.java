/**
 * 
 */
package com.etihad.book.vcr.adaptor;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.util.Utils;
import com.etihad.book.vcr.schema.CouponInfo;
import com.etihad.book.vcr.schema.CustomerInfos;
import com.etihad.book.vcr.schema.FlightSegementInfo;
import com.etihad.book.vcr.schema.ItineraryRef;
import com.etihad.book.vcr.schema.MaskedPaymentCard;
import com.etihad.book.vcr.schema.PTCFareCalculation;
import com.etihad.book.vcr.schema.PaymentCard;
import com.etihad.book.vcr.schema.TaxInfo;
import com.etihad.book.vcr.schema.TicketDataInfo;
import com.etihad.book.vcr.schema.TicketingResInfo;
import com.etihad.book.vcr.schema.VCRDisplayResponse;
import com.etihad.book.vcr.schema.VCRTicketingInfo;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos.TicketingInfo;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes.Tax;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo.Payment;
import com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData;

/**
 * @author CTS
 *
 */
@Component
public class VCRDisplayResponseAdaptor {

	@Autowired
	private Config config;
	@Autowired
	private Utils utils;
	private VCRTicketingInfo ticketInfo = null;

	public VCRDisplayResponse processJSONResponseForVCRDisplayData(VCRDisplayRS soapResponse) {

		VCRDisplayResponse response = new VCRDisplayResponse();

		if (null != soapResponse) {
			com.sabre.webservices.sabrexml._2011._10.VCRDisplayRS.TicketingInfos ticketInfos = soapResponse
					.getTicketingInfos();
			if (checkNotNull(ticketInfos)) {
				if (checkNotNull(ticketInfos.getTicketingInfo())) {

					VCRTicketingInfo respone = getVCRTicketObject(ticketInfos.getTicketingInfo());
					response.setVcrTicketingInfo(respone);
				}
			}
		}

		return response;
	}

	private VCRTicketingInfo getAirItenary(AirItineraryPricingInfo airItinerObject) {

		VCRTicketingInfo ticketInfo = null;
		List<TaxInfo> taxInfoList = new ArrayList<TaxInfo>();

		if (checkNotNull(airItinerObject)) {
			ticketInfo = new VCRTicketingInfo();
			if (checkNotNull(airItinerObject.getItinTotalFare())) {
				if (checkNotNull(airItinerObject.getItinTotalFare().getBaseFare())) {
					ticketInfo.setItenaryBaseFairAmount(
							checkStringObj(airItinerObject.getItinTotalFare().getBaseFare().getAmount()));
					ticketInfo.setItenaryBaseCurrencyCode(
							checkStringObj(airItinerObject.getItinTotalFare().getBaseFare().getCurrencyCode()));
				}
				if (checkNotNull(airItinerObject.getItinTotalFare().getTaxes())
						&& checkNotNull(airItinerObject.getItinTotalFare().getTaxes().getTax())) {

					for (Tax soapTaxObj : airItinerObject.getItinTotalFare().getTaxes().getTax()) {
						TaxInfo taxObject = new TaxInfo();
						taxObject.setAmount(checkStringObj(soapTaxObj.getAmount()));
						taxObject.setCurrencyCode(checkStringObj(soapTaxObj.getCurrencyCode()));
						taxObject.setTaxCode(checkStringObj(soapTaxObj.getTaxCode()));
						taxInfoList.add(taxObject);
					}
				}
				ticketInfo.setTax(taxInfoList);
				if (checkNotNull(airItinerObject.getItinTotalFare().getTotalFare())) {
					ticketInfo.setItenaryTotalFairAmount(checkStringObj(airItinerObject.getItinTotalFare().getTotalFare().getAmount()));
					ticketInfo.setItenaryTotalCurrencyCode(checkStringObj(
							airItinerObject.getItinTotalFare().getTotalFare().getCurrencyCode()));
				}
			}
			if (checkNotNull(airItinerObject.getPTCFareBreakdown())) {
				ticketInfo.setPtcFareCalculation(getPTCFareCalc(airItinerObject.getPTCFareBreakdown()));
			}
		}

		return ticketInfo;
	}

	private VCRTicketingInfo getVCRTicketObject(TicketingInfo soapTicketInfo) {
			ticketInfo = new VCRTicketingInfo();
			if (checkNotNull(soapTicketInfo.getCouponData())) {
				if (checkNotNull(soapTicketInfo.getCouponData().getAirItineraryPricingInfo())) {
					ticketInfo = getAirItenary(soapTicketInfo.getCouponData().getAirItineraryPricingInfo());
				}
				if (checkNotNull(soapTicketInfo.getCouponData().getCoupons())) {
					ticketInfo.setCoupon(getCouponInfo(soapTicketInfo.getCouponData().getCoupons()));
				}
			}

			if (checkNotNull(soapTicketInfo.getTicketing())) {
				ticketInfo.setTicketing(getTicketingResponseInfo(soapTicketInfo.getTicketing()));
			}
		
		return ticketInfo;

	}

	/**
	 * Method will be used tpo get PTC fare list
	 * 
	 * @param ptcObject
	 * 
	 * @return List<PTCFareCalculation>
	 */ 
	private List<PTCFareCalculation> getPTCFareCalc(PTCFareBreakdown ptcObject) {
		List<PTCFareCalculation> ptcList = new ArrayList<>();
		if (checkNotNull(ptcObject.getFareCalculation()) && checkNotNull(ptcObject.getFareCalculation().getText())
				&& checkNotNull(ptcObject.getFareCalculation().getText().size() > 0)) {
			for (String textObject : ptcObject.getFareCalculation().getText()) {
				PTCFareCalculation fareCalculation = new PTCFareCalculation();
					fareCalculation.setText(textObject);
					ptcList.add(fareCalculation);
				}
			}
		return ptcList;
	}

	/**
	 * Method is used to get list of coupons
	 * 
	 * @param couponsSoapRes
	 * @return List<CouponInfo>
	 */
	private List<CouponInfo> getCouponInfo(Coupons couponsSoapRes) {
		List<CouponInfo> coupons = new ArrayList<>();
		if (checkNotNull(couponsSoapRes)) {
			if (checkNotNull(couponsSoapRes.getCoupon())) {
				for (Coupon couponObj : couponsSoapRes.getCoupon()) {
					CouponInfo coupon = new CouponInfo();
					if (checkNotNull(couponObj.getFlightSegment())) {
						FlightSegementInfo flightInfo = getFlightSegment(couponObj.getFlightSegment());
						coupon.setFlightSegement(flightInfo);
					}
					if (checkNotNull(couponObj.getCustLoyalty())) {
						coupon.setCustLoyaltyMembId(checkStringObj(couponObj.getCustLoyalty().getMembershipID()));
						coupon.setCustLoyaltyProgramID(checkStringObj(couponObj.getCustLoyalty().getProgramID()));
					}

					coupon.setNumber(
							checkNotNull(couponObj.getNumber()) ? couponObj.getNumber() : new ArrayList<String>());
					coupon.setEntitlementNumber(checkStringObj(couponObj.getEntitlementNumber()));
					coupon.setStatusCode(checkStringObj(couponObj.getStatusCode()));
					coupons.add(coupon);
				}
			}
		}
		return coupons;
	}

	/**
	 * Method is used to get flight segment
	 * 
	 * @param flightSegment
	 * @return FlightSegementInfo
	 */
	private FlightSegementInfo getFlightSegment(FlightSegment flightSegment) {

		FlightSegementInfo flightSegmentInfo = new FlightSegementInfo();

		flightSegmentInfo.setConnectionInd(checkStringObj(flightSegment.getConnectionInd()));
		flightSegmentInfo.setDepartureDateTime(checkStringObj(flightSegment.getDepartureDateTime()));
		flightSegmentInfo.setFlightNumber(checkStringObj(flightSegment.getFlightNumber()));
		flightSegmentInfo.setResBookDesigCode(checkStringObj(flightSegment.getResBookDesigCode()));
		flightSegmentInfo.setStatus(checkStringObj(flightSegment.getStatus()));
		if (checkNotNull(flightSegment.getBaggageAllowance())) {
			flightSegmentInfo.setBaggageAllowance(checkStringObj(flightSegment.getBaggageAllowance().getNumber()));
		}
		if (checkNotNull(flightSegment.getDestinationLocation())) {
			flightSegmentInfo.setDestinationLocationCode(
					checkStringObj(flightSegment.getDestinationLocation().getLocationCode()));
		}
		if (checkNotNull(flightSegment.getFareBasis())) {
			flightSegmentInfo.setFareBasis(checkStringObj(flightSegment.getFareBasis().getCode()));
		}
		if (checkNotNull(flightSegment.getMarketingAirline())) {
			flightSegmentInfo.setMarketingAirlineCode(checkStringObj(flightSegment.getMarketingAirline().getCode()));
			flightSegmentInfo
					.setMarketingFlightNumber(checkStringObj(flightSegment.getMarketingAirline().getFlightNumber()));
		}
		if (checkNotNull(flightSegment.getOriginLocation())) {
			flightSegmentInfo
					.setOriginLocationCode(checkStringObj(flightSegment.getOriginLocation().getLocationCode()));
		}
		if (checkNotNull(flightSegment.getValidityDates())) {
			flightSegmentInfo.setNotValidAfter(checkStringObj(flightSegment.getValidityDates().getNotValidAfter()));
			flightSegmentInfo.setNotValidBefore(checkStringObj(flightSegment.getValidityDates().getNotValidBefore()));
		}

		return flightSegmentInfo;
	}

	private TicketingResInfo getTicketingResponseInfo(Ticketing ticketingSoapResp) {

		TicketingResInfo ticketingResInfo = new TicketingResInfo();
		ticketingResInfo.setAccountingCode(checkStringObj(ticketingSoapResp.getAccountingCode()));
		ticketingResInfo.setIssueDate(checkStringObj(ticketingSoapResp.getIssueDate().toXMLFormat()));
		ticketingResInfo.setIssuingAgent(checkStringObj(ticketingSoapResp.getIssuingAgent()));
		ticketingResInfo.setNumCoupons(checkStringObj(ticketingSoapResp.getNumCoupons()));
		ticketingResInfo.seteTicketNumber(checkStringObj(ticketingSoapResp.getETicketNumber()));
		ticketingResInfo.setCustomerInfo(getCustomerInfo(ticketingSoapResp.getCustomerInfo()));
		ItineraryRef itineraryRef = new ItineraryRef();
		if (checkNotNull(ticketingSoapResp.getItineraryRef())) {
			itineraryRef.setId(checkStringObj(ticketingSoapResp.getItineraryRef().getID()));
			if (checkNotNull(ticketingSoapResp.getItineraryRef().getSource())) {
				if (checkNotNull(ticketingSoapResp.getItineraryRef().getSource().getCreateDateTime())) {
					itineraryRef.setSourceCreateDateTime(checkStringObj(
							ticketingSoapResp.getItineraryRef().getSource().getCreateDateTime().toXMLFormat()));
				}
			}
		}

		ticketingResInfo.setItineraryRef(itineraryRef);
		List<TicketData> ticketDataList = ticketingSoapResp.getTicketData();
		List<TicketDataInfo> ticketInfoList = new ArrayList<>();

		if (checkNotNull(ticketDataList) && ticketDataList.size() > 0) {
			for (TicketData ticketDataResp : ticketDataList) {

				TicketDataInfo ticketData = new TicketDataInfo();
				if (checkNotNull(ticketDataResp.getEndorsements())) {
					ticketData.setEndorsementsText(checkNotNull(ticketDataResp.getEndorsements().getText())
							? ticketDataResp.getEndorsements().getText()
							: new ArrayList<String>());
				}
				if (checkNotNull(ticketDataResp.getExchangeData())) {
					ticketData.setExchangeDataLocation(
							checkStringObj(ticketDataResp.getExchangeData().getLocationName()));
				}
				if (checkNotNull(ticketDataResp.getIssueDate())) {
					ticketData.setIssueDate(checkStringObj(ticketDataResp.getIssueDate().toXMLFormat()));
				}
				if (checkNotNull(ticketDataResp.getIssueDate())) {
					ticketData.setPrintStation(checkStringObj(ticketDataResp.getPrintStation()));
				}
				ticketInfoList.add(ticketData);

				MaskedPaymentCard maskedPaymentCard = new MaskedPaymentCard();
				if (checkNotNull(ticketDataResp.getPaymentInfo())
						&& checkNotNull(ticketDataResp.getPaymentInfo().getPayment())
						&& checkNotNull(ticketDataResp.getPaymentInfo().getPayment().getCCInfo())) {

					if (checkNotNull(ticketDataResp.getPaymentInfo().getPayment().getCCInfo().getMaskedPaymentCard())) {
						maskedPaymentCard.setExpireDate(checkStringObj(ticketDataResp.getPaymentInfo().getPayment()
								.getCCInfo().getMaskedPaymentCard().getExpireDate()));
						maskedPaymentCard.setNumber(checkStringObj(ticketDataResp.getPaymentInfo().getPayment()
								.getCCInfo().getMaskedPaymentCard().getNumber()));
					}
					ticketingResInfo.setMaskedPaymentCard(maskedPaymentCard);
					PaymentCard paymentCard = new PaymentCard();

					if (checkNotNull(ticketDataResp.getPaymentInfo().getPayment().getCCInfo().getPaymentCard())) {
						paymentCard.setAuthorizationCode(checkStringObj(ticketDataResp.getPaymentInfo().getPayment()
								.getCCInfo().getPaymentCard().getAuthorizationCode()));
						paymentCard.setAuthorizationType(checkStringObj(ticketDataResp.getPaymentInfo().getPayment()
								.getCCInfo().getPaymentCard().getAuthorizationType()));
						paymentCard.setCode(checkStringObj(
								ticketDataResp.getPaymentInfo().getPayment().getCCInfo().getPaymentCard().getCode()));
						paymentCard.setExpireDate(checkStringObj(ticketDataResp.getPaymentInfo().getPayment()
								.getCCInfo().getPaymentCard().getExpireDate()));
						paymentCard.setExtendedPayment(String.valueOf(ticketDataResp.getPaymentInfo().getPayment()
								.getCCInfo().getPaymentCard().isExtendedPayment()));
						paymentCard.setNumber(checkStringObj(
								ticketDataResp.getPaymentInfo().getPayment().getCCInfo().getPaymentCard().getNumber()));
						paymentCard.setPresentCC_Ind(String.valueOf(ticketDataResp.getPaymentInfo().getPayment()
								.getCCInfo().getPaymentCard().isPresentCCInd()));
					}
					ticketingResInfo.setPaymentCard(paymentCard);

				}
			}
		}
		ticketingResInfo.setTicketDataInfo(ticketInfoList);
		return ticketingResInfo;
	}

	private CustomerInfos getCustomerInfo(CustomerInfo soapCustInfoResp) {
		CustomerInfos customerInfos = new CustomerInfos();
		// List<Payment> paymentObj = new ArrayList<>();
		if (checkNotNull(soapCustInfoResp.getPaymentInfo())) {
			if (checkNotNull(soapCustInfoResp.getPaymentInfo().getPayment())) {
				for (Payment payment : soapCustInfoResp.getPaymentInfo().getPayment()) {
					customerInfos.setCashCheckAmount(checkStringObj(payment.getCashCheckAmount()));
					customerInfos.setCode1(checkStringObj(payment.getCode1()));
					customerInfos.setCode2(checkStringObj(payment.getCode2()));
					customerInfos
							.setForm(checkNotNull(payment.getForm()) ? payment.getForm() : new ArrayList<String>());
				}
			}
		}
		if (checkNotNull(soapCustInfoResp.getPersonName())) {
			customerInfos.setPassengerType(checkStringObj(soapCustInfoResp.getPersonName().getPassengerType()));
			customerInfos.setSurname(checkStringObj(soapCustInfoResp.getPersonName().getSurname()));
		}
		return customerInfos;
	}

	private boolean checkNotNull(Object objectVal) {
		if (null != objectVal) {
			return true;
		}
		return false;
	}

	private String checkStringObj(String strVal) {
		if (StringUtils.isNotEmpty(strVal)) {
			return strVal;
		}
		return "";
	}

}
