package com.etihad.book.vcr.util;

import org.springframework.web.client.RestTemplate;

import com.etihad.book.vcr.schema.CreateSessionResponse;

public class VCRClient {

	/**
	 * Method is used to call create session microservice
	 * 
	 * @return String
	 */
	public static String createSessionSCHD() {
		final String uri = "https://api.us.apiconnect.ibmcloud.com/selfservicehub-dev/ey-ssh/service/session/create-session/";
	    
	    RestTemplate restTemplate = new RestTemplate();
	    CreateSessionResponse createSessionObject = restTemplate.getForObject(uri, CreateSessionResponse.class); 
	    
	    return createSessionObject.getCreateSession().getBinarySecurityToken();
		   
	}
}
