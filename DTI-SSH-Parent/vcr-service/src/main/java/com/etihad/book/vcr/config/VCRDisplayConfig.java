/**
 * 
 */
package com.etihad.book.vcr.config;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.vcr.handler.LoggingHandler;



/**
 * @author CTS
 *
 */
@Configuration
public class VCRDisplayConfig {
	
	@Autowired
	private Config config;
	
	@Bean(name = "vcrDisplayObjectProxy")
    public JaxWsPortProxyFactoryBean createProxy() throws MalformedURLException {
        JaxWsPortProxyFactoryBean bean = new JaxWsPortProxyFactoryBean();
            bean.setServiceInterface(com.webservices_sabre_com.websvc.VCRDisplayPortType.class);
    		bean.setWsdlDocumentUrl(new URL("http://wsdl-crt.cert.sabre.com/wsdl/tpfc/VCR_DisplayLLS2.2.2RQ.wsdl"));
            bean.setNamespaceUri("https://webservices.sabre.com/websvc");
            bean.setServiceName("VCR_DisplayService");
            bean.setPortName("VCR_DisplayPortType");
            bean.setEndpointAddress(config.getProperty("tripSearch.endpointAddress"));
            bean.setLookupServiceOnStartup(false);
            bean.setHandlerResolver(handlerResolver());
        return bean; 
    }
	
	 public HandlerResolver handlerResolver() {
		    return portInfo -> {
			    @SuppressWarnings("rawtypes")
				List<Handler> handlerChain = new ArrayList<>();
			    handlerChain.add(new LoggingHandler());
			    return handlerChain;
			};
		 }
}