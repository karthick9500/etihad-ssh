/**
 * 
 */
package com.etihad.book.serviceorchestration.schema;

/**
 * @author 526837
 *
 */
public class Meal {
	
	private String meal;

	/**
	 * @return the meal
	 */
	public String getMeal() {
		return meal;
	}

	/**
	 * @param meal the meal to set
	 */
	public void setMeal(String meal) {
		this.meal = meal;
	}
	
	

}
