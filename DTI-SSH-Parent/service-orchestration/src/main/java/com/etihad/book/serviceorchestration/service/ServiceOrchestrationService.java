/**
 * 
 */
package com.etihad.book.serviceorchestration.service;

import com.etihad.book.serviceorchestration.schema.ServiceOrchestrationResponse;

/**
 * @author CTS
 *
 */
public interface ServiceOrchestrationService 
{
	
	public ServiceOrchestrationResponse processPNRData(String pnr,String lastName,String langCode);
	
}