/**
 * 
 */
package com.etihad.book.serviceorchestration.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.schema.common.Message;
import com.etihad.book.serviceorchestration.schema.CreateSessionResponse;
import com.etihad.book.serviceorchestration.schema.GetReservationRequest;
import com.etihad.book.serviceorchestration.schema.GetReservationResponse;
import com.etihad.book.serviceorchestration.schema.RetrievePNRRequest;
import com.etihad.book.serviceorchestration.schema.RetrievePNRResponse;
import com.etihad.book.serviceorchestration.schema.SearchParameters;
import com.etihad.book.serviceorchestration.schema.ServiceOrchestrationResponse;

/**
 * @author CTS
 *
 */
@Service
public class ServiceOrchestrationServiceImpl  implements ServiceOrchestrationService{
	
	@Autowired
	private Config config;
	

	@Override
	public ServiceOrchestrationResponse processPNRData(String pnr, String lastName,String langCode) 
	{
		ServiceOrchestrationResponse response = new ServiceOrchestrationResponse();
	    String binarySecurityToken = null;
	    boolean flag = true;
	    RetrievePNRResponse retrivePNRResponse = null;
	    
	    RestTemplate restTemplateforToken = new RestTemplate();
	    CreateSessionResponse createSessionResp = restTemplateforToken.getForObject(config.getProperty("sessionServiceURL"), 
	    		CreateSessionResponse.class);
	    if(createSessionResp.getMessages().getCode() == 0 && createSessionResp.getMessages().getCount() ==1)
	    {
	    	binarySecurityToken = processCreateSessionResponse(binarySecurityToken, createSessionResp);
	    }
	    else if(createSessionResp.getMessages().getCode() >= 1 && createSessionResp.getMessages().getCount() >= 1)
	    {
	    	response.setCreateSession(createSessionResp.getCreateSession());
	    	response.setMessages(createSessionResp.getMessages());
	    	flag = false;
	    }
	    
	    /* Reference data call */
	    /*RestTemplate restTemplateforRef = new RestTemplate();
	    String refData = restTemplateforRef.getForObject(config.getProperty("refDataURL"),String.class);*/
	    
	    
	    /* Get Res call based on alpha pnr's*/
	    if ( flag && StringUtils.isAlpha(pnr) ) 
		{
			
	    	GetReservationResponse getResResponse = processGetReservation(pnr, lastName, langCode, binarySecurityToken);
		    
		    flag = processGetResResponse(response, flag, getResResponse);
		}
		
	    /* Trip Search call -gets called for three scenarios--
	     * 1-if alpha pnr's like SITA is not available in Get Res
	     * 2-for ticket id's
	     * 3-alpha numeric pnr's
	     * */
		if ( flag ) 
		{
			retrivePNRResponse = processPNR(pnr, lastName, binarySecurityToken);
		     
		    flag = processPNRResponse(response, flag, retrivePNRResponse);
		}
		
		/* Get Res call for second time to get data for Landing page if alpha pnr's 
		 * like SITA  is not returned from get res*/
		if(flag)
		{
			String sabrePnr = null;
			if( null != retrivePNRResponse)
			{
				sabrePnr = retrivePNRResponse.getPnr();
			}
			GetReservationResponse getResResponse = processGetReservation(sabrePnr, lastName, langCode, binarySecurityToken);
		    
			processGetResResponse(response, flag, getResResponse);
		}
		return response;
	}


	/**
	 * @param response
	 * @param flag
	 * @param retrivePNRResponse
	 * @return
	 */
	private boolean processPNRResponse(ServiceOrchestrationResponse response, boolean flag,
			RetrievePNRResponse retrivePNRResponse) {
		if(retrivePNRResponse.getMessages().getCode() == 0 && retrivePNRResponse.getMessages().getCount() ==1)
		{
			List<Message> msgList = retrivePNRResponse.getMessages().getMessage();
			if( null  != msgList && !msgList.isEmpty())
			{
				Message msg = msgList.get(0);
		    	if(("0").equals(msg.getCode()) && ("Success").equals(msg.getText()) )
		    	{
		    		 /* do nothing */
		    	}
			}
		}
		else if(retrivePNRResponse.getMessages().getCode() >= 1 && retrivePNRResponse.getMessages().getCount() >= 1)
		{
			response.setPnr(retrivePNRResponse.getPnr());
			response.setPNRDataAvailable(retrivePNRResponse.isPNRDataAvailable());
			response.setMessages(retrivePNRResponse.getMessages());
		    flag =false;
		}
		return flag;
	}


	/**
	 * @param pnr
	 * @param lastName
	 * @param binarySecurityToken
	 * @return
	 */
	private RetrievePNRResponse processPNR(String pnr, String lastName, String binarySecurityToken) {
		RetrievePNRRequest retrievePNRReq = new RetrievePNRRequest();
		SearchParameters searchParams = new SearchParameters();
		searchParams.setPnr(pnr);
		searchParams.setLastName(lastName);
		searchParams.setBinarySecurityToken(binarySecurityToken);
		retrievePNRReq.setSearchParameters(searchParams);
		 
		RestTemplate restTemplateforPnrRetrieval= new RestTemplate();
		return restTemplateforPnrRetrieval.postForObject(config.getProperty("pnrRetrievalURL"), 
				retrievePNRReq, RetrievePNRResponse.class);
	}


	/**
	 * @param response
	 * @param flag
	 * @param getResResponse
	 * @return
	 */
	private boolean processGetResResponse(ServiceOrchestrationResponse response, boolean flag,
			GetReservationResponse getResResponse) {
		if(getResResponse.getMessages().getCode() == 0 && getResResponse.getMessages().getCount() ==1)
		{
			List<Message> msgList = getResResponse.getMessages().getMessage();
			if( null  != msgList && !msgList.isEmpty())
			{
				Message msg = msgList.get(0);
		    	if(("0").equals(msg.getCode()) && ("Success").equals(msg.getText()) )
		    	{
		    		response.setBookingDetails(getResResponse.getBookingDetails());
		 		    response.setFlightDetails(getResResponse.getFlightDetails());
		 		    response.setPaxDetails(getResResponse.getPaxDetails());
		 		    response.setMessages(getResResponse.getMessages());
		    		response.setMessages(getResResponse.getMessages());
		    		flag =false;
		    	}
			}
		}
		else if(getResResponse.getMessages().getCode() >= 1 && getResResponse.getMessages().getCount() >= 1)
		{
			List<Message> msgList = getResResponse.getMessages().getMessage();
			if( null  != msgList && !msgList.isEmpty())
			{
				Message msg = msgList.get(0);
		    	if( ("101").equals(msg.getCode()) )
		    	{
		    		/* do nothing */
		    	}
		    	else
		    	{
		    		response.setBookingDetails(getResResponse.getBookingDetails());
		 		    response.setFlightDetails(getResResponse.getFlightDetails());
		 		    response.setPaxDetails(getResResponse.getPaxDetails());
		 		    response.setMessages(getResResponse.getMessages());
		    		flag =false;
		    	}
			}	
		}
		return flag;
	}


	/**
	 * @param binarySecurityToken
	 * @param createSessionResp
	 * @return
	 */
	private String processCreateSessionResponse(String binarySecurityToken, CreateSessionResponse createSessionResp) {
		List<Message> msgList = createSessionResp.getMessages().getMessage();
		if( null  != msgList && !msgList.isEmpty())
		{
			Message msg = msgList.get(0);
			if(("0").equals(msg.getCode()) && ("Success").equals(msg.getText()) )
			{
				binarySecurityToken = createSessionResp.getCreateSession().getBinarySecurityToken();
			}
		}
		return binarySecurityToken;
	}


	/**
	 * @param pnr
	 * @param lastName
	 * @param langCode
	 * @param binarySecurityToken
	 * @return
	 */
	private GetReservationResponse processGetReservation(String pnr, String lastName, String langCode,
			String binarySecurityToken) {
		GetReservationRequest getResReq = new GetReservationRequest();
		SearchParameters searchParams = new SearchParameters();
		searchParams.setPnr(pnr);
		searchParams.setLastName(lastName);
		searchParams.setBinarySecurityToken(binarySecurityToken);
		searchParams.setLangCode(langCode);
		getResReq.setSearchParameters(searchParams);
		 
		RestTemplate restTemplateforGetRes= new RestTemplate();
		return restTemplateforGetRes.postForObject(config.getProperty("getResURL"), 
				getResReq, GetReservationResponse.class);
	}

}