package com.etihad.book.serviceorchestration.schema;

/**
 * Cabbin baggage (carry on details)
 *
 */
public class CabinBaggageDetails {

	private String unit;
	private String weightDescr;
	private String additionalBaggage;

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getWeightDescr() {
		return weightDescr;
	}

	public void setWeightDescr(String weightDescr) {
		this.weightDescr = weightDescr;
	}

	public String getAdditionalBaggage() {
		return additionalBaggage;
	}

	public void setAdditionalBaggage(String additionalBaggage) {
		this.additionalBaggage = additionalBaggage;
	}

	

}
