/**
 * 
 */
package com.etihad.book.serviceorchestration.schema;

import com.etihad.book.schema.common.GenericResponse;

/**
 * @author CTS
 *
 */
public class CreateSessionResponse extends GenericResponse {
	
	private static final long serialVersionUID = 1L;
	
	private CreateSession createSession;

	public CreateSession getCreateSession() {
		return createSession;
	}

	public void setCreateSession(CreateSession createSession) {
		this.createSession = createSession;
	}

}
