/**
 * 
 */
package com.etihad.book.serviceorchestration.schema;

import java.util.List;

import com.etihad.book.schema.common.GenericResponse;

/**
 * @author CTS
 *
 */
public class ServiceOrchestrationResponse  extends GenericResponse{
	
	
/*	private RetrievePNRResponse retrivePNRResponse;
	
	private GetReservationResponse getResResponse;
	
	private CreateSessionResponse createSessionResp;

	*//**
	 * @return the retrivePNRResponse
	 *//*
	public RetrievePNRResponse getRetrivePNRResponse() {
		return retrivePNRResponse;
	}

	*//**
	 * @param retrivePNRResponse the retrivePNRResponse to set
	 *//*
	public void setRetrivePNRResponse(RetrievePNRResponse retrivePNRResponse) {
		this.retrivePNRResponse = retrivePNRResponse;
	}

	*//**
	 * @return the getResResponse
	 *//*
	public GetReservationResponse getGetResResponse() {
		return getResResponse;
	}

	*//**
	 * @param getResResponse the getResResponse to set
	 *//*
	public void setGetResResponse(GetReservationResponse getResResponse) {
		this.getResResponse = getResResponse;
	}

	*//**
	 * @return the createSessionResp
	 *//*
	public CreateSessionResponse getCreateSessionResp() {
		return createSessionResp;
	}

	*//**
	 * @param createSessionResp the createSessionResp to set
	 *//*
	public void setCreateSessionResp(CreateSessionResponse createSessionResp) {
		this.createSessionResp = createSessionResp;
	}
*/
	
	private BookingDetails bookingDetails;
	
	private List<FlightDetails> flightDetails;
	
	private List<PassengerDetails2> paxDetails;
	
	private boolean isPNRDataAvailable;
	private String pnr;
	
	private CreateSession createSession;

	public CreateSession getCreateSession() {
		return createSession;
	}

	public void setCreateSession(CreateSession createSession) {
		this.createSession = createSession;
	}


	/**
	 * @return the isPNRDataAvailable
	 */
	public boolean isPNRDataAvailable() {
		return isPNRDataAvailable;
	}

	/**
	 * @param isPNRDataAvailable the isPNRDataAvailable to set
	 */
	public void setPNRDataAvailable(boolean isPNRDataAvailable) {
		this.isPNRDataAvailable = isPNRDataAvailable;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}



	/**
	 * @return the paxDetails
	 */
	public List<PassengerDetails2> getPaxDetails() {
		return paxDetails;
	}

	/**
	 * @param paxDetails the paxDetails to set
	 */
	public void setPaxDetails(List<PassengerDetails2> paxDetails) {
		this.paxDetails = paxDetails;
	}

	/**
	 * @return the bookingDetails
	 */
	public BookingDetails getBookingDetails() {
		return bookingDetails;
	}

	/**
	 * @param bookingDetails the bookingDetails to set
	 */
	public void setBookingDetails(BookingDetails bookingDetails) {
		this.bookingDetails = bookingDetails;
	}

	/**
	 * @return the flightDetails
	 */
	public List<FlightDetails> getFlightDetails() {
		return flightDetails;
	}

	/**
	 * @param flightDetails the flightDetails to set
	 */
	public void setFlightDetails(List<FlightDetails> flightDetails) {
		this.flightDetails = flightDetails;
	}

	
	
	
}
