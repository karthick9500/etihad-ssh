/**
 * 
 */
package com.etihad.book.serviceorchestration.schema;

import java.io.Serializable;

/**
 * @author CTS
 *
 */
public class CreateSession implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String conversationId;
	private String binarySecurityToken;

	public String getBinarySecurityToken() {
		return binarySecurityToken;
	}

	public void setBinarySecurityToken(String binarySecurityToken) {
		this.binarySecurityToken = binarySecurityToken;
	}

	public String getConversationId() {
		return conversationId;
	}

	public void setConversationId(String conversationId) {
		this.conversationId = conversationId;
	}

}
