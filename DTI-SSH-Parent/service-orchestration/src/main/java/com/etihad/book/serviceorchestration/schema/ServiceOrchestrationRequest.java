/**
 * 
 */
package com.etihad.book.serviceorchestration.schema;

import com.etihad.book.schema.common.AbstractRequest;

/**
 * @author CTS
 *
 */
public class ServiceOrchestrationRequest extends AbstractRequest {
	
	private SearchParameters searchParameters;
	
	public SearchParameters getSearchParameters() {
		return searchParameters;
	}
	public void setSearchParameters(SearchParameters searchParameters) {
		this.searchParameters = searchParameters;
	}
}
