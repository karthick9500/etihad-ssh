/**
 * 
 */
package com.etihad.book.serviceorchestration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.serviceorchestration.schema.ServiceOrchestrationResponse;
import com.etihad.book.serviceorchestration.service.ServiceOrchestrationServiceImpl;



/**
 * @author CTS
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/service/v1")
public class ServiceOrchestrationController {
	

	@Autowired
	private Config config;
	
	@Autowired
	private ServiceOrchestrationServiceImpl service;
	
	
	/**
	 * This method will be used to map the request to a specific URI and return the response 
	 * @return
	 * @throws ApplicationException 
	 */
	@CrossOrigin
	@RequestMapping(path="/processPNRData", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public ServiceOrchestrationResponse processPNRData(@RequestParam("pnr") String pnr,
			@RequestParam("lastName") String lastName,
			@RequestParam("langCode") String langCode)   
	{
		
		return service.processPNRData(pnr,lastName,langCode);
	}
}
