package com.etihad.book.serviceorchestration.schema;

/**
 * 
 * Class for baggage details
 *
 */
public class BaggageDetails {

	private CabinBaggageDetails cabinBaggage;
	private CheckinBaggageDetails checkinBaggage;

	/**
	 * Checkin baggage details
	 * @return CheckinBaggageDetails
	 */
	public CheckinBaggageDetails getCheckinBaggage() {
		return checkinBaggage;
	}

	public void setCheckinBaggage(CheckinBaggageDetails checkinBaggage) {
		this.checkinBaggage = checkinBaggage;
	}
	/**
	 * Cabin baggage details(carry-on)
	 * @return CabinBaggageDetails
	 */
	public CabinBaggageDetails getCabinBaggage() {
		return cabinBaggage;
	}

	public void setCabinBaggage(CabinBaggageDetails cabinBaggage) {
		this.cabinBaggage = cabinBaggage;
	}

	

}
