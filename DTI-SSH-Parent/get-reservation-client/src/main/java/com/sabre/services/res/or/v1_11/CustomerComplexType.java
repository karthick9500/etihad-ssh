
package com.sabre.services.res.or.v1_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerComplexType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerComplexType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REGULAR"/>
 *     &lt;enumeration value="TVLYPREF"/>
 *     &lt;enumeration value="PREFELITE"/>
 *     &lt;enumeration value="LOYALTY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CustomerComplexType")
@XmlEnum
public enum CustomerComplexType {


    /**
     * Regular customer type.
     * 
     */
    REGULAR,

    /**
     * TVLY_PREFERRED customer type.
     * 
     */
    TVLYPREF,

    /**
     * PREFERED_ELITE customer type.
     * 
     */
    PREFELITE,

    /**
     * LOYALTY customer type.
     * 
     */
    LOYALTY;

    public String value() {
        return name();
    }

    public static CustomerComplexType fromValue(String v) {
        return valueOf(v);
    }

}
