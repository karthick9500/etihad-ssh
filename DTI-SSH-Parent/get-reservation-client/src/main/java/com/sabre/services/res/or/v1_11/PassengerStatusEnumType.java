
package com.sabre.services.res.or.v1_11;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PassengerStatusEnumType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PassengerStatusEnumType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="R"/>
 *     &lt;enumeration value="E"/>
 *     &lt;enumeration value="N"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PassengerStatusEnumType")
@XmlEnum
public enum PassengerStatusEnumType {


    /**
     * Residency.
     * 
     */
    R,

    /**
     * Employment.
     * 
     */
    E,

    /**
     * Nationality.
     * 
     */
    N;

    public String value() {
        return name();
    }

    public static PassengerStatusEnumType fromValue(String v) {
        return valueOf(v);
    }

}
