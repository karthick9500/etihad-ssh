
package com.sabre.services.res.or.v1_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PricingRequestInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PricingRequestInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="AirPriceQualifiers" type="{http://services.sabre.com/res/or/v1_11}AirPriceQualifiersType" minOccurs="0"/>
 *         &lt;element name="HotelRateQualifiers" type="{http://services.sabre.com/res/or/v1_11}HotelRateQualifiersType" minOccurs="0"/>
 *         &lt;element name="CarRateQualifiers" type="{http://services.sabre.com/res/or/v1_11}CarRateQualifiersType" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PricingRequestInformationType", propOrder = {
    "airPriceQualifiers",
    "hotelRateQualifiers",
    "carRateQualifiers"
})
public class PricingRequestInformationType {

    @XmlElement(name = "AirPriceQualifiers")
    protected AirPriceQualifiersType airPriceQualifiers;
    @XmlElement(name = "HotelRateQualifiers")
    protected HotelRateQualifiersType hotelRateQualifiers;
    @XmlElement(name = "CarRateQualifiers")
    protected CarRateQualifiersType carRateQualifiers;

    /**
     * Gets the value of the airPriceQualifiers property.
     * 
     * @return
     *     possible object is
     *     {@link AirPriceQualifiersType }
     *     
     */
    public AirPriceQualifiersType getAirPriceQualifiers() {
        return airPriceQualifiers;
    }

    /**
     * Sets the value of the airPriceQualifiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link AirPriceQualifiersType }
     *     
     */
    public void setAirPriceQualifiers(AirPriceQualifiersType value) {
        this.airPriceQualifiers = value;
    }

    /**
     * Gets the value of the hotelRateQualifiers property.
     * 
     * @return
     *     possible object is
     *     {@link HotelRateQualifiersType }
     *     
     */
    public HotelRateQualifiersType getHotelRateQualifiers() {
        return hotelRateQualifiers;
    }

    /**
     * Sets the value of the hotelRateQualifiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link HotelRateQualifiersType }
     *     
     */
    public void setHotelRateQualifiers(HotelRateQualifiersType value) {
        this.hotelRateQualifiers = value;
    }

    /**
     * Gets the value of the carRateQualifiers property.
     * 
     * @return
     *     possible object is
     *     {@link CarRateQualifiersType }
     *     
     */
    public CarRateQualifiersType getCarRateQualifiers() {
        return carRateQualifiers;
    }

    /**
     * Sets the value of the carRateQualifiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link CarRateQualifiersType }
     *     
     */
    public void setCarRateQualifiers(CarRateQualifiersType value) {
        this.carRateQualifiers = value;
    }

}
