
package com.sabre.services.res.or.v1_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ApoXmlData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApoXmlData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element ref="{http://services.sabre.com/res/or/v1_11}AncillaryServiceData"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApoXmlData", propOrder = {
    "ancillaryServiceData"
})
public class ApoXmlData {

    @XmlElement(name = "AncillaryServiceData")
    protected AncillaryServiceData ancillaryServiceData;

    /**
     * Gets the value of the ancillaryServiceData property.
     * 
     * @return
     *     possible object is
     *     {@link AncillaryServiceData }
     *     
     */
    public AncillaryServiceData getAncillaryServiceData() {
        return ancillaryServiceData;
    }

    /**
     * Sets the value of the ancillaryServiceData property.
     * 
     * @param value
     *     allowed object is
     *     {@link AncillaryServiceData }
     *     
     */
    public void setAncillaryServiceData(AncillaryServiceData value) {
        this.ancillaryServiceData = value;
    }

}
