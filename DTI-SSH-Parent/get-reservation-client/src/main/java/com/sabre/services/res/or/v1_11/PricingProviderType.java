
package com.sabre.services.res.or.v1_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PricingProviderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PricingProviderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProviderName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PricingOptions" type="{http://services.sabre.com/res/or/v1_11}PricingRequestInformationType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PricingProviderType", propOrder = {
    "providerName",
    "pricingOptions"
})
public class PricingProviderType {

    @XmlElement(name = "ProviderName", required = true)
    protected String providerName;
    @XmlElement(name = "PricingOptions")
    protected PricingRequestInformationType pricingOptions;

    /**
     * Gets the value of the providerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderName() {
        return providerName;
    }

    /**
     * Sets the value of the providerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderName(String value) {
        this.providerName = value;
    }

    /**
     * Gets the value of the pricingOptions property.
     * 
     * @return
     *     possible object is
     *     {@link PricingRequestInformationType }
     *     
     */
    public PricingRequestInformationType getPricingOptions() {
        return pricingOptions;
    }

    /**
     * Sets the value of the pricingOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricingRequestInformationType }
     *     
     */
    public void setPricingOptions(PricingRequestInformationType value) {
        this.pricingOptions = value;
    }

}
