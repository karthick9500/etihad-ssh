
package com.sabre.services.res.or.v1_11;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FareComponentWithId complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FareComponentWithId">
 *   &lt;complexContent>
 *     &lt;extension base="{http://services.sabre.com/res/or/v1_11}FareComponent">
 *       &lt;sequence>
 *         &lt;element name="FareComponentID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FareComponentWithId", propOrder = {
    "fareComponentID"
})
public class FareComponentWithId
    extends FareComponent
{

    @XmlElement(name = "FareComponentID")
    protected Integer fareComponentID;

    /**
     * Gets the value of the fareComponentID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFareComponentID() {
        return fareComponentID;
    }

    /**
     * Sets the value of the fareComponentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFareComponentID(Integer value) {
        this.fareComponentID = value;
    }

}
