
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PassengerReservation.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PassengerReservation.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Passengers" type="{http://webservices.sabre.com/pnrbuilder/v1_17}Passengers.PNRB" minOccurs="0"/>
 *         &lt;element name="Segments" type="{http://webservices.sabre.com/pnrbuilder/v1_17}SegmentType.PNRB" minOccurs="0"/>
 *         &lt;element name="FormsOfPayment" type="{http://webservices.sabre.com/pnrbuilder/v1_17}FormsOfPayment.PNRB" minOccurs="0"/>
 *         &lt;element name="TicketingInfo" type="{http://webservices.sabre.com/pnrbuilder/v1_17}TicketingInfo.PNRB" minOccurs="0"/>
 *         &lt;element name="ItineraryPricing" type="{http://webservices.sabre.com/pnrbuilder/v1_17}ItineraryPricing.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PassengerReservation.PNRB", propOrder = {
    "passengers",
    "segments",
    "formsOfPayment",
    "ticketingInfo",
    "itineraryPricing"
})
public class PassengerReservationPNRB {

    @XmlElement(name = "Passengers")
    protected PassengersPNRB passengers;
    @XmlElement(name = "Segments")
    protected SegmentTypePNRB segments;
    @XmlElement(name = "FormsOfPayment")
    protected FormsOfPaymentPNRB formsOfPayment;
    @XmlElement(name = "TicketingInfo")
    protected TicketingInfoPNRB ticketingInfo;
    @XmlElement(name = "ItineraryPricing")
    protected ItineraryPricingPNRB itineraryPricing;

    /**
     * Gets the value of the passengers property.
     * 
     * @return
     *     possible object is
     *     {@link PassengersPNRB }
     *     
     */
    public PassengersPNRB getPassengers() {
        return passengers;
    }

    /**
     * Sets the value of the passengers property.
     * 
     * @param value
     *     allowed object is
     *     {@link PassengersPNRB }
     *     
     */
    public void setPassengers(PassengersPNRB value) {
        this.passengers = value;
    }

    /**
     * Gets the value of the segments property.
     * 
     * @return
     *     possible object is
     *     {@link SegmentTypePNRB }
     *     
     */
    public SegmentTypePNRB getSegments() {
        return segments;
    }

    /**
     * Sets the value of the segments property.
     * 
     * @param value
     *     allowed object is
     *     {@link SegmentTypePNRB }
     *     
     */
    public void setSegments(SegmentTypePNRB value) {
        this.segments = value;
    }

    /**
     * Gets the value of the formsOfPayment property.
     * 
     * @return
     *     possible object is
     *     {@link FormsOfPaymentPNRB }
     *     
     */
    public FormsOfPaymentPNRB getFormsOfPayment() {
        return formsOfPayment;
    }

    /**
     * Sets the value of the formsOfPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormsOfPaymentPNRB }
     *     
     */
    public void setFormsOfPayment(FormsOfPaymentPNRB value) {
        this.formsOfPayment = value;
    }

    /**
     * Gets the value of the ticketingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TicketingInfoPNRB }
     *     
     */
    public TicketingInfoPNRB getTicketingInfo() {
        return ticketingInfo;
    }

    /**
     * Sets the value of the ticketingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TicketingInfoPNRB }
     *     
     */
    public void setTicketingInfo(TicketingInfoPNRB value) {
        this.ticketingInfo = value;
    }

    /**
     * Gets the value of the itineraryPricing property.
     * 
     * @return
     *     possible object is
     *     {@link ItineraryPricingPNRB }
     *     
     */
    public ItineraryPricingPNRB getItineraryPricing() {
        return itineraryPricing;
    }

    /**
     * Sets the value of the itineraryPricing property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItineraryPricingPNRB }
     *     
     */
    public void setItineraryPricing(ItineraryPricingPNRB value) {
        this.itineraryPricing = value;
    }

}
