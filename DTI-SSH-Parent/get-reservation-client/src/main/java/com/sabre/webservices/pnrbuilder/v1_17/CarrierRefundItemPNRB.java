
package com.sabre.webservices.pnrbuilder.v1_17;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CarrierRefundItem.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CarrierRefundItem.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="CarrierRecordLocator" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="SegmentSubset" type="{http://webservices.sabre.com/pnrbuilder/v1_17}SegmentSubset"/>
 *         &lt;/choice>
 *         &lt;element name="PaymentAuthorizationCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="2"/>
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CarrierRefundItem.PNRB", propOrder = {
    "carrierRecordLocator",
    "segmentSubset",
    "paymentAuthorizationCode"
})
public class CarrierRefundItemPNRB {

    @XmlElement(name = "CarrierRecordLocator")
    protected List<String> carrierRecordLocator;
    @XmlElement(name = "SegmentSubset")
    @XmlSchemaType(name = "string")
    protected SegmentSubset segmentSubset;
    @XmlElement(name = "PaymentAuthorizationCode")
    protected String paymentAuthorizationCode;

    /**
     * Gets the value of the carrierRecordLocator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the carrierRecordLocator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCarrierRecordLocator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCarrierRecordLocator() {
        if (carrierRecordLocator == null) {
            carrierRecordLocator = new ArrayList<String>();
        }
        return this.carrierRecordLocator;
    }

    /**
     * Gets the value of the segmentSubset property.
     * 
     * @return
     *     possible object is
     *     {@link SegmentSubset }
     *     
     */
    public SegmentSubset getSegmentSubset() {
        return segmentSubset;
    }

    /**
     * Sets the value of the segmentSubset property.
     * 
     * @param value
     *     allowed object is
     *     {@link SegmentSubset }
     *     
     */
    public void setSegmentSubset(SegmentSubset value) {
        this.segmentSubset = value;
    }

    /**
     * Gets the value of the paymentAuthorizationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentAuthorizationCode() {
        return paymentAuthorizationCode;
    }

    /**
     * Sets the value of the paymentAuthorizationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentAuthorizationCode(String value) {
        this.paymentAuthorizationCode = value;
    }

}
