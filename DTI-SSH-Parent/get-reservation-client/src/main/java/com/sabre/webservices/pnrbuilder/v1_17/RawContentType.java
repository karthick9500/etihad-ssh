
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RawContentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RawContentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PnrContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="GlobalSecurityContent" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RawContentType", propOrder = {
    "pnrContent",
    "globalSecurityContent"
})
public class RawContentType {

    @XmlElement(name = "PnrContent")
    protected byte[] pnrContent;
    @XmlElement(name = "GlobalSecurityContent")
    protected byte[] globalSecurityContent;

    /**
     * Gets the value of the pnrContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPnrContent() {
        return pnrContent;
    }

    /**
     * Sets the value of the pnrContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPnrContent(byte[] value) {
        this.pnrContent = value;
    }

    /**
     * Gets the value of the globalSecurityContent property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getGlobalSecurityContent() {
        return globalSecurityContent;
    }

    /**
     * Sets the value of the globalSecurityContent property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setGlobalSecurityContent(byte[] value) {
        this.globalSecurityContent = value;
    }

}
