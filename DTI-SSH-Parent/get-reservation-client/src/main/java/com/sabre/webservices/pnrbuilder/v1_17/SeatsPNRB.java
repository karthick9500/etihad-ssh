
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Seats.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Seats.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PreReservedSeats" type="{http://webservices.sabre.com/pnrbuilder/v1_17}PreReservedSeats.PNRB" minOccurs="0"/>
 *         &lt;element name="SeatSpecialRequests" type="{http://webservices.sabre.com/pnrbuilder/v1_17}SeatRequests.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Seats.PNRB", propOrder = {
    "preReservedSeats",
    "seatSpecialRequests"
})
public class SeatsPNRB {

    @XmlElement(name = "PreReservedSeats")
    protected PreReservedSeatsPNRB preReservedSeats;
    @XmlElement(name = "SeatSpecialRequests")
    protected SeatRequestsPNRB seatSpecialRequests;

    /**
     * Gets the value of the preReservedSeats property.
     * 
     * @return
     *     possible object is
     *     {@link PreReservedSeatsPNRB }
     *     
     */
    public PreReservedSeatsPNRB getPreReservedSeats() {
        return preReservedSeats;
    }

    /**
     * Sets the value of the preReservedSeats property.
     * 
     * @param value
     *     allowed object is
     *     {@link PreReservedSeatsPNRB }
     *     
     */
    public void setPreReservedSeats(PreReservedSeatsPNRB value) {
        this.preReservedSeats = value;
    }

    /**
     * Gets the value of the seatSpecialRequests property.
     * 
     * @return
     *     possible object is
     *     {@link SeatRequestsPNRB }
     *     
     */
    public SeatRequestsPNRB getSeatSpecialRequests() {
        return seatSpecialRequests;
    }

    /**
     * Sets the value of the seatSpecialRequests property.
     * 
     * @param value
     *     allowed object is
     *     {@link SeatRequestsPNRB }
     *     
     */
    public void setSeatSpecialRequests(SeatRequestsPNRB value) {
        this.seatSpecialRequests = value;
    }

}
