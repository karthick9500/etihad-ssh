
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HeaderUpdateItem.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HeaderUpdateItem.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OpenSystemReferenceId" type="{http://webservices.sabre.com/pnrbuilder/v1_17}OpenSystemReferenceId.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeaderUpdateItem.PNRB", propOrder = {
    "openSystemReferenceId"
})
public class HeaderUpdateItemPNRB {

    @XmlElement(name = "OpenSystemReferenceId")
    protected String openSystemReferenceId;

    /**
     * Gets the value of the openSystemReferenceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpenSystemReferenceId() {
        return openSystemReferenceId;
    }

    /**
     * Sets the value of the openSystemReferenceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpenSystemReferenceId(String value) {
        this.openSystemReferenceId = value;
    }

}
