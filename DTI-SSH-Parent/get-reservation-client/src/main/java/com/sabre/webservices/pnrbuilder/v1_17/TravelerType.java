
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TravelerType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TravelerType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ADT"/>
 *     &lt;enumeration value="INF"/>
 *     &lt;enumeration value="CNN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TravelerType")
@XmlEnum
public enum TravelerType {

    ADT,
    INF,
    CNN;

    public String value() {
        return name();
    }

    public static TravelerType fromValue(String v) {
        return valueOf(v);
    }

}
