
package com.sabre.webservices.pnrbuilder.v1_17;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmailLines.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmailLines.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmailLine" type="{http://webservices.sabre.com/pnrbuilder/v1_17}EmailLine.PNRB" maxOccurs="3" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmailLines.PNRB", propOrder = {
    "emailLine"
})
public class EmailLinesPNRB {

    @XmlElement(name = "EmailLine")
    protected List<EmailLinePNRB> emailLine;

    /**
     * Gets the value of the emailLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the emailLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmailLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmailLinePNRB }
     * 
     * 
     */
    public List<EmailLinePNRB> getEmailLine() {
        if (emailLine == null) {
            emailLine = new ArrayList<EmailLinePNRB>();
        }
        return this.emailLine;
    }

}
