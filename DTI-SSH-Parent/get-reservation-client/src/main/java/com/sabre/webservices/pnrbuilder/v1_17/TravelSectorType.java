
package com.sabre.webservices.pnrbuilder.v1_17;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TravelSectorType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TravelSectorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Air"/>
 *     &lt;enumeration value="Car"/>
 *     &lt;enumeration value="Hotel"/>
 *     &lt;enumeration value="Insurance"/>
 *     &lt;enumeration value="Golf"/>
 *     &lt;enumeration value="Tour"/>
 *     &lt;enumeration value="Rail"/>
 *     &lt;enumeration value="Cruise"/>
 *     &lt;enumeration value="Excursion"/>
 *     &lt;enumeration value="Ferry"/>
 *     &lt;enumeration value="GroundTransport"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TravelSectorType")
@XmlEnum
public enum TravelSectorType {

    @XmlEnumValue("Air")
    AIR("Air"),
    @XmlEnumValue("Car")
    CAR("Car"),
    @XmlEnumValue("Hotel")
    HOTEL("Hotel"),
    @XmlEnumValue("Insurance")
    INSURANCE("Insurance"),
    @XmlEnumValue("Golf")
    GOLF("Golf"),
    @XmlEnumValue("Tour")
    TOUR("Tour"),
    @XmlEnumValue("Rail")
    RAIL("Rail"),
    @XmlEnumValue("Cruise")
    CRUISE("Cruise"),
    @XmlEnumValue("Excursion")
    EXCURSION("Excursion"),
    @XmlEnumValue("Ferry")
    FERRY("Ferry"),
    @XmlEnumValue("GroundTransport")
    GROUND_TRANSPORT("GroundTransport");
    private final String value;

    TravelSectorType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TravelSectorType fromValue(String v) {
        for (TravelSectorType c: TravelSectorType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
