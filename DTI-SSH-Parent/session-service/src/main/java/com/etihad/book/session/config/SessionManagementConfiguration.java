/**
 * 
 */
package com.etihad.book.session.config;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.session.handler.LoggingHandlerRefresh;
import com.etihad.book.session.handler.SecurityHandler;
import com.etihad.book.session.util.SessionConstants;

/**
 * @author Danish
 *
 */
@Configuration
public class SessionManagementConfiguration {
	
	@Autowired
	Config config;
	
	@Bean(name = "sessionCreateProxy")
    public JaxWsPortProxyFactoryBean createProxy() throws MalformedURLException {
        JaxWsPortProxyFactoryBean bean = new JaxWsPortProxyFactoryBean();
            bean.setServiceInterface(https.webservices_sabre_com.websvc.SessionCreatePortType.class);
            bean.setWsdlDocumentUrl(new URL(config.getProperty(SessionConstants.CREATE_SESSION_WSDLURL)));
            bean.setNamespaceUri(config.getProperty(SessionConstants.NAMESPACEURI));
            bean.setServiceName(config.getProperty(SessionConstants.CREATE_SESSION_SERVICE));
            bean.setPortName(config.getProperty(SessionConstants.CREATE_SESSION_PORT));
            bean.setEndpointAddress(config.getProperty(SessionConstants.ENDPOINT));
            bean.setLookupServiceOnStartup(false);
            bean.setHandlerResolver(handlerResolver()); 
        return bean;
    }
	
	 public HandlerResolver handlerResolver() {
		    return portInfo -> {
			    @SuppressWarnings("rawtypes")
				List<Handler> handlerChain = new ArrayList<>();
			    handlerChain.add(new SecurityHandler());
			    return handlerChain;
			};
		 }
	 
	 
	 @Bean(name = "refreshSessionProxy")
	    public JaxWsPortProxyFactoryBean refreshSessionProxy() throws MalformedURLException {
	        JaxWsPortProxyFactoryBean bean = new JaxWsPortProxyFactoryBean();
	            bean.setServiceInterface(https.webservices_sabre_com.websvc.OTAPingPortType.class);
	            bean.setWsdlDocumentUrl(new URL(config.getProperty(SessionConstants.REFRESH_SESSION_WSDLURL)));
	            bean.setNamespaceUri(config.getProperty(SessionConstants.NAMESPACEURI));
	            bean.setServiceName(config.getProperty(SessionConstants.REFRESH_SESSION_SERVICE));
	            bean.setPortName(config.getProperty(SessionConstants.REFRESH_SESSION_PORT));
	            bean.setEndpointAddress(config.getProperty(SessionConstants.ENDPOINT));
	            bean.setLookupServiceOnStartup(false);
	            bean.setHandlerResolver(handlerResolverForRefresh());
	        return bean;
	    }
	 
	 public HandlerResolver handlerResolverForRefresh() {
		    return portInfo -> {
			    @SuppressWarnings("rawtypes")
				List<Handler> handlerChain = new ArrayList<>();
			    handlerChain.add(new LoggingHandlerRefresh());
			    return handlerChain;
			};
		 }
	 
	 	@Bean(name = "closeSessionProxy")
	    public JaxWsPortProxyFactoryBean closeSessionProxy() throws MalformedURLException {
	        JaxWsPortProxyFactoryBean bean = new JaxWsPortProxyFactoryBean();
	            bean.setServiceInterface(https.webservices_sabre_com.websvc.SessionClosePortType.class);
	            bean.setWsdlDocumentUrl(new URL(config.getProperty(SessionConstants.CLOSE_SESSION_WSDLURL)));
	            bean.setNamespaceUri(config.getProperty(SessionConstants.NAMESPACEURI));
	            bean.setServiceName(config.getProperty(SessionConstants.CLOSE_SESSION_SERVICE));
	            bean.setPortName(config.getProperty(SessionConstants.CLOSE_SESSION_PORT));
	            bean.setEndpointAddress(config.getProperty(SessionConstants.ENDPOINT));
	            bean.setLookupServiceOnStartup(false);
	            bean.setHandlerResolver(handlerResolverForClose());
	        return bean;
	    }
	 
	 public HandlerResolver handlerResolverForClose() {
		    return portInfo -> {
			    @SuppressWarnings("rawtypes")
				List<Handler> handlerChain = new ArrayList<>();
			    handlerChain.add(new LoggingHandlerRefresh());
			    return handlerChain;
			};
		 }
}
