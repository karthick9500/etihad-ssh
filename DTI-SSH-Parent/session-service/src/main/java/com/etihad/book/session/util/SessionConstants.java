/**
 * 
 */
package com.etihad.book.session.util;

/**
 * @author Danish
 *
 */
public class SessionConstants {

	private SessionConstants() {
		
	}
	
	public static final String SECURITYNS = "http://schemas.xmlsoap.org/ws/2002/12/secext";
	public static final String SECURITYLOCALNAME = "Security";
	public static final String CONVERSATIONID = "sabre.conversationId";
	public static final String ACTION_CREATE = "sabre.action.create";
	public static final String CPAID = "sabre.cpaId";
	public static final String MESSAGEID = "sabre.messageId";
	public static final String FROMPARTY = "sabre.fromParty";
	public static final String TOPARTY = "sabre.toParty";
	public static final String DOMAIN = "sabre.domain";
	public static final String ORGANIZATION = "sabre.organization";
	public static final String SABRE_SECRET = "sabre.password";
	public static final String USERNAME = "sabre.username";
	public static final String ACTION_REFRESH = "sabre.action.refresh";
	public static final String ACTION_CLOSE = "sabre.action.close";
	public static final String CREATE_SESSION_WSDLURL = "sabre.create-session.wsdlURL";
	public static final String NAMESPACEURI = "sabre.namespaceURI";
	public static final String CREATE_SESSION_SERVICE = "sabre.create-session.service";
	public static final String CREATE_SESSION_PORT = "sabre.create-session.port";
	public static final String ENDPOINT = "sabre.endpoint";
	public static final String REFRESH_SESSION_WSDLURL = "sabre.refresh-session.wsdlURL";
	public static final String REFRESH_SESSION_SERVICE= "sabre.refresh-session.service";
	public static final String REFRESH_SESSION_PORT = "sabre.refresh-session.port";
	public static final String CLOSE_SESSION_WSDLURL = "sabre.close-session.wsdlURL";
	public static final String CLOSE_SESSION_SERVICE = "sabre.close-session.service";
	public static final String CLOSE_SESSION_PORT = "sabre.close-session.port";
	public static final String TIMESTAMP="YYYYMMddhhmmss";

}
