/**
 * 
 */
package com.etihad.book.session.controller;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.session.domain.CloseSessionRequest;
import com.etihad.book.session.domain.CloseSessionResponse;
import com.etihad.book.session.domain.CreateSessionResponse;
import com.etihad.book.session.domain.RefreshSessionRequest;
import com.etihad.book.session.domain.RefreshSessionResponse;
import com.etihad.book.session.service.impl.SessionServiceImpl;

import io.swagger.annotations.Api;

/**
 * @author Danish
 *
 */

@CrossOrigin
@RestController
@Api(value = "This API will handle session operations(Create/Refresh) from sabre sonic API. ")
@RequestMapping("/service/session")
public class SessionManagementController {
	
	protected Logger logger = Logger.getLogger(SessionManagementController.class.getName());

	@Autowired
	SessionServiceImpl sessionManagementService;

	/**
	 * This method will be used to map the request to a specific uri and return the response 
	 * @return
	 * @throws ApplicationException 
	 */
	@RequestMapping(path = "/create-session", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public CreateSessionResponse createSessionToken() throws ApplicationException  {
		return sessionManagementService.createSession();

	}
	
	/**
	 * This method will be used to map the request to a specific uri and return the response 
	 * @return
	 * @throws ApplicationException 
	 */
	@RequestMapping(path = "/refresh-session", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public RefreshSessionResponse refreshSessionToken(@RequestBody RefreshSessionRequest refreshSessionRequest) throws ApplicationException  {
		return sessionManagementService.refreshSession(refreshSessionRequest);
	}
	
	/**
	 * This method will be used to map the request to a specific uri and return the response 
	 * @return
	 * @throws ApplicationException 
	 */
	@RequestMapping(path = "/close-session", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public CloseSessionResponse closeSession(@RequestBody CloseSessionRequest closeSessionRequest) throws ApplicationException  {
		return sessionManagementService.closeSession(closeSessionRequest);
	}
	
}
		