/**
 * 
 */
package com.etihad.book.session.adaptor;

import java.util.List;

import org.opentravel.ota._2002._11.ObjectFactory;
import org.opentravel.ota._2002._11.SessionCloseRQ;
import org.opentravel.ota._2002._11.SessionCloseRS;
import org.opentravel.ota._2002._11.SessionCreateRQ;
import org.opentravel.ota._2002._11.SessionCreateRS;
import org.opentravel.ota._2003._05.OTAPingRQ;
import org.opentravel.ota._2003._05.OTAPingRS;
import org.springframework.stereotype.Component;

import com.etihad.book.session.domain.CloseSession;
import com.etihad.book.session.domain.CloseSessionResponse;
import com.etihad.book.session.domain.CreateSession;
import com.etihad.book.session.domain.CreateSessionResponse;
import com.etihad.book.session.domain.RefreshSession;
import com.etihad.book.session.domain.RefreshSessionResponse;
import com.etihad.book.session.util.TokenUtil;

/**
 * @author Danish
 *
 */
@Component
public class SessionManagementAdaptor {
	
	private static final ObjectFactory factory = new ObjectFactory();
	private static final org.opentravel.ota._2003._05.ObjectFactory refreshfactory = new org.opentravel.ota._2003._05.ObjectFactory();

	public SessionCreateRQ createSOAPRequestForCreateSession() {
		
		SessionCreateRQ requestBody = factory.createSessionCreateRQ();
		SessionCreateRQ.POS pos = new SessionCreateRQ.POS();
		SessionCreateRQ.POS.Source source = new SessionCreateRQ.POS.Source();
		source.setPseudoCityCode("EY");
		pos.setSource(source);
		requestBody.setPOS(pos);
		return requestBody;
	}

	public CreateSessionResponse createJSONResponseForCreateSession(SessionCreateRS soapResponse) {
		
		CreateSessionResponse response = new CreateSessionResponse();
		CreateSession createSessionResponse = new CreateSession();
		createSessionResponse.setConversationId(soapResponse.getConversationId());
		createSessionResponse.setBinarySecurityToken(TokenUtil.getToken());
		response.setCreateSession(createSessionResponse);
		return response;
	}

	public OTAPingRQ createSOAPRequestForRefreshSession() {
		
		OTAPingRQ requestBody = refreshfactory.createOTAPingRQ();
		requestBody.setEchoData("Are u there");
		requestBody.setVersion("1.0.0");
		return requestBody;
	}

	public RefreshSessionResponse createJSONResponseForRefreshSession(OTAPingRS soapResponse) {
		
		RefreshSessionResponse refreshSessionResponse= new RefreshSessionResponse();
		RefreshSession refreshSession =new RefreshSession();
		List<Object> refreshResults = soapResponse.getSuccessAndWarningsAndEchoData();
		for(Object refreshResult: refreshResults){
			if(refreshResult instanceof String){
				refreshSession.setRefreshData(refreshResult);
				break;
			}
		}
		refreshSessionResponse.setRefreshSession(refreshSession);
		return refreshSessionResponse;
	}

	public SessionCloseRQ createSOAPRequestForCloseSession() {
		
		SessionCloseRQ requestBody = factory.createSessionCloseRQ();
		SessionCloseRQ.POS pos = new SessionCloseRQ.POS();
		SessionCloseRQ.POS.Source source = new SessionCloseRQ.POS.Source();
		source.setPseudoCityCode("EY");
		pos.setSource(source);
		requestBody.setPOS(pos);
		return requestBody;
	}

	public CloseSessionResponse createJSONResponseForCloseSession(SessionCloseRS soapResponse) {
		
		CloseSessionResponse response = new CloseSessionResponse();
		CloseSession closeSession = new CloseSession();
		closeSession.setStatus(soapResponse.getStatus());
		response.setCloseSession(closeSession);
		return response;
	}

}
