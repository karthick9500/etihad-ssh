package com.etihad.book.session.domain;

import java.io.Serializable;

/**
 * @author Danish
 *
 */
public class CloseSession implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7760230720090701091L;
	private String status;

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
