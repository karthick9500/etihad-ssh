/**
 * 
 */
package com.etihad.book.session.domain;

import com.etihad.book.schema.common.GenericResponse;

/**
 * @author Danish
 *
 */
public class CloseSessionResponse extends GenericResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private CloseSession closeSession;

	/**
	 * @return the closeSession
	 */
	public CloseSession getCloseSession() {
		return closeSession;
	}

	/**
	 * @param closeSession the closeSession to set
	 */
	public void setCloseSession(CloseSession closeSession) {
		this.closeSession = closeSession;
	}
	
	

}
