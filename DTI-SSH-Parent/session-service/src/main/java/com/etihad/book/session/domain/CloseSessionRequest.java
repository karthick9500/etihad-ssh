/**
 * 
 */
package com.etihad.book.session.domain;

import com.etihad.book.schema.common.AbstractRequest;

/**
 * @author Danish
 *
 */
public class CloseSessionRequest extends AbstractRequest {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TokenHolder tokenHolder;
	
	public TokenHolder getTokenHolder() {
		return tokenHolder;
	}

	public void setTokenHolder(TokenHolder tokenHolder) {
		this.tokenHolder = tokenHolder;
	}

}
