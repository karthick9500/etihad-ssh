/**
 * 
 */
package com.etihad.book.session.service;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.session.domain.CloseSessionRequest;
import com.etihad.book.session.domain.CloseSessionResponse;
import com.etihad.book.session.domain.CreateSessionResponse;
import com.etihad.book.session.domain.RefreshSessionRequest;
import com.etihad.book.session.domain.RefreshSessionResponse;

/**
 * @author Danish
 *
 */
public interface ISesionService {
	
	public CreateSessionResponse createSession() throws ApplicationException ;

	public RefreshSessionResponse refreshSession(RefreshSessionRequest refreshSessionRequest) throws ApplicationException;

	public CloseSessionResponse closeSession(CloseSessionRequest closeSessionRequest) throws ApplicationException;

}
