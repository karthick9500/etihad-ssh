/**
 * 
 */
package com.etihad.book.session.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.client.RestTemplate;

import com.etihad.book.session.domain.CreateSessionResponse;
import com.etihad.book.session.domain.RefreshSessionRequest;
import com.etihad.book.session.domain.RefreshSessionResponse;
import com.etihad.book.session.domain.TokenHolder;

/**
 * @author Danish
 *
 */
public class TokenUtil {
	
	private TokenUtil() {
		
	}

	private static Map<String,String> map = new HashMap<>();

	public static void setToken(String str) {
		map.put("security", str);
	}

	public static String getToken() {
		return map.get("security");
	}
	
	private static String TOKEN = null;
	
	
	public static void refreshSessionSCHD()
	{
	    final String uri = "https://api.us.apiconnect.ibmcloud.com/selfservicehub-dev/ey-ssh/service/session/refresh-session/";
	    
	    
	    if(null == TOKEN) {
	    	TOKEN = createSessionSCHD();
	    }
	    
	    RefreshSessionRequest refreshReq = new RefreshSessionRequest();
		TokenHolder token = new TokenHolder();
		token.setSecurityToken(TOKEN);
		refreshReq.setTokenHolder(token);
	    
	    RestTemplate restTemplate = new RestTemplate();
	    restTemplate.postForObject( uri, refreshReq, RefreshSessionResponse.class);
	}
	
	private static String createSessionSCHD() {
		final String uri = "https://api.us.apiconnect.ibmcloud.com/selfservicehub-dev/ey-ssh/service/session/create-session/";
	    
	    RestTemplate restTemplate = new RestTemplate();
	    return restTemplate.getForObject(uri, CreateSessionResponse.class).getCreateSession().getBinarySecurityToken();
	}


}
