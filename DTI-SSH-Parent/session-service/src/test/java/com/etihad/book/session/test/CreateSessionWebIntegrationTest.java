/**
 * 
 */
package com.etihad.book.session.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.session.domain.CreateSessionResponse;

/**
 * @author Danish
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class CreateSessionWebIntegrationTest {

	@Test
	public void testCreateSessionApi() throws ApplicationException {
		
		TestRestTemplate restTemplate = new TestRestTemplate();
		
		ResponseEntity<CreateSessionResponse> response = restTemplate.getForEntity("http://localhost:8301/service/session/create-session", CreateSessionResponse.class);
		
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		
		



	}

}
