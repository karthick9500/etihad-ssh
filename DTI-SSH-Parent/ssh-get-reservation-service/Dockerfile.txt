FROM ibmjava:8-sfj
ENV LANG C.UTF-8
ADD target/get-reservation.jar  get-reservation.jar
EXPOSE 8303
ENTRYPOINT ["java", "-jar", "get-reservation.jar"]