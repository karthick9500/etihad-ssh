package com.etihad.book.getreservationdata.constants;

/**
 * 
 * @author CTS
 *
 */
public class Constants {
	
	private Constants() {
		
	}

	public static final int PNR_LENGTH = 6;
	public static final int TICKET_LENGTH = 13;
	public static final String PNR_REGEX = "[6][0][7]{1}\\d{10}";
	public static final String PNR_REGEX_ANOTHER = "[0][0][0]{1}\\d{10}";
	public static final String GET_RES_SSR_TYPE_WHEELCHAIR = "wheelchair";
	public static final String GET_RES_SSR_TYPE_MEAL = "meal";
	public static final String GET_RES_SSR_TYPE_BASDESCR = "bassinet";
	public static final String GET_RES_SSR_TYPE_MEAL_BBML = "BBML";
	public static final String GET_RES_SSR_TYPE_BAS = "BSCT";
	public static final String EMD_REGEX = "(\\d+)(\\s*(?i)kg.*)";
	public static final String BAGGAGE_ABC_GROUP="BG";
	public static final String EMD_PIECE_REGEX = "(\\w*)(\\s*(?i)checked.*)";
	public static final String PREMIUM_SEAT_CODE = "0B5";
	public static final String ADT = "ADT";
	public static final String INFANT_WITH_SEAT = "INS";
	public static final int DEFAULT_WT_KG_ADT = 23;
	public static final String PNR_NOT_FOUND = "PNR not found";
	public static final String SPCL_INFANT = "INFT";
	public static final String BUS_EQUIPMENT = "BUS";
	public static final String TRAIN_EQUIPMENT = "TRN";
	public static final String TRAIN = "TRAIN";


	
}
