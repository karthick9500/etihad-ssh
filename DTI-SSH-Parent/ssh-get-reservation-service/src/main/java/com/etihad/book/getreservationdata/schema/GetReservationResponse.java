/**
 * 
 */
package com.etihad.book.getreservationdata.schema;

import java.util.List;

import com.etihad.book.schema.common.GenericResponse;

/**
 * @author CTS
 *
 */
public class GetReservationResponse extends GenericResponse {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BookingDetails bookingDetails;
	
	private List<FlightDetails> flightDetails;
	
	private List<PassengerDetails2> paxDetails;

	/**
	 * @return the paxDetails
	 */
	public List<PassengerDetails2> getPaxDetails() {
		return paxDetails;
	}

	/**
	 * @param paxDetails the paxDetails to set
	 */
	public void setPaxDetails(List<PassengerDetails2> paxDetails) {
		this.paxDetails = paxDetails;
	}

	/**
	 * @return the bookingDetails
	 */
	public BookingDetails getBookingDetails() {
		return bookingDetails;
	}

	/**
	 * @param bookingDetails the bookingDetails to set
	 */
	public void setBookingDetails(BookingDetails bookingDetails) {
		this.bookingDetails = bookingDetails;
	}

	/**
	 * @return the flightDetails
	 */
	public List<FlightDetails> getFlightDetails() {
		return flightDetails;
	}

	/**
	 * @param flightDetails the flightDetails to set
	 */
	public void setFlightDetails(List<FlightDetails> flightDetails) {
		this.flightDetails = flightDetails;
	}

	
	
	

}
