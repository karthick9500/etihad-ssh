package com.etihad.book.getreservationdata.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.etihad.book.common.annotations.EnableAutoLog;
import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.getreservationdata.adaptor.GetReservationAdaptor;
import com.etihad.book.getreservationdata.adaptor.GetReservationFlightResponseAdaptor2;
import com.etihad.book.getreservationdata.adaptor.GetReservationResponseAdaptor;
import com.etihad.book.getreservationdata.broker.GetReservationBroker;
import com.etihad.book.getreservationdata.broker.GetReservationMockBroker;
import com.etihad.book.getreservationdata.constants.Constants;
import com.etihad.book.getreservationdata.schema.BaggageTierObj;
import com.etihad.book.getreservationdata.schema.CabinBaggageObj;
import com.etihad.book.getreservationdata.schema.GetReservationRequest;
import com.etihad.book.getreservationdata.schema.GetReservationResponse;
import com.etihad.book.getreservationdata.schema.ONDDataObj;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sabre.webservices.pnrbuilder.v1_17.ErrorsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ErrorsPNRB.Error;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRQ;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRS;

/**
 * @author CTS
 *
 */
@Service
public class GetReservationServiceImpl implements GetReservationService {

	@Autowired
	private GetReservationBroker getReservationBroker;

	@Autowired
	private GetReservationMockBroker getReservationMockBroker;

	@Autowired
	private GetReservationAdaptor getReservationAdaptor;

	@Autowired
	private GetReservationResponseAdaptor getReservationResponseAdaptor;

	@Autowired
	private GetReservationFlightResponseAdaptor2 getReservationFlightResponseAdaptor;

	@Autowired
	private Utils utils;

	@Autowired
	private Config config;

	public static final Map<String, Object> referenceDataMap = new HashMap<>();


	private static final String ENG_OND_CODE = "en-ae";
	private static final String ARA_OND_CODE = "ar-ae";

	@EnableAutoLog(logStackTrace=true)
	public GetReservationResponse getReservationData(GetReservationRequest request) throws ApplicationException {

		GetReservationResponse response = new GetReservationResponse();
		GetReservationRQ soapRequest;
		GetReservationRS soapResponse;
		List<String> errorCodes = validateForGetReservationData(request);
		if (!errorCodes.isEmpty()) {
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		soapRequest = getReservationAdaptor.createSOAPRequestForGetReservationData(request);
		try {
			soapResponse = getReservationBroker.processGetReservationData(soapRequest,
					request.getSearchParameters().getBinarySecurityToken());
		} catch (Exception e) {
			errorCodes.add("pnrretrieval.101");
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}

		if (null != soapResponse) {
			ErrorsPNRB errors = soapResponse.getErrors();
			if (null == errors) {
				response = getReservationFlightResponseAdaptor.processJSONResponseForGetReservationData(soapResponse,
						request.getSearchParameters().getLastName(), request.getSearchParameters().getLangCode());
			} 
			else if (null != errors.getError() && !errors.getError().isEmpty())
			{
				Error error = errors.getError().get(0);
				String errMsg = error.getMessage();
				if(Constants.PNR_NOT_FOUND.equals(errMsg))
				{
					errorCodes.add("pnrNotFound.101");
				}
				throw new ApplicationException(utils.populateResponseHeader(errorCodes));
			}
		}
		System.out.println("response.."+response);
		System.out.println("utils.."+utils);
		response.setMessages(utils.populateResponseHeader(errorCodes));
		return response;

	}

	/**
	 * This method validates the PNR enquiry request
	 * 
	 * @param request
	 * @return
	 */
	private List<String> validateForGetReservationData(GetReservationRequest request) {

		List<String> errorCodes = new ArrayList<>(5);
		if (null == request) {
			errorCodes.add("get-reservation.103");
		} else {
			if (null == request.getSearchParameters()) {
				errorCodes.add("get-reservation.104");
			} else {

				if (StringUtils.isEmpty(request.getSearchParameters().getPnr())) {
					errorCodes.add("get-reservation.105");
				}
			}
		}
		return errorCodes;
	}

	@PostConstruct
	public void loadStaticData()
	{
		String refFilePath = config.getProperty("refDatafilePath");
		String ssrfilePath = config.getProperty("ssrFilePath");
		String mealsFilePath = config.getProperty("mealsFilePath");
		String rbdClassFilePath = config.getProperty("rbdClassFilePath");
		String baggageFilePath = config.getProperty("baggageFilePath");
	    try 
	    {
	    	loadRefData(refFilePath);
	    	Map<String,ONDDataObj> engDataMap = getONDData(config.getProperty("refData.ond.url"));
        	Map<String,ONDDataObj> araDataMap = getONDData(config.getProperty("refData.ond.arabic.url"));
        	setAirportTimeZoneData(config.getProperty("airportZoneData.url"));
        	loadRefData(ssrfilePath);
        	loadRefData(mealsFilePath);
        	loadRefData(rbdClassFilePath);
        	loadRefData(baggageFilePath);
        	referenceDataMap.put(ENG_OND_CODE, engDataMap);
        	referenceDataMap.put(ARA_OND_CODE, araDataMap);
		} 
	    catch (Exception e) 
	    {
	    }
	}  

	private void loadRefData(String filePath)
	{
		String line = null;
		InputStream in = null;
	    OutputStream out = null;
	    BufferedReader br = null;
	    try {
	            in = getClass().getResourceAsStream(filePath); 
	            File file = File.createTempFile("tempfile", ".tmp");
	            out = new FileOutputStream(file);
	            int read;
	            byte[] bytes = new byte[1024];
	
	            while ((read = in.read(bytes)) != -1) {
	                out.write(bytes, 0, read);
	            }
	    	    br = new BufferedReader(new FileReader(file));
	    	    int length = 0;
	    	    CabinBaggageObj cabinObj = null;
	    	    BaggageTierObj baggageObj = null;
		    	while ((line = br.readLine()) != null) {
		    		line = line.replaceAll("\"", "");
		            String[] data = line.split(",");
		            length = data.length;
		            if(length ==5)
		            {
		            	cabinObj = new CabinBaggageObj();
		            	cabinObj.setCabinClass(data[1]);
		            	cabinObj.setBagNum(data[2]);
		            	cabinObj.setBagWeight(data[3]);
		            	cabinObj.setFoldableBag(data[4]);
		            	referenceDataMap.put(data[0],cabinObj);
		            }
		            else if(length ==4)
		            {
		            	baggageObj = new BaggageTierObj(); 
		            	baggageObj.setPieces(data[2]);
		            	baggageObj.setWeight(data[3]);
		            	referenceDataMap.put(data[0]+"-"+data[1],baggageObj);
		            }
		            else if(length ==3)
		            {
		            	referenceDataMap.put((data[2]+data[0]),data[1]);
		            }
		            else
		            {
		            	referenceDataMap.put(data[0], data[1]);
		            }
	    	    }
		    	file.deleteOnExit();
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    finally 
	    {
			if (in != null)
			{
				try 
				{
					in.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
			if (out != null) 
			{
				try 
				{
					out.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
			if (br != null) 
			{
				try 
				{
					br.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
	    }
	}
	
	private void setAirportTimeZoneData(String airportZoneDataUrl)
	{
		BufferedReader br = null;
		try {
			URL urlCSV = new URL(airportZoneDataUrl);
			URLConnection urlConn = urlCSV.openConnection();
			InputStreamReader inputCSV = new InputStreamReader(urlConn.getInputStream());
			br = new BufferedReader(inputCSV);
			String line;
			String[] values = null;
			while ((line = br.readLine()) != null) {
				line = line.replaceAll("\"", "");
				values = line.split(","); 
				referenceDataMap.put(values[4], values[11]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != br)
				{
					br.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    }
	
	private static Map<String,ONDDataObj> getONDData(String ondUrl) throws IOException {
	    	
    	Map<String,ONDDataObj> dataMap = new HashMap<>();
    	URL url;
    	HttpURLConnection request = null;
		try {
			url = new URL(ondUrl);
			request = (HttpURLConnection) url.openConnection();
			request.connect();
		} catch (Exception e) {
		}

        JsonParser jp = new JsonParser(); 
        if( null != request)
        {
	        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); 
	        JsonObject rootobj = root.getAsJsonObject();
	        JsonArray dataJ =	rootobj.getAsJsonArray("Data");
	        for (JsonElement jsonElement : dataJ) {
	        	JsonObject objectData = jsonElement.getAsJsonObject();
	        	ONDDataObj dataObject = new ONDDataObj();
	        	dataObject.setCode(objectData.get("Code").getAsString());
	        	dataObject.setName(objectData.get("Name").getAsString());
	        	dataObject.setCityName(objectData.get("CityName").getAsString());
	        	dataObject.setCityCode(objectData.get("CityCode").getAsString());
	        	dataObject.setCountryCode(objectData.get("CountryCode").getAsString());
	        	dataObject.setCountryName(objectData.get("CountryName").getAsString());
	        	dataObject.setAirportName(objectData.get("AirportName").getAsString());
				dataMap.put(dataObject.getCode(), dataObject);
	        }
        }
		return dataMap;
    }

}