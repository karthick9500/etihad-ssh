package com.etihad.book.getreservationdata.schema;

/**
 * This class is used to map Cabin baggage reference objects
 * 
 * @author CTS
 *
 */
public class BaggageTierObj { 

	private String pieces;
	private String weight;

	/**
	 * @return the pieces
	 */
	public String getPieces() {
		return pieces;
	}

	/**
	 * @param pieces the pieces to set
	 */
	public void setPieces(String pieces) {
		this.pieces = pieces;
	}

	/**
	 * @return the weight
	 */
	public String getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(String weight) {
		this.weight = weight;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BaggageTierObj [pieces=" + pieces + ", weight=" + weight + "]";
	}
	
		
}
