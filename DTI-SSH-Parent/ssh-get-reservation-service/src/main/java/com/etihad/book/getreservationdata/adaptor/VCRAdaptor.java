/**
 * 
 */
package com.etihad.book.getreservationdata.adaptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.etihad.book.getreservationdata.vcrschema.VCRDisplayResponse;
import com.etihad.book.getreservationdata.vcrschema.VCRTicketingInfo;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengersPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.TicketDetailsType;
import com.sabre.webservices.pnrbuilder.v1_17.TicketingInfoPNRB;

/**
 * @author 526837
 *
 */
public class VCRAdaptor {

	public static Map<String, VCRTicketingInfo> callVCRService(PassengerReservationPNRB passengerReservation,
			String vcrURL) {

		Map<String, VCRTicketingInfo> vcrResponseMap = new HashMap<>();
		PassengersPNRB passengers = passengerReservation.getPassengers();
		String adultPassengerTicketName = null;
		String infantPassengerTicketName = null;
		String childPassengerTicketName = null;
		String infantWithSeatPassengerTicketName = null;
		String adultTicketNumber = null;
		String infantTicketNumber = null;
		String infantWithSeatTicketNumber = null;
		String childTicketNumber = null;
		VCRTicketingInfo adultVCRResponse = null;
		VCRTicketingInfo infantVCRResponse = null;
		VCRTicketingInfo infantWithSeatVCRResponse = null;
		VCRTicketingInfo childVCRResponse = null;
		String adultWhenNoPaxTypeTicketName = null;
		String adultWhenNoPaxTypeTicketNumber = null;
		VCRTicketingInfo adultWhenNoPaxTypeVCRResponse = null;
		String infantWhenNoPaxTypeTicketName = null;
		String infantWhenNoPaxTypeTicketNumber = null;
		VCRTicketingInfo infantWhenNoPaxTypeVCRResponse = null;

		boolean isNopaxType = true;

		if (null != passengers) {
			List<PassengerPNRB> passengerList = passengers.getPassenger();
			if (null != passengerList && !passengerList.isEmpty()) {
				PassengerPNRB adultPassenger = filterPassengerOnPaxType(passengerList, "ADT");
				PassengerPNRB infantPassenger = filterPassengerOnPaxType(passengerList, "INF");
				PassengerPNRB childPassenger = filterPassengerOnPaxType(passengerList, "CHD");
				PassengerPNRB infantWithSeatPassenger = filterPassengerOnPaxType(passengerList, "INS");
				if (null != adultPassenger || null != infantPassenger || null != childPassenger
						|| null != infantWithSeatPassenger) {
					isNopaxType = false;
				}
				if (isNopaxType) {
					PassengerPNRB adultWhenNoPaxType = filterPassengerWhenNoPaxType(passengerList, "S");
					if (null != adultWhenNoPaxType) {
						adultWhenNoPaxTypeTicketName = getTicketName(adultWhenNoPaxType);
						adultWhenNoPaxTypeTicketNumber = getTicketDetails(adultWhenNoPaxTypeTicketName,
								passengerReservation);
						adultWhenNoPaxTypeVCRResponse = callVCR(adultWhenNoPaxTypeTicketNumber, vcrURL);
						vcrResponseMap.put("S", adultWhenNoPaxTypeVCRResponse);
					}
					PassengerPNRB infantWhenNoPaxType = filterPassengerWhenNoPaxType(passengerList, "I");
					if (null != infantWhenNoPaxType) {
						infantWhenNoPaxTypeTicketName = getTicketName(infantWhenNoPaxType);
						infantWhenNoPaxTypeTicketNumber = getTicketDetails(infantWhenNoPaxTypeTicketName,
								passengerReservation);
						infantWhenNoPaxTypeVCRResponse = callVCR(infantWhenNoPaxTypeTicketNumber, vcrURL);
						vcrResponseMap.put("I", infantWhenNoPaxTypeVCRResponse);
					}
				}

				if (null != adultPassenger) {
					adultPassengerTicketName = getTicketName(adultPassenger);
					adultTicketNumber = getTicketDetails(adultPassengerTicketName, passengerReservation);
					adultVCRResponse = callVCR(adultTicketNumber, vcrURL);
					vcrResponseMap.put("ADT", adultVCRResponse);
				}
				if (null != infantPassenger) {
					infantPassengerTicketName = getTicketName(infantPassenger);
					infantTicketNumber = getTicketDetails(infantPassengerTicketName, passengerReservation);
					infantVCRResponse = callVCR(infantTicketNumber, vcrURL);
					vcrResponseMap.put("INF", infantVCRResponse);
				}
				if (null != childPassenger) {
					childPassengerTicketName = getTicketName(childPassenger);
					childTicketNumber = getTicketDetails(childPassengerTicketName, passengerReservation);
					childVCRResponse = callVCR(childTicketNumber, vcrURL);
					vcrResponseMap.put("CHD", childVCRResponse);
				}
				if (null != infantWithSeatPassenger) {
					infantWithSeatPassengerTicketName = getTicketName(infantWithSeatPassenger);
					infantWithSeatTicketNumber = getTicketDetails(infantWithSeatPassengerTicketName,
							passengerReservation);
					infantWithSeatVCRResponse = callVCR(infantWithSeatTicketNumber, vcrURL);
					vcrResponseMap.put("INS", infantWithSeatVCRResponse);
				}
			}
		}
		return vcrResponseMap;

	}

	private static PassengerPNRB filterPassengerWhenNoPaxType(List<PassengerPNRB> passengerList, String nameType) {
		PassengerPNRB passenger = null;
		passenger = passengerList.stream().filter(psr -> psr.getNameType() != null)
				.filter(psr -> nameType.equals(psr.getNameType().name())).findFirst().orElse(null);
		return passenger;
	}

	private static String getTicketDetails(String ticketName, PassengerReservationPNRB passengerReservation) {

		String ticketNumber = "";
		TicketingInfoPNRB ticketingInfo = passengerReservation.getTicketingInfo();
		if (null != ticketingInfo) {
			List<TicketDetailsType> ticketDetailList = ticketingInfo.getTicketDetails();
			if (!ticketDetailList.isEmpty()) {
				TicketDetailsType adultTicketDetail = ticketDetailList.stream()
						.filter(ticket -> ticketName.equals(ticket.getPassengerName())
								&& !ticket.getOriginalTicketDetails().contains("-"))
						.findAny().orElse(null);
				if (null != adultTicketDetail) {
					ticketNumber = adultTicketDetail.getTicketNumber();
				}
			}
		}
		return ticketNumber;
	}

	private static String getTicketName(PassengerPNRB passenger) {
		String ticketName = "";
		ticketName = getLastName(passenger.getLastName()) + "/" + passenger.getFirstName().substring(0, 1);
		return ticketName;
	}

	private static String getLastName(String lastName) {
		String name = "";
		name = lastName.length() > 5 ? lastName.substring(0, 5) : lastName;
		return name;
	}

	private static PassengerPNRB filterPassengerOnPaxType(List<PassengerPNRB> passengerList, String paxType) {
		PassengerPNRB passenger = null;
		passenger = passengerList.stream().filter(psr -> paxType.equals(psr.getReferenceNumber())).findFirst()
				.orElse(null);
		return passenger;
	}

	private static VCRTicketingInfo callVCR(String ticketNumber, String vcrURL) {
		RestTemplate restTemplate = new RestTemplate();
		VCRTicketingInfo vcrTiketInfo = null;
		ResponseEntity<VCRDisplayResponse> response = null;
		VCRDisplayResponse vcrResponse = null;

		String url = vcrURL + ticketNumber;
		try {
			response = restTemplate.getForEntity(url, VCRDisplayResponse.class);
		} catch (Exception e) {
			return vcrTiketInfo;
		}
		if (null != response) {
			vcrResponse = response.getBody();
		}
		if (null != vcrResponse) {
			vcrTiketInfo = vcrResponse.getVcrTicketingInfo();
		}
		return vcrTiketInfo;
	}

}
