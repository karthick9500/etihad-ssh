package com.etihad.book.getreservationdata.adaptor;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.etihad.book.getreservationdata.constants.BAGGAGE_LOYALTY;
import com.etihad.book.getreservationdata.constants.BAGGAGE_PIECE;
import com.etihad.book.getreservationdata.constants.Constants;
import com.etihad.book.getreservationdata.schema.BaggageDetails;
import com.etihad.book.getreservationdata.schema.BaggageTierObj;
import com.etihad.book.getreservationdata.schema.CabinBaggageDetails;
import com.etihad.book.getreservationdata.schema.CabinBaggageObj;
import com.etihad.book.getreservationdata.schema.CheckinBaggage;
import com.etihad.book.getreservationdata.schema.CheckinBaggageDetails;
import com.etihad.book.getreservationdata.schema.ExtraBaggage;
import com.etihad.book.getreservationdata.schema.FlightDetails;
import com.etihad.book.getreservationdata.service.GetReservationServiceImpl;
import com.etihad.book.getreservationdata.vcrschema.CouponInfo;
import com.etihad.book.getreservationdata.vcrschema.FlightSegementInfo;
import com.etihad.book.getreservationdata.vcrschema.VCRTicketingInfo;
import com.sabre.webservices.pnrbuilder.v1_17.AirType.AncillaryServices.AncillaryService;
import com.sabre.webservices.pnrbuilder.v1_17.AncillaryServicesPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB.AncillaryServices;

public class BaggageAdaptor {
	public static void mapCabinBaggage(String classOfService, final boolean isEYSector,
			final BaggageDetails baggage) {
		if (isEYSector) {
			CabinBaggageDetails cabinBgDetails = getCabinBgDetails(classOfService);
			baggage.setCabinBaggage(cabinBgDetails);
		}
	}

	public static CabinBaggageDetails getCabinBgDetails(String serviceClassCode) {
		CabinBaggageDetails cabinDetails = new CabinBaggageDetails();
		Object cabin = GetReservationServiceImpl.referenceDataMap.get(serviceClassCode);
		if (cabin instanceof CabinBaggageObj) {
			CabinBaggageObj baggageObj = (CabinBaggageObj) cabin;
			if (null != baggageObj) {
				cabinDetails.setUnit(baggageObj.getBagNum());
				cabinDetails.setAdditionalBaggage(baggageObj.getFoldableBag());
				cabinDetails.setWeightDescr(baggageObj.getBagWeight());
			}
		}
		return cabinDetails;
	}

	/**
	 * Getting extra baggage from ancillaries
	 * 
	 * @param flightAncillaryServices
	 * @param passenger
	 * @return List<ExtraBaggage>
	 */
	public static List<ExtraBaggage> getMappedExtraBaggage(List<AncillaryService> flightAncillaryServices,
			PassengerPNRB passenger, List<ExtraBaggage> mappedExtraBaggage) {
		final AncillaryServices passengerAncillary = passenger.getAncillaryServices();
		if (passengerAncillary != null && flightAncillaryServices != null) {
			final List<AncillaryServicesPNRB> passengerAncillaryServices = passengerAncillary.getAncillaryService();
			for (AncillaryServicesPNRB pnrb : passengerAncillaryServices) {
				final String ancillaryId = pnrb.getId();
				if (StringUtils.isNotBlank(ancillaryId) && Constants.BAGGAGE_ABC_GROUP.equals(pnrb.getGroupCode())) {
					final boolean filterByAncId = filterByAncId(flightAncillaryServices, ancillaryId);
					if (filterByAncId) {
						final String commercialName = pnrb.getCommercialName();
						if (StringUtils.isNotBlank(commercialName)) {
							Matcher matcher = Pattern.compile(Constants.EMD_REGEX).matcher(commercialName);
							Matcher matcherPiece = Pattern.compile(Constants.EMD_PIECE_REGEX).matcher(commercialName);

							String unitKg = StringUtils.EMPTY;
							while (matcher.find()) {
								unitKg = matcher.group(1);
							}
							ExtraBaggage extraBg = new ExtraBaggage();
							extraBg.setCode(pnrb.getRficSubcode());
							extraBg.setDescription(unitKg);
							extraBg.setRawDescription(commercialName);
							if (matcherPiece.matches()) {
								if (matcherPiece.group(2) != null) {
									extraBg.setPiece(true);
									String matchingUnitPiece = matcherPiece.group(1);
									String unitPiece = matchingUnitPiece != null
											? BAGGAGE_PIECE.getValfromString(matchingUnitPiece)
											: StringUtils.EMPTY;
									extraBg.setUnitPiece(unitPiece);
								}
							}
							mappedExtraBaggage.add(extraBg);
						}
					}
				}

			}

		}
		return mappedExtraBaggage;

	}

	/**
	 * Predicate to test if flight ancilary is present in passenger ancilary
	 * -baggage list
	 * 
	 * @param flightSSR
	 * @param passengerSSR
	 * @return boolean
	 */
	private static boolean filterByAncId(List<AncillaryService> flightAnc, String ancId) {
		for (AncillaryService service : flightAnc) {
			if (ancId.equals(service.getRef())) {
				return true;
			}
		}
		return false;
	}

	public static CheckinBaggage getCheckinBaggageWeight(String loyaltyCode, String loyaltyDescr) {
		CheckinBaggage baggage = new CheckinBaggage();
		final int weightInKg = BAGGAGE_LOYALTY.getValfromString(loyaltyCode);
		baggage.setLoyalty(loyaltyDescr);
		baggage.setPiece(false);
		baggage.setWeightInKgs(weightInKg);
		return baggage;

	}

	public static CheckinBaggage getCheckinBaggagePiece(String loyaltyCode, String loyaltyDescr,
			String classOfService) {
		CheckinBaggage baggage = new CheckinBaggage();
		baggage.setPiece(true);
		final StringBuilder builder = new StringBuilder();
		BaggageTierObj obj = null;
		builder.append(classOfService).append("-").append(loyaltyCode);
		Object tierObj = GetReservationServiceImpl.referenceDataMap.get(builder.toString());
		if (tierObj instanceof BaggageTierObj) {
			obj = (BaggageTierObj) tierObj;
		}
		if (null != obj) {
			int weight = Integer.parseInt(obj.getWeight().replaceAll("[^0-9]", ""));
			int pieces = Integer.parseInt(obj.getPieces().replaceAll("[^0-9]", ""));
			baggage.setLoyalty(loyaltyDescr);
			baggage.setUnitPiece(pieces);
			baggage.setLoyalty(loyaltyDescr);
			baggage.setWeightInKgs(weight);
		}

		return baggage;

	}
	public static void setAddOrEditBaggage(final boolean isPurchaseExtraBaggage, CheckinBaggageDetails details) {
			
		details.setAddOrEditBaggage(isPurchaseExtraBaggage);
		
	}
	public static String getBaggageAllowance(final VCRTicketingInfo vcrTicketingInfo, final FlightDetails flightdetails) {
		final List<CouponInfo> couponList = vcrTicketingInfo.getCoupon();
		if(couponList!=null)
		{
			for(CouponInfo couponInfo:couponList)
			{
				final FlightSegementInfo flightSegement = couponInfo.getFlightSegement();
				if(flightSegement!=null)
				{
					final String flightNumber = flightSegement.getFlightNumber();
					final String destinationLocationCode = flightSegement.getDestinationLocationCode();
					final String originLocationCode = flightSegement.getOriginLocationCode();
					if(flightNumber!=null && destinationLocationCode!=null && originLocationCode!=null)
					{
						 if(flightNumber.equals(flightdetails.getFlightNumber()) && destinationLocationCode.equals(flightdetails.getDestinationAirportCode()) && originLocationCode.equals(flightdetails.getOriginAirportCode())) 
						 {
							 return flightSegement.getBaggageAllowance();
						 }
					}
				}
			}
		}
		return StringUtils.EMPTY;
	}
}
