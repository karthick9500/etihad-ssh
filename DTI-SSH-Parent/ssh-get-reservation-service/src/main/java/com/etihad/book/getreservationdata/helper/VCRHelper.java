/**
 * 
 */
package com.etihad.book.getreservationdata.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.etihad.book.getreservationdata.vcrschema.CouponInfo;
import com.etihad.book.getreservationdata.vcrschema.PaymentCard;
import com.etihad.book.getreservationdata.vcrschema.TicketingResInfo;
import com.etihad.book.getreservationdata.vcrschema.VCRTicketingInfo;

/**
 * @author 526837
 *
 */
public class VCRHelper {

	public static boolean getPCCDetails(Map<String, VCRTicketingInfo> vcrResponseMap) {

		boolean isPCC = false;
		if (!vcrResponseMap.isEmpty()) {
			Map.Entry<String, VCRTicketingInfo> vcrEntry = vcrResponseMap.entrySet().iterator().next();
			VCRTicketingInfo vcrTicketingInfo = vcrEntry.getValue();
			TicketingResInfo ticketing = null;
			if (null != vcrTicketingInfo) {
				ticketing = vcrTicketingInfo.getTicketing();
			}
			if (null != ticketing) {
				PaymentCard paymentCard = ticketing.getPaymentCard();
				if (null != paymentCard) {
					isPCC = Boolean.parseBoolean(paymentCard.getPresentCC_Ind());
				}
			}
		}
		return isPCC;
	}

	public static String getSegmentStatus(Map<String, VCRTicketingInfo> vcrResponseMap, String arrivalAirport,
			String departureAirport, String flightNumber) {

		String status = null;
		List<CouponInfo> coupon = new ArrayList<>();
		if (!vcrResponseMap.isEmpty()) {
			Map.Entry<String, VCRTicketingInfo> vcrEntry = vcrResponseMap.entrySet().iterator().next();
			VCRTicketingInfo vcrTicketingInfo = vcrEntry.getValue();
			if (null != vcrTicketingInfo) {
				coupon = vcrTicketingInfo.getCoupon();
			}
			CouponInfo couponInfo = coupon.stream().filter(
					flightSegment -> departureAirport.equals(flightSegment.getFlightSegement().getOriginLocationCode())
							&& arrivalAirport.equals(flightSegment.getFlightSegement().getDestinationLocationCode()))
					.findAny().orElse(null);
			if (null != couponInfo) {
				status = couponInfo.getStatusCode();
			}

		}
		return status;
	}

}
