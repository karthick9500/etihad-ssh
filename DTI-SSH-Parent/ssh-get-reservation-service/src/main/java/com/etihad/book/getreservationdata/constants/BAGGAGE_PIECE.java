package com.etihad.book.getreservationdata.constants;

import org.apache.commons.lang.StringUtils;

public enum BAGGAGE_PIECE {
	FIRST("1"), SECOND("2"), THIRD("3"), FOURTH("4"), FIFTH("5"), SIXTH("6"), SEVENTH("7"), EIGHT("8"), NINETH("9"), TENTH("10");
	private String unitValueNumber;

	private BAGGAGE_PIECE(String unitValueNumber) {
		this.unitValueNumber = unitValueNumber;
	}

	public String getUnitValueNumber() {
		return unitValueNumber;
	}

	public void setUnitValueNumber(String unitValueNumber) {
		this.unitValueNumber = unitValueNumber;
	}
	public static String getValfromString(String text) {
	    for (BAGGAGE_PIECE b : BAGGAGE_PIECE.values()) {
	      if (b.name().equalsIgnoreCase(text)) {
	        return b.unitValueNumber;
	      }
	    }
	    return StringUtils.EMPTY;
	  }

}
