package com.etihad.book.getreservationdata.schema;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Checkin Baggage details
 *
 */
public class CheckinBaggageDetails {

	private List<ExtraBaggage> extraBaggageDetails;
	private List<CheckinBaggage> checkinBaggageDetails;
	private boolean isAddOrEditBaggage;

	/**
	 * Checkin baggage details
	 * @return CheckinBaggage
	 */
	public List<CheckinBaggage> getCheckinBaggageDetails() {
		return checkinBaggageDetails;
	}

	public void setCheckinBaggageDetails(List<CheckinBaggage> checkinBaggageDetails) {
		this.checkinBaggageDetails = checkinBaggageDetails;
	}
	/**
	 * Extra baggage details
	 * @return  List<ExtraBaggage>
	 */
	public List<ExtraBaggage> getExtraBaggageDetails() {
		return extraBaggageDetails;
	}

	public void setExtraBaggageDetails(List<ExtraBaggage> extraBaggageDetails) {
		this.extraBaggageDetails = extraBaggageDetails;
	}

	/**
	 * Flag to add or edit baggage
	 * @return boolean isAddOrEditBaggage
	 */
	@JsonProperty("isAddOrEditBaggage")
	public boolean isAddOrEditBaggage() {
		return isAddOrEditBaggage;
	}

	public void setAddOrEditBaggage(boolean isAddOrEditBaggage) {
		this.isAddOrEditBaggage = isAddOrEditBaggage;
	}

}
