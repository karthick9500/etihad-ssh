/**
 * 
 */
package com.etihad.book.getreservationdata.helper;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import com.etihad.book.getreservationdata.service.GetReservationServiceImpl;

/**
 *
 */
public class LoyaltyHelper {
	
	public static String getLoyaltyTier(Integer tierLevelNumber, String langCode) {
	  String descr=null;
		final StringBuilder builder = new StringBuilder();
		String[] langCodeArr = langCode.contains("-") ? langCode.split("-") : ArrayUtils.EMPTY_STRING_ARRAY;
		langCode = ArrayUtils.isNotEmpty(langCodeArr)
				? builder.append(langCodeArr[0]).append("_").append((langCodeArr[1]).toUpperCase()).toString()
				: langCode;
		String tierLevelNumberLang = langCode + tierLevelNumber;
		Object tierDescr = GetReservationServiceImpl.referenceDataMap.get(tierLevelNumberLang);
		if (tierDescr instanceof String) {
			descr = (String) tierDescr;
		}
		return descr;
	}

}
