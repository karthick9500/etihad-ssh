/**
 * 
 */
package com.etihad.book.getreservationdata.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.etihad.book.common.util.Utils;
import com.etihad.book.getreservationdata.adaptor.GetReservationFlightResponseAdaptor2;
import com.etihad.book.getreservationdata.constants.Constants;
import com.etihad.book.getreservationdata.exception.NameNotFoundException;
import com.etihad.book.getreservationdata.schema.SpecialRequest;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ReservationPNRB;

/**
 * @author 526837
 *
 */
public class PrimaryPaxHelper {

	@Autowired
	private Utils utils;

	public static PassengerPNRB getPrimaryPassengerDetails(ReservationPNRB reservation, String lastName)
			throws NameNotFoundException {

		List<PassengerPNRB> passengerList = reservation.getPassengerReservation().getPassengers().getPassenger();
		HashSet<String> paxSet = new HashSet<>();
		PassengerPNRB pax = new PassengerPNRB();
		List<String> paxlists = new ArrayList<>();
		Set<String> infantsAssociatedNames = new HashSet<>();
		final Map<PassengerPNRB, Set<String>> infantsAssocMap = new HashMap<>();

		for (PassengerPNRB passengerPNRB : passengerList) {
			infantsAssociatedNames = new HashSet<>();
			if (lastName.equals(passengerPNRB.getLastName())) {
				paxlists.add(passengerPNRB.getLastName());
			}
			List<SpecialRequest> ssrPassenger = new ArrayList<>();
			GetReservationFlightResponseAdaptor2.populateGenericSpecialRequest(
					passengerPNRB.getSpecialRequests().getGenericSpecialRequest(), ssrPassenger, infantsAssociatedNames,
					false);
			if (!CollectionUtils.isEmpty(infantsAssociatedNames)) {
				infantsAssocMap.put(passengerPNRB, infantsAssociatedNames);
			}
		}

		if (paxlists.size() == 0) {
			throw new NameNotFoundException("Name not Found");
		}

		if (passengerList.size() == 1 && lastName.equals(passengerList.get(0).getLastName())) {
			pax = passengerList.get(0);
		} else {
			for (PassengerPNRB passengerPNRB : passengerList) {
				paxSet.add(passengerPNRB.getLastName());
			}
			if (paxSet.size() == 1) {
				pax = passengerList.get(0);
			} else {
				for (PassengerPNRB passenger : passengerList) {
					if (!StringUtils.isEmpty(passenger.getReferenceNumber())) {
						if (lastName.equals(passenger.getLastName())
								&& Constants.ADT.equals(passenger.getReferenceNumber())) {
							pax = passenger;
							break;
						} else {
							final boolean isInfant = passenger.getReferenceNumber().equals("INF")
									|| passenger.getReferenceNumber().equals("INS");
							if (lastName.equals(passenger.getLastName())
									&& (isInfant || passenger.getReferenceNumber().equals("CHD"))) {
								pax = getPrimaryPaxWhenInfant(passengerList, lastName);
								if (null != pax) {
									return pax;
								} else {
									// if infant find associated adults else
									// return first adult
									if (isInfant) {
										final StringBuilder builder = new StringBuilder();

										final String surname = passenger.getLastName();
										if (StringUtils.isNotBlank(surname)) {
											builder.append(surname);
										}
										final String forename = passenger.getFirstName();
										if (StringUtils.isNotBlank(forename)) {
											builder.append("/");
											builder.append(forename);
										}
										Optional<PassengerPNRB> nameAssocId = infantsAssocMap.entrySet().stream()
												.filter(entry -> entry.getValue().contains(builder.toString()))
												.map(Map.Entry::getKey).findFirst();

										if (nameAssocId.isPresent()) {
											return nameAssocId.get();
										} else {
											return getPaxByType(Constants.ADT, passengerList);
										}
									} else {
										// else return first adult
										return getPaxByType(Constants.ADT, passengerList);
									}
								}
							}
						}
					} else {
						if (null != passenger.getNameType()) {
							if (lastName.equals(passenger.getLastName())
									&& "I".equals(passenger.getNameType().name())) {
								pax = getPrimaryPaxwhenInfantAndNoPaxType(passengerList, lastName);
								break;
							} else {
								if (lastName.equals(passenger.getLastName())
										&& "S".equals(passenger.getNameType().name())) {
									pax = getPrimaryPaxwhenAdultAndNoPaxType(passengerList, lastName);
								}
							}
						} else {
							pax = passengerList.get(0);
						}
					}
				}
			}
		}
		return pax;
	}

	private static PassengerPNRB getPrimaryPaxwhenAdultAndNoPaxType(List<PassengerPNRB> passengerList,
			String lastName) {
		PassengerPNRB pax = null;
		for (PassengerPNRB passenger : passengerList) {
			if (lastName.equals(passenger.getLastName()) && "S".equals(passenger.getNameType().name())) {
				pax = passenger;
				break;
			}
		}
		return pax;
	}

	private static PassengerPNRB getPrimaryPaxwhenInfantAndNoPaxType(List<PassengerPNRB> passengerList,
			String lastName) {
		PassengerPNRB pax = null;
		for (PassengerPNRB passenger : passengerList) {
			if (lastName.equals(passenger.getLastName()) && "S".equals(passenger.getNameType())) {
				pax = passenger;
				break;
			} else {
				pax = passengerList.get(0);
			}
		}
		return pax;
	}

	private static PassengerPNRB getPrimaryPaxWhenInfant(List<PassengerPNRB> passengerList, String lastName) {

		PassengerPNRB pax = null;
		for (PassengerPNRB passenger : passengerList) {
			if (Constants.ADT.equals(passenger.getReferenceNumber()) && lastName.equals(passenger.getLastName())) {
				pax = passenger;
				break;
			}
		}
		return pax;
	}

	private static PassengerPNRB getPaxByType(String paxType, List<PassengerPNRB> passengerList) {
		Optional<PassengerPNRB> findFirst = passengerList.stream()
				.filter(psr -> paxType.equals(psr.getReferenceNumber())).findFirst();

		if (findFirst.isPresent()) {
			return findFirst.get();
		} else {
			return passengerList.get(0);
		}

	}

}
