/**
 * 
 */
package com.etihad.book.getreservationdata.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.plexus.util.StringUtils;

import com.etihad.book.getreservationdata.constants.Constants;
import com.etihad.book.getreservationdata.schema.CabinBaggageDetails;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ReservationPNRB;

/**
 * This class contains passenger data helper methods and static map data
 * TBD-move all master data to one source
 *
 */
public class PassengerHelper {

	private static final Map<String, String> mapTitle = new HashMap<>();

	static {
		mapTitle.put("MR", "Mr.");
		mapTitle.put("MRS", "Mrs.");
		mapTitle.put("MS", "Ms.");
		mapTitle.put("MISS", "Miss");
		mapTitle.put("HE", "HE");
		mapTitle.put("HH", "HH");
		mapTitle.put("HRH", "HRH");
		mapTitle.put("DR", "Dr");
		mapTitle.put("PROF", "Prof");
		mapTitle.put("SHEK", "Sheikh");
		mapTitle.put("SHKA", "Shaikha");
		mapTitle.put("MSTR", "Master");
	}

	public static String getPassengerTitle(String primaryPaxFirstName) {

		String title = null;
		if (!StringUtils.isEmpty(primaryPaxFirstName)) {
			for (String str : mapTitle.keySet()) {
				String firstNameUpper = primaryPaxFirstName.toUpperCase();
				if (firstNameUpper.contains(str)) {
					int startIndex = firstNameUpper.indexOf(str);
					title = firstNameUpper.substring(startIndex, firstNameUpper.length());
					break;
				}
			}
		}
		return mapTitle.get(title);
	}

	public static String getPassengerFirstName(String firstName) {
		String name = null;
		if (!StringUtils.isEmpty(firstName)) {
			 name = firstName.toUpperCase();
			for (String str : mapTitle.keySet()) {
				if (name.contains(str)) {
					int startIndex = name.indexOf(str);
					name = name.substring(0, startIndex - 1);
					break;
				}
			}
		}
		return name;
	}

	public static PassengerPNRB getPrimaryPassengerDetails(ReservationPNRB reservation, String lastName) {

		List<PassengerPNRB> passengerList = reservation.getPassengerReservation().getPassengers().getPassenger();

		PassengerPNRB pax = new PassengerPNRB();
		for (PassengerPNRB passenger : passengerList) {
			if (!StringUtils.isEmpty(passenger.getReferenceNumber())) {
				if (lastName.equals(passenger.getLastName()) && Constants.ADT.equals(passenger.getReferenceNumber())) {
					pax = passenger;
					break;
				} else if (lastName.equals(passenger.getLastName()) && (passenger.getReferenceNumber().equals("INF")
						|| passenger.getReferenceNumber().equals("INS"))) {
					pax = getPrimaryPaxWhenInfant(passengerList);
					break;
				} else if (lastName.equals(passenger.getLastName()) && passenger.getReferenceNumber().equals("CHD")) {
					pax = getPrimaryPaxWhenChild(passengerList);
					break;
				}
			}
		}
		return pax;
	}

	private static PassengerPNRB getPrimaryPaxWhenChild(List<PassengerPNRB> passengerList) {
		PassengerPNRB pax = null;
		for (PassengerPNRB passenger : passengerList) {
			if (Constants.ADT.equals(passenger.getReferenceNumber())) {
				pax = passenger;
				break;
			} else {
				pax = passengerList.get(0);
			}
		}
		return pax;
	}

	private static PassengerPNRB getPrimaryPaxWhenInfant(List<PassengerPNRB> passengerList) {

		PassengerPNRB pax = null;
		for (PassengerPNRB passenger : passengerList) {
			if (passenger.isWithInfant().equals(true)) {
				pax = passenger;
				break;
			}
		}
		return pax;
	}

}
