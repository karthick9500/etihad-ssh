/**
 * 
 */
package com.etihad.book.getreservationdata.schema;

import com.etihad.book.schema.common.AbstractRequest;

/**
 * @author CTS
 *
 */
public class GetReservationRequest extends AbstractRequest {
	
	private SearchParameters searchParameters;
	
	public SearchParameters getSearchParameters() {
		return searchParameters;
	}
	public void setSearchParameters(SearchParameters searchParameters) {
		this.searchParameters = searchParameters;
	}
}
