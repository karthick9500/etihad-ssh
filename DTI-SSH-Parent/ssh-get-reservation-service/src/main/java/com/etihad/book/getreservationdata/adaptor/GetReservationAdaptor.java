/**
 * 
 */
package com.etihad.book.getreservationdata.adaptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.getreservationdata.schema.BookingDetails;
import com.etihad.book.getreservationdata.schema.FlightDetails;
import com.etihad.book.getreservationdata.schema.GetReservationRequest;
import com.etihad.book.getreservationdata.schema.GetReservationResponse;
import com.etihad.book.getreservationdata.schema.PassengerDetails;
import com.sabre.services.res.or.v1_11.OpenReservationElementType;
import com.sabre.services.res.or.v1_11.SegmentAssociation;
import com.sabre.services.res.or.v1_11.SegmentAssociationTag;
import com.sabre.webservices.pnrbuilder.v1_17.APISRequestPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.AirType.MarriageGrp;
import com.sabre.webservices.pnrbuilder.v1_17.BookingDetailsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.BookingDetailsPNRB.FlightsRange;
import com.sabre.webservices.pnrbuilder.v1_17.DOCSEntryPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.FrequentFlyerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.GenderDOCSEntryPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.GenericSpecialRequestPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRQ;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRSType;
import com.sabre.webservices.pnrbuilder.v1_17.NameAssociatedSpecialRequestsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ObjectFactory;
import com.sabre.webservices.pnrbuilder.v1_17.OpenReservationElementsType;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengersPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PreReservedSeatPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PreReservedSeatsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SeatsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Air;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment;
import com.sabre.webservices.pnrbuilder.v1_17.SpecialMealRequestPNRB;

/**
 * @author CTS
 *
 */
@Component
public class GetReservationAdaptor {

	@Autowired
	private Config config;

	@Autowired
	private Utils utils;

	Map<String, PassengerDetails> mapSeatWithPassenger = new HashMap<>();
	List<PassengerDetails> passengerDetailsList = new ArrayList<>();
	List<FlightDetails> flightDetailsList = new ArrayList<>();
	PassengerDetails paxDetails;
	FlightDetails flightDetails;
	GetReservationResponse response = new GetReservationResponse();
	BookingDetails bookingData = new BookingDetails();

	public GetReservationRQ createSOAPRequestForGetReservationData(GetReservationRequest request)
			throws ApplicationException {

		final ObjectFactory factory = new ObjectFactory();
		GetReservationRQ soapRequest = factory.createGetReservationRQ();
		soapRequest.setVersion("1.17.0");
		soapRequest.setLocator(request.getSearchParameters().getPnr());
		return soapRequest;
	}

}
