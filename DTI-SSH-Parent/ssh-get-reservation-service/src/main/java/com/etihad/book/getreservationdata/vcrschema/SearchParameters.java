/**
 * 
 */
package com.etihad.book.getreservationdata.vcrschema;

import com.etihad.book.schema.common.AbstractRequest;

/**
 * Search Parameters class
 *
 */
public class SearchParameters extends AbstractRequest {

	private static final long serialVersionUID = 1L;
	
	private String eTicketNumber;
	private String binarySecurityToken;
	private String langCode;
	/**
	 * @return the eTicketNumber
	 */
	public String geteTicketNumber() {
		return eTicketNumber;
	}
	/**
	 * @param eTicketNumber the eTicketNumber to set
	 */
	public void seteTicketNumber(String eTicketNumber) {
		this.eTicketNumber = eTicketNumber;
	}
	/**
	 * @return the binarySecurityToken
	 */
	public String getBinarySecurityToken() {
		return binarySecurityToken;
	}
	/**
	 * @param binarySecurityToken the binarySecurityToken to set
	 */
	public void setBinarySecurityToken(String binarySecurityToken) {
		this.binarySecurityToken = binarySecurityToken;
	}
	/**
	 * @return the langCode
	 */
	public String getLangCode() {
		return langCode;
	}
	/**
	 * @param langCode the langCode to set
	 */
	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}
	

}
