package com.etihad.book.getreservationdata.constants;

/**
 * Seat type enum
 *
 */
public enum SEAT_TYPE_ENUM {
	STANDARD_SEAT("Standard Seat", false), PAID_SEAT("Paid Seat", true),NONE("",false);
	private String seatTypeDescription;
	private boolean isPaidSeat;

	private SEAT_TYPE_ENUM(String seatTypeDescription, boolean isPaidSeat) {
		this.seatTypeDescription = seatTypeDescription;
		this.isPaidSeat = isPaidSeat;
	}

	public String getSeatTypeDescription() {
		return seatTypeDescription;
	}

	public void setSeatTypeDescription(String seatTypeDescription) {
		this.seatTypeDescription = seatTypeDescription;
	}

	public boolean isPaidSeat() {
		return isPaidSeat;
	}

	public void setPaidSeat(boolean isPaidSeat) {
		this.isPaidSeat = isPaidSeat;
	}

}
