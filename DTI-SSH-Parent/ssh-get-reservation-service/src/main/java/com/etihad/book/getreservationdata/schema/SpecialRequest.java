/**
 * DTO class for Special Request (SSR)
 */
package com.etihad.book.getreservationdata.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SpecialRequest {

	private String code;
	private String description;
	private String type;
	private boolean isEditable;
	private String ssrId;
	private boolean isInfantSSR;
	private String statusCode;

	/**
	 * If ssr can be changed or added
	 * 
	 * @return the isEditable
	 */
	@JsonProperty(value = "isEditable")

	public boolean isEditable() {
		return isEditable;
	}

	/**
	 * @param isEditable
	 *            the isEditable to set
	 */
	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	/**
	 * The ssr description(mapped to master data)
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * The ssr type for ex. meal,wheelchair,bassinet,others
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	/**
	 * The ssr code
	 * 
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@JsonIgnore
	public String getSsrId() {
		return ssrId;
	}

	public void setSsrId(String ssrId) {
		this.ssrId = ssrId;
	}

	@JsonProperty(value = "isInfantSSR")
	public boolean isInfantSSR() {
		return isInfantSSR;
	}

	public void setInfantSSR(boolean isInfantSSR) {
		this.isInfantSSR = isInfantSSR;
	}

	/**
	 * @return Status code of SSr
	 */
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public String toString() {
		return "SpecialRequest [code=" + code + ", description=" + description + ", type=" + type + ", isEditable="
				+ isEditable + ", ssrId=" + ssrId + ", isInfant=" + isInfantSSR + "]";
	}
}
