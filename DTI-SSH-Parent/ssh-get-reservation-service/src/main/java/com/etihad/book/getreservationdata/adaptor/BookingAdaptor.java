/**
 * 
 */
package com.etihad.book.getreservationdata.adaptor;

import java.util.List;
import java.util.Map;

import com.etihad.book.common.annotations.EnableAutoLog;
import com.etihad.book.getreservationdata.exception.NameNotFoundException;
import com.etihad.book.getreservationdata.helper.BookingHelper;
import com.etihad.book.getreservationdata.helper.PassengerHelper;
import com.etihad.book.getreservationdata.helper.PrimaryPaxHelper;
import com.etihad.book.getreservationdata.helper.VCRHelper;
import com.etihad.book.getreservationdata.schema.BookingDetails;
import com.etihad.book.getreservationdata.schema.FlightDetails;
import com.etihad.book.getreservationdata.schema.ONDDataObj;
import com.etihad.book.getreservationdata.service.GetReservationServiceImpl;
import com.etihad.book.getreservationdata.vcrschema.VCRTicketingInfo;
import com.sabre.webservices.pnrbuilder.v1_17.BookingDetailsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.BookingDetailsPNRB.FlightsRange;

/**
 * @author 526837
 *
 */
public class BookingAdaptor {

	@EnableAutoLog(logStackTrace = true)
	public static BookingDetails setBookingDetails(ReservationPNRB reservation, String lastName,
			List<FlightDetails> flightDetailsList, String langCode, Map<String, VCRTicketingInfo> vcrResponseMap) throws NameNotFoundException {

		BookingDetails bookingData = new BookingDetails();
		ONDDataObj originDataObj = null;
		ONDDataObj destDataObj = null;
		BookingDetailsPNRB bookingDetails = reservation.getBookingDetails();
		if (null != bookingDetails) {
			bookingData.setLoyaltyHeader((null != bookingDetails.getHeader() && !bookingDetails.getHeader().isEmpty()) ? bookingDetails.getHeader().get(0) : null);
			bookingData.setBookingReference(bookingDetails.getRecordLocator());
			bookingData.setIsRoundTrip(BookingHelper.checkRoundTrip(reservation));
			String originAirportCode = BookingHelper.getOriginAirportCodeForBookingSummary(flightDetailsList);
			bookingData.setOriginCityCode(originAirportCode);
			Map<String, ONDDataObj> ondMap = (Map<String, ONDDataObj>) GetReservationServiceImpl.referenceDataMap
					.get(langCode);
			if (null != ondMap) {
				originDataObj = ondMap.get(originAirportCode);
			}
			if (null != originDataObj) {
				bookingData.setOriginCity(originDataObj.getCityName());
				bookingData.setOriginCountry(originDataObj.getCountryName());
			}
			String departureAirportCode = BookingHelper.getDestinationAirportCodeForBookingSummary(flightDetailsList,
					bookingData.getIsRoundTrip());
			bookingData.setDestinationCityCode(departureAirportCode);
			if (null != ondMap) {
				destDataObj = ondMap.get(departureAirportCode);
			}
			if (null != destDataObj) {
				bookingData.setDestinationCity(destDataObj.getCityName());
				bookingData.setDestinationCountry(destDataObj.getCountryName());
			}
			FlightsRange flightsRange = bookingDetails.getFlightsRange();
			if (null != flightsRange) {
				bookingData.setJourneyStartDate(flightsRange.getStart().toString());
				bookingData.setJourneyEndDate(flightsRange.getEnd().toString());
				PassengerPNRB passengerObj = PrimaryPaxHelper.getPrimaryPassengerDetails(reservation, lastName);
				if (!org.springframework.util.StringUtils.isEmpty(passengerObj)) {
					bookingData.setPaxId(passengerObj.getId());
					bookingData.setPrimaryPaxLastName(passengerObj.getLastName());
					bookingData.setPrimaryPaxFirstName(
							null != PassengerHelper.getPassengerFirstName(passengerObj.getFirstName())
									? PassengerHelper.getPassengerFirstName(passengerObj.getFirstName())
									: passengerObj.getFirstName());
					bookingData.setPrimaryPaxTitle(null != passengerObj.getTitle() ? passengerObj.getTitle()
							: PassengerHelper.getPassengerTitle(passengerObj.getFirstName()));
				}
			}
			bookingData.setPCC(VCRHelper.getPCCDetails(vcrResponseMap));
		}
		return bookingData;

	}

}
