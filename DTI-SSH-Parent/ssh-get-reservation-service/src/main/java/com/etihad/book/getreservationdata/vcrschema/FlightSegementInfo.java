/**
 * 
 */
package com.etihad.book.getreservationdata.vcrschema;

/**
 * @author 641852
 *
 */
public class FlightSegementInfo {

	private String connectionInd;
	private String departureDateTime;
	private String flightNumber;
	private String resBookDesigCode;
	private String status;
	private String baggageAllowance;
	private String destinationLocationCode;
	private String fareBasis;
	private String marketingAirlineCode;
	private String marketingFlightNumber;
	private String originLocationCode;
	private String notValidAfter;
	private String notValidBefore;
	/**
	 * @return the connectionInd
	 */
	public String getConnectionInd() {
		return connectionInd;
	}
	/**
	 * @param connectionInd the connectionInd to set
	 */
	public void setConnectionInd(String connectionInd) {
		this.connectionInd = connectionInd;
	}
	/**
	 * @return the departureDateTime
	 */
	public String getDepartureDateTime() {
		return departureDateTime;
	}
	/**
	 * @param departureDateTime the departureDateTime to set
	 */
	public void setDepartureDateTime(String departureDateTime) {
		this.departureDateTime = departureDateTime;
	}
	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}
	/**
	 * @param flightNumber the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	/**
	 * @return the resBookDesigCode
	 */
	public String getResBookDesigCode() {
		return resBookDesigCode;
	}
	/**
	 * @param resBookDesigCode the resBookDesigCode to set
	 */
	public void setResBookDesigCode(String resBookDesigCode) {
		this.resBookDesigCode = resBookDesigCode;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the baggageAllowance
	 */
	public String getBaggageAllowance() {
		return baggageAllowance;
	}
	/**
	 * @param baggageAllowance the baggageAllowance to set
	 */
	public void setBaggageAllowance(String baggageAllowance) {
		this.baggageAllowance = baggageAllowance;
	}
	/**
	 * @return the destinationLocationCode
	 */
	public String getDestinationLocationCode() {
		return destinationLocationCode;
	}
	/**
	 * @param destinationLocationCode the destinationLocationCode to set
	 */
	public void setDestinationLocationCode(String destinationLocationCode) {
		this.destinationLocationCode = destinationLocationCode;
	}
	/**
	 * @return the fareBasis
	 */
	public String getFareBasis() {
		return fareBasis;
	}
	/**
	 * @param fareBasis the fareBasis to set
	 */
	public void setFareBasis(String fareBasis) {
		this.fareBasis = fareBasis;
	}
	/**
	 * @return the marketingAirlineCode
	 */
	public String getMarketingAirlineCode() {
		return marketingAirlineCode;
	}
	/**
	 * @param marketingAirlineCode the marketingAirlineCode to set
	 */
	public void setMarketingAirlineCode(String marketingAirlineCode) {
		this.marketingAirlineCode = marketingAirlineCode;
	}
	/**
	 * @return the marketingFlightNumber
	 */
	public String getMarketingFlightNumber() {
		return marketingFlightNumber;
	}
	/**
	 * @param marketingFlightNumber the marketingFlightNumber to set
	 */
	public void setMarketingFlightNumber(String marketingFlightNumber) {
		this.marketingFlightNumber = marketingFlightNumber;
	}
	/**
	 * @return the originLocationCode
	 */
	public String getOriginLocationCode() {
		return originLocationCode;
	}
	/**
	 * @param originLocationCode the originLocationCode to set
	 */
	public void setOriginLocationCode(String originLocationCode) {
		this.originLocationCode = originLocationCode;
	}
	/**
	 * @return the notValidAfter
	 */
	public String getNotValidAfter() {
		return notValidAfter;
	}
	/**
	 * @param notValidAfter the notValidAfter to set
	 */
	public void setNotValidAfter(String notValidAfter) {
		this.notValidAfter = notValidAfter;
	}
	/**
	 * @return the notValidBefore
	 */
	public String getNotValidBefore() {
		return notValidBefore;
	}
	/**
	 * @param notValidBefore the notValidBefore to set
	 */
	public void setNotValidBefore(String notValidBefore) {
		this.notValidBefore = notValidBefore;
	}
}
