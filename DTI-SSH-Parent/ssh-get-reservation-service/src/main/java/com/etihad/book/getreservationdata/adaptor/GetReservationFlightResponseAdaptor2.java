/**
 * 
 */
package com.etihad.book.getreservationdata.adaptor;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etihad.book.common.annotations.EnableAutoLog;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.CoreConstants;
import com.etihad.book.common.util.Utils;
import com.etihad.book.getreservationdata.constants.Constants;
import com.etihad.book.getreservationdata.exception.NameNotFoundException;
import com.etihad.book.getreservationdata.helper.VCRHelper;
import com.etihad.book.getreservationdata.schema.BookingDetails;
import com.etihad.book.getreservationdata.schema.CabinBaggageObj;
import com.etihad.book.getreservationdata.schema.FlightDetails;
import com.etihad.book.getreservationdata.schema.GetReservationResponse;
import com.etihad.book.getreservationdata.schema.ONDDataObj;
import com.etihad.book.getreservationdata.schema.SpecialRequest;
import com.etihad.book.getreservationdata.service.GetReservationServiceImpl;
import com.etihad.book.getreservationdata.vcrschema.VCRTicketingInfo;
import com.sabre.webservices.pnrbuilder.v1_17.APISRequestPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.AirType.AncillaryServices;
import com.sabre.webservices.pnrbuilder.v1_17.AirType.AncillaryServices.AncillaryService;
import com.sabre.webservices.pnrbuilder.v1_17.DOCSEntryPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.GenderDOCSEntryPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.GenericSpecialRequestPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRSType;
import com.sabre.webservices.pnrbuilder.v1_17.ItinerarySpecialRequestsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment;
import com.sabre.webservices.pnrbuilder.v1_17.SpecialMealRequestPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.TravelerGroupPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.WheelchairRequestPNRB;

/**
 * @author CTS
 *
 */
@Component
public class GetReservationFlightResponseAdaptor2 {

	@Autowired
	private Utils utils;

	Map<String, String> flightLayoverMap = new HashMap<>();

	@EnableAutoLog(logStackTrace = true)
	public GetReservationResponse processJSONResponseForGetReservationData(GetReservationRSType soapResponse,
			String lastName, String langCode) throws ApplicationException {

		GetReservationResponse response = new GetReservationResponse();
		BookingDetails bookingData = new BookingDetails();
		List<FlightDetails> flightDetailsList = new ArrayList<>();
		List<String> errorCodes = new ArrayList<>();
		Map<String, VCRTicketingInfo> vcrResponseMap = new HashMap<>();
		if (null != soapResponse) {
			ReservationPNRB reservation = soapResponse.getReservation();
			if (null != reservation) {
				PassengerReservationPNRB passengerReservation = reservation.getPassengerReservation();
				if (null != passengerReservation) {
					checkGroupPNR(passengerReservation, errorCodes);
					vcrResponseMap = VCRAdaptor.callVCRService(passengerReservation,
							utils.getPropFromConfig(CoreConstants.VCR_URL));
					flightDetailsList = setJourneyDetails(passengerReservation, langCode, vcrResponseMap);
				}
				try {
					bookingData = BookingAdaptor.setBookingDetails(reservation, lastName, flightDetailsList, langCode,
							vcrResponseMap);
				} catch (NameNotFoundException e) {
					errorCodes.add("pnrretrieval.101");
					throw new ApplicationException(utils.populateResponseHeader(errorCodes));
				}
			}
			response.setBookingDetails(bookingData);
			response.setFlightDetails(flightDetailsList);
		}
		return response;
	}

	private void checkGroupPNR(PassengerReservationPNRB passengerReservation, List<String> errorCodes)
			throws ApplicationException {

		TravelerGroupPNRB corp = passengerReservation.getPassengers().getCorporate();
		if (null != corp) {
			String nameId = corp.getNameId();

			if (!StringUtils.isEmpty(nameId)) {
				errorCodes.add("pnrretrieval.105");
				throw new ApplicationException(utils.populateResponseHeader(errorCodes));
			}
		}

	}

	private List<FlightDetails> setJourneyDetails(PassengerReservationPNRB passengerReservation, String langCode,
			Map<String, VCRTicketingInfo> vcrResponseMap) {

		FlightDetails flightDetails;
		List<FlightDetails> flightDetailsList = new ArrayList<>();
		SegmentTypePNRB segments = passengerReservation.getSegments();
		ONDDataObj originDataObj = null;
		ONDDataObj destDataObj = null;
		Map<String, ONDDataObj> ondMap = null;
		Map<String, String> flightSeatMap = new HashMap<>();
		Map<Integer, List<SpecialRequest>> specialRequestInSequence = new HashMap<>();
		int count = 0;
		CabinBaggageObj cabinBaggage = null;
		if (null != segments) {

			List<Segment> segmentList = segments.getSegment();
			if (null != segmentList && !segmentList.isEmpty()) {
				for (Segment segment : segmentList) {
					if (null != segment) {
						com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment.Air air = segment.getAir();
						if (null != air) {
							if (air.getActionCode().equals("WK")) {
								continue;
							}
							count++;
							List<SpecialRequest> specialRequests = populateSegmentSSR(air.getSegmentSpecialRequests(),
									count);
							final String classOfService = air.getClassOfService();
							final AncillaryServices anc = air.getAncillaryServices();
							final List<AncillaryService> ancillaryServices = anc != null ? anc.getAncillaryService()
									: null;
							specialRequestInSequence.put(count, specialRequests);
							flightDetails = new FlightDetails();
							flightDetails.setPoc(segments.getPoc().getAirport());
							flightDetails.setOriginAirportCode(air.getDepartureAirport());
							if (GetReservationServiceImpl.referenceDataMap.get(langCode) instanceof Map<?, ?>) {
								ondMap = (Map<String, ONDDataObj>) GetReservationServiceImpl.referenceDataMap
										.get(langCode);
							}
							if (null != ondMap) {
								originDataObj = ondMap.get(air.getDepartureAirport());
							}
							if (null != originDataObj) {
								flightDetails.setOriginCity(originDataObj.getCityName());
								flightDetails.setOriginAirportName(originDataObj.getAirportName());
								flightDetails.setOriginCountry(originDataObj.getCountryName());
							}
							flightDetails.setDestinationAirportCode(air.getArrivalAirport());
							if (null != ondMap) {
								destDataObj = ondMap.get(air.getArrivalAirport());
							}
							if (null != destDataObj) {
								flightDetails.setDestinationAirportName(destDataObj.getAirportName());
								flightDetails.setDestinationCity(destDataObj.getCityName());
								flightDetails.setDestinationCountry(destDataObj.getCountryName());
							}
							final String operatingAirlineCode = air.getOperatingAirlineCode();
							flightDetails.setOperatedBy(operatingAirlineCode);
							String equipmentType = air.getEquipmentType();
							if (StringUtils.isNotBlank(equipmentType)) {
								if (Constants.BUS_EQUIPMENT.equals(equipmentType)) {
									equipmentType = Constants.BUS_EQUIPMENT;
								} else if (Constants.TRAIN_EQUIPMENT.equals(equipmentType)) {
									equipmentType = Constants.TRAIN;
								} else if (GetReservationServiceImpl.referenceDataMap
										.get(air.getEquipmentType()) instanceof String) {
									equipmentType = (String) GetReservationServiceImpl.referenceDataMap
											.get(air.getEquipmentType());
								}
							}
							flightDetails.setAircraft(equipmentType);

							final String marketingAirlineCode = air.getMarketingAirlineCode();
							flightDetails.setMarketingAirlineCode(marketingAirlineCode);
							String flightClass = null;
							if (GetReservationServiceImpl.referenceDataMap
									.get(air.getClassOfService()) instanceof CabinBaggageObj) {
								cabinBaggage = (CabinBaggageObj) GetReservationServiceImpl.referenceDataMap
										.get(air.getClassOfService());
								if (null != cabinBaggage) {
									flightClass = cabinBaggage.getCabinClass();
								}
							}
							flightDetails.setFlightClass(null != flightClass ? flightClass : air.getClassOfService());
							flightDetails.setFlightStartDate(air.getDepartureDateTime().substring(0, 10));
							flightDetails.setFlightStartTime(
									air.getDepartureDateTime().substring(11, air.getDepartureDateTime().length()));
							flightDetails.setFlightEndDate(air.getArrivalDateTime().substring(0, 10));
							flightDetails.setFlightEndTime(
									air.getArrivalDateTime().substring(11, air.getArrivalDateTime().length()));
							flightDetails.setFlightNumber(air.getFlightNumber());
							flightDetails.setDepartureTerminalName(air.getDepartureTerminalName());
							flightDetails.setArrivalTerminalName(air.getArrivalTerminalName());
							flightDetails.setOperatingAirlineCode(air.getOperatingAirlineCode());
							flightDetails.setOperatingAirlineShortName(air.getOperatingAirlineShortName());
							flightDetails.setOperatingFlightNumber(air.getOperatingFlightNumber());
							flightDetails.setMarketingAirlineCode(air.getMarketingAirlineCode());
							final String flightNoWtZeroes = StringUtils.stripStart(air.getFlightNumber(), "0");
							flightDetails.setFlightNumber(flightNoWtZeroes);
							flightDetails.setOperatingClassOfService(air.getOperatingClassOfService());
							flightDetails.setMarketingClassOfService(air.getMarketingClassOfService());
							flightDetails.setSegment(air.getMarriageGrp().getGroup());
							flightDetails.setLegs(air.getMarriageGrp().getSequence());
							flightDetails.setFlightDuration(calculateDuration(air.getArrivalDateTime(),
									air.getDepartureDateTime(), air.getArrivalAirport(), air.getDepartureAirport()));
							flightDetails.setDurationDiffDays(calculateDurationDaysDifference(air.getArrivalDateTime(),
									air.getDepartureDateTime()));
							flightDetails.setLayoverTime(calculateLayOver(segmentList, air.getDepartureDateTime()));
							flightDetails.setActiveSegment(air.isIsPast().equals(false) ? true : false);
							flightDetails.setCheckinTimeDifference(
									calculateCheckinTime(air.getDepartureAirport(), air.getDepartureDateTime()));
							final Short sequence = air.getSequence();
							flightSeatMap = SeatAdaptor.setSeatsOfFlight(sequence, air.getSeats());
							flightDetails.setSegmentStatus(VCRHelper.getSegmentStatus(vcrResponseMap,
									air.getArrivalAirport(), air.getDepartureAirport(), air.getFlightNumber()));
							flightDetails.setPaxDetails(PassengerAdaptor.setPassengersDetails(passengerReservation,
									flightSeatMap, sequence, specialRequests, classOfService, ancillaryServices,
									langCode, flightDetails, vcrResponseMap));
							flightDetailsList.add(flightDetails);
						}
					}
				}
			}
		}
		return flightDetailsList;
	}

	private String calculateCheckinTime(String departureAirport, String actualDepartureTime) {

		String checkinTimeDiff = null;
		Date dt = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		String strGMTDate = dateFormatter.format(dt);
		String depatureTimeinGMT = utils.changeTimezoneFrom(actualDepartureTime, "yyyy-MM-dd'T'HH:mm:ss",
				getTimeZoneForAirportCode(departureAirport));
		long diffTime = differenceTimeinSeconds(depatureTimeinGMT, strGMTDate);
		if (diffTime > 0) {
			checkinTimeDiff = diffTime + "";
		}
		return checkinTimeDiff;
	}

	private long differenceTimeinSeconds(String depatureTimeinGMT, String strGMTDate) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = format.parse(depatureTimeinGMT);
			date2 = format.parse(strGMTDate);
		} catch (ParseException e) {
			//
		}
		long difference = date1.getTime() - 172800000 - date2.getTime();
		return difference / 1000;
	}

	private String calculateDurationDaysDifference(String arrivalDateTime, String departureDateTime) {

		Integer daysDifference = (Integer.parseInt(arrivalDateTime.substring(8, 10)))
				- (Integer.parseInt(departureDateTime.substring(8, 10)));
		return daysDifference + "";
	}

	private String calculateDuration(String arrivalDateTime, String departureDateTime, String arrivalAirportCode,
			String departureAirportCode) {
		String changedArrivalDate = utils.changeTimezoneFrom(arrivalDateTime, "yyyy-MM-dd'T'HH:mm:ss",
				getTimeZoneForAirportCode(arrivalAirportCode));
		String changedDepartureDate = utils.changeTimezoneFrom(departureDateTime, "yyyy-MM-dd'T'HH:mm:ss",
				getTimeZoneForAirportCode(departureAirportCode));
		return differenceTime(changedArrivalDate, changedDepartureDate);
	}

	private String getTimeZoneForAirportCode(String airportCode) {
		String timezone = null;
		if (GetReservationServiceImpl.referenceDataMap.get(airportCode) instanceof String) {
			timezone = (String) GetReservationServiceImpl.referenceDataMap.get(airportCode);
		}
		return timezone;
	}

	private static String differenceTime(String changedArrivalDate, String changedDepartureDate) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = format.parse(changedArrivalDate);
			date2 = format.parse(changedDepartureDate);
		} catch (ParseException e) {
			//
		}

		long difference = date1.getTime() - date2.getTime();

		return difference / (1000 * 60) + "";
	}

	private String calculateLayOver(List<Segment> segmentList, String departureDate) {

		String layover = null;
		flightLayoverMap = calculateLayOverFromMap(segmentList);
		layover = flightLayoverMap.get(departureDate);
		return layover;

	}

	private Map<String, String> calculateLayOverFromMap(List<Segment> segmentList) {

		String layOverTime = null;
		if (segmentList.size() > 1) {
			for (int i = 0; i < segmentList.size() - 1; i++) {
				Segment segment = segmentList.get(i);
				String gropup = segment.getAir().getMarriageGrp().getGroup();
				String arrivalAirport = segment.getAir().getArrivalAirport();
				String flightNumber = segment.getAir().getFlightNumber();
				Segment nextSegment = segmentList.get(i + 1);
				String nextGroup = nextSegment.getAir().getMarriageGrp().getGroup();
				String flightNumberNext = nextSegment.getAir().getFlightNumber();
				String departureAirport = nextSegment.getAir().getDepartureAirport();
				if (gropup.equals(nextGroup) && arrivalAirport.equals(departureAirport)) {
					if (!flightNumber.equals(flightNumberNext)
							&& (!segment.getAir().getMarriageGrp().getGroup().equals("0"))
							&& (!segment.getAir().getMarriageGrp().getSequence().equals("0"))) {
						layOverTime = calculateLayoverTime(segment.getAir().getArrivalDateTime(),
								nextSegment.getAir().getDepartureDateTime());
						flightLayoverMap.put(segment.getAir().getDepartureDateTime(), layOverTime);
					}
				}
			}
		}
		return flightLayoverMap;
	}

	private String calculateLayoverTime(String arrivalDateTime, String departureDateTime) {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = format.parse(arrivalDateTime);
			date2 = format.parse(departureDateTime);
		} catch (ParseException e) {
			//
		}

		long difference = date2.getTime() - date1.getTime();

		return difference / (1000 * 60) + "";
	}

	private List<SpecialRequest> populateSegmentSSR(ItinerarySpecialRequestsPNRB specialRequests, int sequenceId) {
		List<SpecialRequest> specialRequestList = new ArrayList<>();
		if (specialRequests != null) {
			GetReservationFlightResponseAdaptor2.populateGenericSpecialRequest(
					specialRequests.getGenericSpecialRequest(), specialRequestList, null, true);
			GetReservationFlightResponseAdaptor2.populateWheelChrSpecialRequest(specialRequests.getWheelchairRequest(),
					specialRequestList, true);

		}
		return specialRequestList;

	}

	public static void populateGenericSpecialRequest(List<GenericSpecialRequestPNRB> specialGenRequests,
			List<SpecialRequest> specialRequestList, Set<String> infantsAssociatedNames, final boolean checkDuplicate) {
		final Set<String> genericSpecialRequestSet = new HashSet<>();
		for (GenericSpecialRequestPNRB genericSpecialRequestPNRB : specialGenRequests) {
			SpecialRequest request = new SpecialRequest();
			final String code = genericSpecialRequestPNRB.getCode();
			request.setCode(code);
			request.setSsrId(genericSpecialRequestPNRB.getId());
			request.setStatusCode(genericSpecialRequestPNRB.getActionCode());
			if (Constants.GET_RES_SSR_TYPE_BAS.equals(code)) {
				request.setType(Constants.GET_RES_SSR_TYPE_BASDESCR);
			}
			if (checkDuplicate) {
				if (!genericSpecialRequestSet.contains(code)) {
					specialRequestList.add(request);
					genericSpecialRequestSet.add(code);
				}
			} else {
				specialRequestList.add(request);
			}
			if (infantsAssociatedNames != null) {
				if (Constants.SPCL_INFANT.equals(code)) {
					final String inFreeText = genericSpecialRequestPNRB.getFreeText();
					if (StringUtils.isNotBlank(inFreeText)) {
						String[] infantArray = inFreeText.split("/");
						if (infantArray.length >= 3) {
							final StringBuilder builder = new StringBuilder();
							builder.append(infantArray[1]);
							builder.append("/");
							builder.append(infantArray[2]);
							infantsAssociatedNames.add(builder.toString());

						}
					}

				}
			}

		}
	}

	public static void populateWheelChrSpecialRequest(List<WheelchairRequestPNRB> specialWheelRequests,
			List<SpecialRequest> specialRequestList, final boolean checkDuplicate) {
		final Set<String> genericSpecialRequestSet = new HashSet<>();
		for (WheelchairRequestPNRB genericSpecialRequestPNRB : specialWheelRequests) {
			SpecialRequest request = new SpecialRequest();
			final String code = genericSpecialRequestPNRB.getWheelchairCode().value();
			request.setCode(code);
			request.setSsrId(genericSpecialRequestPNRB.getId());
			request.setStatusCode(genericSpecialRequestPNRB.getActionCode());
			request.setType(Constants.GET_RES_SSR_TYPE_WHEELCHAIR);
			if (checkDuplicate) {
				if (!genericSpecialRequestSet.contains(code)) {
					specialRequestList.add(request);
					genericSpecialRequestSet.add(code);
				}
			} else {
				specialRequestList.add(request);
			}

		}
	}

	public static void populateMealSpecialRequest(List<SpecialMealRequestPNRB> specialMealRequests,
			List<SpecialRequest> specialRequestList, final boolean checkDuplicate) {
		final Set<String> genericSpecialRequestSet = new HashSet<>();
		for (SpecialMealRequestPNRB mealRequestPNRB : specialMealRequests) {
			SpecialRequest request = new SpecialRequest();
			final String mealType = mealRequestPNRB.getMealType();
			request.setCode(mealType);
			request.setSsrId(mealRequestPNRB.getId());
			request.setStatusCode(mealRequestPNRB.getActionCode());
			request.setType(Constants.GET_RES_SSR_TYPE_MEAL);
			if (checkDuplicate) {
				if (!genericSpecialRequestSet.contains(mealType)) {
					specialRequestList.add(request);
					genericSpecialRequestSet.add(mealType);
				}
			} else {
				specialRequestList.add(request);
			}
		}
	}

	public static void populateAssociatedInfantsRequest(List<APISRequestPNRB> specialApisRequests,
			List<String> infantsAssociatedNames) {
		for (APISRequestPNRB apisReq : specialApisRequests) {
			final DOCSEntryPNRB docsEntry = apisReq.getDOCSEntry();
			if (docsEntry != null) {
				final GenderDOCSEntryPNRB gender = docsEntry.getGender();
				final StringBuilder builder = new StringBuilder();
				if (GenderDOCSEntryPNRB.MI == gender || GenderDOCSEntryPNRB.FI == gender) {
					final String forename = docsEntry.getForename();
					if (StringUtils.isNotBlank(forename)) {
						builder.append(forename);
					}
					final String surname = docsEntry.getSurname();
					if (StringUtils.isNotBlank(surname)) {
						builder.append("/");
						builder.append(surname);
					}
					infantsAssociatedNames.add(builder.toString());
				}

			}

		}
	}
}
