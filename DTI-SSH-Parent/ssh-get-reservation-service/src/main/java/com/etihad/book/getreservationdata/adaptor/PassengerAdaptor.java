
/**
 * 
 */
package com.etihad.book.getreservationdata.adaptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.etihad.book.common.annotations.EnableAutoLog;
import com.etihad.book.common.util.Utils;
import com.etihad.book.getreservationdata.constants.Constants;
import com.etihad.book.getreservationdata.helper.LoyaltyHelper;
import com.etihad.book.getreservationdata.helper.PassengerHelper;
import com.etihad.book.getreservationdata.schema.BaggageDetails;
import com.etihad.book.getreservationdata.schema.CheckinBaggage;
import com.etihad.book.getreservationdata.schema.CheckinBaggageDetails;
import com.etihad.book.getreservationdata.schema.ExtraBaggage;
import com.etihad.book.getreservationdata.schema.FlightDetails;
import com.etihad.book.getreservationdata.schema.PassengerDetails;
import com.etihad.book.getreservationdata.schema.SpecialRequest;
import com.etihad.book.getreservationdata.service.GetReservationServiceImpl;
import com.etihad.book.getreservationdata.vcrschema.VCRTicketingInfo;
import com.sabre.webservices.pnrbuilder.v1_17.AirType.AncillaryServices.AncillaryService;
import com.sabre.webservices.pnrbuilder.v1_17.FrequentFlyerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.NameAssociatedSpecialRequestsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerTypePNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengersPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PreReservedSeatPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PreReservedSeatsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SeatsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.TicketDetailsType;
import com.sabre.webservices.pnrbuilder.v1_17.TicketingInfoPNRB;

/**
 * Passenger Adaptor for PAX details
 *
 */

public class PassengerAdaptor {

	/**
	 * @param passengerReservation
	 * @param flightSeatMap
	 * @param sequence
	 * @param flightSSR
	 * @param classOfService
	 * @param flightAncillaryServices
	 * @param langCode
	 * @param flightdetails
	 * @param vcrResponseMap
	 * @return
	 */
	@EnableAutoLog(logStackTrace = true)
	public static List<PassengerDetails> setPassengersDetails(final PassengerReservationPNRB passengerReservation,
			Map<String, String> flightSeatMap, Short sequence, List<SpecialRequest> flightSSR, String classOfService,
			List<AncillaryService> flightAncillaryServices, String langCode, final FlightDetails flightdetails,
			final Map<String, VCRTicketingInfo> vcrResponseMap) {

		final List<PassengerDetails> passengersList = new ArrayList<>();
		PassengerDetails paxDetails = null;
		Map<String, String> ticketPassengerMap = getTicketForPassenger(passengerReservation);
		final PassengersPNRB passengers = passengerReservation.getPassengers();
		final Map<String, List<SpecialRequest>> infantSSRMap = new HashMap<>();
		final Map<String, Set<String>> infantsAssocMap = new HashMap<>();

		int count = 0;
		if (null != passengers) {
			List<PassengerPNRB> passengersListSoap = passengers.getPassenger();
			if (null != passengersListSoap) {
				for (PassengerPNRB passenger : passengersListSoap) {
					if (null != passenger) {
						count++;
						String paxType = passenger.getReferenceNumber();
						final boolean isInfantonLap = "INF".equalsIgnoreCase(paxType);
						// is infant flag
						final PassengerTypePNRB paxNameTypePRNB = passenger.getNameType();
						final String paxNameType = paxNameTypePRNB != null ? paxNameTypePRNB.name() : StringUtils.EMPTY;
						final boolean infantNameType = "I".equals(paxNameType);
						final boolean isInfant = isInfantonLap || Constants.INFANT_WITH_SEAT.equals(paxType)
								|| infantNameType;
						paxType = isInfant && paxType == null ? "I" : paxType;
						final boolean isInfantonLapFlag = infantNameType || isInfantonLap;
						// operated sector flag
						final boolean isEYOperated = "EY".equals(flightdetails.getOperatingAirlineCode());
						final boolean isEYSectorAll = StringUtils.isNotBlank(flightdetails.getOperatingAirlineCode())
								&& StringUtils.isNotBlank(flightdetails.getMarketingAirlineCode())
										? flightdetails.getOperatingAirlineCode()
												.equals(flightdetails.getMarketingAirlineCode()) && isEYOperated
										: false;
						// add edit baggage flag
						final boolean isPurchaseExtraBaggage = isInfant
								? isEYSectorAll && Constants.INFANT_WITH_SEAT.equals(paxType) : isEYSectorAll;
						paxDetails = new PassengerDetails();
						paxDetails.setLastName(passenger.getLastName());
						paxDetails.setFirstName(PassengerHelper.getPassengerFirstName(passenger.getFirstName()));
						paxDetails.setTitle(PassengerHelper.getPassengerTitle(passenger.getFirstName()));
						paxDetails.setGenderCode(passenger.getGender());
						paxDetails.setEmailAddress(
								(null != passenger.getEmailAddress() && !passenger.getEmailAddress().isEmpty())
										? passenger.getEmailAddress().get(0).getAddress() : null);
						paxDetails.setPhoneNumber("");
						String ticketLastName = passenger.getLastName().length() > 5
								? passenger.getLastName().substring(0, 5) : passenger.getLastName();
						String paxCount = ticketLastName + "/" + passenger.getFirstName().substring(0, 1) + count + "";
						paxDetails.setTicketNumber(ticketPassengerMap.get(paxCount));
						setLoyaltyDetails(passenger, paxDetails, langCode);
						/* Start of baggage mapping section */
						final BaggageDetails baggage = new BaggageDetails();
						/* Carry on/Cabin baggage */
						classOfService = isInfant ? "INFANT" : classOfService;
						BaggageAdaptor.mapCabinBaggage(classOfService, isEYOperated, baggage);
						/* Map Checkin baggage */
						final List<ExtraBaggage> mappedExtraBaggage = new ArrayList<>();
						BaggageAdaptor.getMappedExtraBaggage(flightAncillaryServices, passenger, mappedExtraBaggage);
						CheckinBaggageDetails details = new CheckinBaggageDetails();
						details.setExtraBaggageDetails(mappedExtraBaggage);
						BaggageAdaptor.setAddOrEditBaggage(isPurchaseExtraBaggage, details);
						final String loyaltyTierCode = paxDetails.getLoyaltyTierCode();
						final String loyaltyTier = paxDetails.getLoyaltyTier();
						CheckinBaggage checkinWtBaggage = BaggageAdaptor.getCheckinBaggageWeight(loyaltyTierCode,
								loyaltyTier);
						CheckinBaggage checkinPcBaggage = BaggageAdaptor.getCheckinBaggagePiece(loyaltyTierCode,
								loyaltyTier, classOfService);
						List<CheckinBaggage> checkinBaggageDetails = new ArrayList<>();
						if (!vcrResponseMap.isEmpty()) {
							String paxVcrType = paxType == null ? "S" : paxType;
							final VCRTicketingInfo vcrTicketingInfo = vcrResponseMap.get(paxVcrType);
							if (vcrTicketingInfo != null) {
								final String baggageAllowance = BaggageAdaptor.getBaggageAllowance(vcrTicketingInfo,
										flightdetails);
								if (StringUtils.isNotBlank(baggageAllowance)) {
									boolean isInWeight = baggageAllowance.contains("K");
									boolean isInPiece = baggageAllowance.contains("PC");
									final String baggageAllowanceNum = baggageAllowance.replaceAll("[^0-9]",
											StringUtils.EMPTY);
									int baggageAmount = StringUtils.isNotBlank(baggageAllowanceNum)
											? Integer.parseInt(baggageAllowanceNum) : 0;
									if (isInWeight) {
										checkinWtBaggage
												.setWeightInKgs(checkinWtBaggage.getWeightInKgs() + baggageAmount);
										checkinBaggageDetails.add(checkinWtBaggage);
									} else if (isInPiece) {
										checkinPcBaggage.setUnitPiece(checkinPcBaggage.getUnitPiece() + baggageAmount);
										final int checkinweightInKgs = checkinPcBaggage.getWeightInKgs();
										int weightInKgs = checkinweightInKgs == 0 ? Constants.DEFAULT_WT_KG_ADT
												: checkinweightInKgs;
										checkinPcBaggage.setWeightInKgs(weightInKgs);
										checkinBaggageDetails.add(checkinPcBaggage);

									}

								}
								details.setCheckinBaggageDetails(checkinBaggageDetails);
							}
						}
						baggage.setCheckinBaggage(details);
						paxDetails.setBaggageDetails(baggage);
						/* End of baggage mapping section */
						paxDetails.setPaxId(passenger.getId());
						paxDetails.setPaxTypeCode(paxType);
						paxDetails.setPaxTypeDescription(getPaxDescription(paxType));
						PreReservedSeatPNRB seatObject = getPassengersSeats(passenger, flightSeatMap, sequence);
						if (null != seatObject) {
							SeatAdaptor.setSeat(paxDetails, passenger, seatObject);
						}
						SeatAdaptor.setSeatTypeIndicator(paxDetails, isInfantonLapFlag, isEYOperated);
						/* SSR section */
						final List<SpecialRequest> passengerSSR = new ArrayList<>();
						final Set<String> infantsAssociatedNames = new HashSet<>();
						populatePassengerSSR(passenger.getSpecialRequests(), passengerSSR, infantsAssociatedNames);
						final List<SpecialRequest> infantSSR = new ArrayList<>();
						final List<SpecialRequest> mappedSSR = mapToFlightSSR(passengerSSR, flightSSR, infantSSR,
								langCode, isEYSectorAll);
						if (!CollectionUtils.isEmpty(infantsAssociatedNames)) {
							infantsAssocMap.put(passenger.getNameAssocId(), infantsAssociatedNames);
						}
						final boolean withInfant = BooleanUtils.isTrue(passenger.isWithInfant());
						paxDetails.setWithInfant(withInfant);
						paxDetails.setInfant(isInfant);
						if (withInfant && !CollectionUtils.isEmpty(infantSSR)) {
							infantSSRMap.put(passenger.getNameAssocId(), infantSSR);
						}
						if (isInfant && !infantSSRMap.isEmpty()) {
							List<SpecialRequest> ssrs = null;
							if (!infantsAssocMap.isEmpty()) {
								ssrs = getMappedADTAssocSSR(infantSSRMap, infantsAssocMap, paxDetails);
							} else {
								Map.Entry<String, List<SpecialRequest>> entry = infantSSRMap.entrySet().iterator()
										.next();
								ssrs = entry.getValue();
							}
							paxDetails.setSpecialRequests(ssrs);
						} else {
							paxDetails.setSpecialRequests(mappedSSR);
						}
						if (isEYSectorAll) {
							paxDetails.setAddEditSSR(true);
						}
						/* SSR section end */
						passengersList.add(paxDetails);
					}
				}
			}
		}
		return passengersList;
	}

	private static void setLoyaltyDetails(PassengerPNRB passenger, PassengerDetails paxDetails, String langCode) {
		List<FrequentFlyerPNRB> frequentFlyerList = passenger.getFrequentFlyer();
		if (null != frequentFlyerList && !frequentFlyerList.isEmpty()) {
			for (FrequentFlyerPNRB frequentFlyer : frequentFlyerList) {
				if (null != frequentFlyer) {
					paxDetails.setLoyaltySupplierCarrier(frequentFlyer.getSupplierCode());
					paxDetails.setLoyaltyReceiverCarrier(frequentFlyer.getReceivingCarrierCode());
					paxDetails.setLoyaltyNumber(frequentFlyer.getNumber());
					paxDetails
							.setLoyaltyTier(LoyaltyHelper.getLoyaltyTier(frequentFlyer.getTierLevelNumber(), langCode));
					paxDetails.setLoyaltyTierCode(frequentFlyer.getTierLevelNumber() + "");
					paxDetails.setLoyaltyStatusCode(frequentFlyer.getStatusCode());
				}
			}
		}
	}

	private static Map<String, String> getTicketForPassenger(PassengerReservationPNRB passengerReservation) {

		Map<String, String> ticketPassengerMap = new HashMap<>();
		TicketingInfoPNRB ticketingInfo = passengerReservation.getTicketingInfo();
		int count = 0;
		if (null != ticketingInfo) {
			List<TicketDetailsType> ticketDetailList = ticketingInfo.getTicketDetails();
			if (null != ticketDetailList && !ticketDetailList.isEmpty()) {
				for (TicketDetailsType ticketDetailsType : ticketDetailList) {
					if (ticketDetailsType.getOriginalTicketDetails().contains("-")) {
						continue;
					}
					StringBuilder builder = new StringBuilder();
					count++;
					builder.append(ticketDetailsType.getPassengerName()).append(count).append(StringUtils.EMPTY);
					String passengerCount = builder.toString();
					ticketPassengerMap.put(passengerCount, ticketDetailsType.getTicketNumber());
				}
			}
		}
		return ticketPassengerMap;
	}

	private static PreReservedSeatPNRB getPassengersSeats(PassengerPNRB passenger, Map<String, String> flightSeatMap,
			Short sequence) {

		PreReservedSeatPNRB seat = null;
		SeatsPNRB seats = passenger.getSeats();
		PreReservedSeatsPNRB preReservedSeats = seats.getPreReservedSeats();
		if (null != preReservedSeats) {
			List<PreReservedSeatPNRB> preReservedSeatList = preReservedSeats.getPreReservedSeat();
			if (null != preReservedSeatList) {
				for (PreReservedSeatPNRB preReservedSeat : preReservedSeatList) {
					if (null != preReservedSeat) {
						String seatSequence = sequence + "" + preReservedSeat.getSeatNumber();
						if (preReservedSeat.getId().equals(flightSeatMap.get(seatSequence))) {
							seat = preReservedSeat;
							break;
						}
					}
				}
			}
		}
		return seat;
	}

	private static String getPaxDescription(String referenceNumber) {

		Map<String, String> paxTypeMap = new HashMap<>();

		paxTypeMap.put("ADT", "Adult");
		paxTypeMap.put("CHD", "Child");
		paxTypeMap.put("INF", "Infant");
		paxTypeMap.put(Constants.INFANT_WITH_SEAT, "Infant With Seat");
		paxTypeMap.put("I", "Infant");

		return paxTypeMap.get(referenceNumber);
	}

	private static void populatePassengerSSR(final NameAssociatedSpecialRequestsPNRB requestsPNRB,
			List<SpecialRequest> ssrPassenger, Set<String> infantsAssociatedNames) {
		if (requestsPNRB != null) {
			GetReservationFlightResponseAdaptor2.populateGenericSpecialRequest(requestsPNRB.getGenericSpecialRequest(),
					ssrPassenger, infantsAssociatedNames, false);
			GetReservationFlightResponseAdaptor2.populateWheelChrSpecialRequest(requestsPNRB.getWheelchairRequest(),
					ssrPassenger, false);
			GetReservationFlightResponseAdaptor2.populateMealSpecialRequest(requestsPNRB.getSpecialMealRequest(),
					ssrPassenger, false);

		}

	}

	/**
	 * Map flights ssr to passenger ssr
	 * 
	 * @param passengerSSSR
	 * @param flightSSR
	 * @param infantSSR
	 * @return List<SpecialRequest>
	 */
	private static List<SpecialRequest> mapToFlightSSR(List<SpecialRequest> passengerSSSR,
			List<SpecialRequest> flightSSR, List<SpecialRequest> infantSSR, String langCode, boolean EYsectorAll) {
		BiFunction<List<SpecialRequest>, SpecialRequest, Boolean> biFunction = PassengerAdaptor::filterBySsrId;
		Predicate<SpecialRequest> predicate = Utils.bind(biFunction, flightSSR);
		List<SpecialRequest> mappedList = passengerSSSR.stream().filter(splReq -> splReq.getSsrId() != null)
				.filter(predicate::test)
				.map(splReq -> PassengerAdaptor.mapValuesFromRef(splReq, infantSSR, langCode, EYsectorAll))
				.filter(splReq -> StringUtils.isNotBlank(splReq.getDescription())).collect(Collectors.toList());

		mappedList.removeAll(infantSSR);
		return mappedList;

	}

	/**
	 * Predicate to test if flight ssr is present in passenger ssr list
	 * 
	 * @param flightSSR
	 * @param passengerSSR
	 * @return boolean
	 */
	private static boolean filterBySsrId(List<SpecialRequest> flightSSR, SpecialRequest passengerSSR) {
		boolean isSSrId = flightSSR.stream().filter(splReq -> splReq.getSsrId() != null)
				.anyMatch(splReq -> splReq.getSsrId().equals(passengerSSR.getSsrId()));
		return isSSrId;
	}

	/**
	 * SpecialRequest after mapping description and pulling out infant ssr
	 * 
	 * @param passengerSSR
	 * @param infantSSR
	 * @return SpecialRequest
	 */
	private static SpecialRequest mapValuesFromRef(SpecialRequest passengerSSR, List<SpecialRequest> infantSSR,
			String langCode, boolean EYsectorAll) {
		String descr = null;
		boolean isEditableSSR = false;
		final StringBuilder builder = new StringBuilder();
		String[] langCodeArr = langCode.contains("-") ? langCode.split("-") : ArrayUtils.EMPTY_STRING_ARRAY;
		langCode = ArrayUtils.isNotEmpty(langCodeArr)
				? builder.append(langCodeArr[0]).append("_").append((langCodeArr[1]).toUpperCase()).toString()
				: langCode;
		String langCodewithSSRCode = langCode + passengerSSR.getCode();
		Object ssrDescr = GetReservationServiceImpl.referenceDataMap.get(langCodewithSSRCode);

		if (ssrDescr instanceof String) {
			descr = (String) ssrDescr;

		}
		if (EYsectorAll) {
			isEditableSSR = true;
		}
		passengerSSR.setEditable(isEditableSSR);
		passengerSSR.setDescription(descr);
		if (Constants.GET_RES_SSR_TYPE_BAS.equalsIgnoreCase(passengerSSR.getCode())
				|| Constants.GET_RES_SSR_TYPE_MEAL_BBML.equals(passengerSSR.getCode())) {
			passengerSSR.setInfantSSR(true);
			infantSSR.add(passengerSSR);
		}
		return passengerSSR;
	}

	private static List<SpecialRequest> getMappedADTAssocSSR(Map<String, List<SpecialRequest>> infantSSRMap,
			Map<String, Set<String>> infantsAssociatedMap, PassengerDetails paxDetails) {
		final StringBuilder builder = new StringBuilder();
		final String surname = paxDetails.getLastName();
		if (StringUtils.isNotBlank(surname)) {
			builder.append(surname);
		}
		final String forename = paxDetails.getFirstName();
		if (StringUtils.isNotBlank(forename)) {
			builder.append("/");
			builder.append(forename);
		}
		Optional<String> nameAssocId = infantsAssociatedMap.entrySet().stream()
				.filter(entry -> entry.getValue().contains(builder.toString())).map(Map.Entry::getKey).findFirst();

		if (nameAssocId.isPresent()) {
			return infantSSRMap.get(nameAssocId.get());
		}
		return null;

	}
}
