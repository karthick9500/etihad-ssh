package com.etihad.book.getreservationdata.schema;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Checkin baggage (checkin details)
 *
 */
public class CheckinBaggage {

	private int unitPiece;
	private int weightInKgs;
	private String description;
	private boolean isPiece;
	private String loyalty;
	
	

	/**
	 * Description of  allowed item
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**If weight in pieces(for US)
	 * @return if in pieces
	 */
	@JsonProperty("isPiece")
	public boolean isPiece() {
		return isPiece;
	}

	public void setPiece(boolean isPiece) {
		this.isPiece = isPiece;
	}

	/**
	 * @return loyalty
	 */
	public String getLoyalty() {
		return loyalty;
	}

	public void setLoyalty(String loyalty) {
		this.loyalty = loyalty;
	}

	public int getUnitPiece() {
		return unitPiece;
	}

	public void setUnitPiece(int unitPiece) {
		this.unitPiece = unitPiece;
	}

	public int getWeightInKgs() {
		return weightInKgs;
	}

	public void setWeightInKgs(int weightInKgs) {
		this.weightInKgs = weightInKgs;
	}

}
