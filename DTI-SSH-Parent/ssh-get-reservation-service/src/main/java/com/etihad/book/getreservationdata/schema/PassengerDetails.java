/**
 * 
 */
package com.etihad.book.getreservationdata.schema;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Danish
 *
 */
public class PassengerDetails {

	private String firstName;

	private String lastName;

	private String genderCode;

	private String title;

	private boolean isWithInfant;

	private String paxTypeDescription;

	private String paxTypeCode;

	private String seatNumber;

	private String seatTypeDescription;

	private String seatTypeCode;

	private String seatStatusCode;

	private boolean seatIndicator;

	private String loyaltyNumber;

	private String loyaltyReceiverCarrier;

	private String loyaltyTier;

	private String loyaltyTierCode;

	private String loyaltySupplierCarrier;

	private String loyaltyStatusCode;

	private List<SpecialRequest> specialRequests;

	private String emailAddress;

	private String phoneNumber;

	private String paxId;

	private String ticketNumber;

	private BaggageDetails baggageDetails;

	private boolean isPaidSeat;
	
	private boolean isInfant;
	
	private boolean isAddEditSSR;


	/**
	 * @return the isInfant
	 */
	@JsonProperty("isInfant")
	public boolean isInfant() {
		return isInfant;
	}

	/**
	 * @param isInfant the isInfant to set
	 */
	public void setInfant(boolean isInfant) {
		this.isInfant = isInfant;
	}

	/**
	 * @return if paid seat
	 */
	@JsonProperty("isPaidSeat")
	public boolean isPaidSeat() {
		return isPaidSeat;
	}

	public void setPaidSeat(boolean isPaidSeat) {
		this.isPaidSeat = isPaidSeat;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the genderCode
	 */
	public String getGenderCode() {
		return genderCode;
	}

	/**
	 * @param genderCode
	 *            the genderCode to set
	 */
	public void setGenderCode(String genderCode) {
		this.genderCode = genderCode;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the isWithInfant
	 */
	public boolean isWithInfant() {
		return isWithInfant;
	}

	/**
	 * @param isWithInfant
	 *            the isWithInfant to set
	 */
	public void setWithInfant(boolean isWithInfant) {
		this.isWithInfant = isWithInfant;
	}

	/**
	 * @return the paxTypeDescription
	 */
	public String getPaxTypeDescription() {
		return paxTypeDescription;
	}

	/**
	 * @param paxTypeDescription
	 *            the paxTypeDescription to set
	 */
	public void setPaxTypeDescription(String paxTypeDescription) {
		this.paxTypeDescription = paxTypeDescription;
	}

	/**
	 * @return the paxTypeCode
	 */
	public String getPaxTypeCode() {
		return paxTypeCode;
	}

	/**
	 * @param paxTypeCode
	 *            the paxTypeCode to set
	 */
	public void setPaxTypeCode(String paxTypeCode) {
		this.paxTypeCode = paxTypeCode;
	}

	/**
	 * @return the seatNumber
	 */
	public String getSeatNumber() {
		return seatNumber;
	}

	/**
	 * @param seatNumber
	 *            the seatNumber to set
	 */
	public void setSeatNumber(String seatNumber) {
		this.seatNumber = seatNumber;
	}

	/**
	 * @return the seatTypeDescription
	 */
	public String getSeatTypeDescription() {
		return seatTypeDescription;
	}

	/**
	 * @param seatTypeDescription
	 *            the seatTypeDescription to set
	 */
	public void setSeatTypeDescription(String seatTypeDescription) {
		this.seatTypeDescription = seatTypeDescription;
	}

	/**
	 * @return the seatTypeCode
	 */
	public String getSeatTypeCode() {
		return seatTypeCode;
	}

	/**
	 * @param seatTypeCode
	 *            the seatTypeCode to set
	 */
	public void setSeatTypeCode(String seatTypeCode) {
		this.seatTypeCode = seatTypeCode;
	}

	/**
	 * @return the seatStatusCode
	 */
	public String getSeatStatusCode() {
		return seatStatusCode;
	}

	/**
	 * @param seatStatusCode
	 *            the seatStatusCode to set
	 */
	public void setSeatStatusCode(String seatStatusCode) {
		this.seatStatusCode = seatStatusCode;
	}

	/**
	 * @return the seatIndicator
	 */
	public boolean isSeatIndicator() {
		return seatIndicator;
	}

	/**
	 * @param seatIndicator
	 *            the seatIndicator to set
	 */
	public void setSeatIndicator(boolean seatIndicator) {
		this.seatIndicator = seatIndicator;
	}

	/**
	 * @return the loyaltyNumber
	 */
	public String getLoyaltyNumber() {
		return loyaltyNumber;
	}

	/**
	 * @param loyaltyNumber
	 *            the loyaltyNumber to set
	 */
	public void setLoyaltyNumber(String loyaltyNumber) {
		this.loyaltyNumber = loyaltyNumber;
	}

	/**
	 * @return the loyaltyReceiverCarrier
	 */
	public String getLoyaltyReceiverCarrier() {
		return loyaltyReceiverCarrier;
	}

	/**
	 * @param loyaltyReceiverCarrier
	 *            the loyaltyReceiverCarrier to set
	 */
	public void setLoyaltyReceiverCarrier(String loyaltyReceiverCarrier) {
		this.loyaltyReceiverCarrier = loyaltyReceiverCarrier;
	}

	/**
	 * @return the loyaltyTier
	 */
	public String getLoyaltyTier() {
		return loyaltyTier;
	}

	/**
	 * @param loyaltyTier
	 *            the loyaltyTier to set
	 */
	public void setLoyaltyTier(String loyaltyTier) {
		this.loyaltyTier = loyaltyTier;
	}

	/**
	 * @return the loyaltyTierCode
	 */
	public String getLoyaltyTierCode() {
		return loyaltyTierCode;
	}

	/**
	 * @param loyaltyTierCode
	 *            the loyaltyTierCode to set
	 */
	public void setLoyaltyTierCode(String loyaltyTierCode) {
		this.loyaltyTierCode = loyaltyTierCode;
	}

	/**
	 * @return the loyaltySupplierCarrier
	 */
	public String getLoyaltySupplierCarrier() {
		return loyaltySupplierCarrier;
	}

	/**
	 * @param loyaltySupplierCarrier
	 *            the loyaltySupplierCarrier to set
	 */
	public void setLoyaltySupplierCarrier(String loyaltySupplierCarrier) {
		this.loyaltySupplierCarrier = loyaltySupplierCarrier;
	}

	/**
	 * @return the loyaltyStatusCode
	 */
	public String getLoyaltyStatusCode() {
		return loyaltyStatusCode;
	}

	/**
	 * @param loyaltyStatusCode
	 *            the loyaltyStatusCode to set
	 */
	public void setLoyaltyStatusCode(String loyaltyStatusCode) {
		this.loyaltyStatusCode = loyaltyStatusCode;
	}

	/**
	 * @return the specialRequests
	 */
	public List<SpecialRequest> getSpecialRequests() {
		return specialRequests;
	}

	/**
	 * @param specialRequests
	 *            the specialRequests to set
	 */
	public void setSpecialRequests(List<SpecialRequest> specialRequests) {
		this.specialRequests = specialRequests;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber
	 *            the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the paxId
	 */
	public String getPaxId() {
		return paxId;
	}

	/**
	 * @param paxId
	 *            the paxId to set
	 */
	public void setPaxId(String paxId) {
		this.paxId = paxId;
	}

	/**
	 * @return the ticketNumber
	 */
	public String getTicketNumber() {
		return ticketNumber;
	}

	/**
	 * @param ticketNumber
	 *            the ticketNumber to set
	 */
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	/**
	 * @return the entire baggage details
	 */
	public BaggageDetails getBaggageDetails() {
		return baggageDetails;
	}

	public void setBaggageDetails(BaggageDetails baggageDetails) {
		this.baggageDetails = baggageDetails;
	}
	/**
	 * If all ssr can be changed or added
	 * 
	 * @return the isEditable
	 */
	@JsonProperty(value = "isAddEditSSR")
	public boolean isAddEditSSR() {
		return isAddEditSSR;
	}

	public void setAddEditSSR(boolean isAddEditSSR) {
		this.isAddEditSSR = isAddEditSSR;
	}

	
}
