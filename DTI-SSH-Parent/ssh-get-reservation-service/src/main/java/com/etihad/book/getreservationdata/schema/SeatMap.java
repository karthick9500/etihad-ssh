/**
 * 
 */
package com.etihad.book.getreservationdata.schema;

import java.util.Map;

/**
 * @author Danish
 *
 */
public class SeatMap {
	
	private Map<String,String> flightSeatMap;
	
	private String sequenceCount;

	/**
	 * @return the flightSeatMap
	 */
	public Map<String, String> getFlightSeatMap() {
		return flightSeatMap;
	}

	/**
	 * @param flightSeatMap the flightSeatMap to set
	 */
	public void setFlightSeatMap(Map<String, String> flightSeatMap) {
		this.flightSeatMap = flightSeatMap;
	}

	/**
	 * @return the sequenceCount
	 */
	public String getSequenceCount() {
		return sequenceCount;
	}

	/**
	 * @param sequenceCount the sequenceCount to set
	 */
	public void setSequenceCount(String sequenceCount) {
		this.sequenceCount = sequenceCount;
	}
	
	

}
