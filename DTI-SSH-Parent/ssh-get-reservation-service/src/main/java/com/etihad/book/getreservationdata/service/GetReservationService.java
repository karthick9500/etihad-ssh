/**
 * 
 */
package com.etihad.book.getreservationdata.service;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.getreservationdata.schema.GetReservationRequest;
import com.etihad.book.getreservationdata.schema.GetReservationResponse;

/**
 * @author CTS
 *
 */
public interface GetReservationService 
{
	public GetReservationResponse getReservationData(GetReservationRequest request)  throws ApplicationException ;
}