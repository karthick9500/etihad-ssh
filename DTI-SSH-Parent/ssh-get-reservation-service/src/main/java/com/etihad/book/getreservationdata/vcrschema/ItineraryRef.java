package com.etihad.book.getreservationdata.vcrschema;

public class ItineraryRef {

	private String id;
	private String sourceCreateDateTime;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the sourceCreateDateTime
	 */
	public String getSourceCreateDateTime() {
		return sourceCreateDateTime;
	}
	/**
	 * @param sourceCreateDateTime the sourceCreateDateTime to set
	 */
	public void setSourceCreateDateTime(String sourceCreateDateTime) {
		this.sourceCreateDateTime = sourceCreateDateTime;
	}
}
