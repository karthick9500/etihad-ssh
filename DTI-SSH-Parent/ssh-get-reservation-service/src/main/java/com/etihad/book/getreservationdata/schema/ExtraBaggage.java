package com.etihad.book.getreservationdata.schema;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExtraBaggage {

	private String code;
	private String rawDescription;
	private boolean isPiece;
	private String description;
	private String unitPiece;

	public String getRawDescription() {
		return rawDescription;
	}

	public void setRawDescription(String rawDescription) {
		this.rawDescription = rawDescription;
	}
    @JsonProperty("isPiece")
	public boolean isPiece() {
		return isPiece;
	}

	public void setPiece(boolean isPiece) {
		this.isPiece = isPiece;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUnitPiece() {
		return unitPiece;
	}

	public void setUnitPiece(String unitPiece) {
		this.unitPiece = unitPiece;
	}

}
