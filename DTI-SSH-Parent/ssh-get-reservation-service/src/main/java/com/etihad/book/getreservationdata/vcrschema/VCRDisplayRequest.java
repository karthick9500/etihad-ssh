/**
 * 
 */
package com.etihad.book.getreservationdata.vcrschema;

import com.etihad.book.schema.common.AbstractRequest;

/**
 * @author CTS
 *
 */
public class VCRDisplayRequest extends AbstractRequest {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SearchParameters searchParameters;
	
	public SearchParameters getSearchParameters() {
		return searchParameters;
	}
	public void setSearchParameters(SearchParameters searchParameters) {
		this.searchParameters = searchParameters;
	}
}
