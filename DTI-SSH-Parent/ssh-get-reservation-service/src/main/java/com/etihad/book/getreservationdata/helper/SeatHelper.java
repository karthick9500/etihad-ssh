
/**
 * 
 */
package com.etihad.book.getreservationdata.helper;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.etihad.book.getreservationdata.constants.Constants;
import com.etihad.book.getreservationdata.constants.SEAT_TYPE_ENUM;
import com.sabre.webservices.pnrbuilder.v1_17.AncillaryServicesPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB.AncillaryServices;

/**
 * @author 526837
 *
 */
public class SeatHelper {

	public static SEAT_TYPE_ENUM getSeatsDescription(String seatNumber, PassengerPNRB passenger) {

		SEAT_TYPE_ENUM seatDescription = SEAT_TYPE_ENUM.STANDARD_SEAT;
		AncillaryServices ancillaryServices = passenger.getAncillaryServices();
		if (null != ancillaryServices) {
			List<AncillaryServicesPNRB> ancillaryServiceList = ancillaryServices.getAncillaryService();
			if (null != ancillaryServiceList && !ancillaryServiceList.isEmpty()) {
				AncillaryServicesPNRB ancillary = ancillaryServiceList.stream()
						.filter(ancillaryService -> StringUtils.isNotBlank(ancillaryService.getPdcSeat()))
						.filter(ancillaryService -> StringUtils.stripStart(ancillaryService.getPdcSeat(), "0")
								.equals(seatNumber))
						.findAny().orElse(null);
				if (null != ancillary) {
					if (Constants.PREMIUM_SEAT_CODE.equals(ancillary.getRficSubcode())) {
						seatDescription = SEAT_TYPE_ENUM.PAID_SEAT;
					}
				}
			}
		}

		return seatDescription;
	}

}
