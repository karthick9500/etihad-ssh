package com.etihad.book.getreservationdata.constants;

import java.util.Arrays;
import java.util.List;

public enum BAGGAGE_LOYALTY {

	SILVER(Arrays.asList("NS", "MS", "HS","2"), 10), GOLD(Arrays.asList("NG", "MG", "HG","3"),
			15), PLATINUM(Arrays.asList("GP", "HE","4"), 20);

	private BAGGAGE_LOYALTY(List<String> loyaltyCodes, int weightInKgs) {
		this.loyaltyCodes = loyaltyCodes;
		this.weightInKgs = weightInKgs;
	}

	private List<String> loyaltyCodes;
	private int weightInKgs;

	public List<String> getLoyaltyCodes() {
		return loyaltyCodes;
	}

	public void setLoyaltyCodes(List<String> loyaltyCodes) {
		this.loyaltyCodes = loyaltyCodes;
	}

	public int getWeightInKgs() {
		return weightInKgs;
	}

	public void setWeightInKgs(int weightInKgs) {
		this.weightInKgs = weightInKgs;
	}

	public static int getValfromString(String text) {
		int weightInKgs = 0;
		for (BAGGAGE_LOYALTY b : BAGGAGE_LOYALTY.values()) {
			if (b.getLoyaltyCodes().contains(text)) {
				return b.weightInKgs;
			}
		}
		return weightInKgs;
	}

}
