/**
 * 
 */
package com.etihad.book.getreservationdata.broker;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.sabre.webservices.pnrbuilder.v1_17.GetReservationRS;


/**
 * This is the implementation class of Retrieve PNR broker which calls the
 * service.
 * 
 * @author CTS
 * 
 */
@Component
public class GetReservationMockBroker {

	@Autowired
	private Utils utils;
	
	@Autowired
	private Config config;

	public GetReservationRS processGetReservationData() throws ApplicationException {

		GetReservationRS soapResponse = null;
		List<String> errorCodes = new ArrayList<>();
		JAXBContext context;
		javax.xml.bind.Unmarshaller unmarshaller = null;
		
		try{
			context = JAXBContext.newInstance(GetReservationRS.class);
			unmarshaller = context.createUnmarshaller();
			Resource resource = new ClassPathResource("GetReservation1.xml");	
			soapResponse = (GetReservationRS) unmarshaller.unmarshal(resource.getFile());
		} 
		catch(Exception e){
			errorCodes.add("pnrretrieval.102");
			throw new ApplicationException(utils.populateResponseHeader(errorCodes));
		}
		return soapResponse;
	}


}