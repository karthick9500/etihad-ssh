/**
 * 
 */
package com.etihad.book.getreservationdata.helper;

import java.util.List;

import com.etihad.book.getreservationdata.schema.FlightDetails;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.ReservationPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment;

/**
 * @author 526837
 *
 */
public class BookingHelper {

	public static Boolean checkRoundTrip(ReservationPNRB reservation) {

		boolean isRoundTrip = false;
		if (null != reservation) {
			PassengerReservationPNRB passengerReservation = reservation.getPassengerReservation();
			if (null != passengerReservation) {
				SegmentTypePNRB segments = passengerReservation.getSegments();
				if (null != segments) {
					List<Segment> segmentList = segments.getSegment();
					String poc = segments.getPoc().getAirport();
					if (null != segmentList && !segmentList.isEmpty()) {
						for (Segment segment : segmentList) {
							if (null != segment) {
								com.sabre.webservices.pnrbuilder.v1_17.SegmentTypePNRB.Segment.Air air = segment
										.getAir();
								if (null != air) {
									if (poc.equals(air.getArrivalAirport())) {
										isRoundTrip = true;
									}
								}
							}
						}
					}
				}
			}
		}
		return isRoundTrip;
	}

	public static String getOriginAirportCodeForBookingSummary(List<FlightDetails> flightDetailsList) {

		String originAirportCode = null;
		for (FlightDetails flightDetails : flightDetailsList) {
			originAirportCode = flightDetails.getPoc();
		}
		return originAirportCode;
	}

	public static String getDestinationAirportCodeForBookingSummary(List<FlightDetails> flightDetailsList,
			Boolean isRoundTrip) {

		String destinationAirportCode = null;
		if (isRoundTrip) {
			destinationAirportCode = flightDetailsList.get((flightDetailsList.size() / 2) - 1)
					.getDestinationAirportCode();
		} else {
			destinationAirportCode = flightDetailsList.get(flightDetailsList.size() - 1).getDestinationAirportCode();
		}

		return destinationAirportCode;
	}
}
