/**
 * 
 */
package com.etihad.book.getreservationdata.adaptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.etihad.book.getreservationdata.constants.SEAT_TYPE_ENUM;
import com.etihad.book.getreservationdata.helper.SeatHelper;
import com.etihad.book.getreservationdata.schema.PassengerDetails;
import com.sabre.webservices.pnrbuilder.v1_17.PassengerPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PreReservedSeatPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.PreReservedSeatsPNRB;
import com.sabre.webservices.pnrbuilder.v1_17.SeatsPNRB;

/**
 * Seat adaptor
 *
 */
public class SeatAdaptor {

	public static Map<String, String> setSeatsOfFlight(Short sequence, SeatsPNRB seats) {

		Map<String, String> flightSeatMap = new HashMap<>();
		PreReservedSeatsPNRB preReservedSeats = seats.getPreReservedSeats();
		if (null != preReservedSeats) {
			List<PreReservedSeatPNRB> preReservedSeatList = preReservedSeats.getPreReservedSeat();
			if (null != preReservedSeatList && !preReservedSeatList.isEmpty()) {
				for (PreReservedSeatPNRB preReservedSeat : preReservedSeatList) {
					String seatSequence = sequence + "" + preReservedSeat.getSeatNumber();
					flightSeatMap.put(seatSequence, preReservedSeat.getId());
				}
			}
		}
		return flightSeatMap;
	}

	public static void setSeat(final PassengerDetails paxDetails, final PassengerPNRB passenger,
			final PreReservedSeatPNRB seatObject) {
		final String seatNumber = seatObject.getSeatNumber();
		final String seatWtZeroes = StringUtils.isNotBlank(seatNumber) ? StringUtils.stripStart(seatNumber, "0") : null;
		paxDetails.setSeatNumber(seatWtZeroes);
		paxDetails.setSeatStatusCode(seatObject.getSeatStatusCode());
		SEAT_TYPE_ENUM seatType = null != seatWtZeroes ? SeatHelper.getSeatsDescription(seatWtZeroes, passenger)
				: SEAT_TYPE_ENUM.NONE;
		paxDetails.setSeatTypeDescription(seatType.getSeatTypeDescription());
		final String seatTypeCode = seatType == SEAT_TYPE_ENUM.STANDARD_SEAT ? "S" : seatObject.getSeatTypeCode();
		paxDetails.setSeatTypeCode(seatTypeCode);
		paxDetails.setPaidSeat(seatType.isPaidSeat());
	}

	public static void setSeatTypeIndicator(PassengerDetails paxDetails, final boolean isInfantonLap,
			final boolean isEYOperated) {
		boolean seatIndicator = false;
		if (isEYOperated) {
			seatIndicator = true;
		}
		if (isInfantonLap) {
			seatIndicator = false;
		}
		// TBD 3 more cases
		paxDetails.setSeatIndicator(seatIndicator);
	}
}
