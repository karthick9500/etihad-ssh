/**
 * 
 */
package com.etihad.book.getreservationdata.vcrschema;

import java.util.List;

/**
 * @author 641852
 *
 */
public class TicketDataInfo {

	private String issueDate;
	private String printStation;
	private List<String> endorsementsText;
	private String exchangeDataLocation;
	/**
	 * @return the issueDate
	 */
	public String getIssueDate() {
		return issueDate;
	}
	/**
	 * @param issueDate the issueDate to set
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	
	/**
	 * @return the printStation
	 */
	public String getPrintStation() {
		return printStation;
	}
	/**
	 * @param printStation the printStation to set
	 */
	public void setPrintStation(String printStation) {
		this.printStation = printStation;
	}

	/**
	 * @return the exchangeDataLocation
	 */
	public String getExchangeDataLocation() {
		return exchangeDataLocation;
	}
	/**
	 * @param exchangeDataLocation the exchangeDataLocation to set
	 */
	public void setExchangeDataLocation(String exchangeDataLocation) {
		this.exchangeDataLocation = exchangeDataLocation;
	}
	/**
	 * @return the endorsementsText
	 */
	public List<String> getEndorsementsText() {
		return endorsementsText;
	}
	/**
	 * @param endorsementsText the endorsementsText to set
	 */
	public void setEndorsementsText(List<String> endorsementsText) {
		this.endorsementsText = endorsementsText;
	}
}
