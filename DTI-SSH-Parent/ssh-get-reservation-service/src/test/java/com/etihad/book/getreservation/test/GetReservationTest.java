/**
 * 
 */
package com.etihad.book.getreservation.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.getreservationdata.controller.GetReservationController;
import com.etihad.book.getreservationdata.schema.BookingDetails;
import com.etihad.book.getreservationdata.schema.GetReservationRequest;
import com.etihad.book.getreservationdata.schema.GetReservationResponse;
import com.etihad.book.getreservationdata.schema.SearchParameters;
import com.etihad.book.getreservationdata.service.GetReservationServiceImpl;
import com.etihad.book.schema.common.Message;
import com.etihad.book.schema.common.Messages;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Danish
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GetReservationTest {
	
	
	private MockMvc mockMvc;

	@MockBean
	GetReservationServiceImpl getReservationService;

	@InjectMocks
	private GetReservationController getReservationController;

	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private Config config;

	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(getReservationController).build();

	}

	@Test
	public void testControllerForRetrievePNR() throws Exception {

		Mockito.when(getReservationService.getReservationData(Mockito.anyObject()))
				.thenReturn(formTestResponseForGetReservationData());

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/service/v1/get-reservation")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(fromRetrievePNRRequest()))
				.accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content()
						.string(objectMapper.writeValueAsString(formTestResponseForGetReservationData())))
				.andExpect(jsonPath("$.messages", notNullValue()))
				.andExpect(jsonPath("$.bookingDetails.bookingReference", is("BZJHCS")))
				.andExpect(jsonPath("$.bookingDetails.originCity", is("Abu Dhabi")))
				.andExpect(jsonPath("$.bookingDetails.originCityCode", is("AUH")))
				.andExpect(jsonPath("$.bookingDetails.originCountry", is("United Arab Emirates")))
				.andExpect(jsonPath("$.bookingDetails.destinationCity", is("Los Angeles CA")))
				.andExpect(jsonPath("$.bookingDetails.destinationCountry", is("United States of America")))
				.andExpect(jsonPath("$.bookingDetails.destinationCityCode", is("LAX")))
				.andExpect(jsonPath("$.bookingDetails.journeyStartDate", is("2018-07-14T09:55:00")))
				.andExpect(jsonPath("$.bookingDetails.journeyEndDate", is("2018-07-30T20:05:00")))
				.andExpect(jsonPath("$.bookingDetails.primaryPaxLastName", is("RAJA")))
				.andExpect(jsonPath("$.bookingDetails.primaryPaxFirstName", is("GANESH")))
				.andExpect(jsonPath("$.bookingDetails.primaryPaxTitle", is("Mr.")))
				.andExpect(jsonPath("$.messages.message[0].code", is("0")))
				.andExpect(jsonPath("$.messages.message[0].text", is("Success")));

	}
	
	private  GetReservationRequest fromRetrievePNRRequest() {
		
		final GetReservationRequest req = new GetReservationRequest(); 
		SearchParameters searchParams = new SearchParameters();
		searchParams.setLastName(config.getProperty("tripsearch.lastName"));
		searchParams.setPnr(config.getProperty("tripsearch.pnr"));
		searchParams.setBinarySecurityToken("Shared/IDL:IceSess\\/SessMgr:1\\.0.IDL/Common/!ICESMS\\/CERTG!ICESMSLB\\/CRT.LB!-3170940443955548277!1507542!0");
		req.setSearchParameters(searchParams);
		return req;
	}

	

	private GetReservationResponse formTestResponseForGetReservationData() {
		
		final GetReservationResponse response = new GetReservationResponse();
		
		final Messages messages = new Messages();
		final List<Message> msg = new ArrayList<>();
		final Message message = new Message();
		message.setCode("0");
		message.setText("Success");
		msg.add(message);
		messages.setErrorMessages(msg);
		
		final BookingDetails bookingDetails = new BookingDetails();
		bookingDetails.setBookingReference("BZJHCS");
		bookingDetails.setDestinationCity("Los Angeles CA");
		bookingDetails.setDestinationCityCode("LAX");
		bookingDetails.setDestinationCountry("United States of America");
		bookingDetails.setJourneyEndDate("2018-07-30T20:05:00");
		bookingDetails.setJourneyStartDate("2018-07-14T09:55:00");
		bookingDetails.setOriginCity("Abu Dhabi");
		bookingDetails.setOriginCityCode("AUH");
		bookingDetails.setOriginCountry("United Arab Emirates");
		bookingDetails.setPrimaryPaxFirstName("GANESH");
		bookingDetails.setPrimaryPaxLastName("RAJA");
		bookingDetails.setPrimaryPaxTitle("Mr.");
		response.setMessages(messages);
		response.setBookingDetails(bookingDetails);
		return response;
	}
	
	

}
