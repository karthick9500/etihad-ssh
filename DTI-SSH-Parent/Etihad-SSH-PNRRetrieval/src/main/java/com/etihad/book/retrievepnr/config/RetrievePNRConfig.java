/**
 * 
 */
package com.etihad.book.retrievepnr.config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import com.etihad.book.common.configuration.Config;


/**
 * @author CTS
 *
 */
@Configuration
public class RetrievePNRConfig {
	
	@Autowired
	private Config config;
	
	/**
	 * This method is used to create Proxy to connect with Sabre for Trip Search
	 * @return
	 * @throws MalformedURLException
	 */
	@Bean(name = "tripSearchProxy")
    public JaxWsPortProxyFactoryBean createProxy() throws MalformedURLException {
        JaxWsPortProxyFactoryBean bean = new JaxWsPortProxyFactoryBean();
            bean.setServiceInterface(https.webservices_sabre_com.websvc.TripSearchPortType.class);
            InputStream in = null;
            OutputStream out = null;
            try {
            	URL url = null;
                in = getClass().getResourceAsStream(config.getProperty("tripSearch.WSDL.location")); 
                File file = File.createTempFile(config.getProperty("tripSearch.tempfile"), ".tmp");
                out = new FileOutputStream(file);
                int read;
                byte[] bytes = new byte[1024];

                while ((read = in.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
        		url = file.toURI().toURL();
        		bean.setWsdlDocumentUrl(url);
                bean.setNamespaceUri(config.getProperty("tripSearch.namespace"));
                bean.setServiceName(config.getProperty("tripSearch.serviceName"));
                bean.setPortName(config.getProperty("tripSearch.portName"));
                bean.setEndpointAddress(config.getProperty("tripSearch.endpointAddress"));
                bean.setLookupServiceOnStartup(false);
                file.deleteOnExit();
            }
            catch(Exception e)
            {
            	e.printStackTrace();
            }
            finally 
            {
        		if (in != null)
        		{
        			try 
        			{
        				in.close();
        			} catch (IOException e) 
        			{
        				e.printStackTrace();
        			}
        		}
        		if (out != null) 
        		{
        			try 
        			{
        				out.close();
        			} catch (IOException e) 
        			{
        				e.printStackTrace();
        			}
        		}
            }
        return bean;
    }
}