/**
 * 
 */
package com.etihad.book.retrievepnr.service;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.retrievepnr.schema.RetrievePNRRequest;
import com.etihad.book.retrievepnr.schema.RetrievePNRResponse;

/**
 * @author CTS
 *
 */
public interface RetrievePNRService 
{
	public RetrievePNRResponse retrievePNR(RetrievePNRRequest request)  throws ApplicationException ;
}