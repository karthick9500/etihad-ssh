package com.etihad.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author CTS
 *
 */
@SpringBootApplication
public class RetrievePNRApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetrievePNRApplication.class, args);
	}
}
