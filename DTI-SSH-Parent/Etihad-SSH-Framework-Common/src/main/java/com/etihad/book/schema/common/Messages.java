package com.etihad.book.schema.common;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Danish
 *
 */
public class Messages implements Serializable {

	private static final long serialVersionUID = 6652977396829460169L;
	private long count;
	private long code;
	private List<Message> message;

	/**
	 * @return the count
	 */
	public long getCount() {
		return count;
	}

	/**
	 * @return the code
	 */
	public long getCode() {
		return code;
	}

	/**
	 * @return the message
	 */
	public List<Message> getMessage() {
		return message;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(long count) {
		this.count = count;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(long code) {
		this.code = code;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(List<Message> message) {
		this.message = message;
	}

	public void setErrorMessages(List<Message> messages){
		this.setCode(1);
		this.setCount(messages.size());
		this.setMessage(messages);
	}
	public void setSuccessMessages(List<Message> messages){
		this.setCode(0);
		this.setCount(messages.size());
		this.setMessage(messages);
	}
}
