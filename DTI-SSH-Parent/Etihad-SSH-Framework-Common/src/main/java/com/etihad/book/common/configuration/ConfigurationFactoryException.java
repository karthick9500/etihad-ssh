package com.etihad.book.common.configuration;

public class ConfigurationFactoryException extends RuntimeException {

	private static final long serialVersionUID = -7927342910009417339L;

	public ConfigurationFactoryException(String message) {
		super(message);
	}

	public ConfigurationFactoryException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConfigurationFactoryException(Throwable cause) {
		super(cause);
	}
}
