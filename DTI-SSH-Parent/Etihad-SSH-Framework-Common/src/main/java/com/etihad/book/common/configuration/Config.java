/**
 * 
 */
package com.etihad.book.common.configuration;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.etihad.book.common.util.CoreConstants;

/**
 * 
 * @author Danish
 *
 */
@Component
public class Config {

	private static Configuration cConfiguration = ConfigurationFactory.getInstance()
			.getConfiguration(CoreConstants.PROPERTIES_FILE_PATH);

	public String getProperty(String key) {
		return (String) (cConfiguration.getProperty(key) != null ? cConfiguration.getProperty(key) : key);
	}

	public String getPropertyForEmail(String key) {
		return (String) (cConfiguration.getProperty(key) != null ? cConfiguration.getProperty(key) : null);
	}

	@SuppressWarnings("unchecked")
	public List<String> getPropertyAsList(String key) {
		List<String> valueList = new ArrayList<>();
		if (!StringUtils.isEmpty(cConfiguration.getProperty(key))) {
			valueList = (List<String>) cConfiguration.getProperty(key);
		}
		return valueList;
	}

	public boolean exists(String key) {
		return cConfiguration.getProperty(key) != null ? true : false;
	}

}
