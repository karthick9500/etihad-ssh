package com.etihad.book.common.exception;

import com.etihad.book.schema.common.Messages;
import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * 
 * @author Danish
 *
 */

public class ApplicationException extends Exception {

	private static final long serialVersionUID = -7510570940690977175L;
	private static final ObjectMapper mapper = new ObjectMapper();

	private Messages messages;
	
	private String errorMsg;

	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public ApplicationException(Messages messages) {
		this.messages = messages;
	}

	/**
	 * @return the messages
	 */
	public Messages getMessages() {
		return messages;
	}

	@Override
	public String toString() {
		try {
			return mapper.writeValueAsString(this.getMessages());
		} catch (Exception e) {
			return null;
		}
	}

}
