
package com.sabre.webservices.sabrexml._2011._10;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sabre.webservices.sabrexml._2011._10 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sabre.webservices.sabrexml._2011._10
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VCRDisplayRS }
     * 
     */
    public VCRDisplayRS createVCRDisplayRS() {
        return new VCRDisplayRS();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ }
     * 
     */
    public VCRDisplayRQ createVCRDisplayRQ() {
        return new VCRDisplayRQ();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SimilarNameList }
     * 
     */
    public VCRDisplayRQ.SimilarNameList createVCRDisplayRQSimilarNameList() {
        return new VCRDisplayRQ.SimilarNameList();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions }
     * 
     */
    public VCRDisplayRQ.SearchOptions createVCRDisplayRQSearchOptions() {
        return new VCRDisplayRQ.SearchOptions();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.Ticketing }
     * 
     */
    public VCRDisplayRQ.SearchOptions.Ticketing createVCRDisplayRQSearchOptionsTicketing() {
        return new VCRDisplayRQ.SearchOptions.Ticketing();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.PaymentCardDateName }
     * 
     */
    public VCRDisplayRQ.SearchOptions.PaymentCardDateName createVCRDisplayRQSearchOptionsPaymentCardDateName() {
        return new VCRDisplayRQ.SearchOptions.PaymentCardDateName();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.FOID }
     * 
     */
    public VCRDisplayRQ.SearchOptions.FOID createVCRDisplayRQSearchOptionsFOID() {
        return new VCRDisplayRQ.SearchOptions.FOID();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.FlightDateName }
     * 
     */
    public VCRDisplayRQ.SearchOptions.FlightDateName createVCRDisplayRQSearchOptionsFlightDateName() {
        return new VCRDisplayRQ.SearchOptions.FlightDateName();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment }
     * 
     */
    public VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment createVCRDisplayRQSearchOptionsFlightDateNameFlightSegment() {
        return new VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.CustLoyaltyName }
     * 
     */
    public VCRDisplayRQ.SearchOptions.CustLoyaltyName createVCRDisplayRQSearchOptionsCustLoyaltyName() {
        return new VCRDisplayRQ.SearchOptions.CustLoyaltyName();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.CustLoyaltyName.FlightSegment }
     * 
     */
    public VCRDisplayRQ.SearchOptions.CustLoyaltyName.FlightSegment createVCRDisplayRQSearchOptionsCustLoyaltyNameFlightSegment() {
        return new VCRDisplayRQ.SearchOptions.CustLoyaltyName.FlightSegment();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.CityDateName }
     * 
     */
    public VCRDisplayRQ.SearchOptions.CityDateName createVCRDisplayRQSearchOptionsCityDateName() {
        return new VCRDisplayRQ.SearchOptions.CityDateName();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment }
     * 
     */
    public VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment createVCRDisplayRQSearchOptionsCityDateNameFlightSegment() {
        return new VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos }
     * 
     */
    public VCRDisplayRS.TicketingInfos createVCRDisplayRSTicketingInfos() {
        return new VCRDisplayRS.TicketingInfos();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo createVCRDisplayRSTicketingInfosTicketingInfo() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing createVCRDisplayRSTicketingInfosTicketingInfoTicketing() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketData() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataPaymentInfo() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataPaymentInfoPayment() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataPaymentInfoPaymentCCInfo() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataExchangeData() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef createVCRDisplayRSTicketingInfosTicketingInfoTicketingItineraryRef() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo createVCRDisplayRSTicketingInfosTicketingInfoTicketingCustomerInfo() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo createVCRDisplayRSTicketingInfosTicketingInfoTicketingCustomerInfoPaymentInfo() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData createVCRDisplayRSTicketingInfosTicketingInfoCouponData() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCoupons() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCoupon() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCouponFlownFlightSegment() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCouponFlightSegment() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo createVCRDisplayRSTicketingInfosTicketingInfoCouponDataAirItineraryPricingInfo() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown createVCRDisplayRSTicketingInfosTicketingInfoCouponDataAirItineraryPricingInfoPTCFareBreakdown() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare createVCRDisplayRSTicketingInfosTicketingInfoCouponDataAirItineraryPricingInfoItinTotalFare() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes createVCRDisplayRSTicketingInfosTicketingInfoCouponDataAirItineraryPricingInfoItinTotalFareTaxes() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.SimilarNameList }
     * 
     */
    public VCRDisplayRS.TicketingInfos.SimilarNameList createVCRDisplayRSTicketingInfosSimilarNameList() {
        return new VCRDisplayRS.TicketingInfos.SimilarNameList();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon }
     * 
     */
    public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon createVCRDisplayRSTicketingInfosSimilarNameListCoupon() {
        return new VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment }
     * 
     */
    public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment createVCRDisplayRSTicketingInfosSimilarNameListCouponFlightSegment() {
        return new VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SimilarNameList.Line }
     * 
     */
    public VCRDisplayRQ.SimilarNameList.Line createVCRDisplayRQSimilarNameListLine() {
        return new VCRDisplayRQ.SimilarNameList.Line();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.Ticketing.OperatingAirline }
     * 
     */
    public VCRDisplayRQ.SearchOptions.Ticketing.OperatingAirline createVCRDisplayRQSearchOptionsTicketingOperatingAirline() {
        return new VCRDisplayRQ.SearchOptions.Ticketing.OperatingAirline();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.PaymentCardDateName.FlightSegment }
     * 
     */
    public VCRDisplayRQ.SearchOptions.PaymentCardDateName.FlightSegment createVCRDisplayRQSearchOptionsPaymentCardDateNameFlightSegment() {
        return new VCRDisplayRQ.SearchOptions.PaymentCardDateName.FlightSegment();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.PaymentCardDateName.PaymentCard }
     * 
     */
    public VCRDisplayRQ.SearchOptions.PaymentCardDateName.PaymentCard createVCRDisplayRQSearchOptionsPaymentCardDateNamePaymentCard() {
        return new VCRDisplayRQ.SearchOptions.PaymentCardDateName.PaymentCard();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.PaymentCardDateName.PersonName }
     * 
     */
    public VCRDisplayRQ.SearchOptions.PaymentCardDateName.PersonName createVCRDisplayRQSearchOptionsPaymentCardDateNamePersonName() {
        return new VCRDisplayRQ.SearchOptions.PaymentCardDateName.PersonName();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.FOID.FlightSegment }
     * 
     */
    public VCRDisplayRQ.SearchOptions.FOID.FlightSegment createVCRDisplayRQSearchOptionsFOIDFlightSegment() {
        return new VCRDisplayRQ.SearchOptions.FOID.FlightSegment();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.FlightDateName.PersonName }
     * 
     */
    public VCRDisplayRQ.SearchOptions.FlightDateName.PersonName createVCRDisplayRQSearchOptionsFlightDateNamePersonName() {
        return new VCRDisplayRQ.SearchOptions.FlightDateName.PersonName();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.DestinationLocation }
     * 
     */
    public VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.DestinationLocation createVCRDisplayRQSearchOptionsFlightDateNameFlightSegmentDestinationLocation() {
        return new VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.DestinationLocation();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.MarketingAirline }
     * 
     */
    public VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.MarketingAirline createVCRDisplayRQSearchOptionsFlightDateNameFlightSegmentMarketingAirline() {
        return new VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.MarketingAirline();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.OperatingAirline }
     * 
     */
    public VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.OperatingAirline createVCRDisplayRQSearchOptionsFlightDateNameFlightSegmentOperatingAirline() {
        return new VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.OperatingAirline();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.OriginLocation }
     * 
     */
    public VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.OriginLocation createVCRDisplayRQSearchOptionsFlightDateNameFlightSegmentOriginLocation() {
        return new VCRDisplayRQ.SearchOptions.FlightDateName.FlightSegment.OriginLocation();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.CustLoyaltyName.CustLoyalty }
     * 
     */
    public VCRDisplayRQ.SearchOptions.CustLoyaltyName.CustLoyalty createVCRDisplayRQSearchOptionsCustLoyaltyNameCustLoyalty() {
        return new VCRDisplayRQ.SearchOptions.CustLoyaltyName.CustLoyalty();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.CustLoyaltyName.PersonName }
     * 
     */
    public VCRDisplayRQ.SearchOptions.CustLoyaltyName.PersonName createVCRDisplayRQSearchOptionsCustLoyaltyNamePersonName() {
        return new VCRDisplayRQ.SearchOptions.CustLoyaltyName.PersonName();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.CustLoyaltyName.FlightSegment.OperatingAirline }
     * 
     */
    public VCRDisplayRQ.SearchOptions.CustLoyaltyName.FlightSegment.OperatingAirline createVCRDisplayRQSearchOptionsCustLoyaltyNameFlightSegmentOperatingAirline() {
        return new VCRDisplayRQ.SearchOptions.CustLoyaltyName.FlightSegment.OperatingAirline();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.CityDateName.PersonName }
     * 
     */
    public VCRDisplayRQ.SearchOptions.CityDateName.PersonName createVCRDisplayRQSearchOptionsCityDateNamePersonName() {
        return new VCRDisplayRQ.SearchOptions.CityDateName.PersonName();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment.DestinationLocation }
     * 
     */
    public VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment.DestinationLocation createVCRDisplayRQSearchOptionsCityDateNameFlightSegmentDestinationLocation() {
        return new VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment.DestinationLocation();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment.OperatingAirline }
     * 
     */
    public VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment.OperatingAirline createVCRDisplayRQSearchOptionsCityDateNameFlightSegmentOperatingAirline() {
        return new VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment.OperatingAirline();
    }

    /**
     * Create an instance of {@link VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment.OriginLocation }
     * 
     */
    public VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment.OriginLocation createVCRDisplayRQSearchOptionsCityDateNameFlightSegmentOriginLocation() {
        return new VCRDisplayRQ.SearchOptions.CityDateName.FlightSegment.OriginLocation();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.DynamicCurrency }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.DynamicCurrency createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataDynamicCurrency() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.DynamicCurrency();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Endorsements }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Endorsements createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataEndorsements() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Endorsements();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ManualRemarks }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ManualRemarks createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataManualRemarks() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ManualRemarks();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Restrictions }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Restrictions createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataRestrictions() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Restrictions();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.TourInfo }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.TourInfo createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataTourInfo() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.TourInfo();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.MaskedPaymentCard }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.MaskedPaymentCard createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataPaymentInfoPaymentCCInfoMaskedPaymentCard() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.MaskedPaymentCard();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.PaymentCard }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.PaymentCard createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataPaymentInfoPaymentCCInfoPaymentCard() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.PaymentCard();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.AdditionalExchangedTicket }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.AdditionalExchangedTicket createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataExchangeDataAdditionalExchangedTicket() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.AdditionalExchangedTicket();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.EquivalentAmountPaid }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.EquivalentAmountPaid createVCRDisplayRSTicketingInfosTicketingInfoTicketingTicketDataExchangeDataEquivalentAmountPaid() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.EquivalentAmountPaid();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef.Source }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef.Source createVCRDisplayRSTicketingInfosTicketingInfoTicketingItineraryRefSource() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef.Source();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.InvoluntaryInfo }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.InvoluntaryInfo createVCRDisplayRSTicketingInfosTicketingInfoTicketingCustomerInfoInvoluntaryInfo() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.InvoluntaryInfo();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PersonName }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PersonName createVCRDisplayRSTicketingInfosTicketingInfoTicketingCustomerInfoPersonName() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PersonName();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo.Payment }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo.Payment createVCRDisplayRSTicketingInfosTicketingInfoTicketingCustomerInfoPaymentInfoPayment() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo.Payment();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.CustLoyalty }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.CustLoyalty createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCouponCustLoyalty() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.CustLoyalty();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.DestinationLocation }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.DestinationLocation createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCouponFlownFlightSegmentDestinationLocation() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.DestinationLocation();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.OriginLocation }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.OriginLocation createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCouponFlownFlightSegmentOriginLocation() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.OriginLocation();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.BaggageAllowance }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.BaggageAllowance createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCouponFlightSegmentBaggageAllowance() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.BaggageAllowance();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.DestinationLocation }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.DestinationLocation createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCouponFlightSegmentDestinationLocation() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.DestinationLocation();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.FareBasis }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.FareBasis createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCouponFlightSegmentFareBasis() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.FareBasis();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.MarketingAirline }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.MarketingAirline createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCouponFlightSegmentMarketingAirline() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.MarketingAirline();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.OriginLocation }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.OriginLocation createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCouponFlightSegmentOriginLocation() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.OriginLocation();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.ValidityDates }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.ValidityDates createVCRDisplayRSTicketingInfosTicketingInfoCouponDataCouponsCouponFlightSegmentValidityDates() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.ValidityDates();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown.FareCalculation }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown.FareCalculation createVCRDisplayRSTicketingInfosTicketingInfoCouponDataAirItineraryPricingInfoPTCFareBreakdownFareCalculation() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown.FareCalculation();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.BaseFare }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.BaseFare createVCRDisplayRSTicketingInfosTicketingInfoCouponDataAirItineraryPricingInfoItinTotalFareBaseFare() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.BaseFare();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.EquivFare }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.EquivFare createVCRDisplayRSTicketingInfosTicketingInfoCouponDataAirItineraryPricingInfoItinTotalFareEquivFare() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.EquivFare();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.TotalFare }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.TotalFare createVCRDisplayRSTicketingInfosTicketingInfoCouponDataAirItineraryPricingInfoItinTotalFareTotalFare() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.TotalFare();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes.Tax }
     * 
     */
    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes.Tax createVCRDisplayRSTicketingInfosTicketingInfoCouponDataAirItineraryPricingInfoItinTotalFareTaxesTax() {
        return new VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes.Tax();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.ItineraryRef }
     * 
     */
    public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.ItineraryRef createVCRDisplayRSTicketingInfosSimilarNameListCouponItineraryRef() {
        return new VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.ItineraryRef();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.PersonName }
     * 
     */
    public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.PersonName createVCRDisplayRSTicketingInfosSimilarNameListCouponPersonName() {
        return new VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.PersonName();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.DestinationLocation }
     * 
     */
    public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.DestinationLocation createVCRDisplayRSTicketingInfosSimilarNameListCouponFlightSegmentDestinationLocation() {
        return new VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.DestinationLocation();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.MarketingAirline }
     * 
     */
    public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.MarketingAirline createVCRDisplayRSTicketingInfosSimilarNameListCouponFlightSegmentMarketingAirline() {
        return new VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.MarketingAirline();
    }

    /**
     * Create an instance of {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.OriginLocation }
     * 
     */
    public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.OriginLocation createVCRDisplayRSTicketingInfosSimilarNameListCouponFlightSegmentOriginLocation() {
        return new VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.OriginLocation();
    }

}
