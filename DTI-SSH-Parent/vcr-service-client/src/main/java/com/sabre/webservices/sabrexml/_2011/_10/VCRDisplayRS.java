
package com.sabre.webservices.sabrexml._2011._10;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import com.sabre.services.stl.v01.ApplicationResults;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://services.sabre.com/STL/v01}ApplicationResults"/>
 *         &lt;element name="TicketingInfos" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SimilarNameList" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Coupon" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="FlightSegment" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="DestinationLocation" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="MarketingAirline" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="OriginLocation" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                               &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                               &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                               &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="ItineraryRef" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                               &lt;attribute name="PurgeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="PersonName" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="GivenName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                 &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="AccountCodeExpansion" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="AccountingCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
 *                                     &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="RPH" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *                                     &lt;attribute name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="TicketingInfo" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="CouponData" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="AirItineraryPricingInfo" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="ItinTotalFare" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="BaseFare" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="EquivFare" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="Taxes" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="Tax" maxOccurs="unbounded" minOccurs="0">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                             &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                             &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                             &lt;attribute name="PaidInd" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                             &lt;attribute name="PFC_RefusalInd" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                             &lt;attribute name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="TotalFare" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="PTC_FareBreakdown" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="FareCalculation" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="5" minOccurs="0"/>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="Coupons" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="Coupon" maxOccurs="unbounded" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="CustLoyalty" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="MembershipID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="ProgramID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="ShortText" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="FlightSegment" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="BaggageAllowance" minOccurs="0">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="DestinationLocation" minOccurs="0">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="FareBasis" minOccurs="0">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="MarketingAirline" minOccurs="0">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                             &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="OriginLocation" minOccurs="0">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="ValidityDates" minOccurs="0">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;sequence>
 *                                                                               &lt;element name="NotValidAfter" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
 *                                                                               &lt;element name="NotValidBefore" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
 *                                                                             &lt;/sequence>
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                   &lt;/sequence>
 *                                                                   &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}dateTime" />
 *                                                                   &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="FlownFlightSegment" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="DestinationLocation" minOccurs="0">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="OriginLocation" minOccurs="0">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                   &lt;/sequence>
 *                                                                   &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
 *                                                                   &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="OriginDate" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
 *                                                                   &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="15" minOccurs="0"/>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="EntitlementNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="OA_ControllingSegmentCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="Ticketing" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="CustomerInfo" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="InvoluntaryInfo" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="PaymentInfo" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="Payment" maxOccurs="unbounded" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="4" minOccurs="0"/>
 *                                                                   &lt;/sequence>
 *                                                                   &lt;attribute name="CashCheckAmount" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="ChangeFeeInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="Code1" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                   &lt;attribute name="Code2" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="PersonName" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="PassengerType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="ItineraryRef" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="Source" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;attribute name="CreateDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                               &lt;attribute name="PurgeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                               &lt;attribute name="PurgeDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="TicketData" maxOccurs="unbounded" minOccurs="0">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="DynamicCurrency" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;attribute name="AmountPerDocument" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="Rate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="Endorsements" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="2" minOccurs="0"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="ExchangeData" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="AdditionalExchangedTicket" maxOccurs="2" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="EquivalentAmountPaid" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                         &lt;/sequence>
 *                                                         &lt;attribute name="IssuedInExchangeFor" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="OriginalIssuanceData" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                         &lt;attribute name="OriginalIssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
 *                                                         &lt;attribute name="OriginalIssuePlace" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="ManualRemarks" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="PaymentInfo" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="Payment" minOccurs="0">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="CC_Info" minOccurs="0">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;sequence>
 *                                                                               &lt;element name="MaskedPaymentCard" minOccurs="0">
 *                                                                                 &lt;complexType>
 *                                                                                   &lt;complexContent>
 *                                                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                                       &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                                       &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                                     &lt;/restriction>
 *                                                                                   &lt;/complexContent>
 *                                                                                 &lt;/complexType>
 *                                                                               &lt;/element>
 *                                                                               &lt;element name="PaymentCard" minOccurs="0">
 *                                                                                 &lt;complexType>
 *                                                                                   &lt;complexContent>
 *                                                                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                                       &lt;attribute name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                                       &lt;attribute name="AuthorizationType" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                                       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                                       &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                                       &lt;attribute name="ExtendedPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                                                                                       &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                                                       &lt;attribute name="PresentCC_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                                                                                     &lt;/restriction>
 *                                                                                   &lt;/complexContent>
 *                                                                                 &lt;/complexType>
 *                                                                               &lt;/element>
 *                                                                             &lt;/sequence>
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                     &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="Restrictions" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="22" minOccurs="0"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                                 &lt;element name="TourInfo" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                               &lt;attribute name="FirstCertificate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                               &lt;attribute name="ISI" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                               &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
 *                                               &lt;attribute name="IssuingAgent" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                               &lt;attribute name="PrintStation" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                               &lt;attribute name="SecondCertificate" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="AccountingCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="EndConjunctionRange" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
 *                                     &lt;attribute name="IssuingAgent" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="NumCoupons" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="Version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "applicationResults",
    "ticketingInfos"
})
@XmlRootElement(name = "VCR_DisplayRS")
public class VCRDisplayRS {

    @XmlElement(name = "ApplicationResults", namespace = "http://services.sabre.com/STL/v01", required = true)
    protected ApplicationResults applicationResults;
    @XmlElement(name = "TicketingInfos")
    protected VCRDisplayRS.TicketingInfos ticketingInfos;
    @XmlAttribute(name = "Version")
    protected String version;

    /**
     * Gets the value of the applicationResults property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationResults }
     *     
     */
    public ApplicationResults getApplicationResults() {
        return applicationResults;
    }

    /**
     * Sets the value of the applicationResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationResults }
     *     
     */
    public void setApplicationResults(ApplicationResults value) {
        this.applicationResults = value;
    }

    /**
     * Gets the value of the ticketingInfos property.
     * 
     * @return
     *     possible object is
     *     {@link VCRDisplayRS.TicketingInfos }
     *     
     */
    public VCRDisplayRS.TicketingInfos getTicketingInfos() {
        return ticketingInfos;
    }

    /**
     * Sets the value of the ticketingInfos property.
     * 
     * @param value
     *     allowed object is
     *     {@link VCRDisplayRS.TicketingInfos }
     *     
     */
    public void setTicketingInfos(VCRDisplayRS.TicketingInfos value) {
        this.ticketingInfos = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SimilarNameList" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Coupon" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="FlightSegment" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="DestinationLocation" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="MarketingAirline" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="OriginLocation" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                     &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                     &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                     &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="ItineraryRef" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                     &lt;attribute name="PurgeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="PersonName" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="GivenName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                       &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                           &lt;attribute name="AccountCodeExpansion" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="AccountingCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
     *                           &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="RPH" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *                           &lt;attribute name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="TicketingInfo" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="CouponData" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="AirItineraryPricingInfo" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="ItinTotalFare" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="BaseFare" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="EquivFare" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="Taxes" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="Tax" maxOccurs="unbounded" minOccurs="0">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                   &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                   &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                   &lt;attribute name="PaidInd" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                   &lt;attribute name="PFC_RefusalInd" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                   &lt;attribute name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                         &lt;/sequence>
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="TotalFare" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="PTC_FareBreakdown" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="FareCalculation" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="5" minOccurs="0"/>
     *                                                         &lt;/sequence>
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="Coupons" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="Coupon" maxOccurs="unbounded" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="CustLoyalty" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="MembershipID" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="ProgramID" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="ShortText" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="FlightSegment" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="BaggageAllowance" minOccurs="0">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                           &lt;element name="DestinationLocation" minOccurs="0">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                           &lt;element name="FareBasis" minOccurs="0">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                           &lt;element name="MarketingAirline" minOccurs="0">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                   &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                           &lt;element name="OriginLocation" minOccurs="0">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                           &lt;element name="ValidityDates" minOccurs="0">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;sequence>
     *                                                                     &lt;element name="NotValidAfter" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
     *                                                                     &lt;element name="NotValidBefore" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
     *                                                                   &lt;/sequence>
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                         &lt;/sequence>
     *                                                         &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}dateTime" />
     *                                                         &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="FlownFlightSegment" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="DestinationLocation" minOccurs="0">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                           &lt;element name="OriginLocation" minOccurs="0">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                         &lt;/sequence>
     *                                                         &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
     *                                                         &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="OriginDate" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
     *                                                         &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="15" minOccurs="0"/>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="EntitlementNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute name="OA_ControllingSegmentCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="Ticketing" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="CustomerInfo" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="InvoluntaryInfo" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="PaymentInfo" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="Payment" maxOccurs="unbounded" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="4" minOccurs="0"/>
     *                                                         &lt;/sequence>
     *                                                         &lt;attribute name="CashCheckAmount" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="ChangeFeeInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="Code1" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                         &lt;attribute name="Code2" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="PersonName" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="PassengerType" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="ItineraryRef" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="Source" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;attribute name="CreateDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                     &lt;attribute name="PurgeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                     &lt;attribute name="PurgeDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="TicketData" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="DynamicCurrency" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;attribute name="AmountPerDocument" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute name="Rate" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="Endorsements" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="2" minOccurs="0"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="ExchangeData" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="AdditionalExchangedTicket" maxOccurs="2" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="EquivalentAmountPaid" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                               &lt;/sequence>
     *                                               &lt;attribute name="IssuedInExchangeFor" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute name="OriginalIssuanceData" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                               &lt;attribute name="OriginalIssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
     *                                               &lt;attribute name="OriginalIssuePlace" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="ManualRemarks" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="PaymentInfo" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="Payment" minOccurs="0">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="CC_Info" minOccurs="0">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;sequence>
     *                                                                     &lt;element name="MaskedPaymentCard" minOccurs="0">
     *                                                                       &lt;complexType>
     *                                                                         &lt;complexContent>
     *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                             &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                             &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                           &lt;/restriction>
     *                                                                         &lt;/complexContent>
     *                                                                       &lt;/complexType>
     *                                                                     &lt;/element>
     *                                                                     &lt;element name="PaymentCard" minOccurs="0">
     *                                                                       &lt;complexType>
     *                                                                         &lt;complexContent>
     *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                             &lt;attribute name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                             &lt;attribute name="AuthorizationType" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                             &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                             &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                             &lt;attribute name="ExtendedPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *                                                                             &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                                                             &lt;attribute name="PresentCC_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *                                                                           &lt;/restriction>
     *                                                                         &lt;/complexContent>
     *                                                                       &lt;/complexType>
     *                                                                     &lt;/element>
     *                                                                   &lt;/sequence>
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                           &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
     *                                                         &lt;/sequence>
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="Restrictions" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="22" minOccurs="0"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                       &lt;element name="TourInfo" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                     &lt;attribute name="FirstCertificate" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                     &lt;attribute name="ISI" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                     &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
     *                                     &lt;attribute name="IssuingAgent" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                     &lt;attribute name="PrintStation" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                     &lt;attribute name="SecondCertificate" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                           &lt;attribute name="AccountingCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="EndConjunctionRange" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
     *                           &lt;attribute name="IssuingAgent" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="NumCoupons" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "similarNameList",
        "ticketingInfo"
    })
    public static class TicketingInfos {

        @XmlElement(name = "SimilarNameList")
        protected VCRDisplayRS.TicketingInfos.SimilarNameList similarNameList;
        @XmlElement(name = "TicketingInfo")
        protected VCRDisplayRS.TicketingInfos.TicketingInfo ticketingInfo;

        /**
         * Gets the value of the similarNameList property.
         * 
         * @return
         *     possible object is
         *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList }
         *     
         */
        public VCRDisplayRS.TicketingInfos.SimilarNameList getSimilarNameList() {
            return similarNameList;
        }

        /**
         * Sets the value of the similarNameList property.
         * 
         * @param value
         *     allowed object is
         *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList }
         *     
         */
        public void setSimilarNameList(VCRDisplayRS.TicketingInfos.SimilarNameList value) {
            this.similarNameList = value;
        }

        /**
         * Gets the value of the ticketingInfo property.
         * 
         * @return
         *     possible object is
         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo }
         *     
         */
        public VCRDisplayRS.TicketingInfos.TicketingInfo getTicketingInfo() {
            return ticketingInfo;
        }

        /**
         * Sets the value of the ticketingInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo }
         *     
         */
        public void setTicketingInfo(VCRDisplayRS.TicketingInfos.TicketingInfo value) {
            this.ticketingInfo = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Coupon" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="FlightSegment" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="DestinationLocation" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="MarketingAirline" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="OriginLocation" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                           &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="ItineraryRef" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="PurgeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="PersonName" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="GivenName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                             &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *                 &lt;attribute name="AccountCodeExpansion" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="AccountingCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
         *                 &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="RPH" type="{http://www.w3.org/2001/XMLSchema}integer" />
         *                 &lt;attribute name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "coupon"
        })
        public static class SimilarNameList {

            @XmlElement(name = "Coupon")
            protected List<VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon> coupon;

            /**
             * Gets the value of the coupon property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the coupon property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCoupon().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon }
             * 
             * 
             */
            public List<VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon> getCoupon() {
                if (coupon == null) {
                    coupon = new ArrayList<VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon>();
                }
                return this.coupon;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="FlightSegment" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="DestinationLocation" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="MarketingAirline" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="OriginLocation" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *                 &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="ItineraryRef" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="PurgeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="PersonName" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="GivenName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                   &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *       &lt;attribute name="AccountCodeExpansion" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="AccountingCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
             *       &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="RPH" type="{http://www.w3.org/2001/XMLSchema}integer" />
             *       &lt;attribute name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "flightSegment",
                "itineraryRef",
                "personName"
            })
            public static class Coupon {

                @XmlElement(name = "FlightSegment")
                protected VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment flightSegment;
                @XmlElement(name = "ItineraryRef")
                protected VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.ItineraryRef itineraryRef;
                @XmlElement(name = "PersonName")
                protected VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.PersonName personName;
                @XmlAttribute(name = "AccountCodeExpansion")
                protected String accountCodeExpansion;
                @XmlAttribute(name = "AccountingCode")
                protected String accountingCode;
                @XmlAttribute(name = "eTicketNumber")
                protected String eTicketNumber;
                @XmlAttribute(name = "IssueDate")
                protected XMLGregorianCalendar issueDate;
                @XmlAttribute(name = "Number")
                protected String number;
                @XmlAttribute(name = "RPH")
                protected BigInteger rph;
                @XmlAttribute(name = "StatusCode")
                protected String statusCode;

                /**
                 * Gets the value of the flightSegment property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment }
                 *     
                 */
                public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment getFlightSegment() {
                    return flightSegment;
                }

                /**
                 * Sets the value of the flightSegment property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment }
                 *     
                 */
                public void setFlightSegment(VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment value) {
                    this.flightSegment = value;
                }

                /**
                 * Gets the value of the itineraryRef property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.ItineraryRef }
                 *     
                 */
                public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.ItineraryRef getItineraryRef() {
                    return itineraryRef;
                }

                /**
                 * Sets the value of the itineraryRef property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.ItineraryRef }
                 *     
                 */
                public void setItineraryRef(VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.ItineraryRef value) {
                    this.itineraryRef = value;
                }

                /**
                 * Gets the value of the personName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.PersonName }
                 *     
                 */
                public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.PersonName getPersonName() {
                    return personName;
                }

                /**
                 * Sets the value of the personName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.PersonName }
                 *     
                 */
                public void setPersonName(VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.PersonName value) {
                    this.personName = value;
                }

                /**
                 * Gets the value of the accountCodeExpansion property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAccountCodeExpansion() {
                    return accountCodeExpansion;
                }

                /**
                 * Sets the value of the accountCodeExpansion property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAccountCodeExpansion(String value) {
                    this.accountCodeExpansion = value;
                }

                /**
                 * Gets the value of the accountingCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAccountingCode() {
                    return accountingCode;
                }

                /**
                 * Sets the value of the accountingCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAccountingCode(String value) {
                    this.accountingCode = value;
                }

                /**
                 * Gets the value of the eTicketNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getETicketNumber() {
                    return eTicketNumber;
                }

                /**
                 * Sets the value of the eTicketNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setETicketNumber(String value) {
                    this.eTicketNumber = value;
                }

                /**
                 * Gets the value of the issueDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getIssueDate() {
                    return issueDate;
                }

                /**
                 * Sets the value of the issueDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setIssueDate(XMLGregorianCalendar value) {
                    this.issueDate = value;
                }

                /**
                 * Gets the value of the number property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumber() {
                    return number;
                }

                /**
                 * Sets the value of the number property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumber(String value) {
                    this.number = value;
                }

                /**
                 * Gets the value of the rph property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getRPH() {
                    return rph;
                }

                /**
                 * Sets the value of the rph property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setRPH(BigInteger value) {
                    this.rph = value;
                }

                /**
                 * Gets the value of the statusCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStatusCode() {
                    return statusCode;
                }

                /**
                 * Sets the value of the statusCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStatusCode(String value) {
                    this.statusCode = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="DestinationLocation" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="MarketingAirline" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="OriginLocation" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *       &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "destinationLocation",
                    "marketingAirline",
                    "originLocation"
                })
                public static class FlightSegment {

                    @XmlElement(name = "DestinationLocation")
                    protected VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.DestinationLocation destinationLocation;
                    @XmlElement(name = "MarketingAirline")
                    protected VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.MarketingAirline marketingAirline;
                    @XmlElement(name = "OriginLocation")
                    protected VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.OriginLocation originLocation;
                    @XmlAttribute(name = "ConnectionInd")
                    protected String connectionInd;
                    @XmlAttribute(name = "FlightNumber")
                    protected String flightNumber;
                    @XmlAttribute(name = "ResBookDesigCode")
                    protected String resBookDesigCode;
                    @XmlAttribute(name = "Status")
                    protected String status;

                    /**
                     * Gets the value of the destinationLocation property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.DestinationLocation }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.DestinationLocation getDestinationLocation() {
                        return destinationLocation;
                    }

                    /**
                     * Sets the value of the destinationLocation property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.DestinationLocation }
                     *     
                     */
                    public void setDestinationLocation(VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.DestinationLocation value) {
                        this.destinationLocation = value;
                    }

                    /**
                     * Gets the value of the marketingAirline property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.MarketingAirline }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.MarketingAirline getMarketingAirline() {
                        return marketingAirline;
                    }

                    /**
                     * Sets the value of the marketingAirline property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.MarketingAirline }
                     *     
                     */
                    public void setMarketingAirline(VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.MarketingAirline value) {
                        this.marketingAirline = value;
                    }

                    /**
                     * Gets the value of the originLocation property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.OriginLocation }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.OriginLocation getOriginLocation() {
                        return originLocation;
                    }

                    /**
                     * Sets the value of the originLocation property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.OriginLocation }
                     *     
                     */
                    public void setOriginLocation(VCRDisplayRS.TicketingInfos.SimilarNameList.Coupon.FlightSegment.OriginLocation value) {
                        this.originLocation = value;
                    }

                    /**
                     * Gets the value of the connectionInd property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getConnectionInd() {
                        return connectionInd;
                    }

                    /**
                     * Sets the value of the connectionInd property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setConnectionInd(String value) {
                        this.connectionInd = value;
                    }

                    /**
                     * Gets the value of the flightNumber property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getFlightNumber() {
                        return flightNumber;
                    }

                    /**
                     * Sets the value of the flightNumber property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFlightNumber(String value) {
                        this.flightNumber = value;
                    }

                    /**
                     * Gets the value of the resBookDesigCode property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getResBookDesigCode() {
                        return resBookDesigCode;
                    }

                    /**
                     * Sets the value of the resBookDesigCode property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setResBookDesigCode(String value) {
                        this.resBookDesigCode = value;
                    }

                    /**
                     * Gets the value of the status property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getStatus() {
                        return status;
                    }

                    /**
                     * Sets the value of the status property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setStatus(String value) {
                        this.status = value;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class DestinationLocation {

                        @XmlAttribute(name = "LocationCode")
                        protected String locationCode;

                        /**
                         * Gets the value of the locationCode property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getLocationCode() {
                            return locationCode;
                        }

                        /**
                         * Sets the value of the locationCode property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setLocationCode(String value) {
                            this.locationCode = value;
                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class MarketingAirline {

                        @XmlAttribute(name = "Code")
                        protected String code;
                        @XmlAttribute(name = "FlightNumber")
                        protected String flightNumber;

                        /**
                         * Gets the value of the code property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCode() {
                            return code;
                        }

                        /**
                         * Sets the value of the code property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCode(String value) {
                            this.code = value;
                        }

                        /**
                         * Gets the value of the flightNumber property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getFlightNumber() {
                            return flightNumber;
                        }

                        /**
                         * Sets the value of the flightNumber property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setFlightNumber(String value) {
                            this.flightNumber = value;
                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class OriginLocation {

                        @XmlAttribute(name = "LocationCode")
                        protected String locationCode;

                        /**
                         * Gets the value of the locationCode property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getLocationCode() {
                            return locationCode;
                        }

                        /**
                         * Sets the value of the locationCode property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setLocationCode(String value) {
                            this.locationCode = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="PurgeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class ItineraryRef {

                    @XmlAttribute(name = "ID")
                    protected String id;
                    @XmlAttribute(name = "PurgeIndicator")
                    protected String purgeIndicator;

                    /**
                     * Gets the value of the id property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getID() {
                        return id;
                    }

                    /**
                     * Sets the value of the id property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setID(String value) {
                        this.id = value;
                    }

                    /**
                     * Gets the value of the purgeIndicator property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPurgeIndicator() {
                        return purgeIndicator;
                    }

                    /**
                     * Sets the value of the purgeIndicator property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPurgeIndicator(String value) {
                        this.purgeIndicator = value;
                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="GivenName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *         &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "givenName",
                    "surname"
                })
                public static class PersonName {

                    @XmlElement(name = "GivenName")
                    protected String givenName;
                    @XmlElement(name = "Surname")
                    protected String surname;

                    /**
                     * Gets the value of the givenName property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getGivenName() {
                        return givenName;
                    }

                    /**
                     * Sets the value of the givenName property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setGivenName(String value) {
                        this.givenName = value;
                    }

                    /**
                     * Gets the value of the surname property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getSurname() {
                        return surname;
                    }

                    /**
                     * Sets the value of the surname property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setSurname(String value) {
                        this.surname = value;
                    }

                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="CouponData" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="AirItineraryPricingInfo" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="ItinTotalFare" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="BaseFare" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                       &lt;element name="EquivFare" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                       &lt;element name="Taxes" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="Tax" maxOccurs="unbounded" minOccurs="0">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                         &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                         &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                         &lt;attribute name="PaidInd" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                         &lt;attribute name="PFC_RefusalInd" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                         &lt;attribute name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                               &lt;/sequence>
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                       &lt;element name="TotalFare" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="PTC_FareBreakdown" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="FareCalculation" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="5" minOccurs="0"/>
         *                                               &lt;/sequence>
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="Coupons" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="Coupon" maxOccurs="unbounded" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="CustLoyalty" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="MembershipID" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="ProgramID" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="ShortText" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                       &lt;element name="FlightSegment" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="BaggageAllowance" minOccurs="0">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                                 &lt;element name="DestinationLocation" minOccurs="0">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                                 &lt;element name="FareBasis" minOccurs="0">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                                 &lt;element name="MarketingAirline" minOccurs="0">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                         &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                                 &lt;element name="OriginLocation" minOccurs="0">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                                 &lt;element name="ValidityDates" minOccurs="0">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;sequence>
         *                                                           &lt;element name="NotValidAfter" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
         *                                                           &lt;element name="NotValidBefore" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
         *                                                         &lt;/sequence>
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                               &lt;/sequence>
         *                                               &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}dateTime" />
         *                                               &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                       &lt;element name="FlownFlightSegment" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="DestinationLocation" minOccurs="0">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                                 &lt;element name="OriginLocation" minOccurs="0">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                               &lt;/sequence>
         *                                               &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
         *                                               &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="OriginDate" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
         *                                               &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                       &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="15" minOccurs="0"/>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="EntitlementNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="OA_ControllingSegmentCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Ticketing" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="CustomerInfo" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="InvoluntaryInfo" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="PaymentInfo" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="Payment" maxOccurs="unbounded" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="4" minOccurs="0"/>
         *                                               &lt;/sequence>
         *                                               &lt;attribute name="CashCheckAmount" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="ChangeFeeInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="Code1" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                               &lt;attribute name="Code2" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="PersonName" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="PassengerType" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="ItineraryRef" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="Source" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;attribute name="CreateDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                           &lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="PurgeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="PurgeDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="TicketData" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="DynamicCurrency" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;attribute name="AmountPerDocument" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="Rate" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="Endorsements" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="2" minOccurs="0"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="ExchangeData" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="AdditionalExchangedTicket" maxOccurs="2" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                       &lt;element name="EquivalentAmountPaid" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                       &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                     &lt;/sequence>
         *                                     &lt;attribute name="IssuedInExchangeFor" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="OriginalIssuanceData" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                     &lt;attribute name="OriginalIssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
         *                                     &lt;attribute name="OriginalIssuePlace" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="ManualRemarks" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="PaymentInfo" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="Payment" minOccurs="0">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="CC_Info" minOccurs="0">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;sequence>
         *                                                           &lt;element name="MaskedPaymentCard" minOccurs="0">
         *                                                             &lt;complexType>
         *                                                               &lt;complexContent>
         *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                                   &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                                   &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                                 &lt;/restriction>
         *                                                               &lt;/complexContent>
         *                                                             &lt;/complexType>
         *                                                           &lt;/element>
         *                                                           &lt;element name="PaymentCard" minOccurs="0">
         *                                                             &lt;complexType>
         *                                                               &lt;complexContent>
         *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                                   &lt;attribute name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                                   &lt;attribute name="AuthorizationType" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                                   &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                                   &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                                   &lt;attribute name="ExtendedPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" />
         *                                                                   &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                                                   &lt;attribute name="PresentCC_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" />
         *                                                                 &lt;/restriction>
         *                                                               &lt;/complexContent>
         *                                                             &lt;/complexType>
         *                                                           &lt;/element>
         *                                                         &lt;/sequence>
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                                 &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
         *                                               &lt;/sequence>
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="Restrictions" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="22" minOccurs="0"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                             &lt;element name="TourInfo" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                           &lt;attribute name="FirstCertificate" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="ISI" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
         *                           &lt;attribute name="IssuingAgent" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="PrintStation" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                           &lt;attribute name="SecondCertificate" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *                 &lt;attribute name="AccountingCode" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="EndConjunctionRange" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
         *                 &lt;attribute name="IssuingAgent" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="NumCoupons" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "couponData",
            "ticketing"
        })
        public static class TicketingInfo {

            @XmlElement(name = "CouponData")
            protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData couponData;
            @XmlElement(name = "Ticketing")
            protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing ticketing;

            /**
             * Gets the value of the couponData property.
             * 
             * @return
             *     possible object is
             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData }
             *     
             */
            public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData getCouponData() {
                return couponData;
            }

            /**
             * Sets the value of the couponData property.
             * 
             * @param value
             *     allowed object is
             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData }
             *     
             */
            public void setCouponData(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData value) {
                this.couponData = value;
            }

            /**
             * Gets the value of the ticketing property.
             * 
             * @return
             *     possible object is
             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing }
             *     
             */
            public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing getTicketing() {
                return ticketing;
            }

            /**
             * Sets the value of the ticketing property.
             * 
             * @param value
             *     allowed object is
             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing }
             *     
             */
            public void setTicketing(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing value) {
                this.ticketing = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="AirItineraryPricingInfo" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="ItinTotalFare" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="BaseFare" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                             &lt;element name="EquivFare" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                             &lt;element name="Taxes" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="Tax" maxOccurs="unbounded" minOccurs="0">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                               &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                               &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                               &lt;attribute name="PaidInd" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                               &lt;attribute name="PFC_RefusalInd" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                               &lt;attribute name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                     &lt;/sequence>
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                             &lt;element name="TotalFare" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="PTC_FareBreakdown" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="FareCalculation" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="5" minOccurs="0"/>
             *                                     &lt;/sequence>
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="Coupons" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="Coupon" maxOccurs="unbounded" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="CustLoyalty" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="MembershipID" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="ProgramID" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="ShortText" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                             &lt;element name="FlightSegment" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="BaggageAllowance" minOccurs="0">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                       &lt;element name="DestinationLocation" minOccurs="0">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                       &lt;element name="FareBasis" minOccurs="0">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                       &lt;element name="MarketingAirline" minOccurs="0">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                               &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                       &lt;element name="OriginLocation" minOccurs="0">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                       &lt;element name="ValidityDates" minOccurs="0">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;sequence>
             *                                                 &lt;element name="NotValidAfter" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
             *                                                 &lt;element name="NotValidBefore" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
             *                                               &lt;/sequence>
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                     &lt;/sequence>
             *                                     &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}dateTime" />
             *                                     &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                             &lt;element name="FlownFlightSegment" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="DestinationLocation" minOccurs="0">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                       &lt;element name="OriginLocation" minOccurs="0">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                     &lt;/sequence>
             *                                     &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
             *                                     &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="OriginDate" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
             *                                     &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                             &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="15" minOccurs="0"/>
             *                           &lt;/sequence>
             *                           &lt;attribute name="EntitlementNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="OA_ControllingSegmentCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "airItineraryPricingInfo",
                "coupons"
            })
            public static class CouponData {

                @XmlElement(name = "AirItineraryPricingInfo")
                protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo airItineraryPricingInfo;
                @XmlElement(name = "Coupons")
                protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons coupons;

                /**
                 * Gets the value of the airItineraryPricingInfo property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo }
                 *     
                 */
                public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo getAirItineraryPricingInfo() {
                    return airItineraryPricingInfo;
                }

                /**
                 * Sets the value of the airItineraryPricingInfo property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo }
                 *     
                 */
                public void setAirItineraryPricingInfo(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo value) {
                    this.airItineraryPricingInfo = value;
                }

                /**
                 * Gets the value of the coupons property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons }
                 *     
                 */
                public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons getCoupons() {
                    return coupons;
                }

                /**
                 * Sets the value of the coupons property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons }
                 *     
                 */
                public void setCoupons(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons value) {
                    this.coupons = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="ItinTotalFare" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="BaseFare" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                   &lt;element name="EquivFare" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                   &lt;element name="Taxes" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="Tax" maxOccurs="unbounded" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                     &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                     &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                     &lt;attribute name="PaidInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                     &lt;attribute name="PFC_RefusalInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                     &lt;attribute name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                   &lt;element name="TotalFare" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="PTC_FareBreakdown" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="FareCalculation" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="5" minOccurs="0"/>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "itinTotalFare",
                    "ptcFareBreakdown"
                })
                public static class AirItineraryPricingInfo {

                    @XmlElement(name = "ItinTotalFare")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare itinTotalFare;
                    @XmlElement(name = "PTC_FareBreakdown")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown ptcFareBreakdown;

                    /**
                     * Gets the value of the itinTotalFare property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare getItinTotalFare() {
                        return itinTotalFare;
                    }

                    /**
                     * Sets the value of the itinTotalFare property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare }
                     *     
                     */
                    public void setItinTotalFare(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare value) {
                        this.itinTotalFare = value;
                    }

                    /**
                     * Gets the value of the ptcFareBreakdown property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown getPTCFareBreakdown() {
                        return ptcFareBreakdown;
                    }

                    /**
                     * Sets the value of the ptcFareBreakdown property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown }
                     *     
                     */
                    public void setPTCFareBreakdown(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown value) {
                        this.ptcFareBreakdown = value;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="BaseFare" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="EquivFare" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="Taxes" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="Tax" maxOccurs="unbounded" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                           &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                           &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                           &lt;attribute name="PaidInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                           &lt;attribute name="PFC_RefusalInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                           &lt;attribute name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="TotalFare" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "baseFare",
                        "equivFare",
                        "taxes",
                        "totalFare"
                    })
                    public static class ItinTotalFare {

                        @XmlElement(name = "BaseFare")
                        protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.BaseFare baseFare;
                        @XmlElement(name = "EquivFare")
                        protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.EquivFare equivFare;
                        @XmlElement(name = "Taxes")
                        protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes taxes;
                        @XmlElement(name = "TotalFare")
                        protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.TotalFare totalFare;

                        /**
                         * Gets the value of the baseFare property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.BaseFare }
                         *     
                         */
                        public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.BaseFare getBaseFare() {
                            return baseFare;
                        }

                        /**
                         * Sets the value of the baseFare property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.BaseFare }
                         *     
                         */
                        public void setBaseFare(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.BaseFare value) {
                            this.baseFare = value;
                        }

                        /**
                         * Gets the value of the equivFare property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.EquivFare }
                         *     
                         */
                        public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.EquivFare getEquivFare() {
                            return equivFare;
                        }

                        /**
                         * Sets the value of the equivFare property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.EquivFare }
                         *     
                         */
                        public void setEquivFare(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.EquivFare value) {
                            this.equivFare = value;
                        }

                        /**
                         * Gets the value of the taxes property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes }
                         *     
                         */
                        public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes getTaxes() {
                            return taxes;
                        }

                        /**
                         * Sets the value of the taxes property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes }
                         *     
                         */
                        public void setTaxes(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes value) {
                            this.taxes = value;
                        }

                        /**
                         * Gets the value of the totalFare property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.TotalFare }
                         *     
                         */
                        public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.TotalFare getTotalFare() {
                            return totalFare;
                        }

                        /**
                         * Sets the value of the totalFare property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.TotalFare }
                         *     
                         */
                        public void setTotalFare(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.TotalFare value) {
                            this.totalFare = value;
                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class BaseFare {

                            @XmlAttribute(name = "Amount")
                            protected String amount;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;

                            /**
                             * Gets the value of the amount property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getAmount() {
                                return amount;
                            }

                            /**
                             * Sets the value of the amount property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setAmount(String value) {
                                this.amount = value;
                            }

                            /**
                             * Gets the value of the currencyCode property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Sets the value of the currencyCode property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class EquivFare {

                            @XmlAttribute(name = "Amount")
                            protected String amount;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;

                            /**
                             * Gets the value of the amount property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getAmount() {
                                return amount;
                            }

                            /**
                             * Sets the value of the amount property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setAmount(String value) {
                                this.amount = value;
                            }

                            /**
                             * Gets the value of the currencyCode property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Sets the value of the currencyCode property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="Tax" maxOccurs="unbounded" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                 &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                 &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                 &lt;attribute name="PaidInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                 &lt;attribute name="PFC_RefusalInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                 &lt;attribute name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "tax"
                        })
                        public static class Taxes {

                            @XmlElement(name = "Tax")
                            protected List<VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes.Tax> tax;

                            /**
                             * Gets the value of the tax property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the tax property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getTax().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes.Tax }
                             * 
                             * 
                             */
                            public List<VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes.Tax> getTax() {
                                if (tax == null) {
                                    tax = new ArrayList<VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.ItinTotalFare.Taxes.Tax>();
                                }
                                return this.tax;
                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *       &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *       &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *       &lt;attribute name="PaidInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *       &lt;attribute name="PFC_RefusalInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *       &lt;attribute name="TaxCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class Tax {

                                @XmlAttribute(name = "Amount")
                                protected String amount;
                                @XmlAttribute(name = "CurrencyCode")
                                protected String currencyCode;
                                @XmlAttribute(name = "LocationCode")
                                protected String locationCode;
                                @XmlAttribute(name = "PaidInd")
                                protected String paidInd;
                                @XmlAttribute(name = "PFC_RefusalInd")
                                protected String pfcRefusalInd;
                                @XmlAttribute(name = "TaxCode")
                                protected String taxCode;

                                /**
                                 * Gets the value of the amount property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getAmount() {
                                    return amount;
                                }

                                /**
                                 * Sets the value of the amount property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setAmount(String value) {
                                    this.amount = value;
                                }

                                /**
                                 * Gets the value of the currencyCode property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getCurrencyCode() {
                                    return currencyCode;
                                }

                                /**
                                 * Sets the value of the currencyCode property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setCurrencyCode(String value) {
                                    this.currencyCode = value;
                                }

                                /**
                                 * Gets the value of the locationCode property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getLocationCode() {
                                    return locationCode;
                                }

                                /**
                                 * Sets the value of the locationCode property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setLocationCode(String value) {
                                    this.locationCode = value;
                                }

                                /**
                                 * Gets the value of the paidInd property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getPaidInd() {
                                    return paidInd;
                                }

                                /**
                                 * Sets the value of the paidInd property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setPaidInd(String value) {
                                    this.paidInd = value;
                                }

                                /**
                                 * Gets the value of the pfcRefusalInd property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getPFCRefusalInd() {
                                    return pfcRefusalInd;
                                }

                                /**
                                 * Sets the value of the pfcRefusalInd property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setPFCRefusalInd(String value) {
                                    this.pfcRefusalInd = value;
                                }

                                /**
                                 * Gets the value of the taxCode property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getTaxCode() {
                                    return taxCode;
                                }

                                /**
                                 * Sets the value of the taxCode property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setTaxCode(String value) {
                                    this.taxCode = value;
                                }

                            }

                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;attribute name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class TotalFare {

                            @XmlAttribute(name = "Amount")
                            protected String amount;
                            @XmlAttribute(name = "CurrencyCode")
                            protected String currencyCode;

                            /**
                             * Gets the value of the amount property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getAmount() {
                                return amount;
                            }

                            /**
                             * Sets the value of the amount property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setAmount(String value) {
                                this.amount = value;
                            }

                            /**
                             * Gets the value of the currencyCode property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCurrencyCode() {
                                return currencyCode;
                            }

                            /**
                             * Sets the value of the currencyCode property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCurrencyCode(String value) {
                                this.currencyCode = value;
                            }

                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="FareCalculation" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="5" minOccurs="0"/>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "fareCalculation"
                    })
                    public static class PTCFareBreakdown {

                        @XmlElement(name = "FareCalculation")
                        protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown.FareCalculation fareCalculation;

                        /**
                         * Gets the value of the fareCalculation property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown.FareCalculation }
                         *     
                         */
                        public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown.FareCalculation getFareCalculation() {
                            return fareCalculation;
                        }

                        /**
                         * Sets the value of the fareCalculation property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown.FareCalculation }
                         *     
                         */
                        public void setFareCalculation(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.AirItineraryPricingInfo.PTCFareBreakdown.FareCalculation value) {
                            this.fareCalculation = value;
                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="5" minOccurs="0"/>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "text"
                        })
                        public static class FareCalculation {

                            @XmlElement(name = "Text")
                            protected List<String> text;

                            /**
                             * Gets the value of the text property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the text property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getText().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link String }
                             * 
                             * 
                             */
                            public List<String> getText() {
                                if (text == null) {
                                    text = new ArrayList<String>();
                                }
                                return this.text;
                            }

                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="Coupon" maxOccurs="unbounded" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="CustLoyalty" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="MembershipID" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="ProgramID" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="ShortText" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                   &lt;element name="FlightSegment" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="BaggageAllowance" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                             &lt;element name="DestinationLocation" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                             &lt;element name="FareBasis" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                             &lt;element name="MarketingAirline" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                     &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                             &lt;element name="OriginLocation" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                             &lt;element name="ValidityDates" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;sequence>
                 *                                       &lt;element name="NotValidAfter" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
                 *                                       &lt;element name="NotValidBefore" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
                 *                                     &lt;/sequence>
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                           &lt;/sequence>
                 *                           &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}dateTime" />
                 *                           &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                   &lt;element name="FlownFlightSegment" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="DestinationLocation" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                             &lt;element name="OriginLocation" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                           &lt;/sequence>
                 *                           &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
                 *                           &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="OriginDate" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
                 *                           &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                   &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="15" minOccurs="0"/>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="EntitlementNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="OA_ControllingSegmentCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "coupon"
                })
                public static class Coupons {

                    @XmlElement(name = "Coupon")
                    protected List<VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon> coupon;

                    /**
                     * Gets the value of the coupon property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the coupon property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getCoupon().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon }
                     * 
                     * 
                     */
                    public List<VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon> getCoupon() {
                        if (coupon == null) {
                            coupon = new ArrayList<VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon>();
                        }
                        return this.coupon;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="CustLoyalty" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="MembershipID" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="ProgramID" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="ShortText" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="FlightSegment" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="BaggageAllowance" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                   &lt;element name="DestinationLocation" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                   &lt;element name="FareBasis" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                   &lt;element name="MarketingAirline" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                           &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                   &lt;element name="OriginLocation" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                   &lt;element name="ValidityDates" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;sequence>
                     *                             &lt;element name="NotValidAfter" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
                     *                             &lt;element name="NotValidBefore" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
                     *                           &lt;/sequence>
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                 &lt;/sequence>
                     *                 &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}dateTime" />
                     *                 &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="FlownFlightSegment" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="DestinationLocation" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                   &lt;element name="OriginLocation" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                 &lt;/sequence>
                     *                 &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
                     *                 &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="OriginDate" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
                     *                 &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="Number" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="15" minOccurs="0"/>
                     *       &lt;/sequence>
                     *       &lt;attribute name="EntitlementNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="OA_ControllingSegmentCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="StatusCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "custLoyalty",
                        "flightSegment",
                        "flownFlightSegment",
                        "number"
                    })
                    public static class Coupon {

                        @XmlElement(name = "CustLoyalty")
                        protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.CustLoyalty custLoyalty;
                        @XmlElement(name = "FlightSegment")
                        protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment flightSegment;
                        @XmlElement(name = "FlownFlightSegment")
                        protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment flownFlightSegment;
                        @XmlElement(name = "Number")
                        protected List<String> number;
                        @XmlAttribute(name = "EntitlementNumber")
                        protected String entitlementNumber;
                        @XmlAttribute(name = "OA_ControllingSegmentCode")
                        protected String oaControllingSegmentCode;
                        @XmlAttribute(name = "StatusCode")
                        protected String statusCode;

                        /**
                         * Gets the value of the custLoyalty property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.CustLoyalty }
                         *     
                         */
                        public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.CustLoyalty getCustLoyalty() {
                            return custLoyalty;
                        }

                        /**
                         * Sets the value of the custLoyalty property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.CustLoyalty }
                         *     
                         */
                        public void setCustLoyalty(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.CustLoyalty value) {
                            this.custLoyalty = value;
                        }

                        /**
                         * Gets the value of the flightSegment property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment }
                         *     
                         */
                        public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment getFlightSegment() {
                            return flightSegment;
                        }

                        /**
                         * Sets the value of the flightSegment property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment }
                         *     
                         */
                        public void setFlightSegment(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment value) {
                            this.flightSegment = value;
                        }

                        /**
                         * Gets the value of the flownFlightSegment property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment }
                         *     
                         */
                        public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment getFlownFlightSegment() {
                            return flownFlightSegment;
                        }

                        /**
                         * Sets the value of the flownFlightSegment property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment }
                         *     
                         */
                        public void setFlownFlightSegment(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment value) {
                            this.flownFlightSegment = value;
                        }

                        /**
                         * Gets the value of the number property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the number property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getNumber().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link String }
                         * 
                         * 
                         */
                        public List<String> getNumber() {
                            if (number == null) {
                                number = new ArrayList<String>();
                            }
                            return this.number;
                        }

                        /**
                         * Gets the value of the entitlementNumber property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getEntitlementNumber() {
                            return entitlementNumber;
                        }

                        /**
                         * Sets the value of the entitlementNumber property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setEntitlementNumber(String value) {
                            this.entitlementNumber = value;
                        }

                        /**
                         * Gets the value of the oaControllingSegmentCode property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getOAControllingSegmentCode() {
                            return oaControllingSegmentCode;
                        }

                        /**
                         * Sets the value of the oaControllingSegmentCode property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setOAControllingSegmentCode(String value) {
                            this.oaControllingSegmentCode = value;
                        }

                        /**
                         * Gets the value of the statusCode property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getStatusCode() {
                            return statusCode;
                        }

                        /**
                         * Sets the value of the statusCode property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setStatusCode(String value) {
                            this.statusCode = value;
                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="MembershipID" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="ProgramID" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="ShortText" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="Type" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class CustLoyalty {

                            @XmlAttribute(name = "Code")
                            protected String code;
                            @XmlAttribute(name = "MembershipID")
                            protected String membershipID;
                            @XmlAttribute(name = "ProgramID")
                            protected String programID;
                            @XmlAttribute(name = "ShortText")
                            protected String shortText;
                            @XmlAttribute(name = "Type")
                            protected String type;

                            /**
                             * Gets the value of the code property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCode() {
                                return code;
                            }

                            /**
                             * Sets the value of the code property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCode(String value) {
                                this.code = value;
                            }

                            /**
                             * Gets the value of the membershipID property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getMembershipID() {
                                return membershipID;
                            }

                            /**
                             * Sets the value of the membershipID property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setMembershipID(String value) {
                                this.membershipID = value;
                            }

                            /**
                             * Gets the value of the programID property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getProgramID() {
                                return programID;
                            }

                            /**
                             * Sets the value of the programID property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setProgramID(String value) {
                                this.programID = value;
                            }

                            /**
                             * Gets the value of the shortText property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getShortText() {
                                return shortText;
                            }

                            /**
                             * Sets the value of the shortText property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setShortText(String value) {
                                this.shortText = value;
                            }

                            /**
                             * Gets the value of the type property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getType() {
                                return type;
                            }

                            /**
                             * Sets the value of the type property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setType(String value) {
                                this.type = value;
                            }

                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="BaggageAllowance" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *         &lt;element name="DestinationLocation" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *         &lt;element name="FareBasis" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *         &lt;element name="MarketingAirline" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                 &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *         &lt;element name="OriginLocation" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *         &lt;element name="ValidityDates" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;sequence>
                         *                   &lt;element name="NotValidAfter" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
                         *                   &lt;element name="NotValidBefore" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
                         *                 &lt;/sequence>
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *       &lt;/sequence>
                         *       &lt;attribute name="ConnectionInd" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}dateTime" />
                         *       &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="Status" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "baggageAllowance",
                            "destinationLocation",
                            "fareBasis",
                            "marketingAirline",
                            "originLocation",
                            "validityDates"
                        })
                        public static class FlightSegment {

                            @XmlElement(name = "BaggageAllowance")
                            protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.BaggageAllowance baggageAllowance;
                            @XmlElement(name = "DestinationLocation")
                            protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.DestinationLocation destinationLocation;
                            @XmlElement(name = "FareBasis")
                            protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.FareBasis fareBasis;
                            @XmlElement(name = "MarketingAirline")
                            protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.MarketingAirline marketingAirline;
                            @XmlElement(name = "OriginLocation")
                            protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.OriginLocation originLocation;
                            @XmlElement(name = "ValidityDates")
                            protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.ValidityDates validityDates;
                            @XmlAttribute(name = "ConnectionInd")
                            protected String connectionInd;
                            @XmlAttribute(name = "DepartureDateTime")
                            protected String departureDateTime;
                            @XmlAttribute(name = "FlightNumber")
                            protected String flightNumber;
                            @XmlAttribute(name = "ResBookDesigCode")
                            protected String resBookDesigCode;
                            @XmlAttribute(name = "Status")
                            protected String status;

                            /**
                             * Gets the value of the baggageAllowance property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.BaggageAllowance }
                             *     
                             */
                            public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.BaggageAllowance getBaggageAllowance() {
                                return baggageAllowance;
                            }

                            /**
                             * Sets the value of the baggageAllowance property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.BaggageAllowance }
                             *     
                             */
                            public void setBaggageAllowance(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.BaggageAllowance value) {
                                this.baggageAllowance = value;
                            }

                            /**
                             * Gets the value of the destinationLocation property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.DestinationLocation }
                             *     
                             */
                            public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.DestinationLocation getDestinationLocation() {
                                return destinationLocation;
                            }

                            /**
                             * Sets the value of the destinationLocation property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.DestinationLocation }
                             *     
                             */
                            public void setDestinationLocation(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.DestinationLocation value) {
                                this.destinationLocation = value;
                            }

                            /**
                             * Gets the value of the fareBasis property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.FareBasis }
                             *     
                             */
                            public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.FareBasis getFareBasis() {
                                return fareBasis;
                            }

                            /**
                             * Sets the value of the fareBasis property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.FareBasis }
                             *     
                             */
                            public void setFareBasis(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.FareBasis value) {
                                this.fareBasis = value;
                            }

                            /**
                             * Gets the value of the marketingAirline property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.MarketingAirline }
                             *     
                             */
                            public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.MarketingAirline getMarketingAirline() {
                                return marketingAirline;
                            }

                            /**
                             * Sets the value of the marketingAirline property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.MarketingAirline }
                             *     
                             */
                            public void setMarketingAirline(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.MarketingAirline value) {
                                this.marketingAirline = value;
                            }

                            /**
                             * Gets the value of the originLocation property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.OriginLocation }
                             *     
                             */
                            public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.OriginLocation getOriginLocation() {
                                return originLocation;
                            }

                            /**
                             * Sets the value of the originLocation property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.OriginLocation }
                             *     
                             */
                            public void setOriginLocation(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.OriginLocation value) {
                                this.originLocation = value;
                            }

                            /**
                             * Gets the value of the validityDates property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.ValidityDates }
                             *     
                             */
                            public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.ValidityDates getValidityDates() {
                                return validityDates;
                            }

                            /**
                             * Sets the value of the validityDates property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.ValidityDates }
                             *     
                             */
                            public void setValidityDates(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlightSegment.ValidityDates value) {
                                this.validityDates = value;
                            }

                            /**
                             * Gets the value of the connectionInd property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getConnectionInd() {
                                return connectionInd;
                            }

                            /**
                             * Sets the value of the connectionInd property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setConnectionInd(String value) {
                                this.connectionInd = value;
                            }

                            /**
                             * Gets the value of the departureDateTime property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getDepartureDateTime() {
                                return departureDateTime;
                            }

                            /**
                             * Sets the value of the departureDateTime property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setDepartureDateTime(String value) {
                                this.departureDateTime = value;
                            }

                            /**
                             * Gets the value of the flightNumber property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getFlightNumber() {
                                return flightNumber;
                            }

                            /**
                             * Sets the value of the flightNumber property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setFlightNumber(String value) {
                                this.flightNumber = value;
                            }

                            /**
                             * Gets the value of the resBookDesigCode property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getResBookDesigCode() {
                                return resBookDesigCode;
                            }

                            /**
                             * Sets the value of the resBookDesigCode property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setResBookDesigCode(String value) {
                                this.resBookDesigCode = value;
                            }

                            /**
                             * Gets the value of the status property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getStatus() {
                                return status;
                            }

                            /**
                             * Sets the value of the status property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setStatus(String value) {
                                this.status = value;
                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class BaggageAllowance {

                                @XmlAttribute(name = "Number")
                                protected String number;

                                /**
                                 * Gets the value of the number property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getNumber() {
                                    return number;
                                }

                                /**
                                 * Sets the value of the number property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setNumber(String value) {
                                    this.number = value;
                                }

                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class DestinationLocation {

                                @XmlAttribute(name = "LocationCode")
                                protected String locationCode;

                                /**
                                 * Gets the value of the locationCode property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getLocationCode() {
                                    return locationCode;
                                }

                                /**
                                 * Sets the value of the locationCode property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setLocationCode(String value) {
                                    this.locationCode = value;
                                }

                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class FareBasis {

                                @XmlAttribute(name = "Code")
                                protected String code;

                                /**
                                 * Gets the value of the code property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getCode() {
                                    return code;
                                }

                                /**
                                 * Sets the value of the code property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setCode(String value) {
                                    this.code = value;
                                }

                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *       &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class MarketingAirline {

                                @XmlAttribute(name = "Code")
                                protected String code;
                                @XmlAttribute(name = "FlightNumber")
                                protected String flightNumber;

                                /**
                                 * Gets the value of the code property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getCode() {
                                    return code;
                                }

                                /**
                                 * Sets the value of the code property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setCode(String value) {
                                    this.code = value;
                                }

                                /**
                                 * Gets the value of the flightNumber property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getFlightNumber() {
                                    return flightNumber;
                                }

                                /**
                                 * Sets the value of the flightNumber property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setFlightNumber(String value) {
                                    this.flightNumber = value;
                                }

                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class OriginLocation {

                                @XmlAttribute(name = "LocationCode")
                                protected String locationCode;

                                /**
                                 * Gets the value of the locationCode property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getLocationCode() {
                                    return locationCode;
                                }

                                /**
                                 * Sets the value of the locationCode property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setLocationCode(String value) {
                                    this.locationCode = value;
                                }

                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;sequence>
                             *         &lt;element name="NotValidAfter" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
                             *         &lt;element name="NotValidBefore" type="{http://webservices.sabre.com/sabreXML/2011/10}date" minOccurs="0"/>
                             *       &lt;/sequence>
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "notValidAfter",
                                "notValidBefore"
                            })
                            public static class ValidityDates {

                                @XmlElement(name = "NotValidAfter")
                                protected String notValidAfter;
                                @XmlElement(name = "NotValidBefore")
                                protected String notValidBefore;

                                /**
                                 * Gets the value of the notValidAfter property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getNotValidAfter() {
                                    return notValidAfter;
                                }

                                /**
                                 * Sets the value of the notValidAfter property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setNotValidAfter(String value) {
                                    this.notValidAfter = value;
                                }

                                /**
                                 * Gets the value of the notValidBefore property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getNotValidBefore() {
                                    return notValidBefore;
                                }

                                /**
                                 * Sets the value of the notValidBefore property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setNotValidBefore(String value) {
                                    this.notValidBefore = value;
                                }

                            }

                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="DestinationLocation" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *         &lt;element name="OriginLocation" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *       &lt;/sequence>
                         *       &lt;attribute name="DepartureDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
                         *       &lt;attribute name="FlightNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="OriginDate" type="{http://webservices.sabre.com/sabreXML/2011/10}date" />
                         *       &lt;attribute name="ResBookDesigCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "destinationLocation",
                            "originLocation"
                        })
                        public static class FlownFlightSegment {

                            @XmlElement(name = "DestinationLocation")
                            protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.DestinationLocation destinationLocation;
                            @XmlElement(name = "OriginLocation")
                            protected VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.OriginLocation originLocation;
                            @XmlAttribute(name = "DepartureDateTime")
                            protected String departureDateTime;
                            @XmlAttribute(name = "FlightNumber")
                            protected String flightNumber;
                            @XmlAttribute(name = "OriginDate")
                            protected String originDate;
                            @XmlAttribute(name = "ResBookDesigCode")
                            protected String resBookDesigCode;

                            /**
                             * Gets the value of the destinationLocation property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.DestinationLocation }
                             *     
                             */
                            public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.DestinationLocation getDestinationLocation() {
                                return destinationLocation;
                            }

                            /**
                             * Sets the value of the destinationLocation property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.DestinationLocation }
                             *     
                             */
                            public void setDestinationLocation(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.DestinationLocation value) {
                                this.destinationLocation = value;
                            }

                            /**
                             * Gets the value of the originLocation property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.OriginLocation }
                             *     
                             */
                            public VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.OriginLocation getOriginLocation() {
                                return originLocation;
                            }

                            /**
                             * Sets the value of the originLocation property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.OriginLocation }
                             *     
                             */
                            public void setOriginLocation(VCRDisplayRS.TicketingInfos.TicketingInfo.CouponData.Coupons.Coupon.FlownFlightSegment.OriginLocation value) {
                                this.originLocation = value;
                            }

                            /**
                             * Gets the value of the departureDateTime property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getDepartureDateTime() {
                                return departureDateTime;
                            }

                            /**
                             * Sets the value of the departureDateTime property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setDepartureDateTime(String value) {
                                this.departureDateTime = value;
                            }

                            /**
                             * Gets the value of the flightNumber property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getFlightNumber() {
                                return flightNumber;
                            }

                            /**
                             * Sets the value of the flightNumber property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setFlightNumber(String value) {
                                this.flightNumber = value;
                            }

                            /**
                             * Gets the value of the originDate property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getOriginDate() {
                                return originDate;
                            }

                            /**
                             * Sets the value of the originDate property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setOriginDate(String value) {
                                this.originDate = value;
                            }

                            /**
                             * Gets the value of the resBookDesigCode property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getResBookDesigCode() {
                                return resBookDesigCode;
                            }

                            /**
                             * Sets the value of the resBookDesigCode property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setResBookDesigCode(String value) {
                                this.resBookDesigCode = value;
                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class DestinationLocation {

                                @XmlAttribute(name = "LocationCode")
                                protected String locationCode;

                                /**
                                 * Gets the value of the locationCode property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getLocationCode() {
                                    return locationCode;
                                }

                                /**
                                 * Sets the value of the locationCode property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setLocationCode(String value) {
                                    this.locationCode = value;
                                }

                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;attribute name="LocationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "")
                            public static class OriginLocation {

                                @XmlAttribute(name = "LocationCode")
                                protected String locationCode;

                                /**
                                 * Gets the value of the locationCode property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getLocationCode() {
                                    return locationCode;
                                }

                                /**
                                 * Sets the value of the locationCode property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setLocationCode(String value) {
                                    this.locationCode = value;
                                }

                            }

                        }

                    }

                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="CustomerInfo" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="InvoluntaryInfo" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="PaymentInfo" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="Payment" maxOccurs="unbounded" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="4" minOccurs="0"/>
             *                                     &lt;/sequence>
             *                                     &lt;attribute name="CashCheckAmount" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="ChangeFeeInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="Code1" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                     &lt;attribute name="Code2" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="PersonName" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                           &lt;/sequence>
             *                           &lt;attribute name="PassengerType" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="ItineraryRef" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="Source" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;attribute name="CreateDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *                 &lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="PurgeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="PurgeDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="TicketData" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="DynamicCurrency" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;attribute name="AmountPerDocument" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="Rate" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="Endorsements" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="2" minOccurs="0"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="ExchangeData" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="AdditionalExchangedTicket" maxOccurs="2" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                             &lt;element name="EquivalentAmountPaid" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                             &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                           &lt;/sequence>
             *                           &lt;attribute name="IssuedInExchangeFor" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="OriginalIssuanceData" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                           &lt;attribute name="OriginalIssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
             *                           &lt;attribute name="OriginalIssuePlace" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="ManualRemarks" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="PaymentInfo" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="Payment" minOccurs="0">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="CC_Info" minOccurs="0">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;sequence>
             *                                                 &lt;element name="MaskedPaymentCard" minOccurs="0">
             *                                                   &lt;complexType>
             *                                                     &lt;complexContent>
             *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                                         &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                                         &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                                       &lt;/restriction>
             *                                                     &lt;/complexContent>
             *                                                   &lt;/complexType>
             *                                                 &lt;/element>
             *                                                 &lt;element name="PaymentCard" minOccurs="0">
             *                                                   &lt;complexType>
             *                                                     &lt;complexContent>
             *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                                         &lt;attribute name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                                         &lt;attribute name="AuthorizationType" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                                         &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                                         &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                                         &lt;attribute name="ExtendedPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" />
             *                                                         &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                                                         &lt;attribute name="PresentCC_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" />
             *                                                       &lt;/restriction>
             *                                                     &lt;/complexContent>
             *                                                   &lt;/complexType>
             *                                                 &lt;/element>
             *                                               &lt;/sequence>
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                       &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
             *                                     &lt;/sequence>
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="Restrictions" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="22" minOccurs="0"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                   &lt;element name="TourInfo" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *                 &lt;attribute name="FirstCertificate" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="ISI" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
             *                 &lt;attribute name="IssuingAgent" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="PrintStation" type="{http://www.w3.org/2001/XMLSchema}string" />
             *                 &lt;attribute name="SecondCertificate" type="{http://www.w3.org/2001/XMLSchema}string" />
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *       &lt;attribute name="AccountingCode" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="EndConjunctionRange" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
             *       &lt;attribute name="IssuingAgent" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="NumCoupons" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "customerInfo",
                "itineraryRef",
                "ticketData"
            })
            public static class Ticketing {

                @XmlElement(name = "CustomerInfo")
                protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo customerInfo;
                @XmlElement(name = "ItineraryRef")
                protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef itineraryRef;
                @XmlElement(name = "TicketData")
                protected List<VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData> ticketData;
                @XmlAttribute(name = "AccountingCode")
                protected String accountingCode;
                @XmlAttribute(name = "EndConjunctionRange")
                protected String endConjunctionRange;
                @XmlAttribute(name = "eTicketNumber")
                protected String eTicketNumber;
                @XmlAttribute(name = "IssueDate")
                protected XMLGregorianCalendar issueDate;
                @XmlAttribute(name = "IssuingAgent")
                protected String issuingAgent;
                @XmlAttribute(name = "NumCoupons")
                protected String numCoupons;

                /**
                 * Gets the value of the customerInfo property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo }
                 *     
                 */
                public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo getCustomerInfo() {
                    return customerInfo;
                }

                /**
                 * Sets the value of the customerInfo property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo }
                 *     
                 */
                public void setCustomerInfo(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo value) {
                    this.customerInfo = value;
                }

                /**
                 * Gets the value of the itineraryRef property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef }
                 *     
                 */
                public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef getItineraryRef() {
                    return itineraryRef;
                }

                /**
                 * Sets the value of the itineraryRef property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef }
                 *     
                 */
                public void setItineraryRef(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef value) {
                    this.itineraryRef = value;
                }

                /**
                 * Gets the value of the ticketData property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the ticketData property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getTicketData().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData }
                 * 
                 * 
                 */
                public List<VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData> getTicketData() {
                    if (ticketData == null) {
                        ticketData = new ArrayList<VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData>();
                    }
                    return this.ticketData;
                }

                /**
                 * Gets the value of the accountingCode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAccountingCode() {
                    return accountingCode;
                }

                /**
                 * Sets the value of the accountingCode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAccountingCode(String value) {
                    this.accountingCode = value;
                }

                /**
                 * Gets the value of the endConjunctionRange property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEndConjunctionRange() {
                    return endConjunctionRange;
                }

                /**
                 * Sets the value of the endConjunctionRange property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEndConjunctionRange(String value) {
                    this.endConjunctionRange = value;
                }

                /**
                 * Gets the value of the eTicketNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getETicketNumber() {
                    return eTicketNumber;
                }

                /**
                 * Sets the value of the eTicketNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setETicketNumber(String value) {
                    this.eTicketNumber = value;
                }

                /**
                 * Gets the value of the issueDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getIssueDate() {
                    return issueDate;
                }

                /**
                 * Sets the value of the issueDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setIssueDate(XMLGregorianCalendar value) {
                    this.issueDate = value;
                }

                /**
                 * Gets the value of the issuingAgent property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIssuingAgent() {
                    return issuingAgent;
                }

                /**
                 * Sets the value of the issuingAgent property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIssuingAgent(String value) {
                    this.issuingAgent = value;
                }

                /**
                 * Gets the value of the numCoupons property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNumCoupons() {
                    return numCoupons;
                }

                /**
                 * Sets the value of the numCoupons property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNumCoupons(String value) {
                    this.numCoupons = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="InvoluntaryInfo" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="PaymentInfo" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="Payment" maxOccurs="unbounded" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="4" minOccurs="0"/>
                 *                           &lt;/sequence>
                 *                           &lt;attribute name="CashCheckAmount" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="ChangeFeeInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="Code1" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                           &lt;attribute name="Code2" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="PersonName" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="PassengerType" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "involuntaryInfo",
                    "paymentInfo",
                    "personName"
                })
                public static class CustomerInfo {

                    @XmlElement(name = "InvoluntaryInfo")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.InvoluntaryInfo involuntaryInfo;
                    @XmlElement(name = "PaymentInfo")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo paymentInfo;
                    @XmlElement(name = "PersonName")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PersonName personName;

                    /**
                     * Gets the value of the involuntaryInfo property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.InvoluntaryInfo }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.InvoluntaryInfo getInvoluntaryInfo() {
                        return involuntaryInfo;
                    }

                    /**
                     * Sets the value of the involuntaryInfo property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.InvoluntaryInfo }
                     *     
                     */
                    public void setInvoluntaryInfo(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.InvoluntaryInfo value) {
                        this.involuntaryInfo = value;
                    }

                    /**
                     * Gets the value of the paymentInfo property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo getPaymentInfo() {
                        return paymentInfo;
                    }

                    /**
                     * Sets the value of the paymentInfo property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo }
                     *     
                     */
                    public void setPaymentInfo(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo value) {
                        this.paymentInfo = value;
                    }

                    /**
                     * Gets the value of the personName property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PersonName }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PersonName getPersonName() {
                        return personName;
                    }

                    /**
                     * Sets the value of the personName property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PersonName }
                     *     
                     */
                    public void setPersonName(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PersonName value) {
                        this.personName = value;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "text"
                    })
                    public static class InvoluntaryInfo {

                        @XmlElement(name = "Text")
                        protected String text;

                        /**
                         * Gets the value of the text property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getText() {
                            return text;
                        }

                        /**
                         * Sets the value of the text property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setText(String value) {
                            this.text = value;
                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="Payment" maxOccurs="unbounded" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="4" minOccurs="0"/>
                     *                 &lt;/sequence>
                     *                 &lt;attribute name="CashCheckAmount" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="ChangeFeeInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="Code1" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                 &lt;attribute name="Code2" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "payment"
                    })
                    public static class PaymentInfo {

                        @XmlElement(name = "Payment")
                        protected List<VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo.Payment> payment;

                        /**
                         * Gets the value of the payment property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the payment property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getPayment().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo.Payment }
                         * 
                         * 
                         */
                        public List<VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo.Payment> getPayment() {
                            if (payment == null) {
                                payment = new ArrayList<VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.CustomerInfo.PaymentInfo.Payment>();
                            }
                            return this.payment;
                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="4" minOccurs="0"/>
                         *       &lt;/sequence>
                         *       &lt;attribute name="CashCheckAmount" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="ChangeFeeInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="Code1" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *       &lt;attribute name="Code2" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "form"
                        })
                        public static class Payment {

                            @XmlElement(name = "Form")
                            protected List<String> form;
                            @XmlAttribute(name = "CashCheckAmount")
                            protected String cashCheckAmount;
                            @XmlAttribute(name = "ChangeFeeInfo")
                            protected String changeFeeInfo;
                            @XmlAttribute(name = "Code1")
                            protected String code1;
                            @XmlAttribute(name = "Code2")
                            protected String code2;

                            /**
                             * Gets the value of the form property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the form property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getForm().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link String }
                             * 
                             * 
                             */
                            public List<String> getForm() {
                                if (form == null) {
                                    form = new ArrayList<String>();
                                }
                                return this.form;
                            }

                            /**
                             * Gets the value of the cashCheckAmount property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCashCheckAmount() {
                                return cashCheckAmount;
                            }

                            /**
                             * Sets the value of the cashCheckAmount property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCashCheckAmount(String value) {
                                this.cashCheckAmount = value;
                            }

                            /**
                             * Gets the value of the changeFeeInfo property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getChangeFeeInfo() {
                                return changeFeeInfo;
                            }

                            /**
                             * Sets the value of the changeFeeInfo property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setChangeFeeInfo(String value) {
                                this.changeFeeInfo = value;
                            }

                            /**
                             * Gets the value of the code1 property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCode1() {
                                return code1;
                            }

                            /**
                             * Sets the value of the code1 property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCode1(String value) {
                                this.code1 = value;
                            }

                            /**
                             * Gets the value of the code2 property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCode2() {
                                return code2;
                            }

                            /**
                             * Sets the value of the code2 property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCode2(String value) {
                                this.code2 = value;
                            }

                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="Surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *       &lt;/sequence>
                     *       &lt;attribute name="PassengerType" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "surname"
                    })
                    public static class PersonName {

                        @XmlElement(name = "Surname")
                        protected String surname;
                        @XmlAttribute(name = "PassengerType")
                        protected String passengerType;

                        /**
                         * Gets the value of the surname property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSurname() {
                            return surname;
                        }

                        /**
                         * Sets the value of the surname property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSurname(String value) {
                            this.surname = value;
                        }

                        /**
                         * Gets the value of the passengerType property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getPassengerType() {
                            return passengerType;
                        }

                        /**
                         * Sets the value of the passengerType property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setPassengerType(String value) {
                            this.passengerType = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="Source" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;attribute name="CreateDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *       &lt;attribute name="ID" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="PurgeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="PurgeDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "source"
                })
                public static class ItineraryRef {

                    @XmlElement(name = "Source")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef.Source source;
                    @XmlAttribute(name = "ID")
                    protected String id;
                    @XmlAttribute(name = "PurgeIndicator")
                    protected String purgeIndicator;
                    @XmlAttribute(name = "PurgeDate")
                    protected XMLGregorianCalendar purgeDate;

                    /**
                     * Gets the value of the source property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef.Source }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef.Source getSource() {
                        return source;
                    }

                    /**
                     * Sets the value of the source property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef.Source }
                     *     
                     */
                    public void setSource(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.ItineraryRef.Source value) {
                        this.source = value;
                    }

                    /**
                     * Gets the value of the id property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getID() {
                        return id;
                    }

                    /**
                     * Sets the value of the id property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setID(String value) {
                        this.id = value;
                    }

                    /**
                     * Gets the value of the purgeIndicator property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPurgeIndicator() {
                        return purgeIndicator;
                    }

                    /**
                     * Sets the value of the purgeIndicator property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPurgeIndicator(String value) {
                        this.purgeIndicator = value;
                    }

                    /**
                     * Gets the value of the purgeDate property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public XMLGregorianCalendar getPurgeDate() {
                        return purgeDate;
                    }

                    /**
                     * Sets the value of the purgeDate property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public void setPurgeDate(XMLGregorianCalendar value) {
                        this.purgeDate = value;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;attribute name="CreateDateTime" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Source {

                        @XmlAttribute(name = "CreateDateTime")
                        protected XMLGregorianCalendar createDateTime;

                        /**
                         * Gets the value of the createDateTime property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public XMLGregorianCalendar getCreateDateTime() {
                            return createDateTime;
                        }

                        /**
                         * Sets the value of the createDateTime property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public void setCreateDateTime(XMLGregorianCalendar value) {
                            this.createDateTime = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="DynamicCurrency" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;attribute name="AmountPerDocument" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="Rate" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="Endorsements" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="2" minOccurs="0"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="ExchangeData" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="AdditionalExchangedTicket" maxOccurs="2" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                   &lt;element name="EquivalentAmountPaid" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                   &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                 &lt;/sequence>
                 *                 &lt;attribute name="IssuedInExchangeFor" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="OriginalIssuanceData" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                 &lt;attribute name="OriginalIssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
                 *                 &lt;attribute name="OriginalIssuePlace" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="ManualRemarks" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="PaymentInfo" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="Payment" minOccurs="0">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="CC_Info" minOccurs="0">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;sequence>
                 *                                       &lt;element name="MaskedPaymentCard" minOccurs="0">
                 *                                         &lt;complexType>
                 *                                           &lt;complexContent>
                 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                               &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                               &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                             &lt;/restriction>
                 *                                           &lt;/complexContent>
                 *                                         &lt;/complexType>
                 *                                       &lt;/element>
                 *                                       &lt;element name="PaymentCard" minOccurs="0">
                 *                                         &lt;complexType>
                 *                                           &lt;complexContent>
                 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                               &lt;attribute name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                               &lt;attribute name="AuthorizationType" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                               &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                               &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                               &lt;attribute name="ExtendedPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                 *                                               &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *                                               &lt;attribute name="PresentCC_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                 *                                             &lt;/restriction>
                 *                                           &lt;/complexContent>
                 *                                         &lt;/complexType>
                 *                                       &lt;/element>
                 *                                     &lt;/sequence>
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                             &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="Restrictions" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="22" minOccurs="0"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *         &lt;element name="TourInfo" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *       &lt;attribute name="FirstCertificate" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="ISI" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="IssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
                 *       &lt;attribute name="IssuingAgent" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="PrintStation" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *       &lt;attribute name="SecondCertificate" type="{http://www.w3.org/2001/XMLSchema}string" />
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "dynamicCurrency",
                    "endorsements",
                    "exchangeData",
                    "manualRemarks",
                    "paymentInfo",
                    "restrictions",
                    "tourInfo"
                })
                public static class TicketData {

                    @XmlElement(name = "DynamicCurrency")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.DynamicCurrency dynamicCurrency;
                    @XmlElement(name = "Endorsements")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Endorsements endorsements;
                    @XmlElement(name = "ExchangeData")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData exchangeData;
                    @XmlElement(name = "ManualRemarks")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ManualRemarks manualRemarks;
                    @XmlElement(name = "PaymentInfo")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo paymentInfo;
                    @XmlElement(name = "Restrictions")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Restrictions restrictions;
                    @XmlElement(name = "TourInfo")
                    protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.TourInfo tourInfo;
                    @XmlAttribute(name = "FirstCertificate")
                    protected String firstCertificate;
                    @XmlAttribute(name = "ISI")
                    protected String isi;
                    @XmlAttribute(name = "IssueDate")
                    protected XMLGregorianCalendar issueDate;
                    @XmlAttribute(name = "IssuingAgent")
                    protected String issuingAgent;
                    @XmlAttribute(name = "PrintStation")
                    protected String printStation;
                    @XmlAttribute(name = "SecondCertificate")
                    protected String secondCertificate;

                    /**
                     * Gets the value of the dynamicCurrency property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.DynamicCurrency }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.DynamicCurrency getDynamicCurrency() {
                        return dynamicCurrency;
                    }

                    /**
                     * Sets the value of the dynamicCurrency property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.DynamicCurrency }
                     *     
                     */
                    public void setDynamicCurrency(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.DynamicCurrency value) {
                        this.dynamicCurrency = value;
                    }

                    /**
                     * Gets the value of the endorsements property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Endorsements }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Endorsements getEndorsements() {
                        return endorsements;
                    }

                    /**
                     * Sets the value of the endorsements property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Endorsements }
                     *     
                     */
                    public void setEndorsements(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Endorsements value) {
                        this.endorsements = value;
                    }

                    /**
                     * Gets the value of the exchangeData property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData getExchangeData() {
                        return exchangeData;
                    }

                    /**
                     * Sets the value of the exchangeData property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData }
                     *     
                     */
                    public void setExchangeData(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData value) {
                        this.exchangeData = value;
                    }

                    /**
                     * Gets the value of the manualRemarks property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ManualRemarks }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ManualRemarks getManualRemarks() {
                        return manualRemarks;
                    }

                    /**
                     * Sets the value of the manualRemarks property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ManualRemarks }
                     *     
                     */
                    public void setManualRemarks(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ManualRemarks value) {
                        this.manualRemarks = value;
                    }

                    /**
                     * Gets the value of the paymentInfo property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo getPaymentInfo() {
                        return paymentInfo;
                    }

                    /**
                     * Sets the value of the paymentInfo property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo }
                     *     
                     */
                    public void setPaymentInfo(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo value) {
                        this.paymentInfo = value;
                    }

                    /**
                     * Gets the value of the restrictions property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Restrictions }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Restrictions getRestrictions() {
                        return restrictions;
                    }

                    /**
                     * Sets the value of the restrictions property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Restrictions }
                     *     
                     */
                    public void setRestrictions(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.Restrictions value) {
                        this.restrictions = value;
                    }

                    /**
                     * Gets the value of the tourInfo property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.TourInfo }
                     *     
                     */
                    public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.TourInfo getTourInfo() {
                        return tourInfo;
                    }

                    /**
                     * Sets the value of the tourInfo property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.TourInfo }
                     *     
                     */
                    public void setTourInfo(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.TourInfo value) {
                        this.tourInfo = value;
                    }

                    /**
                     * Gets the value of the firstCertificate property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getFirstCertificate() {
                        return firstCertificate;
                    }

                    /**
                     * Sets the value of the firstCertificate property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFirstCertificate(String value) {
                        this.firstCertificate = value;
                    }

                    /**
                     * Gets the value of the isi property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getISI() {
                        return isi;
                    }

                    /**
                     * Sets the value of the isi property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setISI(String value) {
                        this.isi = value;
                    }

                    /**
                     * Gets the value of the issueDate property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public XMLGregorianCalendar getIssueDate() {
                        return issueDate;
                    }

                    /**
                     * Sets the value of the issueDate property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link XMLGregorianCalendar }
                     *     
                     */
                    public void setIssueDate(XMLGregorianCalendar value) {
                        this.issueDate = value;
                    }

                    /**
                     * Gets the value of the issuingAgent property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getIssuingAgent() {
                        return issuingAgent;
                    }

                    /**
                     * Sets the value of the issuingAgent property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setIssuingAgent(String value) {
                        this.issuingAgent = value;
                    }

                    /**
                     * Gets the value of the printStation property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getPrintStation() {
                        return printStation;
                    }

                    /**
                     * Sets the value of the printStation property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setPrintStation(String value) {
                        this.printStation = value;
                    }

                    /**
                     * Gets the value of the secondCertificate property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getSecondCertificate() {
                        return secondCertificate;
                    }

                    /**
                     * Sets the value of the secondCertificate property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setSecondCertificate(String value) {
                        this.secondCertificate = value;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;attribute name="AmountPerDocument" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="Rate" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class DynamicCurrency {

                        @XmlAttribute(name = "AmountPerDocument")
                        protected String amountPerDocument;
                        @XmlAttribute(name = "Code")
                        protected String code;
                        @XmlAttribute(name = "Rate")
                        protected String rate;

                        /**
                         * Gets the value of the amountPerDocument property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getAmountPerDocument() {
                            return amountPerDocument;
                        }

                        /**
                         * Sets the value of the amountPerDocument property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setAmountPerDocument(String value) {
                            this.amountPerDocument = value;
                        }

                        /**
                         * Gets the value of the code property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCode() {
                            return code;
                        }

                        /**
                         * Sets the value of the code property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCode(String value) {
                            this.code = value;
                        }

                        /**
                         * Gets the value of the rate property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getRate() {
                            return rate;
                        }

                        /**
                         * Sets the value of the rate property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setRate(String value) {
                            this.rate = value;
                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="2" minOccurs="0"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "text"
                    })
                    public static class Endorsements {

                        @XmlElement(name = "Text")
                        protected List<String> text;

                        /**
                         * Gets the value of the text property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the text property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getText().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link String }
                         * 
                         * 
                         */
                        public List<String> getText() {
                            if (text == null) {
                                text = new ArrayList<String>();
                            }
                            return this.text;
                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="AdditionalExchangedTicket" maxOccurs="2" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="EquivalentAmountPaid" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *       &lt;/sequence>
                     *       &lt;attribute name="IssuedInExchangeFor" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="OriginalIssuanceData" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *       &lt;attribute name="OriginalIssueDate" type="{http://webservices.sabre.com/sabreXML/2011/10}fullDate" />
                     *       &lt;attribute name="OriginalIssuePlace" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "additionalExchangedTicket",
                        "equivalentAmountPaid",
                        "text"
                    })
                    public static class ExchangeData {

                        @XmlElement(name = "AdditionalExchangedTicket")
                        protected List<VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.AdditionalExchangedTicket> additionalExchangedTicket;
                        @XmlElement(name = "EquivalentAmountPaid")
                        protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.EquivalentAmountPaid equivalentAmountPaid;
                        @XmlElement(name = "Text")
                        protected String text;
                        @XmlAttribute(name = "IssuedInExchangeFor")
                        protected String issuedInExchangeFor;
                        @XmlAttribute(name = "LocationName")
                        protected String locationName;
                        @XmlAttribute(name = "OriginalIssuanceData")
                        protected String originalIssuanceData;
                        @XmlAttribute(name = "OriginalIssueDate")
                        protected XMLGregorianCalendar originalIssueDate;
                        @XmlAttribute(name = "OriginalIssuePlace")
                        protected String originalIssuePlace;

                        /**
                         * Gets the value of the additionalExchangedTicket property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the additionalExchangedTicket property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getAdditionalExchangedTicket().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.AdditionalExchangedTicket }
                         * 
                         * 
                         */
                        public List<VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.AdditionalExchangedTicket> getAdditionalExchangedTicket() {
                            if (additionalExchangedTicket == null) {
                                additionalExchangedTicket = new ArrayList<VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.AdditionalExchangedTicket>();
                            }
                            return this.additionalExchangedTicket;
                        }

                        /**
                         * Gets the value of the equivalentAmountPaid property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.EquivalentAmountPaid }
                         *     
                         */
                        public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.EquivalentAmountPaid getEquivalentAmountPaid() {
                            return equivalentAmountPaid;
                        }

                        /**
                         * Sets the value of the equivalentAmountPaid property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.EquivalentAmountPaid }
                         *     
                         */
                        public void setEquivalentAmountPaid(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.ExchangeData.EquivalentAmountPaid value) {
                            this.equivalentAmountPaid = value;
                        }

                        /**
                         * Gets the value of the text property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getText() {
                            return text;
                        }

                        /**
                         * Sets the value of the text property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setText(String value) {
                            this.text = value;
                        }

                        /**
                         * Gets the value of the issuedInExchangeFor property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getIssuedInExchangeFor() {
                            return issuedInExchangeFor;
                        }

                        /**
                         * Sets the value of the issuedInExchangeFor property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setIssuedInExchangeFor(String value) {
                            this.issuedInExchangeFor = value;
                        }

                        /**
                         * Gets the value of the locationName property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getLocationName() {
                            return locationName;
                        }

                        /**
                         * Sets the value of the locationName property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setLocationName(String value) {
                            this.locationName = value;
                        }

                        /**
                         * Gets the value of the originalIssuanceData property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getOriginalIssuanceData() {
                            return originalIssuanceData;
                        }

                        /**
                         * Sets the value of the originalIssuanceData property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setOriginalIssuanceData(String value) {
                            this.originalIssuanceData = value;
                        }

                        /**
                         * Gets the value of the originalIssueDate property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public XMLGregorianCalendar getOriginalIssueDate() {
                            return originalIssueDate;
                        }

                        /**
                         * Sets the value of the originalIssueDate property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link XMLGregorianCalendar }
                         *     
                         */
                        public void setOriginalIssueDate(XMLGregorianCalendar value) {
                            this.originalIssueDate = value;
                        }

                        /**
                         * Gets the value of the originalIssuePlace property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getOriginalIssuePlace() {
                            return originalIssuePlace;
                        }

                        /**
                         * Sets the value of the originalIssuePlace property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setOriginalIssuePlace(String value) {
                            this.originalIssuePlace = value;
                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;attribute name="eTicketNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class AdditionalExchangedTicket {

                            @XmlAttribute(name = "eTicketNumber")
                            protected String eTicketNumber;

                            /**
                             * Gets the value of the eTicketNumber property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getETicketNumber() {
                                return eTicketNumber;
                            }

                            /**
                             * Sets the value of the eTicketNumber property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setETicketNumber(String value) {
                                this.eTicketNumber = value;
                            }

                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "")
                        public static class EquivalentAmountPaid {

                            @XmlAttribute(name = "Code")
                            protected String code;

                            /**
                             * Gets the value of the code property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link String }
                             *     
                             */
                            public String getCode() {
                                return code;
                            }

                            /**
                             * Sets the value of the code property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link String }
                             *     
                             */
                            public void setCode(String value) {
                                this.code = value;
                            }

                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "text"
                    })
                    public static class ManualRemarks {

                        @XmlElement(name = "Text")
                        protected String text;

                        /**
                         * Gets the value of the text property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getText() {
                            return text;
                        }

                        /**
                         * Sets the value of the text property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setText(String value) {
                            this.text = value;
                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="Payment" minOccurs="0">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="CC_Info" minOccurs="0">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;sequence>
                     *                             &lt;element name="MaskedPaymentCard" minOccurs="0">
                     *                               &lt;complexType>
                     *                                 &lt;complexContent>
                     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                                     &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                                     &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                                   &lt;/restriction>
                     *                                 &lt;/complexContent>
                     *                               &lt;/complexType>
                     *                             &lt;/element>
                     *                             &lt;element name="PaymentCard" minOccurs="0">
                     *                               &lt;complexType>
                     *                                 &lt;complexContent>
                     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                                     &lt;attribute name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                                     &lt;attribute name="AuthorizationType" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                                     &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                                     &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                                     &lt;attribute name="ExtendedPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                     *                                     &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *                                     &lt;attribute name="PresentCC_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                     *                                   &lt;/restriction>
                     *                                 &lt;/complexContent>
                     *                               &lt;/complexType>
                     *                             &lt;/element>
                     *                           &lt;/sequence>
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                   &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "payment"
                    })
                    public static class PaymentInfo {

                        @XmlElement(name = "Payment")
                        protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment payment;

                        /**
                         * Gets the value of the payment property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment }
                         *     
                         */
                        public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment getPayment() {
                            return payment;
                        }

                        /**
                         * Sets the value of the payment property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment }
                         *     
                         */
                        public void setPayment(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment value) {
                            this.payment = value;
                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="CC_Info" minOccurs="0">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;sequence>
                         *                   &lt;element name="MaskedPaymentCard" minOccurs="0">
                         *                     &lt;complexType>
                         *                       &lt;complexContent>
                         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                           &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                           &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                         &lt;/restriction>
                         *                       &lt;/complexContent>
                         *                     &lt;/complexType>
                         *                   &lt;/element>
                         *                   &lt;element name="PaymentCard" minOccurs="0">
                         *                     &lt;complexType>
                         *                       &lt;complexContent>
                         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                           &lt;attribute name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                           &lt;attribute name="AuthorizationType" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                           &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                           &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                           &lt;attribute name="ExtendedPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                         *                           &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                         *                           &lt;attribute name="PresentCC_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                         *                         &lt;/restriction>
                         *                       &lt;/complexContent>
                         *                     &lt;/complexType>
                         *                   &lt;/element>
                         *                 &lt;/sequence>
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *         &lt;element name="Form" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "ccInfo",
                            "form"
                        })
                        public static class Payment {

                            @XmlElement(name = "CC_Info")
                            protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo ccInfo;
                            @XmlElement(name = "Form")
                            protected List<String> form;

                            /**
                             * Gets the value of the ccInfo property.
                             * 
                             * @return
                             *     possible object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo }
                             *     
                             */
                            public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo getCCInfo() {
                                return ccInfo;
                            }

                            /**
                             * Sets the value of the ccInfo property.
                             * 
                             * @param value
                             *     allowed object is
                             *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo }
                             *     
                             */
                            public void setCCInfo(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo value) {
                                this.ccInfo = value;
                            }

                            /**
                             * Gets the value of the form property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the form property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getForm().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link String }
                             * 
                             * 
                             */
                            public List<String> getForm() {
                                if (form == null) {
                                    form = new ArrayList<String>();
                                }
                                return this.form;
                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;sequence>
                             *         &lt;element name="MaskedPaymentCard" minOccurs="0">
                             *           &lt;complexType>
                             *             &lt;complexContent>
                             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *                 &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *                 &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *               &lt;/restriction>
                             *             &lt;/complexContent>
                             *           &lt;/complexType>
                             *         &lt;/element>
                             *         &lt;element name="PaymentCard" minOccurs="0">
                             *           &lt;complexType>
                             *             &lt;complexContent>
                             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *                 &lt;attribute name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *                 &lt;attribute name="AuthorizationType" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *                 &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *                 &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *                 &lt;attribute name="ExtendedPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                             *                 &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                             *                 &lt;attribute name="PresentCC_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                             *               &lt;/restriction>
                             *             &lt;/complexContent>
                             *           &lt;/complexType>
                             *         &lt;/element>
                             *       &lt;/sequence>
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "maskedPaymentCard",
                                "paymentCard"
                            })
                            public static class CCInfo {

                                @XmlElement(name = "MaskedPaymentCard")
                                protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.MaskedPaymentCard maskedPaymentCard;
                                @XmlElement(name = "PaymentCard")
                                protected VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.PaymentCard paymentCard;

                                /**
                                 * Gets the value of the maskedPaymentCard property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.MaskedPaymentCard }
                                 *     
                                 */
                                public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.MaskedPaymentCard getMaskedPaymentCard() {
                                    return maskedPaymentCard;
                                }

                                /**
                                 * Sets the value of the maskedPaymentCard property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.MaskedPaymentCard }
                                 *     
                                 */
                                public void setMaskedPaymentCard(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.MaskedPaymentCard value) {
                                    this.maskedPaymentCard = value;
                                }

                                /**
                                 * Gets the value of the paymentCard property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.PaymentCard }
                                 *     
                                 */
                                public VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.PaymentCard getPaymentCard() {
                                    return paymentCard;
                                }

                                /**
                                 * Sets the value of the paymentCard property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.PaymentCard }
                                 *     
                                 */
                                public void setPaymentCard(VCRDisplayRS.TicketingInfos.TicketingInfo.Ticketing.TicketData.PaymentInfo.Payment.CCInfo.PaymentCard value) {
                                    this.paymentCard = value;
                                }


                                /**
                                 * <p>Java class for anonymous complex type.
                                 * 
                                 * <p>The following schema fragment specifies the expected content contained within this class.
                                 * 
                                 * <pre>
                                 * &lt;complexType>
                                 *   &lt;complexContent>
                                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                                 *       &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
                                 *       &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                                 *     &lt;/restriction>
                                 *   &lt;/complexContent>
                                 * &lt;/complexType>
                                 * </pre>
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "")
                                public static class MaskedPaymentCard {

                                    @XmlAttribute(name = "ExpireDate")
                                    protected String expireDate;
                                    @XmlAttribute(name = "Number")
                                    protected String number;

                                    /**
                                     * Gets the value of the expireDate property.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getExpireDate() {
                                        return expireDate;
                                    }

                                    /**
                                     * Sets the value of the expireDate property.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setExpireDate(String value) {
                                        this.expireDate = value;
                                    }

                                    /**
                                     * Gets the value of the number property.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getNumber() {
                                        return number;
                                    }

                                    /**
                                     * Sets the value of the number property.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setNumber(String value) {
                                        this.number = value;
                                    }

                                }


                                /**
                                 * <p>Java class for anonymous complex type.
                                 * 
                                 * <p>The following schema fragment specifies the expected content contained within this class.
                                 * 
                                 * <pre>
                                 * &lt;complexType>
                                 *   &lt;complexContent>
                                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                                 *       &lt;attribute name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" />
                                 *       &lt;attribute name="AuthorizationType" type="{http://www.w3.org/2001/XMLSchema}string" />
                                 *       &lt;attribute name="Code" type="{http://www.w3.org/2001/XMLSchema}string" />
                                 *       &lt;attribute name="ExpireDate" type="{http://www.w3.org/2001/XMLSchema}string" />
                                 *       &lt;attribute name="ExtendedPayment" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                                 *       &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                                 *       &lt;attribute name="PresentCC_Ind" type="{http://www.w3.org/2001/XMLSchema}boolean" />
                                 *     &lt;/restriction>
                                 *   &lt;/complexContent>
                                 * &lt;/complexType>
                                 * </pre>
                                 * 
                                 * 
                                 */
                                @XmlAccessorType(XmlAccessType.FIELD)
                                @XmlType(name = "")
                                public static class PaymentCard {

                                    @XmlAttribute(name = "AuthorizationCode")
                                    protected String authorizationCode;
                                    @XmlAttribute(name = "AuthorizationType")
                                    protected String authorizationType;
                                    @XmlAttribute(name = "Code")
                                    protected String code;
                                    @XmlAttribute(name = "ExpireDate")
                                    protected String expireDate;
                                    @XmlAttribute(name = "ExtendedPayment")
                                    protected Boolean extendedPayment;
                                    @XmlAttribute(name = "Number")
                                    protected String number;
                                    @XmlAttribute(name = "PresentCC_Ind")
                                    protected Boolean presentCCInd;

                                    /**
                                     * Gets the value of the authorizationCode property.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getAuthorizationCode() {
                                        return authorizationCode;
                                    }

                                    /**
                                     * Sets the value of the authorizationCode property.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setAuthorizationCode(String value) {
                                        this.authorizationCode = value;
                                    }

                                    /**
                                     * Gets the value of the authorizationType property.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getAuthorizationType() {
                                        return authorizationType;
                                    }

                                    /**
                                     * Sets the value of the authorizationType property.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setAuthorizationType(String value) {
                                        this.authorizationType = value;
                                    }

                                    /**
                                     * Gets the value of the code property.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getCode() {
                                        return code;
                                    }

                                    /**
                                     * Sets the value of the code property.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setCode(String value) {
                                        this.code = value;
                                    }

                                    /**
                                     * Gets the value of the expireDate property.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getExpireDate() {
                                        return expireDate;
                                    }

                                    /**
                                     * Sets the value of the expireDate property.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setExpireDate(String value) {
                                        this.expireDate = value;
                                    }

                                    /**
                                     * Gets the value of the extendedPayment property.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link Boolean }
                                     *     
                                     */
                                    public Boolean isExtendedPayment() {
                                        return extendedPayment;
                                    }

                                    /**
                                     * Sets the value of the extendedPayment property.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link Boolean }
                                     *     
                                     */
                                    public void setExtendedPayment(Boolean value) {
                                        this.extendedPayment = value;
                                    }

                                    /**
                                     * Gets the value of the number property.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link String }
                                     *     
                                     */
                                    public String getNumber() {
                                        return number;
                                    }

                                    /**
                                     * Sets the value of the number property.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link String }
                                     *     
                                     */
                                    public void setNumber(String value) {
                                        this.number = value;
                                    }

                                    /**
                                     * Gets the value of the presentCCInd property.
                                     * 
                                     * @return
                                     *     possible object is
                                     *     {@link Boolean }
                                     *     
                                     */
                                    public Boolean isPresentCCInd() {
                                        return presentCCInd;
                                    }

                                    /**
                                     * Sets the value of the presentCCInd property.
                                     * 
                                     * @param value
                                     *     allowed object is
                                     *     {@link Boolean }
                                     *     
                                     */
                                    public void setPresentCCInd(Boolean value) {
                                        this.presentCCInd = value;
                                    }

                                }

                            }

                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="22" minOccurs="0"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "text"
                    })
                    public static class Restrictions {

                        @XmlElement(name = "Text")
                        protected List<String> text;

                        /**
                         * Gets the value of the text property.
                         * 
                         * <p>
                         * This accessor method returns a reference to the live list,
                         * not a snapshot. Therefore any modification you make to the
                         * returned list will be present inside the JAXB object.
                         * This is why there is not a <CODE>set</CODE> method for the text property.
                         * 
                         * <p>
                         * For example, to add a new item, do as follows:
                         * <pre>
                         *    getText().add(newItem);
                         * </pre>
                         * 
                         * 
                         * <p>
                         * Objects of the following type(s) are allowed in the list
                         * {@link String }
                         * 
                         * 
                         */
                        public List<String> getText() {
                            if (text == null) {
                                text = new ArrayList<String>();
                            }
                            return this.text;
                        }

                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;attribute name="Number" type="{http://www.w3.org/2001/XMLSchema}string" />
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class TourInfo {

                        @XmlAttribute(name = "Number")
                        protected String number;

                        /**
                         * Gets the value of the number property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getNumber() {
                            return number;
                        }

                        /**
                         * Sets the value of the number property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNumber(String value) {
                            this.number = value;
                        }

                    }

                }

            }

        }

    }

}
