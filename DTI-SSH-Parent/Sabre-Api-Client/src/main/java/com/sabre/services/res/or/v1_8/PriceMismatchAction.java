
package com.sabre.services.res.or.v1_8;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PriceMismatchAction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PriceMismatchAction">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REJECT"/>
 *     &lt;enumeration value="ACCEPT_ANY_PRICE"/>
 *     &lt;enumeration value="ACCEPT_LOWER_PRICE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PriceMismatchAction")
@XmlEnum
public enum PriceMismatchAction {

    REJECT,
    ACCEPT_ANY_PRICE,
    ACCEPT_LOWER_PRICE;

    public String value() {
        return name();
    }

    public static PriceMismatchAction fromValue(String v) {
        return valueOf(v);
    }

}
