
package com.sabre.services.res.or.v1_8;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.sabre.services.res.or.v1_8 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AdditionalContent_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "AdditionalContent");
    private final static QName _RailProduct_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "RailProduct");
    private final static QName _ProductBase_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "ProductBase");
    private final static QName _PaymentCard_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "PaymentCard");
    private final static QName _Check_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "Check");
    private final static QName _FormOfPayment_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "FormOfPayment");
    private final static QName _HotelProduct_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "HotelProduct");
    private final static QName _AncillaryProduct_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "AncillaryProduct");
    private final static QName _SocialMediaContact_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "SocialMediaContact");
    private final static QName _AccountingField_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "AccountingField");
    private final static QName _LangDetails_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "LangDetails");
    private final static QName _ParametersMap_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "ParametersMap");
    private final static QName _Cash_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "Cash");
    private final static QName _ProductDetails_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "ProductDetails");
    private final static QName _InvoiceData_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "InvoiceData");
    private final static QName _GroundTransportationProduct_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "GroundTransportationProduct");
    private final static QName _AgencyFees_QNAME = new QName("http://services.sabre.com/res/or/v1_8", "AgencyFees");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.sabre.services.res.or.v1_8
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link com.sabre.services.res.or.v1_8.AncillaryServiceData }
     * 
     */
    public com.sabre.services.res.or.v1_8.AncillaryServiceData createAncillaryServiceData() {
        return new com.sabre.services.res.or.v1_8.AncillaryServiceData();
    }

    /**
     * Create an instance of {@link LoyaltyProgramRedemption }
     * 
     */
    public LoyaltyProgramRedemption createLoyaltyProgramRedemption() {
        return new LoyaltyProgramRedemption();
    }

    /**
     * Create an instance of {@link AccountingTransaction }
     * 
     */
    public AccountingTransaction createAccountingTransaction() {
        return new AccountingTransaction();
    }

    /**
     * Create an instance of {@link ServiceProviderType }
     * 
     */
    public ServiceProviderType createServiceProviderType() {
        return new ServiceProviderType();
    }

    /**
     * Create an instance of {@link PassengerType }
     * 
     */
    public PassengerType createPassengerType() {
        return new PassengerType();
    }

    /**
     * Create an instance of {@link DocumentType }
     * 
     */
    public DocumentType createDocumentType() {
        return new DocumentType();
    }

    /**
     * Create an instance of {@link DocumentType.DocumentRules }
     * 
     */
    public DocumentType.DocumentRules createDocumentTypeDocumentRules() {
        return new DocumentType.DocumentRules();
    }

    /**
     * Create an instance of {@link DocumentType.FormOfPayment }
     * 
     */
    public DocumentType.FormOfPayment createDocumentTypeFormOfPayment() {
        return new DocumentType.FormOfPayment();
    }

    /**
     * Create an instance of {@link DocumentType.FormOfPayment.CreditCard }
     * 
     */
    public DocumentType.FormOfPayment.CreditCard createDocumentTypeFormOfPaymentCreditCard() {
        return new DocumentType.FormOfPayment.CreditCard();
    }

    /**
     * Create an instance of {@link DocumentType.SegmentReferences }
     * 
     */
    public DocumentType.SegmentReferences createDocumentTypeSegmentReferences() {
        return new DocumentType.SegmentReferences();
    }

    /**
     * Create an instance of {@link DocumentType.PassengerReferences }
     * 
     */
    public DocumentType.PassengerReferences createDocumentTypePassengerReferences() {
        return new DocumentType.PassengerReferences();
    }

    /**
     * Create an instance of {@link VirtualCard }
     * 
     */
    public VirtualCard createVirtualCard() {
        return new VirtualCard();
    }

    /**
     * Create an instance of {@link VirtualCard.Transactions }
     * 
     */
    public VirtualCard.Transactions createVirtualCardTransactions() {
        return new VirtualCard.Transactions();
    }

    /**
     * Create an instance of {@link AccountingTransactionAmount }
     * 
     */
    public AccountingTransactionAmount createAccountingTransactionAmount() {
        return new AccountingTransactionAmount();
    }

    /**
     * Create an instance of {@link AccountingTransactionAmountWithRule }
     * 
     */
    public AccountingTransactionAmountWithRule createAccountingTransactionAmountWithRule() {
        return new AccountingTransactionAmountWithRule();
    }

    /**
     * Create an instance of {@link RailFareType }
     * 
     */
    public RailFareType createRailFareType() {
        return new RailFareType();
    }

    /**
     * Create an instance of {@link RailFareType.Fare }
     * 
     */
    public RailFareType.Fare createRailFareTypeFare() {
        return new RailFareType.Fare();
    }

    /**
     * Create an instance of {@link RailFareType.Fare.FareDescription }
     * 
     */
    public RailFareType.Fare.FareDescription createRailFareTypeFareFareDescription() {
        return new RailFareType.Fare.FareDescription();
    }

    /**
     * Create an instance of {@link RailFareType.Fare.SegmentReferences }
     * 
     */
    public RailFareType.Fare.SegmentReferences createRailFareTypeFareSegmentReferences() {
        return new RailFareType.Fare.SegmentReferences();
    }

    /**
     * Create an instance of {@link RailFareType.Fare.PassengerReferences }
     * 
     */
    public RailFareType.Fare.PassengerReferences createRailFareTypeFarePassengerReferences() {
        return new RailFareType.Fare.PassengerReferences();
    }

    /**
     * Create an instance of {@link AccountingTransactionItem }
     * 
     */
    public AccountingTransactionItem createAccountingTransactionItem() {
        return new AccountingTransactionItem();
    }

    /**
     * Create an instance of {@link AirType }
     * 
     */
    public AirType createAirType() {
        return new AirType();
    }

    /**
     * Create an instance of {@link AirType.AncillaryServices }
     * 
     */
    public AirType.AncillaryServices createAirTypeAncillaryServices() {
        return new AirType.AncillaryServices();
    }

    /**
     * Create an instance of {@link RailAccommodationType }
     * 
     */
    public RailAccommodationType createRailAccommodationType() {
        return new RailAccommodationType();
    }

    /**
     * Create an instance of {@link RailAccommodationType.Placement }
     * 
     */
    public RailAccommodationType.Placement createRailAccommodationTypePlacement() {
        return new RailAccommodationType.Placement();
    }

    /**
     * Create an instance of {@link RailAccommodationType.Placement.Coach }
     * 
     */
    public RailAccommodationType.Placement.Coach createRailAccommodationTypePlacementCoach() {
        return new RailAccommodationType.Placement.Coach();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link AddressType.CountryCodes }
     * 
     */
    public AddressType.CountryCodes createAddressTypeCountryCodes() {
        return new AddressType.CountryCodes();
    }

    /**
     * Create an instance of {@link AddressType.StateProvinceCodes }
     * 
     */
    public AddressType.StateProvinceCodes createAddressTypeStateProvinceCodes() {
        return new AddressType.StateProvinceCodes();
    }

    /**
     * Create an instance of {@link AddressType.CityCodes }
     * 
     */
    public AddressType.CityCodes createAddressTypeCityCodes() {
        return new AddressType.CityCodes();
    }

    /**
     * Create an instance of {@link ServiceRefType }
     * 
     */
    public ServiceRefType createServiceRefType() {
        return new ServiceRefType();
    }

    /**
     * Create an instance of {@link ServiceRefType.PassengerReferences }
     * 
     */
    public ServiceRefType.PassengerReferences createServiceRefTypePassengerReferences() {
        return new ServiceRefType.PassengerReferences();
    }

    /**
     * Create an instance of {@link HotelType }
     * 
     */
    public HotelType createHotelType() {
        return new HotelType();
    }

    /**
     * Create an instance of {@link HotelType.AdditionalInformation }
     * 
     */
    public HotelType.AdditionalInformation createHotelTypeAdditionalInformation() {
        return new HotelType.AdditionalInformation();
    }

    /**
     * Create an instance of {@link HotelType.Reservation }
     * 
     */
    public HotelType.Reservation createHotelTypeReservation() {
        return new HotelType.Reservation();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.HotelTotalPricing }
     * 
     */
    public HotelType.Reservation.HotelTotalPricing createHotelTypeReservationHotelTotalPricing() {
        return new HotelType.Reservation.HotelTotalPricing();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.HotelTotalPricing.HotelFees }
     * 
     */
    public HotelType.Reservation.HotelTotalPricing.HotelFees createHotelTypeReservationHotelTotalPricingHotelFees() {
        return new HotelType.Reservation.HotelTotalPricing.HotelFees();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.HotelTotalPricing.TotalSurcharge }
     * 
     */
    public HotelType.Reservation.HotelTotalPricing.TotalSurcharge createHotelTypeReservationHotelTotalPricingTotalSurcharge() {
        return new HotelType.Reservation.HotelTotalPricing.TotalSurcharge();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.HotelTotalPricing.TotalTax }
     * 
     */
    public HotelType.Reservation.HotelTotalPricing.TotalTax createHotelTypeReservationHotelTotalPricingTotalTax() {
        return new HotelType.Reservation.HotelTotalPricing.TotalTax();
    }

    /**
     * Create an instance of {@link AccountingTransactionSource }
     * 
     */
    public AccountingTransactionSource createAccountingTransactionSource() {
        return new AccountingTransactionSource();
    }

    /**
     * Create an instance of {@link CompanyType }
     * 
     */
    public CompanyType createCompanyType() {
        return new CompanyType();
    }

    /**
     * Create an instance of {@link LocationDetailsType }
     * 
     */
    public LocationDetailsType createLocationDetailsType() {
        return new LocationDetailsType();
    }

    /**
     * Create an instance of {@link LocationDetailsType.RailwayStationInfo }
     * 
     */
    public LocationDetailsType.RailwayStationInfo createLocationDetailsTypeRailwayStationInfo() {
        return new LocationDetailsType.RailwayStationInfo();
    }

    /**
     * Create an instance of {@link LocationDetailsType.RailwayStationInfo.LocationCodes }
     * 
     */
    public LocationDetailsType.RailwayStationInfo.LocationCodes createLocationDetailsTypeRailwayStationInfoLocationCodes() {
        return new LocationDetailsType.RailwayStationInfo.LocationCodes();
    }

    /**
     * Create an instance of {@link TourType }
     * 
     */
    public TourType createTourType() {
        return new TourType();
    }

    /**
     * Create an instance of {@link TourType.Price }
     * 
     */
    public TourType.Price createTourTypePrice() {
        return new TourType.Price();
    }

    /**
     * Create an instance of {@link TourType.Price.Breakdown }
     * 
     */
    public TourType.Price.Breakdown createTourTypePriceBreakdown() {
        return new TourType.Price.Breakdown();
    }

    /**
     * Create an instance of {@link TourType.Price.Breakdown.References }
     * 
     */
    public TourType.Price.Breakdown.References createTourTypePriceBreakdownReferences() {
        return new TourType.Price.Breakdown.References();
    }

    /**
     * Create an instance of {@link SupplementaryServiceType }
     * 
     */
    public SupplementaryServiceType createSupplementaryServiceType() {
        return new SupplementaryServiceType();
    }

    /**
     * Create an instance of {@link SupplementaryServiceType.PassengerReferences }
     * 
     */
    public SupplementaryServiceType.PassengerReferences createSupplementaryServiceTypePassengerReferences() {
        return new SupplementaryServiceType.PassengerReferences();
    }

    /**
     * Create an instance of {@link MapType }
     * 
     */
    public MapType createMapType() {
        return new MapType();
    }

    /**
     * Create an instance of {@link HotelProductType }
     * 
     */
    public HotelProductType createHotelProductType() {
        return new HotelProductType();
    }

    /**
     * Create an instance of {@link HotelProductType.HotelPolicy }
     * 
     */
    public HotelProductType.HotelPolicy createHotelProductTypeHotelPolicy() {
        return new HotelProductType.HotelPolicy();
    }

    /**
     * Create an instance of {@link HotelProductType.HotelPolicy.CancellationPolicyInfoList }
     * 
     */
    public HotelProductType.HotelPolicy.CancellationPolicyInfoList createHotelProductTypeHotelPolicyCancellationPolicyInfoList() {
        return new HotelProductType.HotelPolicy.CancellationPolicyInfoList();
    }

    /**
     * Create an instance of {@link RailType }
     * 
     */
    public RailType createRailType() {
        return new RailType();
    }

    /**
     * Create an instance of {@link RailType.Details }
     * 
     */
    public RailType.Details createRailTypeDetails() {
        return new RailType.Details();
    }

    /**
     * Create an instance of {@link ProductDetailsType }
     * 
     */
    public ProductDetailsType createProductDetailsType() {
        return new ProductDetailsType();
    }

    /**
     * Create an instance of {@link PaymentCard }
     * 
     */
    public PaymentCard createPaymentCard() {
        return new PaymentCard();
    }

    /**
     * Create an instance of {@link com.sabre.services.res.or.v1_8.AncillaryServiceData.ProductTextDetails }
     * 
     */
    public com.sabre.services.res.or.v1_8.AncillaryServiceData.ProductTextDetails createAncillaryServiceDataProductTextDetails() {
        return new com.sabre.services.res.or.v1_8.AncillaryServiceData.ProductTextDetails();
    }

    /**
     * Create an instance of {@link NameAssociationList }
     * 
     */
    public NameAssociationList createNameAssociationList() {
        return new NameAssociationList();
    }

    /**
     * Create an instance of {@link SegmentAssociationList }
     * 
     */
    public SegmentAssociationList createSegmentAssociationList() {
        return new SegmentAssociationList();
    }

    /**
     * Create an instance of {@link AncillaryServiceETicketNumberUpdate }
     * 
     */
    public AncillaryServiceETicketNumberUpdate createAncillaryServiceETicketNumberUpdate() {
        return new AncillaryServiceETicketNumberUpdate();
    }

    /**
     * Create an instance of {@link com.sabre.services.res.or.v1_8.AncillaryServiceData.RuleSet }
     * 
     */
    public com.sabre.services.res.or.v1_8.AncillaryServiceData.RuleSet createAncillaryServiceDataRuleSet() {
        return new com.sabre.services.res.or.v1_8.AncillaryServiceData.RuleSet();
    }

    /**
     * Create an instance of {@link com.sabre.services.res.or.v1_8.AncillaryServiceData.OriginalPrice }
     * 
     */
    public com.sabre.services.res.or.v1_8.AncillaryServiceData.OriginalPrice createAncillaryServiceDataOriginalPrice() {
        return new com.sabre.services.res.or.v1_8.AncillaryServiceData.OriginalPrice();
    }

    /**
     * Create an instance of {@link AncillaryPrice }
     * 
     */
    public AncillaryPrice createAncillaryPrice() {
        return new AncillaryPrice();
    }

    /**
     * Create an instance of {@link com.sabre.services.res.or.v1_8.AncillaryServiceData.Taxes }
     * 
     */
    public com.sabre.services.res.or.v1_8.AncillaryServiceData.Taxes createAncillaryServiceDataTaxes() {
        return new com.sabre.services.res.or.v1_8.AncillaryServiceData.Taxes();
    }

    /**
     * Create an instance of {@link com.sabre.services.res.or.v1_8.AncillaryServiceData.TicketCouponNumberAssociation }
     * 
     */
    public com.sabre.services.res.or.v1_8.AncillaryServiceData.TicketCouponNumberAssociation createAncillaryServiceDataTicketCouponNumberAssociation() {
        return new com.sabre.services.res.or.v1_8.AncillaryServiceData.TicketCouponNumberAssociation();
    }

    /**
     * Create an instance of {@link com.sabre.services.res.or.v1_8.AncillaryServiceData.TotalTaxes }
     * 
     */
    public com.sabre.services.res.or.v1_8.AncillaryServiceData.TotalTaxes createAncillaryServiceDataTotalTaxes() {
        return new com.sabre.services.res.or.v1_8.AncillaryServiceData.TotalTaxes();
    }

    /**
     * Create an instance of {@link com.sabre.services.res.or.v1_8.AncillaryServiceData.BagWeight }
     * 
     */
    public com.sabre.services.res.or.v1_8.AncillaryServiceData.BagWeight createAncillaryServiceDataBagWeight() {
        return new com.sabre.services.res.or.v1_8.AncillaryServiceData.BagWeight();
    }

    /**
     * Create an instance of {@link AncillaryPriceQuoteData }
     * 
     */
    public AncillaryPriceQuoteData createAncillaryPriceQuoteData() {
        return new AncillaryPriceQuoteData();
    }

    /**
     * Create an instance of {@link SegmentOrTravelPortion }
     * 
     */
    public SegmentOrTravelPortion createSegmentOrTravelPortion() {
        return new SegmentOrTravelPortion();
    }

    /**
     * Create an instance of {@link com.sabre.services.res.or.v1_8.AncillaryServiceData.TravelPortions }
     * 
     */
    public com.sabre.services.res.or.v1_8.AncillaryServiceData.TravelPortions createAncillaryServiceDataTravelPortions() {
        return new com.sabre.services.res.or.v1_8.AncillaryServiceData.TravelPortions();
    }

    /**
     * Create an instance of {@link Check }
     * 
     */
    public Check createCheck() {
        return new Check();
    }

    /**
     * Create an instance of {@link com.sabre.services.res.or.v1_8.FormOfPayment }
     * 
     */
    public com.sabre.services.res.or.v1_8.FormOfPayment createFormOfPayment() {
        return new com.sabre.services.res.or.v1_8.FormOfPayment();
    }

    /**
     * Create an instance of {@link AdditionalContentType }
     * 
     */
    public AdditionalContentType createAdditionalContentType() {
        return new AdditionalContentType();
    }

    /**
     * Create an instance of {@link Cash }
     * 
     */
    public Cash createCash() {
        return new Cash();
    }

    /**
     * Create an instance of {@link InvoiceData }
     * 
     */
    public InvoiceData createInvoiceData() {
        return new InvoiceData();
    }

    /**
     * Create an instance of {@link ProductBaseType }
     * 
     */
    public ProductBaseType createProductBaseType() {
        return new ProductBaseType();
    }

    /**
     * Create an instance of {@link AncillaryProductObject }
     * 
     */
    public AncillaryProductObject createAncillaryProductObject() {
        return new AncillaryProductObject();
    }

    /**
     * Create an instance of {@link GroundTransportationType }
     * 
     */
    public GroundTransportationType createGroundTransportationType() {
        return new GroundTransportationType();
    }

    /**
     * Create an instance of {@link SocialMediaContactType }
     * 
     */
    public SocialMediaContactType createSocialMediaContactType() {
        return new SocialMediaContactType();
    }

    /**
     * Create an instance of {@link AgencyFeesType }
     * 
     */
    public AgencyFeesType createAgencyFeesType() {
        return new AgencyFeesType();
    }

    /**
     * Create an instance of {@link AccountingField }
     * 
     */
    public AccountingField createAccountingField() {
        return new AccountingField();
    }

    /**
     * Create an instance of {@link LangDetailsType }
     * 
     */
    public LangDetailsType createLangDetailsType() {
        return new LangDetailsType();
    }

    /**
     * Create an instance of {@link Invoice }
     * 
     */
    public Invoice createInvoice() {
        return new Invoice();
    }

    /**
     * Create an instance of {@link GTR }
     * 
     */
    public GTR createGTR() {
        return new GTR();
    }

    /**
     * Create an instance of {@link RateQualifierType }
     * 
     */
    public RateQualifierType createRateQualifierType() {
        return new RateQualifierType();
    }

    /**
     * Create an instance of {@link ServiceType }
     * 
     */
    public ServiceType createServiceType() {
        return new ServiceType();
    }

    /**
     * Create an instance of {@link Docket }
     * 
     */
    public Docket createDocket() {
        return new Docket();
    }

    /**
     * Create an instance of {@link TTYRecordLocatorType }
     * 
     */
    public TTYRecordLocatorType createTTYRecordLocatorType() {
        return new TTYRecordLocatorType();
    }

    /**
     * Create an instance of {@link ProductType }
     * 
     */
    public ProductType createProductType() {
        return new ProductType();
    }

    /**
     * Create an instance of {@link OpenReservationElementType }
     * 
     */
    public OpenReservationElementType createOpenReservationElementType() {
        return new OpenReservationElementType();
    }

    /**
     * Create an instance of {@link ApoXmlData }
     * 
     */
    public ApoXmlData createApoXmlData() {
        return new ApoXmlData();
    }

    /**
     * Create an instance of {@link LangMatchDataType }
     * 
     */
    public LangMatchDataType createLangMatchDataType() {
        return new LangMatchDataType();
    }

    /**
     * Create an instance of {@link OACType }
     * 
     */
    public OACType createOACType() {
        return new OACType();
    }

    /**
     * Create an instance of {@link PaymentCardApproval }
     * 
     */
    public PaymentCardApproval createPaymentCardApproval() {
        return new PaymentCardApproval();
    }

    /**
     * Create an instance of {@link DetailType }
     * 
     */
    public DetailType createDetailType() {
        return new DetailType();
    }

    /**
     * Create an instance of {@link OpenReservationElementsType }
     * 
     */
    public OpenReservationElementsType createOpenReservationElementsType() {
        return new OpenReservationElementsType();
    }

    /**
     * Create an instance of {@link MultilineTextType }
     * 
     */
    public MultilineTextType createMultilineTextType() {
        return new MultilineTextType();
    }

    /**
     * Create an instance of {@link ProductNameType }
     * 
     */
    public ProductNameType createProductNameType() {
        return new ProductNameType();
    }

    /**
     * Create an instance of {@link BookingChannelType }
     * 
     */
    public BookingChannelType createBookingChannelType() {
        return new BookingChannelType();
    }

    /**
     * Create an instance of {@link PersonNameType }
     * 
     */
    public PersonNameType createPersonNameType() {
        return new PersonNameType();
    }

    /**
     * Create an instance of {@link MinMaxType }
     * 
     */
    public MinMaxType createMinMaxType() {
        return new MinMaxType();
    }

    /**
     * Create an instance of {@link TrainDetailsType }
     * 
     */
    public TrainDetailsType createTrainDetailsType() {
        return new TrainDetailsType();
    }

    /**
     * Create an instance of {@link GGR }
     * 
     */
    public GGR createGGR() {
        return new GGR();
    }

    /**
     * Create an instance of {@link CalculationType }
     * 
     */
    public CalculationType createCalculationType() {
        return new CalculationType();
    }

    /**
     * Create an instance of {@link ChargeType }
     * 
     */
    public ChargeType createChargeType() {
        return new ChargeType();
    }

    /**
     * Create an instance of {@link SGR }
     * 
     */
    public SGR createSGR() {
        return new SGR();
    }

    /**
     * Create an instance of {@link SourceType }
     * 
     */
    public SourceType createSourceType() {
        return new SourceType();
    }

    /**
     * Create an instance of {@link EquipmentType }
     * 
     */
    public EquipmentType createEquipmentType() {
        return new EquipmentType();
    }

    /**
     * Create an instance of {@link CodeDescriptionType }
     * 
     */
    public CodeDescriptionType createCodeDescriptionType() {
        return new CodeDescriptionType();
    }

    /**
     * Create an instance of {@link CO2ValueType }
     * 
     */
    public CO2ValueType createCO2ValueType() {
        return new CO2ValueType();
    }

    /**
     * Create an instance of {@link ChargesType }
     * 
     */
    public ChargesType createChargesType() {
        return new ChargesType();
    }

    /**
     * Create an instance of {@link ExternalReservationType }
     * 
     */
    public ExternalReservationType createExternalReservationType() {
        return new ExternalReservationType();
    }

    /**
     * Create an instance of {@link SpecialInputType }
     * 
     */
    public SpecialInputType createSpecialInputType() {
        return new SpecialInputType();
    }

    /**
     * Create an instance of {@link RequestorIDType }
     * 
     */
    public RequestorIDType createRequestorIDType() {
        return new RequestorIDType();
    }

    /**
     * Create an instance of {@link FGR }
     * 
     */
    public FGR createFGR() {
        return new FGR();
    }

    /**
     * Create an instance of {@link PayLaterPlan }
     * 
     */
    public PayLaterPlan createPayLaterPlan() {
        return new PayLaterPlan();
    }

    /**
     * Create an instance of {@link PhoneType }
     * 
     */
    public PhoneType createPhoneType() {
        return new PhoneType();
    }

    /**
     * Create an instance of {@link MoneyType }
     * 
     */
    public MoneyType createMoneyType() {
        return new MoneyType();
    }

    /**
     * Create an instance of {@link SegmentAssociationTag }
     * 
     */
    public SegmentAssociationTag createSegmentAssociationTag() {
        return new SegmentAssociationTag();
    }

    /**
     * Create an instance of {@link FareComponent }
     * 
     */
    public FareComponent createFareComponent() {
        return new FareComponent();
    }

    /**
     * Create an instance of {@link OtherFOP }
     * 
     */
    public OtherFOP createOtherFOP() {
        return new OtherFOP();
    }

    /**
     * Create an instance of {@link PriceType }
     * 
     */
    public PriceType createPriceType() {
        return new PriceType();
    }

    /**
     * Create an instance of {@link HotelUniqueIDType }
     * 
     */
    public HotelUniqueIDType createHotelUniqueIDType() {
        return new HotelUniqueIDType();
    }

    /**
     * Create an instance of {@link FeeType }
     * 
     */
    public FeeType createFeeType() {
        return new FeeType();
    }

    /**
     * Create an instance of {@link AirportInfoType }
     * 
     */
    public AirportInfoType createAirportInfoType() {
        return new AirportInfoType();
    }

    /**
     * Create an instance of {@link LangDataType }
     * 
     */
    public LangDataType createLangDataType() {
        return new LangDataType();
    }

    /**
     * Create an instance of {@link LoyaltyType }
     * 
     */
    public LoyaltyType createLoyaltyType() {
        return new LoyaltyType();
    }

    /**
     * Create an instance of {@link ServiceLevelType }
     * 
     */
    public ServiceLevelType createServiceLevelType() {
        return new ServiceLevelType();
    }

    /**
     * Create an instance of {@link CompanyNameType }
     * 
     */
    public CompanyNameType createCompanyNameType() {
        return new CompanyNameType();
    }

    /**
     * Create an instance of {@link NameAssociationTag }
     * 
     */
    public NameAssociationTag createNameAssociationTag() {
        return new NameAssociationTag();
    }

    /**
     * Create an instance of {@link RestrictionsType }
     * 
     */
    public RestrictionsType createRestrictionsType() {
        return new RestrictionsType();
    }

    /**
     * Create an instance of {@link RailwayStationInfoType }
     * 
     */
    public RailwayStationInfoType createRailwayStationInfoType() {
        return new RailwayStationInfoType();
    }

    /**
     * Create an instance of {@link AncillaryTax }
     * 
     */
    public AncillaryTax createAncillaryTax() {
        return new AncillaryTax();
    }

    /**
     * Create an instance of {@link GR }
     * 
     */
    public GR createGR() {
        return new GR();
    }

    /**
     * Create an instance of {@link ExternalSystemReferenceType }
     * 
     */
    public ExternalSystemReferenceType createExternalSystemReferenceType() {
        return new ExternalSystemReferenceType();
    }

    /**
     * Create an instance of {@link ArrangerDetailsType }
     * 
     */
    public ArrangerDetailsType createArrangerDetailsType() {
        return new ArrangerDetailsType();
    }

    /**
     * Create an instance of {@link RequestorSourceType }
     * 
     */
    public RequestorSourceType createRequestorSourceType() {
        return new RequestorSourceType();
    }

    /**
     * Create an instance of {@link AirPosType }
     * 
     */
    public AirPosType createAirPosType() {
        return new AirPosType();
    }

    /**
     * Create an instance of {@link POSType }
     * 
     */
    public POSType createPOSType() {
        return new POSType();
    }

    /**
     * Create an instance of {@link AirlineType }
     * 
     */
    public AirlineType createAirlineType() {
        return new AirlineType();
    }

    /**
     * Create an instance of {@link LoyaltyProgramRedemption.TierLevel }
     * 
     */
    public LoyaltyProgramRedemption.TierLevel createLoyaltyProgramRedemptionTierLevel() {
        return new LoyaltyProgramRedemption.TierLevel();
    }

    /**
     * Create an instance of {@link AccountingTransaction.Payment }
     * 
     */
    public AccountingTransaction.Payment createAccountingTransactionPayment() {
        return new AccountingTransaction.Payment();
    }

    /**
     * Create an instance of {@link ServiceProviderType.Code }
     * 
     */
    public ServiceProviderType.Code createServiceProviderTypeCode() {
        return new ServiceProviderType.Code();
    }

    /**
     * Create an instance of {@link PassengerType.Type }
     * 
     */
    public PassengerType.Type createPassengerTypeType() {
        return new PassengerType.Type();
    }

    /**
     * Create an instance of {@link DocumentType.File }
     * 
     */
    public DocumentType.File createDocumentTypeFile() {
        return new DocumentType.File();
    }

    /**
     * Create an instance of {@link DocumentType.DocumentRules.Rule }
     * 
     */
    public DocumentType.DocumentRules.Rule createDocumentTypeDocumentRulesRule() {
        return new DocumentType.DocumentRules.Rule();
    }

    /**
     * Create an instance of {@link DocumentType.FormOfPayment.CreditCard.CardHolder }
     * 
     */
    public DocumentType.FormOfPayment.CreditCard.CardHolder createDocumentTypeFormOfPaymentCreditCardCardHolder() {
        return new DocumentType.FormOfPayment.CreditCard.CardHolder();
    }

    /**
     * Create an instance of {@link DocumentType.SegmentReferences.SegmentRef }
     * 
     */
    public DocumentType.SegmentReferences.SegmentRef createDocumentTypeSegmentReferencesSegmentRef() {
        return new DocumentType.SegmentReferences.SegmentRef();
    }

    /**
     * Create an instance of {@link DocumentType.PassengerReferences.PassengerRef }
     * 
     */
    public DocumentType.PassengerReferences.PassengerRef createDocumentTypePassengerReferencesPassengerRef() {
        return new DocumentType.PassengerReferences.PassengerRef();
    }

    /**
     * Create an instance of {@link VirtualCard.Transactions.Transaction }
     * 
     */
    public VirtualCard.Transactions.Transaction createVirtualCardTransactionsTransaction() {
        return new VirtualCard.Transactions.Transaction();
    }

    /**
     * Create an instance of {@link AccountingTransactionAmount.Points }
     * 
     */
    public AccountingTransactionAmount.Points createAccountingTransactionAmountPoints() {
        return new AccountingTransactionAmount.Points();
    }

    /**
     * Create an instance of {@link AccountingTransactionAmountWithRule.RuleSet }
     * 
     */
    public AccountingTransactionAmountWithRule.RuleSet createAccountingTransactionAmountWithRuleRuleSet() {
        return new AccountingTransactionAmountWithRule.RuleSet();
    }

    /**
     * Create an instance of {@link RailFareType.Fare.ClassOfService }
     * 
     */
    public RailFareType.Fare.ClassOfService createRailFareTypeFareClassOfService() {
        return new RailFareType.Fare.ClassOfService();
    }

    /**
     * Create an instance of {@link RailFareType.Fare.ClassCode }
     * 
     */
    public RailFareType.Fare.ClassCode createRailFareTypeFareClassCode() {
        return new RailFareType.Fare.ClassCode();
    }

    /**
     * Create an instance of {@link RailFareType.Fare.FareDescription.Detail }
     * 
     */
    public RailFareType.Fare.FareDescription.Detail createRailFareTypeFareFareDescriptionDetail() {
        return new RailFareType.Fare.FareDescription.Detail();
    }

    /**
     * Create an instance of {@link RailFareType.Fare.FareDescription.Condition }
     * 
     */
    public RailFareType.Fare.FareDescription.Condition createRailFareTypeFareFareDescriptionCondition() {
        return new RailFareType.Fare.FareDescription.Condition();
    }

    /**
     * Create an instance of {@link RailFareType.Fare.FareDescription.Fees }
     * 
     */
    public RailFareType.Fare.FareDescription.Fees createRailFareTypeFareFareDescriptionFees() {
        return new RailFareType.Fare.FareDescription.Fees();
    }

    /**
     * Create an instance of {@link RailFareType.Fare.SegmentReferences.SegmentRef }
     * 
     */
    public RailFareType.Fare.SegmentReferences.SegmentRef createRailFareTypeFareSegmentReferencesSegmentRef() {
        return new RailFareType.Fare.SegmentReferences.SegmentRef();
    }

    /**
     * Create an instance of {@link RailFareType.Fare.PassengerReferences.PassengerRef }
     * 
     */
    public RailFareType.Fare.PassengerReferences.PassengerRef createRailFareTypeFarePassengerReferencesPassengerRef() {
        return new RailFareType.Fare.PassengerReferences.PassengerRef();
    }

    /**
     * Create an instance of {@link AccountingTransactionItem.Tax }
     * 
     */
    public AccountingTransactionItem.Tax createAccountingTransactionItemTax() {
        return new AccountingTransactionItem.Tax();
    }

    /**
     * Create an instance of {@link AccountingTransactionItem.Promotion }
     * 
     */
    public AccountingTransactionItem.Promotion createAccountingTransactionItemPromotion() {
        return new AccountingTransactionItem.Promotion();
    }

    /**
     * Create an instance of {@link AirType.MarriageGrp }
     * 
     */
    public AirType.MarriageGrp createAirTypeMarriageGrp() {
        return new AirType.MarriageGrp();
    }

    /**
     * Create an instance of {@link AirType.AncillaryServices.AncillaryService }
     * 
     */
    public AirType.AncillaryServices.AncillaryService createAirTypeAncillaryServicesAncillaryService() {
        return new AirType.AncillaryServices.AncillaryService();
    }

    /**
     * Create an instance of {@link RailAccommodationType.Placement.Coach.PlacementNumber }
     * 
     */
    public RailAccommodationType.Placement.Coach.PlacementNumber createRailAccommodationTypePlacementCoachPlacementNumber() {
        return new RailAccommodationType.Placement.Coach.PlacementNumber();
    }

    /**
     * Create an instance of {@link AddressType.CountryCodes.Code }
     * 
     */
    public AddressType.CountryCodes.Code createAddressTypeCountryCodesCode() {
        return new AddressType.CountryCodes.Code();
    }

    /**
     * Create an instance of {@link AddressType.StateProvinceCodes.Code }
     * 
     */
    public AddressType.StateProvinceCodes.Code createAddressTypeStateProvinceCodesCode() {
        return new AddressType.StateProvinceCodes.Code();
    }

    /**
     * Create an instance of {@link AddressType.CityCodes.Code }
     * 
     */
    public AddressType.CityCodes.Code createAddressTypeCityCodesCode() {
        return new AddressType.CityCodes.Code();
    }

    /**
     * Create an instance of {@link ServiceRefType.Description }
     * 
     */
    public ServiceRefType.Description createServiceRefTypeDescription() {
        return new ServiceRefType.Description();
    }

    /**
     * Create an instance of {@link ServiceRefType.PassengerReferences.PassengerRef }
     * 
     */
    public ServiceRefType.PassengerReferences.PassengerRef createServiceRefTypePassengerReferencesPassengerRef() {
        return new ServiceRefType.PassengerReferences.PassengerRef();
    }

    /**
     * Create an instance of {@link HotelType.AdditionalInformation.ConfirmationNumber }
     * 
     */
    public HotelType.AdditionalInformation.ConfirmationNumber createHotelTypeAdditionalInformationConfirmationNumber() {
        return new HotelType.AdditionalInformation.ConfirmationNumber();
    }

    /**
     * Create an instance of {@link HotelType.AdditionalInformation.Address }
     * 
     */
    public HotelType.AdditionalInformation.Address createHotelTypeAdditionalInformationAddress() {
        return new HotelType.AdditionalInformation.Address();
    }

    /**
     * Create an instance of {@link HotelType.AdditionalInformation.ContactNumbers }
     * 
     */
    public HotelType.AdditionalInformation.ContactNumbers createHotelTypeAdditionalInformationContactNumbers() {
        return new HotelType.AdditionalInformation.ContactNumbers();
    }

    /**
     * Create an instance of {@link HotelType.AdditionalInformation.Commission }
     * 
     */
    public HotelType.AdditionalInformation.Commission createHotelTypeAdditionalInformationCommission() {
        return new HotelType.AdditionalInformation.Commission();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.SpecialPrefs }
     * 
     */
    public HotelType.Reservation.SpecialPrefs createHotelTypeReservationSpecialPrefs() {
        return new HotelType.Reservation.SpecialPrefs();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.RoomType }
     * 
     */
    public HotelType.Reservation.RoomType createHotelTypeReservationRoomType() {
        return new HotelType.Reservation.RoomType();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.RoomRates }
     * 
     */
    public HotelType.Reservation.RoomRates createHotelTypeReservationRoomRates() {
        return new HotelType.Reservation.RoomRates();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.RateAccessCodeBooked }
     * 
     */
    public HotelType.Reservation.RateAccessCodeBooked createHotelTypeReservationRateAccessCodeBooked() {
        return new HotelType.Reservation.RateAccessCodeBooked();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.GuestCounts }
     * 
     */
    public HotelType.Reservation.GuestCounts createHotelTypeReservationGuestCounts() {
        return new HotelType.Reservation.GuestCounts();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.Guarantee }
     * 
     */
    public HotelType.Reservation.Guarantee createHotelTypeReservationGuarantee() {
        return new HotelType.Reservation.Guarantee();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.HotelCode }
     * 
     */
    public HotelType.Reservation.HotelCode createHotelTypeReservationHotelCode() {
        return new HotelType.Reservation.HotelCode();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.HotelTotalPricing.ApproximateTotal }
     * 
     */
    public HotelType.Reservation.HotelTotalPricing.ApproximateTotal createHotelTypeReservationHotelTotalPricingApproximateTotal() {
        return new HotelType.Reservation.HotelTotalPricing.ApproximateTotal();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.HotelTotalPricing.RateChange }
     * 
     */
    public HotelType.Reservation.HotelTotalPricing.RateChange createHotelTypeReservationHotelTotalPricingRateChange() {
        return new HotelType.Reservation.HotelTotalPricing.RateChange();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.HotelTotalPricing.Disclaimer }
     * 
     */
    public HotelType.Reservation.HotelTotalPricing.Disclaimer createHotelTypeReservationHotelTotalPricingDisclaimer() {
        return new HotelType.Reservation.HotelTotalPricing.Disclaimer();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.HotelTotalPricing.HotelFees.HotelFee }
     * 
     */
    public HotelType.Reservation.HotelTotalPricing.HotelFees.HotelFee createHotelTypeReservationHotelTotalPricingHotelFeesHotelFee() {
        return new HotelType.Reservation.HotelTotalPricing.HotelFees.HotelFee();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.HotelTotalPricing.TotalSurcharge.Surcharge }
     * 
     */
    public HotelType.Reservation.HotelTotalPricing.TotalSurcharge.Surcharge createHotelTypeReservationHotelTotalPricingTotalSurchargeSurcharge() {
        return new HotelType.Reservation.HotelTotalPricing.TotalSurcharge.Surcharge();
    }

    /**
     * Create an instance of {@link HotelType.Reservation.HotelTotalPricing.TotalTax.Tax }
     * 
     */
    public HotelType.Reservation.HotelTotalPricing.TotalTax.Tax createHotelTypeReservationHotelTotalPricingTotalTaxTax() {
        return new HotelType.Reservation.HotelTotalPricing.TotalTax.Tax();
    }

    /**
     * Create an instance of {@link AccountingTransactionSource.SegmentData }
     * 
     */
    public AccountingTransactionSource.SegmentData createAccountingTransactionSourceSegmentData() {
        return new AccountingTransactionSource.SegmentData();
    }

    /**
     * Create an instance of {@link AccountingTransactionSource.AncillaryServiceData }
     * 
     */
    public AccountingTransactionSource.AncillaryServiceData createAccountingTransactionSourceAncillaryServiceData() {
        return new AccountingTransactionSource.AncillaryServiceData();
    }

    /**
     * Create an instance of {@link CompanyType.Code }
     * 
     */
    public CompanyType.Code createCompanyTypeCode() {
        return new CompanyType.Code();
    }

    /**
     * Create an instance of {@link LocationDetailsType.RailwayStationInfo.StationName }
     * 
     */
    public LocationDetailsType.RailwayStationInfo.StationName createLocationDetailsTypeRailwayStationInfoStationName() {
        return new LocationDetailsType.RailwayStationInfo.StationName();
    }

    /**
     * Create an instance of {@link LocationDetailsType.RailwayStationInfo.LocalStationName }
     * 
     */
    public LocationDetailsType.RailwayStationInfo.LocalStationName createLocationDetailsTypeRailwayStationInfoLocalStationName() {
        return new LocationDetailsType.RailwayStationInfo.LocalStationName();
    }

    /**
     * Create an instance of {@link LocationDetailsType.RailwayStationInfo.Amenities }
     * 
     */
    public LocationDetailsType.RailwayStationInfo.Amenities createLocationDetailsTypeRailwayStationInfoAmenities() {
        return new LocationDetailsType.RailwayStationInfo.Amenities();
    }

    /**
     * Create an instance of {@link LocationDetailsType.RailwayStationInfo.LocationCodes.Code }
     * 
     */
    public LocationDetailsType.RailwayStationInfo.LocationCodes.Code createLocationDetailsTypeRailwayStationInfoLocationCodesCode() {
        return new LocationDetailsType.RailwayStationInfo.LocationCodes.Code();
    }

    /**
     * Create an instance of {@link TourType.Passengers }
     * 
     */
    public TourType.Passengers createTourTypePassengers() {
        return new TourType.Passengers();
    }

    /**
     * Create an instance of {@link TourType.Services }
     * 
     */
    public TourType.Services createTourTypeServices() {
        return new TourType.Services();
    }

    /**
     * Create an instance of {@link TourType.Price.Breakdown.References.PassengerRef }
     * 
     */
    public TourType.Price.Breakdown.References.PassengerRef createTourTypePriceBreakdownReferencesPassengerRef() {
        return new TourType.Price.Breakdown.References.PassengerRef();
    }

    /**
     * Create an instance of {@link SupplementaryServiceType.Description }
     * 
     */
    public SupplementaryServiceType.Description createSupplementaryServiceTypeDescription() {
        return new SupplementaryServiceType.Description();
    }

    /**
     * Create an instance of {@link SupplementaryServiceType.PassengerReferences.PassengerRef }
     * 
     */
    public SupplementaryServiceType.PassengerReferences.PassengerRef createSupplementaryServiceTypePassengerReferencesPassengerRef() {
        return new SupplementaryServiceType.PassengerReferences.PassengerRef();
    }

    /**
     * Create an instance of {@link MapType.Entry }
     * 
     */
    public MapType.Entry createMapTypeEntry() {
        return new MapType.Entry();
    }

    /**
     * Create an instance of {@link HotelProductType.HotelPolicy.CheckInCheckOutPolicy }
     * 
     */
    public HotelProductType.HotelPolicy.CheckInCheckOutPolicy createHotelProductTypeHotelPolicyCheckInCheckOutPolicy() {
        return new HotelProductType.HotelPolicy.CheckInCheckOutPolicy();
    }

    /**
     * Create an instance of {@link HotelProductType.HotelPolicy.CancellationPolicyInfoList.CancelPolicyInfo }
     * 
     */
    public HotelProductType.HotelPolicy.CancellationPolicyInfoList.CancelPolicyInfo createHotelProductTypeHotelPolicyCancellationPolicyInfoListCancelPolicyInfo() {
        return new HotelProductType.HotelPolicy.CancellationPolicyInfoList.CancelPolicyInfo();
    }

    /**
     * Create an instance of {@link RailType.MarketingCarrier }
     * 
     */
    public RailType.MarketingCarrier createRailTypeMarketingCarrier() {
        return new RailType.MarketingCarrier();
    }

    /**
     * Create an instance of {@link RailType.Passengers }
     * 
     */
    public RailType.Passengers createRailTypePassengers() {
        return new RailType.Passengers();
    }

    /**
     * Create an instance of {@link RailType.SupplementaryServices }
     * 
     */
    public RailType.SupplementaryServices createRailTypeSupplementaryServices() {
        return new RailType.SupplementaryServices();
    }

    /**
     * Create an instance of {@link RailType.Documents }
     * 
     */
    public RailType.Documents createRailTypeDocuments() {
        return new RailType.Documents();
    }

    /**
     * Create an instance of {@link RailType.Details.Detail }
     * 
     */
    public RailType.Details.Detail createRailTypeDetailsDetail() {
        return new RailType.Details.Detail();
    }

    /**
     * Create an instance of {@link ProductDetailsType.TransactionInfo }
     * 
     */
    public ProductDetailsType.TransactionInfo createProductDetailsTypeTransactionInfo() {
        return new ProductDetailsType.TransactionInfo();
    }

    /**
     * Create an instance of {@link ProductDetailsType.BillingInfo }
     * 
     */
    public ProductDetailsType.BillingInfo createProductDetailsTypeBillingInfo() {
        return new ProductDetailsType.BillingInfo();
    }

    /**
     * Create an instance of {@link PaymentCard.CardNumber }
     * 
     */
    public PaymentCard.CardNumber createPaymentCardCardNumber() {
        return new PaymentCard.CardNumber();
    }

    /**
     * Create an instance of {@link PaymentCard.ApprovalList }
     * 
     */
    public PaymentCard.ApprovalList createPaymentCardApprovalList() {
        return new PaymentCard.ApprovalList();
    }

    /**
     * Create an instance of {@link com.sabre.services.res.or.v1_8.AncillaryServiceData.ProductTextDetails.ProductTextDetailsItem }
     * 
     */
    public com.sabre.services.res.or.v1_8.AncillaryServiceData.ProductTextDetails.ProductTextDetailsItem createAncillaryServiceDataProductTextDetailsProductTextDetailsItem() {
        return new com.sabre.services.res.or.v1_8.AncillaryServiceData.ProductTextDetails.ProductTextDetailsItem();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionalContentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "AdditionalContent")
    public JAXBElement<AdditionalContentType> createAdditionalContent(AdditionalContentType value) {
        return new JAXBElement<AdditionalContentType>(_AdditionalContent_QNAME, AdditionalContentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RailType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "RailProduct")
    public JAXBElement<RailType> createRailProduct(RailType value) {
        return new JAXBElement<RailType>(_RailProduct_QNAME, RailType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductBaseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "ProductBase")
    public JAXBElement<ProductBaseType> createProductBase(ProductBaseType value) {
        return new JAXBElement<ProductBaseType>(_ProductBase_QNAME, ProductBaseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PaymentCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "PaymentCard")
    public JAXBElement<PaymentCard> createPaymentCard(PaymentCard value) {
        return new JAXBElement<PaymentCard>(_PaymentCard_QNAME, PaymentCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Check }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "Check")
    public JAXBElement<Check> createCheck(Check value) {
        return new JAXBElement<Check>(_Check_QNAME, Check.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.sabre.services.res.or.v1_8.FormOfPayment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "FormOfPayment")
    public JAXBElement<com.sabre.services.res.or.v1_8.FormOfPayment> createFormOfPayment(com.sabre.services.res.or.v1_8.FormOfPayment value) {
        return new JAXBElement<com.sabre.services.res.or.v1_8.FormOfPayment>(_FormOfPayment_QNAME, com.sabre.services.res.or.v1_8.FormOfPayment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HotelProductType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "HotelProduct")
    public JAXBElement<HotelProductType> createHotelProduct(HotelProductType value) {
        return new JAXBElement<HotelProductType>(_HotelProduct_QNAME, HotelProductType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AncillaryProductObject }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "AncillaryProduct")
    public JAXBElement<AncillaryProductObject> createAncillaryProduct(AncillaryProductObject value) {
        return new JAXBElement<AncillaryProductObject>(_AncillaryProduct_QNAME, AncillaryProductObject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SocialMediaContactType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "SocialMediaContact")
    public JAXBElement<SocialMediaContactType> createSocialMediaContact(SocialMediaContactType value) {
        return new JAXBElement<SocialMediaContactType>(_SocialMediaContact_QNAME, SocialMediaContactType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountingField }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "AccountingField")
    public JAXBElement<AccountingField> createAccountingField(AccountingField value) {
        return new JAXBElement<AccountingField>(_AccountingField_QNAME, AccountingField.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LangDetailsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "LangDetails")
    public JAXBElement<LangDetailsType> createLangDetails(LangDetailsType value) {
        return new JAXBElement<LangDetailsType>(_LangDetails_QNAME, LangDetailsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MapType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "ParametersMap")
    public JAXBElement<MapType> createParametersMap(MapType value) {
        return new JAXBElement<MapType>(_ParametersMap_QNAME, MapType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cash }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "Cash")
    public JAXBElement<Cash> createCash(Cash value) {
        return new JAXBElement<Cash>(_Cash_QNAME, Cash.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProductDetailsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "ProductDetails")
    public JAXBElement<ProductDetailsType> createProductDetails(ProductDetailsType value) {
        return new JAXBElement<ProductDetailsType>(_ProductDetails_QNAME, ProductDetailsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvoiceData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "InvoiceData")
    public JAXBElement<InvoiceData> createInvoiceData(InvoiceData value) {
        return new JAXBElement<InvoiceData>(_InvoiceData_QNAME, InvoiceData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroundTransportationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "GroundTransportationProduct")
    public JAXBElement<GroundTransportationType> createGroundTransportationProduct(GroundTransportationType value) {
        return new JAXBElement<GroundTransportationType>(_GroundTransportationProduct_QNAME, GroundTransportationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgencyFeesType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.sabre.com/res/or/v1_8", name = "AgencyFees")
    public JAXBElement<AgencyFeesType> createAgencyFees(AgencyFeesType value) {
        return new JAXBElement<AgencyFeesType>(_AgencyFees_QNAME, AgencyFeesType.class, null, value);
    }

}
