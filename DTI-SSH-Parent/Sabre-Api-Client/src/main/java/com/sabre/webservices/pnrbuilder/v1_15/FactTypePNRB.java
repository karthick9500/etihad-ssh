
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FactType.PNRB.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FactType.PNRB">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="G"/>
 *     &lt;enumeration value="H"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FactType.PNRB")
@XmlEnum
public enum FactTypePNRB {

    G,
    H;

    public String value() {
        return name();
    }

    public static FactTypePNRB fromValue(String v) {
        return valueOf(v);
    }

}
