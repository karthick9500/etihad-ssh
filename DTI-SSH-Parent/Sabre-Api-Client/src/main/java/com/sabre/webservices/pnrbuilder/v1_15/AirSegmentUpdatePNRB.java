
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to book/delete air segment
 * 
 * <p>Java class for AirSegmentUpdate.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AirSegmentUpdate.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="AirSegment" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AirType"/>
 *       &lt;/choice>
 *       &lt;attribute name="id" use="required" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AirSegmentUpdate.PNRB", propOrder = {
    "airSegment"
})
public class AirSegmentUpdatePNRB {

    @XmlElement(name = "AirSegment")
    protected AirType airSegment;
    @XmlAttribute(name = "id", required = true)
    protected String id;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the airSegment property.
     * 
     * @return
     *     possible object is
     *     {@link AirType }
     *     
     */
    public AirType getAirSegment() {
        return airSegment;
    }

    /**
     * Sets the value of the airSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link AirType }
     *     
     */
    public void setAirSegment(AirType value) {
        this.airSegment = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
