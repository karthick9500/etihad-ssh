
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GMRUpdateType.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GMRUpdateType.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GMRRecordLocator" type="{http://webservices.sabre.com/pnrbuilder/v1_15}RecordLocatorType"/>
 *         &lt;element name="NbrOfSeats" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Numeric0to99"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GMRUpdateType.PNRB", propOrder = {
    "gmrRecordLocator",
    "nbrOfSeats"
})
public class GMRUpdateTypePNRB {

    @XmlElement(name = "GMRRecordLocator", required = true)
    protected String gmrRecordLocator;
    @XmlElement(name = "NbrOfSeats")
    protected int nbrOfSeats;

    /**
     * Gets the value of the gmrRecordLocator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGMRRecordLocator() {
        return gmrRecordLocator;
    }

    /**
     * Sets the value of the gmrRecordLocator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGMRRecordLocator(String value) {
        this.gmrRecordLocator = value;
    }

    /**
     * Gets the value of the nbrOfSeats property.
     * 
     */
    public int getNbrOfSeats() {
        return nbrOfSeats;
    }

    /**
     * Sets the value of the nbrOfSeats property.
     * 
     */
    public void setNbrOfSeats(int value) {
        this.nbrOfSeats = value;
    }

}
