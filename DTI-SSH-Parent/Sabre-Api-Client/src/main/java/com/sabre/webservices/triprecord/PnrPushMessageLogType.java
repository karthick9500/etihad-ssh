
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PnrPushMessageLogType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PnrPushMessageLogType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/triprecord}MessageLogType">
 *       &lt;sequence>
 *         &lt;element name="MessageDestination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MessageId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PnrPushMessageLogType", propOrder = {
    "messageDestination",
    "messageId"
})
@XmlSeeAlso({
    PnrPushMessageLogTypeV2 .class
})
public class PnrPushMessageLogType
    extends MessageLogType
{

    @XmlElement(name = "MessageDestination")
    protected String messageDestination;
    @XmlElement(name = "MessageId")
    protected String messageId;

    /**
     * Gets the value of the messageDestination property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageDestination() {
        return messageDestination;
    }

    /**
     * Sets the value of the messageDestination property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageDestination(String value) {
        this.messageDestination = value;
    }

    /**
     * Gets the value of the messageId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageId() {
        return messageId;
    }

    /**
     * Sets the value of the messageId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageId(String value) {
        this.messageId = value;
    }

}
