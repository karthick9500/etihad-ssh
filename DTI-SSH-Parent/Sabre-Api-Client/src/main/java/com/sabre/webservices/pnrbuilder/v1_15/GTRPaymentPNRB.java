
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GTRPayment.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GTRPayment.PNRB">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/pnrbuilder/v1_15}Payment.PNRB">
 *       &lt;sequence>
 *         &lt;element name="Number" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GTRPayment.PNRB", propOrder = {
    "number"
})
public class GTRPaymentPNRB
    extends PaymentPNRB
{

    @XmlElement(name = "Number")
    protected String number;

    /**
     * Gets the value of the number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the value of the number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumber(String value) {
        this.number = value;
    }

}
