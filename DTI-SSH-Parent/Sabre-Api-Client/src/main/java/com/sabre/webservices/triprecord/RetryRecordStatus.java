
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RetryRecordStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RetryRecordStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NEW"/>
 *     &lt;enumeration value="IN_PROGRESS"/>
 *     &lt;enumeration value="COMPLETED"/>
 *     &lt;enumeration value="FAILED"/>
 *     &lt;enumeration value="POST_PROCESSING_NEEDED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RetryRecordStatus")
@XmlEnum
public enum RetryRecordStatus {

    NEW,
    IN_PROGRESS,
    COMPLETED,
    FAILED,
    POST_PROCESSING_NEEDED;

    public String value() {
        return name();
    }

    public static RetryRecordStatus fromValue(String v) {
        return valueOf(v);
    }

}
