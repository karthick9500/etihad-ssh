
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RetryRecordRequestType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RetryRecordRequestType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SPANISH"/>
 *     &lt;enumeration value="CDM"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RetryRecordRequestType")
@XmlEnum
public enum RetryRecordRequestType {

    SPANISH,
    CDM;

    public String value() {
        return name();
    }

    public static RetryRecordRequestType fromValue(String v) {
        return valueOf(v);
    }

}
