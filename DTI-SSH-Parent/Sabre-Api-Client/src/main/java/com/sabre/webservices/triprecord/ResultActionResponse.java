
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResultActionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResultActionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice minOccurs="0">
 *         &lt;element name="QueuePlaceActionResponse" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Result" type="{http://webservices.sabre.com/triprecord}ActionResult" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultActionResponse", propOrder = {
    "queuePlaceActionResponse"
})
public class ResultActionResponse {

    @XmlElement(name = "QueuePlaceActionResponse")
    protected ResultActionResponse.QueuePlaceActionResponse queuePlaceActionResponse;

    /**
     * Gets the value of the queuePlaceActionResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ResultActionResponse.QueuePlaceActionResponse }
     *     
     */
    public ResultActionResponse.QueuePlaceActionResponse getQueuePlaceActionResponse() {
        return queuePlaceActionResponse;
    }

    /**
     * Sets the value of the queuePlaceActionResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultActionResponse.QueuePlaceActionResponse }
     *     
     */
    public void setQueuePlaceActionResponse(ResultActionResponse.QueuePlaceActionResponse value) {
        this.queuePlaceActionResponse = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Result" type="{http://webservices.sabre.com/triprecord}ActionResult" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "result"
    })
    public static class QueuePlaceActionResponse {

        @XmlElement(name = "Result")
        protected ActionResult result;

        /**
         * Gets the value of the result property.
         * 
         * @return
         *     possible object is
         *     {@link ActionResult }
         *     
         */
        public ActionResult getResult() {
            return result;
        }

        /**
         * Sets the value of the result property.
         * 
         * @param value
         *     allowed object is
         *     {@link ActionResult }
         *     
         */
        public void setResult(ActionResult value) {
            this.result = value;
        }

    }

}
