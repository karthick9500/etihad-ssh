
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AssociationParentReferenceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AssociationParentReferenceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Index"/>
 *     &lt;enumeration value="UkyId"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AssociationParentReferenceType")
@XmlEnum
public enum AssociationParentReferenceType {

    @XmlEnumValue("Index")
    INDEX("Index"),
    @XmlEnumValue("UkyId")
    UKY_ID("UkyId");
    private final String value;

    AssociationParentReferenceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AssociationParentReferenceType fromValue(String v) {
        for (AssociationParentReferenceType c: AssociationParentReferenceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
