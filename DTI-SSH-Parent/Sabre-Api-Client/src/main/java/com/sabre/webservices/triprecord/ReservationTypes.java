
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReservationTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReservationTypes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="SabrePNR"/>
 *     &lt;enumeration value="TripRecord"/>
 *     &lt;enumeration value="CDI"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReservationTypes")
@XmlEnum
public enum ReservationTypes {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("SabrePNR")
    SABRE_PNR("SabrePNR"),
    @XmlEnumValue("TripRecord")
    TRIP_RECORD("TripRecord"),
    CDI("CDI");
    private final String value;

    ReservationTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReservationTypes fromValue(String v) {
        for (ReservationTypes c: ReservationTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
