
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * formats:
 * 
 *                 * enter SSR special meal request (AFAX and
 *                 GFAX)
 * 
 *                 4SFML1-3.1
 * 
 *                 * enter SSR special meal for baby infant 4(special
 *                 meal
 *                 type)(segment number)-(name number of accompanying
 *                 adult)
 *                 4BBML1-3.1
 * 
 *                 * enter SSR speical meal request for all segments
 *                 4(special meal
 *                 type)A-(name number)
 *                 4VGMLA-1.0
 *             
 * 
 * <p>Java class for SpecialMealRequest.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpecialMealRequest.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MealType" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="FlightNumber" type="{http://webservices.sabre.com/pnrbuilder/v1_15}FlightNumber" minOccurs="0"/>
 *         &lt;element name="VendorCode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="FlightDate" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="BoardCity" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CityCode" minOccurs="0"/>
 *         &lt;element name="OffCity" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CityCode" minOccurs="0"/>
 *         &lt;element name="Comment" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="ActionCode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="NumberInParty" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Numeric0to99999" minOccurs="0"/>
 *         &lt;element name="ClassOfService" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="type" type="{http://webservices.sabre.com/pnrbuilder/v1_15}RequestType.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpecialMealRequest.PNRB", propOrder = {
    "mealType",
    "flightNumber",
    "vendorCode",
    "flightDate",
    "boardCity",
    "offCity",
    "comment",
    "actionCode",
    "numberInParty",
    "classOfService"
})
public class SpecialMealRequestPNRB {

    @XmlElement(name = "MealType")
    protected String mealType;
    @XmlElement(name = "FlightNumber")
    protected String flightNumber;
    @XmlElement(name = "VendorCode")
    protected String vendorCode;
    @XmlElement(name = "FlightDate")
    protected String flightDate;
    @XmlElement(name = "BoardCity")
    protected String boardCity;
    @XmlElement(name = "OffCity")
    protected String offCity;
    @XmlElement(name = "Comment")
    protected String comment;
    @XmlElement(name = "ActionCode")
    protected String actionCode;
    @XmlElement(name = "NumberInParty")
    protected Integer numberInParty;
    @XmlElement(name = "ClassOfService")
    protected String classOfService;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "type")
    protected RequestTypePNRB type;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the mealType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMealType() {
        return mealType;
    }

    /**
     * Sets the value of the mealType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMealType(String value) {
        this.mealType = value;
    }

    /**
     * Gets the value of the flightNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the value of the flightNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Gets the value of the vendorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorCode() {
        return vendorCode;
    }

    /**
     * Sets the value of the vendorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorCode(String value) {
        this.vendorCode = value;
    }

    /**
     * Gets the value of the flightDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightDate() {
        return flightDate;
    }

    /**
     * Sets the value of the flightDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightDate(String value) {
        this.flightDate = value;
    }

    /**
     * Gets the value of the boardCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoardCity() {
        return boardCity;
    }

    /**
     * Sets the value of the boardCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoardCity(String value) {
        this.boardCity = value;
    }

    /**
     * Gets the value of the offCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOffCity() {
        return offCity;
    }

    /**
     * Sets the value of the offCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOffCity(String value) {
        this.offCity = value;
    }

    /**
     * Gets the value of the comment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the value of the comment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComment(String value) {
        this.comment = value;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

    /**
     * Gets the value of the numberInParty property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberInParty() {
        return numberInParty;
    }

    /**
     * Sets the value of the numberInParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberInParty(Integer value) {
        this.numberInParty = value;
    }

    /**
     * Gets the value of the classOfService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassOfService() {
        return classOfService;
    }

    /**
     * Sets the value of the classOfService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassOfService(String value) {
        this.classOfService = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link RequestTypePNRB }
     *     
     */
    public RequestTypePNRB getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestTypePNRB }
     *     
     */
    public void setType(RequestTypePNRB value) {
        this.type = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
