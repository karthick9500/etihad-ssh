
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AsOrTnType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AsOrTnType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AS"/>
 *     &lt;enumeration value="TN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AsOrTnType")
@XmlEnum
public enum AsOrTnType {

    AS,
    TN;

    public String value() {
        return name();
    }

    public static AsOrTnType fromValue(String v) {
        return valueOf(v);
    }

}
