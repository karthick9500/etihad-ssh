
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Agency address restrictions:
 * 
 *                 * minimum 1 line, max
 *                 6 ones * each line has a max of 50
 *                 chars
 * 
 *                 Client address restrictions:
 * 
 *                 * minimum 1 line, max 6 lines * each line has a max of
 *                 50 chars
 * 
 *                 Delivery address restrictions
 * 
 *                 * minimum 1 line, max 6 lines * each
 *                 line has a max of
 *                 39 chars
 * 
 *                 Formats for addresses:
 * 
 *                 * add agency address
 * 
 *                 W-(agency name)‡(street address)‡(city/state or
 *                 country/zip or postal
 *                 code)
 *                 W-ABC TRAVEL‡123 MAIN ST‡DALLAS TX 75201
 * 
 *                 * add agency address
 *                 when more
 *                 than 50 chars
 * 
 *                 W-(agency name)‡(street address) W-(address
 *                 line # to
 *                 insert after)/(city/state or country/zip or postal code)
 *                 W-ABC
 *                 TRAVEL‡123 MAIN ST W-2/DALLAS TX 75201
 * 
 *                 * insert agency address
 *                 line
 *                 W-(address line # to insert
 *                 after)/(address)
 *                 W-3/LANGENBERHEIMERSTR.
 *                 21
 * 
 *                 * delete entire address field W-¤
 * 
 *                 * delete
 *                 specific address line
 * 
 *                 W-(line #)¤ W-1¤
 * 
 *                 * entry customer address into
 *                 remarks for printing
 *                 on
 *                 invoice/itinerary
 * 
 *                 5/(name)§5/(address)§5/(city),(state code or
 *                 country
 *                 code if not
 *                 US)(postal code)) 5/T KHOSROVI§5/123 MAIN
 *                 ST§5/DALLAS, TX 76101)
 * 
 *                 *
 *                 modify the client address within the remarks
 *                 field
 * 
 *                 5(line #)¤/(new
 *                 text) 53¤/65073 DIESBURG
 * 
 *                 * add delivery address
 *                 5DL-(address info)
 *                 5DL-DELIVER TO
 *                 KIRK HADEO 5DL-ESTERHAZY #106
 *                 5DL-KOENJI MINAMI 5-4-12
 *                 5DL-SUGINAMI-KU, TOKYO 166 5DL-JAPAN
 *             
 * 
 * <p>Java class for Address.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Address.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Name" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="NumberAndStreet" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="City" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="StateProvince" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="Country" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="PostCode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="AddressLines" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AddressLines.PNRB" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="type" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AddressType.ACD.PNRB" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Address.PNRB", propOrder = {
    "name",
    "numberAndStreet",
    "city",
    "stateProvince",
    "country",
    "postCode",
    "addressLines"
})
public class AddressPNRB {

    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "NumberAndStreet")
    protected String numberAndStreet;
    @XmlElement(name = "City")
    protected String city;
    @XmlElement(name = "StateProvince")
    protected String stateProvince;
    @XmlElement(name = "Country")
    protected String country;
    @XmlElement(name = "PostCode")
    protected String postCode;
    @XmlElement(name = "AddressLines")
    protected AddressLinesPNRB addressLines;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "type")
    protected AddressTypeACDPNRB type;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the numberAndStreet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberAndStreet() {
        return numberAndStreet;
    }

    /**
     * Sets the value of the numberAndStreet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberAndStreet(String value) {
        this.numberAndStreet = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the stateProvince property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateProvince() {
        return stateProvince;
    }

    /**
     * Sets the value of the stateProvince property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateProvince(String value) {
        this.stateProvince = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the postCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * Sets the value of the postCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostCode(String value) {
        this.postCode = value;
    }

    /**
     * Gets the value of the addressLines property.
     * 
     * @return
     *     possible object is
     *     {@link AddressLinesPNRB }
     *     
     */
    public AddressLinesPNRB getAddressLines() {
        return addressLines;
    }

    /**
     * Sets the value of the addressLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressLinesPNRB }
     *     
     */
    public void setAddressLines(AddressLinesPNRB value) {
        this.addressLines = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link AddressTypeACDPNRB }
     *     
     */
    public AddressTypeACDPNRB getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressTypeACDPNRB }
     *     
     */
    public void setType(AddressTypeACDPNRB value) {
        this.type = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

}
