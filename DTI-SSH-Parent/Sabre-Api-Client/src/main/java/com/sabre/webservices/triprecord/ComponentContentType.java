
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ComponentContentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComponentContentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="XmlBlob" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComponentContentType", propOrder = {
    "xmlBlob"
})
public class ComponentContentType {

    @XmlElement(name = "XmlBlob", required = true)
    protected String xmlBlob;

    /**
     * Gets the value of the xmlBlob property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXmlBlob() {
        return xmlBlob;
    }

    /**
     * Sets the value of the xmlBlob property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXmlBlob(String value) {
        this.xmlBlob = value;
    }

}
