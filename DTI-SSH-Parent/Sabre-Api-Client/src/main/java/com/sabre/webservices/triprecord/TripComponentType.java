
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TripComponentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TripComponentType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://webservices.sabre.com/triprecord}TripComponentUpdateType">
 *       &lt;sequence>
 *         &lt;element name="Content" type="{http://webservices.sabre.com/triprecord}ComponentContentType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TripComponentType", propOrder = {
    "content"
})
public class TripComponentType
    extends TripComponentUpdateType
{

    @XmlElement(name = "Content")
    protected ComponentContentType content;

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link ComponentContentType }
     *     
     */
    public ComponentContentType getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComponentContentType }
     *     
     */
    public void setContent(ComponentContentType value) {
        this.content = value;
    }

}
