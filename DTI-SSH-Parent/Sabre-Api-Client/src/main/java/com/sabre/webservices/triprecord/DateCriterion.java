
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DateCriterion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DateCriterion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="After" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="Before" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="Is" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *       &lt;attribute name="TimeBefore" type="{http://webservices.sabre.com/triprecord}timeType" />
 *       &lt;attribute name="TimeAfter" type="{http://webservices.sabre.com/triprecord}timeType" />
 *       &lt;attribute name="TimeIs" type="{http://webservices.sabre.com/triprecord}timeType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DateCriterion")
public class DateCriterion {

    @XmlAttribute(name = "After")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar after;
    @XmlAttribute(name = "Before")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar before;
    @XmlAttribute(name = "Is")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar is;
    @XmlAttribute(name = "TimeBefore")
    protected String timeBefore;
    @XmlAttribute(name = "TimeAfter")
    protected String timeAfter;
    @XmlAttribute(name = "TimeIs")
    protected String timeIs;

    /**
     * Gets the value of the after property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getAfter() {
        return after;
    }

    /**
     * Sets the value of the after property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setAfter(XMLGregorianCalendar value) {
        this.after = value;
    }

    /**
     * Gets the value of the before property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBefore() {
        return before;
    }

    /**
     * Sets the value of the before property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBefore(XMLGregorianCalendar value) {
        this.before = value;
    }

    /**
     * Gets the value of the is property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getIs() {
        return is;
    }

    /**
     * Sets the value of the is property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setIs(XMLGregorianCalendar value) {
        this.is = value;
    }

    /**
     * Gets the value of the timeBefore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeBefore() {
        return timeBefore;
    }

    /**
     * Sets the value of the timeBefore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeBefore(String value) {
        this.timeBefore = value;
    }

    /**
     * Gets the value of the timeAfter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeAfter() {
        return timeAfter;
    }

    /**
     * Sets the value of the timeAfter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeAfter(String value) {
        this.timeAfter = value;
    }

    /**
     * Gets the value of the timeIs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimeIs() {
        return timeIs;
    }

    /**
     * Sets the value of the timeIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimeIs(String value) {
        this.timeIs = value;
    }

}
