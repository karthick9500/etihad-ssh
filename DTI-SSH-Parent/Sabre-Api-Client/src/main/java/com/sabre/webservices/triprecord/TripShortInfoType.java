
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TripShortInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TripShortInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TripID" type="{http://webservices.sabre.com/triprecord}TripIDType"/>
 *         &lt;element name="TripName" type="{http://webservices.sabre.com/triprecord}string1to255"/>
 *         &lt;element name="BookingSource" type="{http://webservices.sabre.com/triprecord}BookingSourceType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TripShortInfoType", propOrder = {
    "tripID",
    "tripName",
    "bookingSource"
})
public class TripShortInfoType {

    @XmlElement(name = "TripID", required = true)
    protected TripIDType tripID;
    @XmlElement(name = "TripName", required = true)
    protected String tripName;
    @XmlElement(name = "BookingSource", required = true)
    protected BookingSourceType bookingSource;

    /**
     * Gets the value of the tripID property.
     * 
     * @return
     *     possible object is
     *     {@link TripIDType }
     *     
     */
    public TripIDType getTripID() {
        return tripID;
    }

    /**
     * Sets the value of the tripID property.
     * 
     * @param value
     *     allowed object is
     *     {@link TripIDType }
     *     
     */
    public void setTripID(TripIDType value) {
        this.tripID = value;
    }

    /**
     * Gets the value of the tripName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTripName() {
        return tripName;
    }

    /**
     * Sets the value of the tripName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTripName(String value) {
        this.tripName = value;
    }

    /**
     * Gets the value of the bookingSource property.
     * 
     * @return
     *     possible object is
     *     {@link BookingSourceType }
     *     
     */
    public BookingSourceType getBookingSource() {
        return bookingSource;
    }

    /**
     * Sets the value of the bookingSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingSourceType }
     *     
     */
    public void setBookingSource(BookingSourceType value) {
        this.bookingSource = value;
    }

}
