
package com.sabre.webservices.triprecord;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RetryRecordServiceLocation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RetryRecordServiceLocation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Trip"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RetryRecordServiceLocation")
@XmlEnum
public enum RetryRecordServiceLocation {

    @XmlEnumValue("Trip")
    TRIP("Trip");
    private final String value;

    RetryRecordServiceLocation(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RetryRecordServiceLocation fromValue(String v) {
        for (RetryRecordServiceLocation c: RetryRecordServiceLocation.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
