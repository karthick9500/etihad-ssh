
package com.sabre.webservices.pnrbuilder.v1_15;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * * add ticketing time limit for future date and specify a
 *                 downline city 8(downline city)-(carrier
 *                 code)(time)(future date)
 * 
 *                 8SEA-DL6P/17MAY
 * 
 *                 * add ticketing time limit for today's date 8(downline
 *                 city)-(carrier code)(time)
 * 
 *                 8LON-BA6P
 * 
 *                 * add ticketing time limit for American Airlines, assume
 *                 today's date 8(time)
 * 
 *                 84P
 * 
 *                 * add ticketing time limit for American Airlines,
 *                 specify date 8(time)(date)
 * 
 *                 87P/09NOV
 * 
 *                 * add ticketing time limit for American Airlines,
 *                 indicate 30 minute time limit 830
 *             
 * 
 * <p>Java class for TicketingTimeLimit.PNRB complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TicketingTimeLimit.PNRB">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DownlineCity" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="CarrierCode" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AirlineCode" minOccurs="0"/>
 *         &lt;element name="Time" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="FutureDate" type="{http://webservices.sabre.com/pnrbuilder/v1_15}CommonString" minOccurs="0"/>
 *         &lt;element name="ThirtyMinuteInterval" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://webservices.sabre.com/pnrbuilder/v1_15}UniqueIdentifier.PNRB" />
 *       &lt;attribute name="index" type="{http://webservices.sabre.com/pnrbuilder/v1_15}Numeric0to99999" />
 *       &lt;attribute name="op" type="{http://webservices.sabre.com/pnrbuilder/v1_15}OperationType.PNRB" />
 *       &lt;attribute name="elementId" type="{http://webservices.sabre.com/pnrbuilder/v1_15}AssociationMatrixID.PNRB" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TicketingTimeLimit.PNRB", propOrder = {
    "downlineCity",
    "carrierCode",
    "time",
    "futureDate",
    "thirtyMinuteInterval"
})
public class TicketingTimeLimitPNRB {

    @XmlElement(name = "DownlineCity")
    protected String downlineCity;
    @XmlElement(name = "CarrierCode")
    protected String carrierCode;
    @XmlElement(name = "Time")
    protected String time;
    @XmlElement(name = "FutureDate")
    protected String futureDate;
    @XmlElement(name = "ThirtyMinuteInterval")
    protected String thirtyMinuteInterval;
    @XmlAttribute(name = "id")
    protected String id;
    @XmlAttribute(name = "index")
    protected Integer index;
    @XmlAttribute(name = "op")
    protected OperationTypePNRB op;
    @XmlAttribute(name = "elementId")
    protected String elementId;

    /**
     * Gets the value of the downlineCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDownlineCity() {
        return downlineCity;
    }

    /**
     * Sets the value of the downlineCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDownlineCity(String value) {
        this.downlineCity = value;
    }

    /**
     * Gets the value of the carrierCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrierCode() {
        return carrierCode;
    }

    /**
     * Sets the value of the carrierCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrierCode(String value) {
        this.carrierCode = value;
    }

    /**
     * Gets the value of the time property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTime() {
        return time;
    }

    /**
     * Sets the value of the time property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTime(String value) {
        this.time = value;
    }

    /**
     * Gets the value of the futureDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFutureDate() {
        return futureDate;
    }

    /**
     * Sets the value of the futureDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFutureDate(String value) {
        this.futureDate = value;
    }

    /**
     * Gets the value of the thirtyMinuteInterval property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThirtyMinuteInterval() {
        return thirtyMinuteInterval;
    }

    /**
     * Sets the value of the thirtyMinuteInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThirtyMinuteInterval(String value) {
        this.thirtyMinuteInterval = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the index property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIndex() {
        return index;
    }

    /**
     * Sets the value of the index property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIndex(Integer value) {
        this.index = value;
    }

    /**
     * Gets the value of the op property.
     * 
     * @return
     *     possible object is
     *     {@link OperationTypePNRB }
     *     
     */
    public OperationTypePNRB getOp() {
        return op;
    }

    /**
     * Sets the value of the op property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationTypePNRB }
     *     
     */
    public void setOp(OperationTypePNRB value) {
        this.op = value;
    }

    /**
     * Gets the value of the elementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementId() {
        return elementId;
    }

    /**
     * Sets the value of the elementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementId(String value) {
        this.elementId = value;
    }

}
