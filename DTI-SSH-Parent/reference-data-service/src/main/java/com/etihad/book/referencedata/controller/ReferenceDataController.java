/**
 * 
 */
package com.etihad.book.referencedata.controller;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.referencedata.service.ReferenceDataServiceImpl;



/**
 * @author CTS
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/service/v1")
public class ReferenceDataController {
	

	@Autowired
	private ReferenceDataServiceImpl referenceDataService;
	
	/**
	 * This method will be used to map the request to a specific URI and return the response 
	 * @return
	 * @throws ApplicationException 
	 * @throws JSONException 
	 */
	@CrossOrigin
	@RequestMapping(path="/referenceData", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
	public String getReferenceData() throws ApplicationException  {
		JSONObject jsonObj = referenceDataService.getReferenceData();
		return jsonObj.toString().replace("\ufeff", "");
		
		//return referenceDataService.getReferenceData();
	}

}
