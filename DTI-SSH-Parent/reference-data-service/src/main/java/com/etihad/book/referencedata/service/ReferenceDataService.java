/**
 * 
 */
package com.etihad.book.referencedata.service;

import org.json.JSONObject;

import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.referencedata.schema.ReferenceDataResponse;

/**
 * @author CTS
 *
 */
public interface ReferenceDataService 
{
	public JSONObject getReferenceData()  throws ApplicationException ;
	//public ReferenceDataResponse getReferenceData()  throws ApplicationException ;
}