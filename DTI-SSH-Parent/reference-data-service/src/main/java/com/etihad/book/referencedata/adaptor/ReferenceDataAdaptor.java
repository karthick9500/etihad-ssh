/**
 * 
 */
package com.etihad.book.referencedata.adaptor;


import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.util.Utils;
import com.etihad.book.referencedata.schema.ReferenceDataResponse;

/**
 * @author CTS
 *
 */
@Component
public class ReferenceDataAdaptor {
	
	@Autowired
	private Config config;
	
	@Autowired
	private Utils utils;
	
	
	
	public ReferenceDataResponse createJSONResponseForRefData(JSONObject jsonObject, List errorCodes) {
		
		ReferenceDataResponse response = new ReferenceDataResponse();
		String jsonObjStr = jsonObject.toString().replace("\ufeff", "");
		response.setJsonObjStr(jsonObjStr);
		
		return response;
		
	}


		

}
