package com.etihad.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author CTS
 *
 */
@SpringBootApplication
public class ReferenceDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReferenceDataApplication.class, args);
	}
}
