/**
 * 
 */
package com.etihad.book.referencedata.schema;

import com.etihad.book.schema.common.GenericResponse;

/**
 * @author CTS
 *
 */
public class ReferenceDataResponse  extends GenericResponse{
	
	
	private String jsonObjStr;

	/**
	 * @return the jsonObjStr
	 */
	public String getJsonObjStr() {
		return jsonObjStr;
	}

	/**
	 * @param jsonObjStr the jsonObjStr to set
	 */
	public void setJsonObjStr(String jsonObjStr) {
		this.jsonObjStr = jsonObjStr;
	}

	
}
