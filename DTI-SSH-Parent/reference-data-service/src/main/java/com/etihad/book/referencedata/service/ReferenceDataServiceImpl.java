/**
 * 
 */
package com.etihad.book.referencedata.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.common.exception.ApplicationException;
import com.etihad.book.common.util.Utils;
import com.etihad.book.referencedata.adaptor.ReferenceDataAdaptor;
import com.etihad.book.referencedata.schema.BaggageTierObj;
import com.etihad.book.referencedata.schema.CabinBaggageObj;
import com.etihad.book.referencedata.schema.ONDDataObj;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author CTS
 *
 */
@Service
public class ReferenceDataServiceImpl  implements ReferenceDataService{ 
	
	private static final Map<String,Object> referenceDataMap = new HashMap<>();
	private static final String ENG_OND_CODE = "en-ae";
	private static final String ARA_OND_CODE = "ar-ae";

	
	@Autowired
	private ReferenceDataAdaptor referenceDataAdaptor;

	@Autowired
	private Config config;

	@Autowired
	private Utils utils;
	
	/**
	 * This method is used to process reference Data
	 * @return
	 * @throws ApplicationException 
	 */
	@PostConstruct
	public void loadStaticData()
	{
		String refFilePath = config.getProperty("refDatafilePath");
		String ssrfilePath = config.getProperty("ssrFilePath");
		String mealsFilePath = config.getProperty("mealsFilePath");
		String rbdClassFilePath = config.getProperty("rbdClassFilePath");
		String baggageFilePath = config.getProperty("baggageFilePath");
	    try 
	    {
	    	loadRefData(refFilePath);
	    	Map<String,ONDDataObj> engDataMap = getONDData(config.getProperty("refData.ond.url"));
        	Map<String,ONDDataObj> araDataMap = getONDData(config.getProperty("refData.ond.arabic.url"));
        	setAirportTimeZoneData(config.getProperty("airportZoneData.url"));
        	loadRefData(ssrfilePath);
        	loadRefData(mealsFilePath);
        	loadRefData(rbdClassFilePath);
        	loadRefData(baggageFilePath);
        	referenceDataMap.put(ENG_OND_CODE, engDataMap);
        	referenceDataMap.put(ARA_OND_CODE, araDataMap);
		} 
	    catch (Exception e) 
	    {
	    }
	}  

	private void loadRefData(String filePath)
	{
		String line = null;
		InputStream in = null;
	    OutputStream out = null;
	    BufferedReader br = null;
	    try {
	            in = getClass().getResourceAsStream(filePath); 
	            File file = File.createTempFile("tempfile", ".tmp");
	            out = new FileOutputStream(file);
	            int read;
	            byte[] bytes = new byte[1024];
	
	            while ((read = in.read(bytes)) != -1) {
	                out.write(bytes, 0, read);
	            }
	    	    br = new BufferedReader(new FileReader(file));
	    	    int length = 0;
	    	    CabinBaggageObj cabinObj = null;
	    	    BaggageTierObj baggageObj = null;
		    	while ((line = br.readLine()) != null) {
		    		line = line.replaceAll("\"", "");
		            String[] data = line.split(",");
		            length = data.length;
		            if(length ==5)
		            {
		            	cabinObj = new CabinBaggageObj();
		            	cabinObj.setCabinClass(data[1]);
		            	cabinObj.setBagNum(data[2]);
		            	cabinObj.setBagWeight(data[3]);
		            	cabinObj.setFoldableBag(data[4]);
		            	referenceDataMap.put(data[0],cabinObj);
		            }
		            else if(length ==4)
		            {
		            	baggageObj = new BaggageTierObj(); 
		            	baggageObj.setPieces(data[2]);
		            	baggageObj.setWeight(data[3]);
		            	referenceDataMap.put(data[0]+"-"+data[1],baggageObj);
		            }
		            else if(length ==3)
		            {
		            	referenceDataMap.put((data[2]+data[0]),data[1]);
		            }
		            else
		            {
		            	referenceDataMap.put(data[0], data[1]);
		            }
	    	    }
		    	file.deleteOnExit();
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    finally 
	    {
			if (in != null)
			{
				try 
				{
					in.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
			if (out != null) 
			{
				try 
				{
					out.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
			if (br != null) 
			{
				try 
				{
					br.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
	    }
	}
	
	private void setAirportTimeZoneData(String airportZoneDataUrl)
	{
		BufferedReader br = null;
		try {
			URL urlCSV = new URL(airportZoneDataUrl);
			URLConnection urlConn = urlCSV.openConnection();
			InputStreamReader inputCSV = new InputStreamReader(urlConn.getInputStream());
			br = new BufferedReader(inputCSV);
			String line;
			String[] values = null;
			while ((line = br.readLine()) != null) {
				line = line.replaceAll("\"", "");
				values = line.split(","); 
				referenceDataMap.put(values[4], values[11]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != br)
				{
					br.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
    }
	
	private static Map<String,ONDDataObj> getONDData(String ondUrl) throws IOException {
	    	
    	Map<String,ONDDataObj> dataMap = new HashMap<>();
    	URL url;
    	HttpURLConnection request = null;
		try {
			url = new URL(ondUrl);
			request = (HttpURLConnection) url.openConnection();
			request.connect();
		} catch (Exception e) {
		}

        JsonParser jp = new JsonParser(); 
        if( null != request)
        {
	        JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); 
	        JsonObject rootobj = root.getAsJsonObject();
	        JsonArray dataJ =	rootobj.getAsJsonArray("Data");
	        for (JsonElement jsonElement : dataJ) {
	        	JsonObject objectData = jsonElement.getAsJsonObject();
	        	ONDDataObj dataObject = new ONDDataObj();
	        	dataObject.setCode(objectData.get("Code").getAsString());
	        	dataObject.setName(objectData.get("Name").getAsString());
	        	dataObject.setCityName(objectData.get("CityName").getAsString());
	        	dataObject.setCityCode(objectData.get("CityCode").getAsString());
	        	dataObject.setCountryCode(objectData.get("CountryCode").getAsString());
	        	dataObject.setCountryName(objectData.get("CountryName").getAsString());
	        	dataObject.setAirportName(objectData.get("AirportName").getAsString());
				dataMap.put(dataObject.getCode(), dataObject);
	        }
        }
		return dataMap;
    }
	
	@Override
	public JSONObject getReferenceData() throws ApplicationException 
	{
		/*ReferenceDataResponse response = null;
		List errorCodes = new ArrayList(5);*/
		JSONObject jsonObject = new JSONObject();
        for (String key : referenceDataMap.keySet()) 
        {
            try 
            {
                Object obj = referenceDataMap.get(key);
                if (obj instanceof Map) {
                    jsonObject.put(key, (Map) obj );
                }
                else 
                {
                    jsonObject.put(key, obj);
                }
            }
            catch (JSONException jsone) 
            {
            	jsone.printStackTrace();
            }
        }
        /*response = referenceDataAdaptor.createJSONResponseForRefData(jsonObject,errorCodes );
		response.setMessages(utils.populateResponseHeader(errorCodes));
		return response;*/
        return jsonObject;
	  }

}