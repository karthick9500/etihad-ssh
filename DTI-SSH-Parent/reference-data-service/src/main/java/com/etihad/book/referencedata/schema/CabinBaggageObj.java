package com.etihad.book.referencedata.schema;

/**
 * This class is used to map Cabin baggage reference objects
 * 
 * @author CTS
 *
 */
public class CabinBaggageObj { 

	private String cabinClass;
	private String bagNum;
	private String bagWeight;
	private String foldableBag;

	
	/**
	 * @return the bagNum
	 */
	public String getBagNum() {
		return bagNum;
	}


	/**
	 * @param bagNum the bagNum to set
	 */
	public void setBagNum(String bagNum) {
		this.bagNum = bagNum;
	}


	/**
	 * @return the bagWeight
	 */
	public String getBagWeight() {
		return bagWeight;
	}


	/**
	 * @param bagWeight the bagWeight to set
	 */
	public void setBagWeight(String bagWeight) {
		this.bagWeight = bagWeight;
	}


	/**
	 * @return the foldableBag
	 */
	public String getFoldableBag() {
		return foldableBag;
	}


	/**
	 * @param foldableBag the foldableBag to set
	 */
	public void setFoldableBag(String foldableBag) {
		this.foldableBag = foldableBag;
	}


	/**
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}


	/**
	 * @param cabinClass the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}


	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CabinBaggageObj [cabinClass=" + cabinClass + ", bagNum=" + bagNum + ", bagWeight=" + bagWeight + ", foldableBag=" + foldableBag + "]";
	}
	
		
}
