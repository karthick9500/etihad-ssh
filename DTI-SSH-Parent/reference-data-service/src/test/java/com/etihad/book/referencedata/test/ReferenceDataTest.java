/**
 * 
 */
package com.etihad.book.referencedata.test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.etihad.book.common.configuration.Config;
import com.etihad.book.referencedata.controller.ReferenceDataController;
import com.etihad.book.referencedata.schema.ReferenceDataRequest;
import com.etihad.book.referencedata.schema.ReferenceDataResponse;
import com.etihad.book.referencedata.schema.SearchParameters;
import com.etihad.book.referencedata.service.ReferenceDataServiceImpl;
import com.etihad.book.schema.common.Message;
import com.etihad.book.schema.common.Messages;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author CTS
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ReferenceDataTest {
	
	private MockMvc mockMvc;

	@MockBean
	ReferenceDataServiceImpl referenceDataService;

	@InjectMocks
	private ReferenceDataController referenceDataController;
	
	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private Config config;

	@Before
	public void setUp() throws Exception {

		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(referenceDataController).build();

	}

	@Test
	public void testControllerForReferenceData() throws Exception {

		/*Mockito.when(referenceDataService.getReferenceData())
				.thenReturn(formTestResponseForReferenceData());*/

		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/service/v1/referenceData")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(fromReferenceDataRequest()))
				.accept(MediaType.APPLICATION_JSON);

		mockMvc.perform(requestBuilder).andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content()
						.string(objectMapper.writeValueAsString(formTestResponseForReferenceData())))
				.andExpect(jsonPath("$.messages", notNullValue()))
				.andExpect(jsonPath("$.messages.message[0].text", is("Success")))
				.andExpect(jsonPath("$.messages.message[0].code", is("0")));

	}
	
	private  ReferenceDataRequest fromReferenceDataRequest() {
		
		final ReferenceDataRequest req = new ReferenceDataRequest(); 
		SearchParameters searchParams = new SearchParameters();
		req.setSearchParameters(searchParams);
		return req;
	}

	

	private ReferenceDataResponse formTestResponseForReferenceData() {
		
		final ReferenceDataResponse response = new ReferenceDataResponse();
		
		final Messages messages = new Messages();
		final List<Message> msg = new ArrayList<>();
		final Message message = new Message();
		message.setCode("0");
		message.setText("Success");
		msg.add(message);
		messages.setErrorMessages(msg);
		//response.setMessages(messages);
		return response;
	}
}